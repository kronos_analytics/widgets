

jQuery(document).ready(function() {

    // All initialization function should be added to the window.bab object
    // using the addInitFunction method.
    // Init function should take a dom element as parameter.

    var body = document.getElementsByTagName("BODY")[0];

    window.bab.init(body);
});




/*
 * jQuery.appear http://code.google.com/p/jquery-appear/
 * 
 * Copyright (c) 2009 Michael Hixson Licensed under the MIT license
 * (http://www.opensource.org/licenses/mit-license.php)
 */
(function($) {

  $.fn.appear = function(fn, options) {

    var settings = $.extend({

      // arbitrary data to pass to fn
      data: undefined,

      // call fn only on the first appear?
      one: true

    }, options);

    return this.each(function() {

      var t = $(this);

      // whether the element is currently visible
      t.appeared = false;

      if (!fn) {

        // trigger the custom event
        t.trigger('appear', settings.data);
        return;
      }

      var w = $(window);

      // fires the appear event when appropriate
      var check = function() {

        // is the element hidden?
        if (!t.is(':visible')) {

          // it became hidden
          t.appeared = false;
          return;
        }

        // is the element inside the visible window?
        var a = w.scrollLeft();
        var b = w.scrollTop();
        var o = t.offset();
        var x = o.left;
        var y = o.top;

        if (y + t.height() >= b &&
            y <= b + w.height() &&
            x + t.width() >= a &&
            x <= a + w.width()) {

          // trigger the custom event
          if (!t.appeared) t.trigger('appear', settings.data);

        } else {

          // it scrolled out of view
          t.appeared = false;
        }
      };

      // create a modified fn with some additional logic
      var modifiedFn = function() {

        // mark the element as visible
        t.appeared = true;

        // is this supposed to happen only once?
        if (settings.one) {

          // remove the check
          w.unbind('scroll', check);
          var i = $.inArray(check, $.fn.appear.checks);
          if (i >= 0) $.fn.appear.checks.splice(i, 1);
        }

        // trigger the original fn
        fn.apply(this, arguments);
      };

      // bind the modified fn to the element
      if (settings.one) t.one('appear', settings.data, modifiedFn);
      else t.bind('appear', settings.data, modifiedFn);

      // check whenever the window scrolls
      w.scroll(check);

      // check whenever the dom changes
      $.fn.appear.checks.push(check);

      // check now
      (check)();
    });
  };

  // keep a queue of appearance checks
  $.extend($.fn.appear, {

    checks: [],
    timeout: null,

    // process the queue
    checkAll: function() {
      var length = $.fn.appear.checks.length;
      if (length > 0) while (length--) ($.fn.appear.checks[length])();
    },

    // check the queue asynchronously
    run: function() {
      if ($.fn.appear.timeout) clearTimeout($.fn.appear.timeout);
      $.fn.appear.timeout = setTimeout($.fn.appear.checkAll, 20);
    }
  });

  // run checks when these methods are called
  $.each(['append', 'prepend', 'after', 'before', 'attr',
          'removeAttr', 'addClass', 'removeClass', 'toggleClass',
          'remove', 'css', 'show', 'hide'], function(i, n) {
    var old = $.fn[n];
    if (old) {
      $.fn[n] = function() {
        var r = old.apply(this, arguments);
        $.fn.appear.run();
        return r;
      };
    }
  });

})(jQuery);



jQuery.fn.limitMaxLength = function(options) {

    var settings = jQuery.extend({
        attribute: "maxlength",
        onLimit: function(){},
        onEdit: function(){}
    }, options);

    // Event handler to limit the textarea
    var onEdit = function(){
        var textarea = jQuery(this);
        var maxlength = parseInt(textarea.attr(settings.attribute));

        if(textarea.val().length > maxlength){
            textarea.val(textarea.val().substr(0, maxlength));

            // Call the onlimit handler within the scope of the textarea
            jQuery.proxy(settings.onLimit, this)();
        }

        // Call the onEdit handler within the scope of the textarea
        jQuery.proxy(settings.onEdit, this)(maxlength - textarea.val().length);
    };

    this.each(onEdit);

    return this.keyup(onEdit)
                .keydown(onEdit)
                .focus(onEdit)
                .on('input paste', onEdit);
};


jQuery.fn.limitMaxWords = function(options) {

    var settings = jQuery.extend({
        attribute: "maxwords",
        onLimit: function(){},
        onEdit: function(){}
    }, options);

    // Event handler to limit the textarea
    var onEdit = function(){
        var textarea = jQuery(this);
        var maxWords = parseInt(textarea.attr(settings.attribute));

        var text = textarea.val();

        text = text.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');

        var separator = /\s+/;
        var words = text.split(separator);
        var nbWords = words.length;
        var nbRealWords = 0;
        for (var i = 0; i < nbWords; i++) {
            if (words[i].length > 1) {
                nbRealWords++;
            }
        }

        if (nbRealWords > maxWords) {
            textarea.removeClass('widget-valid');
            textarea.addClass('widget-invalid');
            textarea.css('border', '1px solid red');
            // Call the onlimit handler within the scope of the textarea
            jQuery.proxy(settings.onLimit, this)();
        } else {
            textarea.addClass('widget-valid');
            textarea.removeClass('widget-invalid');
            textarea.css('border', '');
        }

        textarea.parent().find('.widget-display-count').text(nbRealWords + ' / ' + maxWords);

        // Call the onEdit handler within the scope of the textarea
        jQuery.proxy(settings.onEdit, this)(maxWords - nbRealWords);
    };

    this.each(onEdit);

    return this.keyup(onEdit)
                .keydown(onEdit)
                .focus(onEdit)
                .on('input paste', onEdit);
};




/*
 * ! Autosize 3.0.8 license: MIT http://www.jacklmoore.com/autosize
 */
(function (global, factory) {
if (typeof define === 'function' && define.amd) {
    define(['exports', 'module'], factory);
} else if (typeof exports !== 'undefined' && typeof module !== 'undefined') {
    factory(exports, module);
} else {
    var mod = {
        exports: {}
    };
    factory(mod.exports, mod);
    global.autosize = mod.exports;
}
})(this, function (exports, module) {
'use strict';

function assign(ta) {
    var _ref = arguments[1] === undefined ? {} : arguments[1];

    var _ref$setOverflowX = _ref.setOverflowX;
    var setOverflowX = _ref$setOverflowX === undefined ? true : _ref$setOverflowX;
    var _ref$setOverflowY = _ref.setOverflowY;
    var setOverflowY = _ref$setOverflowY === undefined ? true : _ref$setOverflowY;

    if (!ta || !ta.nodeName || ta.nodeName !== 'TEXTAREA' || ta.hasAttribute('data-autosize-on')) return;

    var heightOffset = null;
    var overflowY = 'hidden';

    function init() {
        var style = window.getComputedStyle(ta, null);

        if (style.resize === 'vertical') {
            ta.style.resize = 'none';
        } else if (style.resize === 'both') {
            ta.style.resize = 'horizontal';
        }

        if (style.boxSizing === 'content-box') {
            heightOffset = -(parseFloat(style.paddingTop) + parseFloat(style.paddingBottom));
        } else {
            heightOffset = parseFloat(style.borderTopWidth) + parseFloat(style.borderBottomWidth);
        }

        update();
    }

    function changeOverflow(value) {
        {
            // Chrome/Safari-specific fix:
            // When the textarea y-overflow is hidden, Chrome/Safari do not
            // reflow the text to account for the space
            // made available by removing the scrollbar. The following forces
            // the necessary text reflow.
            var width = ta.style.width;
            ta.style.width = '0px';
            // Force reflow:
            /* jshint ignore:start */
            ta.offsetWidth;
            /* jshint ignore:end */
            ta.style.width = width;
        }

        overflowY = value;

        if (setOverflowY) {
            ta.style.overflowY = value;
        }

        resize();
    }

    function resize() {
        var htmlTop = window.pageYOffset;
        var bodyTop = document.body.scrollTop;
        var originalHeight = ta.style.height;

        ta.style.height = 'auto';

        var endHeight = ta.scrollHeight + heightOffset;

        if (ta.scrollHeight === 0) {
            // If the scrollHeight is 0, then the element probably has
            // display:none or is detached from the DOM.
            ta.style.height = originalHeight;
            return;
        }

        ta.style.height = endHeight + 'px';

        // prevents scroll-position jumping
        document.documentElement.scrollTop = htmlTop;
        document.body.scrollTop = bodyTop;
    }

    function update() {
        var startHeight = ta.style.height;

        resize();

        var style = window.getComputedStyle(ta, null);

        if (style.height !== ta.style.height) {
            if (overflowY !== 'visible') {
                changeOverflow('visible');
            }
        } else {
            if (overflowY !== 'hidden') {
                changeOverflow('hidden');
            }
        }

        if (startHeight !== ta.style.height) {
            var evt = document.createEvent('Event');
            evt.initEvent('autosize:resized', true, false);
            ta.dispatchEvent(evt);
        }
    }

    var destroy = (function (style) {
        window.removeEventListener('resize', update);
        ta.removeEventListener('input', update);
        ta.removeEventListener('keyup', update);
        ta.removeAttribute('data-autosize-on');
        ta.removeEventListener('autosize:destroy', destroy);

        Object.keys(style).forEach(function (key) {
            ta.style[key] = style[key];
        });
    }).bind(ta, {
        height: ta.style.height,
        resize: ta.style.resize,
        overflowY: ta.style.overflowY,
        overflowX: ta.style.overflowX,
        wordWrap: ta.style.wordWrap });

    ta.addEventListener('autosize:destroy', destroy);

    // IE9 does not fire onpropertychange or oninput for deletions,
    // so binding to onkeyup to catch most of those events.
    // There is no way that I know of to detect something like 'cut' in IE9.
    if ('onpropertychange' in ta && 'oninput' in ta) {
        ta.addEventListener('keyup', update);
    }

    window.addEventListener('resize', update);
    ta.addEventListener('input', update);
    ta.addEventListener('autosize:update', update);
    ta.setAttribute('data-autosize-on', true);

    if (setOverflowY) {
        ta.style.overflowY = 'hidden';
    }
    if (setOverflowX) {
        ta.style.overflowX = 'hidden';
        ta.style.wordWrap = 'break-word';
    }

    init();
}

function destroy(ta) {
    if (!(ta && ta.nodeName && ta.nodeName === 'TEXTAREA')) return;
    var evt = document.createEvent('Event');
    evt.initEvent('autosize:destroy', true, false);
    ta.dispatchEvent(evt);
}

function update(ta) {
    if (!(ta && ta.nodeName && ta.nodeName === 'TEXTAREA')) return;
    var evt = document.createEvent('Event');
    evt.initEvent('autosize:update', true, false);
    ta.dispatchEvent(evt);
}

var autosize = null;

// Do nothing in Node.js environment and IE8 (or lower)
if (typeof window === 'undefined' || typeof window.getComputedStyle !== 'function') {
    autosize = function (el) {
        return el;
    };
    autosize.destroy = function (el) {
        return el;
    };
    autosize.update = function (el) {
        return el;
    };
} else {
    autosize = function (el, options) {
        if (el) {
            Array.prototype.forEach.call(el.length ? el : [el], function (x) {
                return assign(x, options);
            });
        }
        return el;
    };
    autosize.destroy = function (el) {
        if (el) {
            Array.prototype.forEach.call(el.length ? el : [el], destroy);
        }
        return el;
    };
    autosize.update = function (el) {
        if (el) {
            Array.prototype.forEach.call(el.length ? el : [el], update);
        }
        return el;
    };
}

module.exports = autosize;
});



function widget_delayedInit(item){
    var meta = window.babAddonWidgets.getMetadata(item.id);
    var widget = item;
    if (meta.delayedAction != '') {
        jQuery.ajax({
            url: meta.delayedAction,
            cache: false,
            success: function(response) {
                var parent = jQuery(widget).parent().get(0);
                jQuery(parent).hide();
                jQuery(parent).html(response);
                var newmeta = window.babAddonWidgets.getMetadata(parent.firstChild.id);
                jQuery(parent).html(response);
                newmeta.delayedAction = meta.delayedAction;
                window.babAddonWidgets.addMetadata(parent.firstChild.id, newmeta);
                jQuery(parent).fadeIn('slow');

                window.bab.init(parent);
                window.bab.init(jQuery('body > .ui-dialog').get(0));
            }
        });
    }
}


function widget_baseInit(domNode) {


    jQuery(domNode).find('a[disabled]').on("click", function(event) {
        event.stopImmediatePropagation();
        return false;
    });

    // Prepare tab widgets
    if (jQuery().tabs) {
        jQuery(domNode).find(".widget-tabs").not('.widget-init-done').not('.widget-static-tabs').each(function() {
            jQuery(this).addClass('widget-init-done');
            var myFile = document.location.toString();

            if (myFile.match('#') && !jQuery(this).children('ul').children('.widget-tab-selected').get(0)) {
                var tabid = myFile.split('#')[1];
                jQuery(this).children('ul').children('.widget-tab').find('a[href=#'+tabid+']').parent().addClass('widget-tab-selected');
            }

            var selectedTabNumber = 0;
            var tagNumber = 0;
            jQuery(this).children('ul').children('.widget-tab').each(function() {
                if (jQuery(this).hasClass('widget-tab-selected')) {
                    selectedTabNumber = tagNumber;
                }
                tagNumber++;
            });

            // http://stackoverflow.com/questions/13837304/jquery-ui-non-ajax-tab-loading-whole-website-into-itself

            jQuery(this).children('ul').children('.widget-tab').find("a").each(function() {
                jQuery(this).attr("href", myFile.split('#')[0]+jQuery(this).attr("href"));
            });


            jQuery(this).tabs({
                active: selectedTabNumber
            }).on('tabsactivate', function(event, ui) {
                var tabs = jQuery(this).closest('.widget-tabs');
                if (tabs.hasClass('widget-persistent')) {
                    var expiryDate = new Date;
                    expiryDate.setMonth(expiryDate.getMonth() + 6);
                    var cookiePath = '/';

                    var index = ui.newTab.index();
                    
                    var cookieString = index;

                    document.cookie = 'widgets_Tabs_' + tabs.attr('id') + '=' + escape(cookieString)
                    // + '; expires=' + expiryDate.toGMTString()
                      + '; path=' + cookiePath;
                }
            });
           
        });
        
        

    }

    if (jQuery().accordion) {
        // Prepare accordions widgets
        jQuery(domNode).find('.widget-accordions').not('.widget-init-done').each(function () {

            jQuery(this).addClass('widget-init-done');

            var meta = window.babAddonWidgets.getMetadata(this.id);

            var accordionOptions = {
                header: "span.widget-accordion-header",
                navigation: true,
                heightStyle: 'content',
                active: false,
                clearStyle: true,
                collapsible: true,
                active: false
            };
            if (typeof meta.openPanel !== 'undefined') {
                accordionOptions.active = meta.openPanel;
            }
            if (!meta.animated) {
                accordionOptions.animated = false;
            }
            if (meta.collapsible == false) {
                accordionOptions.collapsible = false;
            }
            jQuery(this)
                .accordion(accordionOptions);
        });
    }


    jQuery(domNode).find('.widget-ajax').not('.widget-submitbutton').not('.widget-form').not('.widget-ajax-init-done').each(function() {

        var item = this;

        jQuery(item).addClass('widget-ajax-init-done');

        var meta = window.babAddonWidgets.getMetadata(jQuery(this).attr('id'));

        if(jQuery.isEmptyObject(meta) && jQuery(item).hasClass('widget-link')){
            var eventName = 'click';
        }else{
            var eventName = meta.ajaxActionEvent;
        }

        jQuery(item).bind(eventName, function(e) {
            if (jQuery(item).hasClass('widget-confirm')) {
                try {
                    if (confirmationMessage = window.babAddonWidgets.getMetadata(item.id).confirmationMessage) {
                        var confirmed = window.confirm(confirmationMessage);
                        if (!confirmed) {
                            e.stopPropagation();
                            return false;
                        }
                    }
                } catch (ex) {
                }
            }

            // If the item has a name, we add the name and value as a new
            // parameter to the url.
            if (jQuery.isEmptyObject(meta) && jQuery(item).hasClass('widget-link')) {
                var ajaxAction = jQuery(item).attr('href');
            } else {
                var ajaxAction = meta.ajaxAction;
            }

            if (item.name) {
                var nameLookup = jQuery(item).parent().find('[name="' +  item.name + '"]');

                for (var i = 0; i < nameLookup.length; i++) {
                    var field = jQuery(nameLookup[i]);
                    if (!field.is('[type=checkbox]') || field.prop('checked')) {
                        var fieldVal = field.val();
                        var fieldName = field.prop('name');
                        ajaxAction += '&' + encodeURIComponent(fieldName) + '=' + encodeURIComponent(fieldVal);
                    }
                }
            }

            window.babAddonWidgets.ajax(ajaxAction, item, meta.ajaxActionReload);

            if (item.tagName == 'A') {
                e.stopPropagation();
                return false;
            }
        });
    });


    jQuery(domNode).find('.widget-link').not('.widget-init-done').each(function() {

        jQuery(this).addClass('widget-init-done');

        if (jQuery(this).hasClass('widget-popup')) {
            jQuery(this).click(function(e) {
                if(jQuery(this).hasClass('widget-confirm')){
                    try {
                        if (confirmationMessage = window.babAddonWidgets.getMetadata(this.id).confirmationMessage) {
                            var confirmed = window.confirm(confirmationMessage);
                            if (!confirmed) {
                                e.stopPropagation();
                                return false;
                            }
                        }
                        e.stopPropagation();
                    } catch (ex) {
                    }
                }
                bab_popup(jQuery(this).attr('href'));
                e.stopPropagation();
                return false;
            });
        }
        
        
        /**
         * Get first ID in ancestors
         * 
         * @param {jQuery}
         *            jqElem
         * @return {String}
         */
        function getFirstId(jqElem) {
            if (jqElem && undefined === jqElem.attr('id')) {
                return getFirstId(jqElem.parent());
            }

            return jqElem.attr('id');
        }
        

        /**
         * Add click event on an element Call jax function on click
         * 
         * @param {DOMElement}
         *            element
         * @param {string[]|HTMLElement[]|Function}
         *            reloadItem
         */
        function attachDialogEvent(element, reloadItem)
        {
            jQuery(element).click(function(e) {
                if(jQuery(this).hasClass('widget-confirm')){
                    try {
                        if (confirmationMessage = window.babAddonWidgets.getMetadata(this.id).confirmationMessage) {
                            var confirmed = window.confirm(confirmationMessage);
                            if (!confirmed) {
                                e.stopPropagation();
                                return false;
                            }
                        }
                        e.stopPropagation();
                    } catch (ex) {
                    }
                }

                window.babAddonWidgets.ajax(jQuery(this).attr('href'), element, reloadItem);
                e.stopPropagation();
                return false;
            });
        }

        //
        if (jQuery(this).hasClass('widget-popup-dialog')) {
            attachDialogEvent(this, []);
        }
        
        if (jQuery(this).hasClass('widget-popup-dialog-and-reload-page')) {
            attachDialogEvent(this, function() {
            	document.location.reload();
            });
        }

        //
        if (jQuery(this).hasClass('widget-popup-dialog-and-reload')) {


            var meta = window.babAddonWidgets.getMetadata(jQuery(this).attr('id'));
            var linkReload = [];
            if (meta.dialogActionReload) {
                linkReload = meta.dialogActionReload;
            } else {
                linkReload[0] = getFirstId(jQuery(this));
            }

            attachDialogEvent(this, linkReload);

        }

        // DEPRECATED contact ANTGAL
        // YOU SHOULD USE an other method to execute ajax action
        /*
         * if(jQuery(this).hasClass('widget-close-dialog')){ }
         */

        if (jQuery(this).hasClass('widget-link-ajax-action')) {

            var linkReload = [];
            linkReload[0] = getFirstId(jQuery(this));

            jQuery(this).click(function(e) {
                if(jQuery(this).hasClass('widget-confirm')){
                    try {
                        if (confirmationMessage = window.babAddonWidgets.getMetadata(this.id).confirmationMessage) {
                            var confirmed = window.confirm(confirmationMessage);
                            if (!confirmed) {
                                e.stopPropagation();
                                return false;
                            }
                        }
                        e.stopPropagation();
                    } catch (ex) {
                    }
                }
                window.babAddonWidgets.ajax(jQuery(this).attr('href'), this, linkReload);
                e.stopPropagation();
                return false;
            });
        }
    });


    // Manage confirmation messages. Ajax events are treated elsewere.
    jQuery(domNode).find('.widget-confirm')
    .not('.widget-popup')
    .not('.widget-popup-dialog')
    .not('.widget-popup-dialog-and-reload')
    .not('.widget-popup-dialog-and-refresh')
    .not('.widget-link-ajax-action')
    .not('.widget-ajax')
    .not('.widget-confirm-init-done')
    .each(function() {
        jQuery(this).addClass('widget-confirm-init-done');
        jQuery(this).click(function(e) {
            try {
                if (confirmationMessage = window.babAddonWidgets.getMetadata(this.id).confirmationMessage) {
                    var confirmed = window.confirm(confirmationMessage);
                    if (!confirmed) {
                        e.stopPropagation();
                        return false;
                    }
                }
                e.stopPropagation();
                return true;
            } catch (ex) {
                return true;
            }
        });
    });



    jQuery(domNode).find('.widget-input-ajax-validate').not('.widget-displaymode').not('.widget-radioset').filter(":visible").each(function() {
        jQuery(this).addClass('widget-input-ajax-validate-init-done');
        jQuery(this).blur(function() {
            var validateInput = jQuery(this);
            var url = window.babAddonWidgets.getMetadata(validateInput.attr('id')).ajaxValidateAction;
            jQuery.ajax({
                url: url,
                dataType: 'html',
                data: { value: validateInput.val() },
                cache: false,
                async: false,
                success: function(response) {
                    try {
                        jsonResponse = jQuery.parseJSON(response);
                        // console.debug(jsonResponse);
                        if (jsonResponse.status === 'OK') {
                            validateInput.css('border', '');
                            validateInput.attr('title', jsonResponse.info);
                            validateInput.addClass('widget-valid');
                            validateInput.removeClass('widget-invalid');
                        } else {
                            validateInput.css('border', '1px solid darkorange');
                            validateInput.addClass('widget-invalid');
                            validateInput.removeClass('widget-valid');
                            validateInput.attr('title', jsonResponse.info);
                        }
                    } catch (e) {
                        if (response === '1') {
                            validateInput.css('border', '');
                            validateInput.addClass('widget-valid');
                            validateInput.removeClass('widget-invalid');
                        } else {
                            validateInput.css('border', '1px solid darkorange');
                            validateInput.addClass('widget-invalid');
                            validateInput.removeClass('widget-valid');
                            validateInput.attr('title', response);
                        }
                    }
                }
            });

        });
    });


    // sortable
    if (jQuery().sortable) {
        jQuery(domNode).find(".widget-sortable").not('.widget-sortable-init-done').each(function () {

            var $item = jQuery(this);
            $item.addClass('widget-sortable-init-done');
            var meta = window.babAddonWidgets.getMetadata(this.id);
            var options = {
                cursorAt: { left: 5, top: 5 },
                placeholder: 'widget-sortable-placeholder',
                dropOnEmpty: true,
                tolerance: "pointer",
                cancel: "input,textarea,button,select,option,.widget_imagecropper_container"
            };
            if (meta.samePlaceholderSize) {
                options.start = function(e, ui) {
                    ui.placeholder.width(ui.item.width() - 2);
                    ui.placeholder.height(ui.item.height() - 2);
                    ui.placeholder.addClass(ui.item.attr('class'));
                };
            }
            if (meta.items) {
                options.items = items;
            }
            if (meta.connectWith) {
                var nbconnectWith = meta.connectWith.length;
                var connectWith = '';
                for (var i = 0; i < nbconnectWith; i++) {
                    if(connectWith == ''){
                        connectWith = meta.connectWith[i];
                    } else {
                        connectWith = connectWith + ', ' + meta.connectWith[i];
                    }
                }
                options.connectWith = connectWith;
                
                if (meta.sortableReload) {
                	options.remove = function(event, ui) {
                		reloadAction = window.babAddonWidgets.getReloadFunction($item);
                		reloadAction();
                	};
                }
                if (meta.sortableReloadReceive) {
                	options.receive = function(event, ui) {
                		reloadAction = window.babAddonWidgets.getReloadFunction($item);
                		reloadAction();
                	};
                }
            }
            $item.sortable(options);
        });
    }


    jQuery(domNode).find(".widget-filter .header").click(function(e) {
        var filter = jQuery(this).parents('.widget-filter').get(0);
        form = jQuery(filter).find('form.widget-form').get(0);
        jQuery(form).toggle('normal');
    });


    jQuery(domNode).find('.widget-textedit').not('.widget-maxlength-init-done').each(function () {
        jQuery(this).addClass('widget-maxlength-init-done');
        var meta = window.babAddonWidgets.getMetadata(this.id);
        var textedit = jQuery(this);
        if (meta && meta.minSize) {
            if (form = textedit.closest('form')) {
                form.on('validate.widgets', function() {
                    var message = meta.submitMinMessage;
                    var minlength = parseInt(meta.minSize);
                    if (textedit.val().length < minlength) {
                        if (window.babAddonWidgets.validatemandatory) {
                            form.addClass('widget-invalid');
                            alert(message);
                            return false;
                        }

                        if (window.babAddonWidgets.validate) {
                            var confirm = window.confirm(message);
                            if (!confirm) {
                                form.addClass('widget-invalid');
                            }
                            return confirm;
                        }
                    }

                    return true;
                });
            }
        }
        if (meta && meta.maxSize) {
            if (form = textedit.closest('form')) {
                form.on('validate.widgets', function() {
                    var message = meta.submitMaxMessage;
                    var maxlength = parseInt(meta.maxSize);
                    if (textedit.val().length > maxlength) {
                        if (window.babAddonWidgets.validatemandatory) {
                            form.addClass('widget-invalid');
                            alert(message);
                            return false;
                        }

                        if (window.babAddonWidgets.validate) {
                            var confirm = window.confirm(message);
                            if (!confirm) {
                                form.addClass('widget-invalid');
                            }
                            return confirm;
                        }
                    }

                    return true;
                });
            }
        }
        if (meta && meta.maxWords) {
            jQuery(this).parent().append('<span class="widget-display-count"></span>');
            jQuery(this).attr('maxwords', meta.maxWords);
            jQuery(this).limitMaxWords({});
        }
    });

    jQuery(domNode).find('.widget-autoresize').not('.widget-autoresize-init-done').each(function () {
       if (jQuery(this).hasClass('widget-simplehtmledit')) {

       } else if (jQuery(this).hasClass('widget-textedit')) {
           jQuery(this).addClass('widget-autoresize-init-done');
           autosize(this);
           // Add also the autoresize on the focus event because the autoresize
            // does not not work with only the first line
           jQuery(domNode).on('focus', '#' + this.id, function () {
               autosize(this);
           });
       }
    });




    // associated displayable widgets

    jQuery(domNode).find('.widget-associated-displayable').not('.widget-associated-displayable-init-done').each(function(key, source) {

        jQuery(this).addClass('widget-associated-displayable-init-done');

        source = jQuery(source);
        var metadata = window.babAddonWidgets.getMetadata(source.attr('id'));

        if (metadata.displayable)
        {
            var eventAction = function() {
                var selected = null;
                var source = jQuery(this);

                switch(true)
                {
                    case source.is('select'):
                        selected = source.find('option:selected').attr('value');
                        break;

                    case source.is('input[type="checkbox"]:checked'):
                        selected = source.attr('value');
                        break;

                    case source.is('input[type="text"]'):
                        selected = source.val();
                        break;

                    case source.is('input[type="checkbox"]'):
                        selected = jQuery('input[type="hidden"][name="'+source.attr('name')+'"]').attr('value');
                        break;

                    case source.is('input[type="radio"]:checked'):
                        selected = source.attr('value');
                        break;
                }

                for(var i =0; i< metadata.displayable.length; i++)
                {
                    var target = jQuery('#'+metadata.displayable[i][0]);
                    var values = metadata.displayable[i][1];
                    var mode = metadata.displayable[i][2];
                    var disp = 0;

                    for (var j=0; j < values.length; j++)
                    {
                        if (values[j] == selected)
                        {
                            // target.show();
                            if (mode) {
                                target.removeClass('widget-discarded');
                                if (target.not('.widget-init-discarded')) {
									target.addClass('widget-init-discarded');
                                	window.bab.init(target);
                            	}
                            } else {
                                target.addClass('widget-discarded');
                            }
                            disp = 1;
                            break;
                        }
                    }

                    if (disp == 0)
                    {
                        if (mode) {
                            target.addClass('widget-discarded');
                        } else {
                            if (target.not('.widget-init-discarded')) {
								target.addClass('widget-init-discarded');
                            	window.bab.init(target);
                        	}
                            target.removeClass('widget-discarded');
                        }
                        // target.hide();
                    }
                }
            };

            if (source.is('.widget-radioset')) {
                if (metadata.attachedRadios) {
                    jQuery(metadata.attachedRadios).click(eventAction);
                    if (jQuery(metadata.attachedRadios).filter(':checked').length == 0) {
                        jQuery(metadata.attachedRadios).first().click();
                    } else {
                        jQuery(metadata.attachedRadios).filter(':checked').click();
                    }
                } else {
                    source.find('input[type=radio]').click(eventAction);

                    if (source.find('input[type=radio]:checked').length == 0) {
                        source.find('input[type=radio]:first').click();
                    } else {
                        source.find('input[type=radio]:checked').click();
                    }
                }

            } else {
                source.change(eventAction);
                source.change();
            }
        }
    });


    // Delayed items
    jQuery(domNode).find(".widget-delayeditem").not('.widget-init-done').each(function() {
        jQuery(this).addClass('widget-init-done');

        if (!jQuery(this).hasClass('widget-delayeditem-onreload')) {
            var meta = window.babAddonWidgets.getMetadata(this.id);
            if(meta.updateMode == 'on-appear'){
                jQuery(this).appear(function() {
                    jQuery(this).addClass('loading');
                    widget_delayedInit(this);
                });
            }else if(meta.updateMode == 'on-page-load'){
                jQuery(this).addClass('loading');
                widget_delayedInit(this);
            }
        }
    });

    jQuery(domNode).find('.widget-hasHelp').not('.widget-hasHelp-init-done').each(function(){
        jQuery(this).addClass('widget-hasHelp-init-done');
        var id = jQuery(this).attr('id');
        var meta = window.babAddonWidgets.getMetadata(id);
        var html = meta.contextualHelp;
        jQuery("body").append('<span class="'+id+'" style="z-index:1; padding: 5px; border-radius: .25em; display: none; background-color: #eee; border: 1px solid #aaa;" class="widget-activeHelp">'+html+'</span>');
        jQuery(this).focus(function() {
            var pos = jQuery('#'+id).offset();
            var bottom = jQuery(window).height() - jQuery('#'+id).height() - pos.top -6;
            var left = pos.left;
            left = left + jQuery('#'+id).width() + 9;
            jQuery('.'+id).css('display', 'inline-block');
            jQuery('.'+id).css('position', 'absolute');
            jQuery('.'+id).css('bottom', bottom);
            jQuery('.'+id).css('left', left);
        });

        jQuery(this).blur(function(){
            jQuery('.'+id).css('display', 'none');
        });
    });


    jQuery(domNode).find('.widget-instant-container .widget-instant-button').not('.widget-instant-container-init-done').each(function() {

        jQuery(this).addClass('widget-instant-container-init-done');

        var form = jQuery(this).parents('.widget-instant-container').find('.widget-instant-form');
        this.instantForm = form;

        jQuery(this).click(function () {
			this.instantForm.detach();
			
			var itemId = this.instantForm.attr('id')+'-dialog';
            var titleLayout = '<div id="'+itemId+'-title" class="widget-dialog-title"><span class="widget-dialog-title-text"></span><button type="button" data-a11y-dialog-hide aria-label="Close dialog" class="widget-dialog-closebutton" title="Close"><span class="ui-button-icon ui-icon ui-icon-closethick"></span></button></div>';
            
        	var dialog = jQuery('<div id="'+itemId+'" aria-labelledby="'+itemId+'-title" aria-hidden="true" class="'+dialogClass+' widget-dialog"><div class="widget-dialog-overlay"></div><div class="widget-dialog-container" role="document">'+titleLayout+'<div class="widget-dialog-content">'+this.instantForm[0]+'</div></div></div>');
        	
        	
			jQuery('body').append(dialog);
            dialogA11y = new A11yDialog(dialog[0]);
            
        	dialogA11y.on('hide', function() {
	            document.documentElement.style.overflowY = '';
			});
			
        	dialogA11y.on('show', function() {
				document.documentElement.style.overflowY = 'hidden'
			});
            
            dialogA11y.show();

            this.instantForm.closest('.ui-dialog').find('.widget-accordions').accordion('refresh');

            return false;
        });

    });
    
    jQuery(domNode).find('.widget-close-alldialog').not('.widget-submitbutton').not('.widget-close-alldialog-init-done').each(function() {
        jQuery(this).addClass('widget-close-alldialog-init-done');
        jQuery(this).click(function (e) {
            jQuery('.widget-dialog-closebutton').each(function() {
				jQuery(this).click();
			});

            e.stopPropagation();
            return false;
        });
    });

    jQuery(domNode).find('.widget-close-dialog').not('.widget-close-dialog-init-done').each(function() {
        jQuery(this).addClass('widget-close-dialog-init-done');
        jQuery(this).click(function (e) {
            var closestDialog = jQuery(this).closest('.widget-dialog');
            if (closestDialog.length > 0) {
				closestDialog.find('.widget-dialog-closebutton').click();
                e.stopPropagation();
                return false;
            } else {
                history.back();
                e.stopPropagation();
                return false;
            }
        });
    });


    jQuery(domNode).find('.widget-cancel').not('.widget-cancel-init-done').each(function() {
        jQuery(this).addClass('widget-cancel-init-done');
        jQuery(this).click(function () {
            history.back();
            return false;
        });
    });

    if (jQuery().matchHeight) {
	    jQuery(domNode).find('.widget-sameheight-elements').each(function() {
	    	jQuery(this).find('.widget-sameheight').matchHeight({byRow: true, property: 'min-height'});
	    });
    }
    
    // For text inputs in tableviews, pressing "Enter" tries to move the focus to the cell on the next row
    // (while pressing Tab usually moves the focus to the next column).
    jQuery(domNode).find('.widget-tableview input[type="text"]').not('.widget-enter-init-done').each(function() {
    	var input = jQuery(this);
    	input.addClass('widget-enter-init-done');
		input.keydown(function (e) {    	
	    	var key = (event.keyCode ? event.keyCode : event.which);
	    	if (key == '13') {
	    		console.debug('Enter');
		    	var currentTd = input.closest('td:not(.widget-layout-hbox-item)');
		    	var currentTr = currentTd.closest('tr:not(.widget-layout-hbox-tr)');
		        var tds = currentTr.children('td');
		        var count = tds.length;
		        var index = tds.index(currentTd);
		        var nextTd = currentTr.next('tr').children('td:eq('+index+')');
		        nextTd.find('input[type="text"]').focus().select();
	    		console.debug(currentTd, nextTd);
    		 }
    	});
    });
}

window.bab.addInitFunction(widget_baseInit);
