
window.babAddonWidgets.initMultilang = function(domNode)
{
    function selectLang(widget, language)
    {
        var $switches = widget.find('.widget-switch-lang');
        var $inputs = widget.find('.widget-lang');
        $switches.removeClass('selected');
        $inputs.hide();
        
        var $selectedSwitch = widget.find('.widget-switch-lang-' + language);
        var $selectedInput = widget.find('.widget-lang-' + language);
        if ($selectedSwitch.length == 0) {
            $selectedSwitch = $switches.first();
            $selectedInput = $inputs.first();            
        }
        $selectedSwitch.addClass('selected');
        $selectedInput.show();
    }


    jQuery('.widget-multilangedit').not('.widget-multilang-init-done').each(function() {

        var multilangedit = jQuery(this);
        multilangedit.addClass('widget-multilang-init-done');

        var clickEventRoot = multilangedit;
        var language = 'en'; // default if no multilang widget
        var multilang = multilangedit.closest('.widget-multilang');
        if (multilang.length) {
            var meta = window.babAddonWidgets.getMetadata(multilang.attr('id'));
            language = meta.lang;
            clickEventRoot = multilang;
        }
        
        selectLang(multilangedit, language);

        clickEventRoot.on('click', '.widget-switch-lang', function() {

            var button = jQuery(this);
            var selected = button.hasClass('selected');
            if (selected) {
                return false;
            }
            var buttonmeta = window.babAddonWidgets.getMetadata(this.id);
            var buttonLanguage = buttonmeta.lang;
            selectLang(multilangedit, buttonmeta.lang);
        });



        // form event

        if (form = multilangedit.closest('form').not('.widget-multilang-mandatory-init-done')) {
            form.addClass('widget-multilang-mandatory-init-done');

            form.on('validate.widgets', function() {

                var arr = jQuery(this).find('.widget-input-mandatory .widget-lang');

                for (var i = 0; i < arr.length; i++) {
                    var field = jQuery(arr[i]);
                    if ('' === field.val()) {
                        field.css('border', '1px solid red');

                        var splited = field.attr('name').split('[');
                        var endofline = splited[splited.length -1];
                        var lang = endofline.substring(0, endofline.length - 1);

                        var widget = field.closest('.widget-multilangedit');

                        var switchButton = widget.find('.widget-switch-lang-'+lang);

                        switchButton.css('border', '1px solid red');
                        var message = window.babAddonWidgets.getMetadata(widget.attr('id')).mandatoryErrorMessage;

                        form.addClass('widget-invalid');
                        alert(lang+': '+message);
                        
                        return false;
                    }
                }

                return true;
            });
        }
    });
};



window.bab.addInitFunction(window.babAddonWidgets.initMultilang);
