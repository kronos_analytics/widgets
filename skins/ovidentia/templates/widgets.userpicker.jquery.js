
function widget_userPickerInit(domNode) {
    jQuery(domNode).find('.widget-userpicker').not('.widget-init-done').each(function() {
        jQuery(this).addClass('widget-init-done');

        var userPicker = jQuery(this);
        var userPickerLineEdit = jQuery(this).children('[type=text][name!=""]').get(0);

        if (null == userPickerLineEdit)
        {
            return;
        }

        jQuery(this).append('<input type="hidden" name="' + userPickerLineEdit.name + '" value="' + userPickerLineEdit.value + '" />');

        jQuery(userPickerLineEdit).hide();

        var text = window.babAddonWidgets.getMetadata(userPicker.attr('id')).userName;

        jQuery(this).append('<div>' + text + '</div>');
        userPickerLineEdit.name = '';


        jQuery(this).click(function() {
            var userPicker = jQuery(this);
            var userPickerLineEdit = jQuery(this).children().get(0);
            var userPickerHidden = jQuery(jQuery(this).children().get(1));
            var userPickerLabel = jQuery(jQuery(this).children().get(2));

//			userPickerLabel.text('Selection...');
            bab_dialog.selectuser(
                function(param) {
                    userPickerLabel.text(param['name']);
                    userPickerHidden.val(param['id_user']);
                },
                'pos='
            );
        });

        jQuery(this).mouseenter(function() {


            var userPicker = jQuery(this);

            if (userPicker.data('removeLink'))
            {
                return;
            }


            var removeLink = jQuery('<a href="" class="icon actions-edit-delete"></a>');
            removeLink.css('position', 'absolute');
            removeLink.css('right', '0px');
            removeLink.css('top', '0px');
            userPicker.append(removeLink);

            userPicker.data('removeLink', removeLink);

            removeLink.click(function() {
                var userPicker = jQuery(this).parent();
                var userPickerHidden = jQuery(userPicker.children().get(1));
                var userPickerLabel = jQuery(userPicker.children().get(2));

                userPickerLabel.text('');
                userPickerHidden.val('0');

                return false;
            });
        });
        jQuery(this).mouseleave(function() {

            var userPicker = jQuery(this);
            var removeLink = userPicker.data('removeLink');

            if (!removeLink)
            {
                return;
            }

            removeLink.remove();
            userPicker.data('removeLink', null);
        });
        
        
        // form event
		
		if (form = jQuery(this).closest('form')) {
		    form.on('validate.widgets', function() {
				var arr = jQuery(this).find('.widget-userpicker');
				
				for (var i = 0; i < arr.length; i++) {
					var field = jQuery(arr[i]);
					var input = field.find('input[type=hidden]');
					if (0 === parseInt(input.val(), 10)) {
						field.css('border', '1px solid red');
						var message = window.babAddonWidgets.getMetadata(field.attr('id')).mandatoryErrorMessage;
						
						if (window.babAddonWidgets.validatemandatory) {
						    form.addClass('widget-invalid');
							alert(message);
							return false;
						}
					}
					return true;
				}
			});
		}
    });
}



window.bab.addInitFunction(widget_userPickerInit);
