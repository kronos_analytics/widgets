
function widget_fileIconThumbnailInit(domNode) {
	
	var searchBase = jQuery(domNode);
	
	if (searchBase.get(0).fileicon != undefined) {
		return;
	}
	
	searchBase.get(0).fileicon = true;
	setTimeout(function() {
		delete searchBase.get(0).fileicon;
	}, 500);

    window.babAddonWidgets.fileicons = [];
    window.babAddonWidgets.selfpage = null;
    window.babAddonWidgets.controller = null;

    // grab all file icon waiting for a thumbnail

    jQuery(domNode).find('.widget-fileicon, .widget-imagethumbnail, .widget-imagezoomerthumbnail')
    .each(function() {

        var fileicon = jQuery(this);

        var meta = window.babAddonWidgets.getMetadata(fileicon.attr('id'));


        if (meta)
        {
            if (meta.pending_thumbnail)
            {
                window.babAddonWidgets.fileicons.push(fileicon.attr('id'));
            }

            if (meta.pending_thumbnail_small)
            {
                window.babAddonWidgets.fileicons.push(fileicon.attr('id')+'_small');
            }

            if (meta.selfpage && null === window.babAddonWidgets.selfpage)
            {
                window.babAddonWidgets.selfpage = meta.selfpage;
            }

            if (meta.controller && null === window.babAddonWidgets.controller)
            {
                window.babAddonWidgets.controller = meta.controller;
            }
        }
    });



    if (null == window.babAddonWidgets.selfpage || null == window.babAddonWidgets.selfpage)
    {
        return;
    }


    // query ovidentia to get thumbnails

    var filecount = 0;
    var nextquery = [];

    while(window.babAddonWidgets.fileicons.length > 0)
    {


        nextquery.push(window.babAddonWidgets.fileicons.shift());
        filecount++;

        if (filecount > 10)
        {
            window.babAddonWidgets.processThumbnails(window.babAddonWidgets.selfpage, nextquery);
            nextquery.length = 0;
            filecount = 0;
        }
    }

    if (nextquery.length > 0)
    {
        window.babAddonWidgets.processThumbnails(window.babAddonWidgets.selfpage, nextquery);
    }
}




/**
 *
 */
window.babAddonWidgets.processThumbnails = function(selfpage, arr)
{
    var ids = arr.join(',');

     controller = window.babAddonWidgets.controller;

    jQuery.ajax({
        async: false,
       url: selfpage,
       data: controller+'&ids='+ids,
       success: function(response){

            if ('' == response)
            {
                return;
            }

            eval('var arr = '+response);


            for(var i = 0; i < arr.length; i++)
            {
                target = jQuery('#'+arr[i].id);
                if (target.hasClass('widget-fileicon'))
                {
                    target.css('background-image', 'url('+arr[i].url+')');
                }

                if (target.hasClass('widget-imagethumbnail'))
                {
                    target.attr('src', arr[i].url);
                }

                if (target.hasClass('widget-imagezoomerthumbnail'))
                {
                    target.attr('href', arr[i].url);
                }

                if (target.parent().hasClass('widget-imagezoomerthumbnail'))
                {
                    target.attr('src', arr[i].url);
                }
            }
        }
    });
};


window.bab.addInitFunction(function(domNode) {
	widget_fileIconThumbnailInit(window.document);
});