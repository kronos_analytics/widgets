





window.babAddonWidgets.dockable = function(dock) {
	
	var getFrame = function()
	{
		var frame = dock.data('frame');
		if (null == frame)
		{
			classFullSize = '';
			divBackground = '<div class="widget-dockable-fullsize-background"></div>';
			if (meta().displayDockFullSize){
				classFullSize = ' widget-dockable-fullsize';
			}
			
			frame = jQuery('<div class="widget-dockable-frame'+classFullSize+'"><div class="slider closed"><div class="widget-dockable-header"><h3></h3><button class="close" /><button class="pin" /></div><div class="widget-dockable-content loading"></div></div></div>');
			jQuery('body').append(frame);
			dock.data('frame', frame);
			frame.data('dock', dock);
			
			// add title frame with dock/undock button
			
			if (meta().displayDockFullSize){
				backgroundFrame = jQuery(divBackground);
				backgroundFrame.insertBefore(frame);
				dock.data('getbackgroundFrame', backgroundFrame);
				backgroundFrame.click(function(){
					jQuery(".widget-dockable-header .close").click();
				});
			}
			
			frame.find('.widget-dockable-header h3').text(dock.attr('title'));
			frame.find('.widget-dockable-header .pin').click(clickDockButton);
			frame.find('.widget-dockable-header .close').click(clickMainButton);
			
			dock.data('status', 'closed');
			dock.data('loaded', false);
			
			if (!meta().displayDockButton)
			{
				frame.find('.widget-dockable-header .pin').hide();
			}
		}
		
		return frame;
	};
	
	var getbackgroundFrame = function()
	{
		var frame = dock.data('getbackgroundFrame');
		return frame;
	}
	
	/**
	 * 
	 */
	var dockFrame = function()
	{
		var frame = getFrame();
		
		// move frame into dock placeholder
		
		getDockPlace().append(frame);
		
		var slider = frame.find('.slider');
		slider.removeClass('open');
		slider.addClass('docked');
		dock.data('status', 'docked');
		
		adjustDockPlaceSize();
		
		jQuery('html, body').animate({
		    scrollTop: jQuery(frame).offset().top
		}, 1000);
	};
	
	
	/**
	 * 
	 */
	var undockFrame = function()
	{
		var frame = getFrame();
		
		// move frame into body
		
		jQuery('body').append(frame);
		
		var slider = frame.find('.slider');
		slider.removeClass('docked');
		slider.addClass('open');
		dock.data('status', 'open');
		
		adjustDockPlaceSize();
	};
	
	
	/**
	 * dock button clicked
	 */
	var clickDockButton = function()
	{
		var dockButton = jQuery(this);
		dockButton.blur();
		
		var frame = dockButton.closest('.widget-dockable-frame');
		// var dock = frame.data('dock');
		
		
		if ('docked' === dock.data('status'))
		{
			undockFrame();
		}
		else 
		{
			dockFrame();
		}
	};
	
	
	/**
	 * Main or close button clicked
	 */
	var clickMainButton = function()
	{
		
		switch(dock.data('status')) {
			case 'closed':
				loadContent();
				openFrame();
				
				if ('docked' == meta().openMode)
				{
					dockFrame();
				}
				
				break;
				
			case 'open':
			case 'docked':
				jQuery(this).blur();
				closeFrame();
		}

	};
	
	/**
	 * get the dock placeholder
	 * this is a div in the page with the widget-dock class
	 * if the div is not found, we try to build one
	 */
	var getDockPlace = function()
	{
		var target = jQuery('.widget-dock');
		
		if (target.length != 1)
		{
			// create dock
			target = jQuery('<div class="widget-dock"></div>');
			jQuery('body').append(target);
			
			var site = jQuery('<div class="widget-dockable-site"></div>')
				.css('position', 'absolute')
				.css('top', 0)
				.css('left',0)
				.css('right',0);
			
			
			
			jQuery('body').append(site);
			
			jQuery('body').contents().each(function() {
				
				
				
				if (!jQuery(this).is('.widget-dockable-site') && !jQuery(this).is('.widget-dock') && !jQuery(this).is('.widget-dockable'))
				{
					site.append(jQuery(this));
				}
			});

			return target;
		}
		
		return target;
	};
	
	/**
	 * 
	 */
	var adjustDockPlaceSize = function()
	{
		var site = jQuery('.widget-dockable-site');
		var target = getDockPlace();
		
		if (0 == site.length)
		{
			return;
		}
		
		if ('docked' === dock.data('status'))
		{
			
			var targetWidth = target.width() + 2; // 2px for the border
			
			site.css('margin-left', targetWidth+'px');
			
			var w = jQuery(window).height();
			var d = jQuery(document).height();
			
			if (d > w) {
				target.height(d);
			} else {
				target.height(w);
			}
			
		} else {
			site.css('margin-left', '0px');
			target.height(0);
		}
	};
	
	
	var meta = function()
	{
		return window.babAddonWidgets.getMetadata(dock.attr('id'));
	};
	
	
	
	/**
	 * load content into frame
	 */
	var loadContent = function()
	{
		// if allready loaded do nothing
		if (dock.data('loaded'))
		{
			return;
		}
		
		
				
		var frame = getFrame();
		
		var dockContent = jQuery(dock).find('.dock-content');

		if (dockContent.length > 0) {
			frame.find('.widget-dockable-content').append(dockContent);
			frame.find('.widget-dockable-content').removeClass('loading');
			dock.data('loaded', true);
		} else {
			frame.find('.widget-dockable-content').load(meta().contentUrl, function() {
				frame.find('.widget-dockable-content').removeClass('loading');
				dock.data('loaded', true);
			});
		}
	};
	
	
	var openFrame = function()
	{
		var frame = getFrame();
		var slider = frame.find('.slider');
		
		slider.removeClass('closed');
		slider.addClass('open');
		dock.data('status', 'open');
		
		if (meta().displayDockFullSize){
			var backgroundFrame = getbackgroundFrame();
			backgroundFrame.removeClass('closed');
			backgroundFrame.addClass('open');
		}
		
		dock.hide();
	};
	
	
	var closeFrame = function()
	{
		var frame = getFrame();
		var slider = frame.find('.slider');
		
		slider.removeClass('docked');
		slider.removeClass('open');
		slider.addClass('closed');
		dock.data('status', 'closed');
		
		if (meta().displayDockFullSize){
			var backgroundFrame = getbackgroundFrame();
			backgroundFrame.removeClass('open');
			backgroundFrame.addClass('closed');
		}
		
		adjustDockPlaceSize();
		
		if (meta().displayMainButton)
		{
			dock.show();
		}
	};
	
	
	

	jQuery.fn.toggleDock = function()
	{
		clickMainButton();
	};

	jQuery.fn.getDockStatus = function()
	{
		return dock.data('status');
	};
	
	
	
	
	// init
	
	
	
	jQuery('body').append(dock);
	
	dock.data('status', 'closed');
	
	if (meta().displayMainButton)
	{
		dock.show(); // display button
		dock.click(clickMainButton);
	}
	
	if (meta().displayDockFullSize){
		jQuery(dock).toggleDock();
	}
};









function widgets_dockableInit(domNode)
{
	jQuery(domNode).find('.widget-dockable').not('.widget-init-done').each(function() {
		
		jQuery(this).addClass('widget-init-done');

		window.babAddonWidgets.dockable(jQuery(this));
		var meta = window.babAddonWidgets.getMetadata(this.id);
		//if(meta.openOnLoad){
		//	jQuery(this).toggleDock();
		//}
	});
	
}


window.bab.addInitFunction(widgets_dockableInit);
