/*
 * widgets.js
 * add some default optional js actions associated to css classes
 * depend only from ovidentia.js
 */

window.bab = {
    initFunctions: [],

    addInitFunction: function (f) {
        this.initFunctions.push(f);
    },

    init: function (domNode) {
        for (var i = 0; i < this.initFunctions.length; i++) {
            var initFunction = this.initFunctions[i];
            initFunction(domNode);
        }
    }
};




window.babAddonWidgets = {

    neededJavascripts: {},

    /**
     * @param {string[]|HTMLElement[]} targetElements - An array of ids or HTMLElements from which reload nodes should be searched.
     * @return {HTMLElement[]}
     */
    getReloadElements: function (targetElements) {
        if (typeof targetElements === 'undefined') {
            return [];
        }
        var nbTargetElements = targetElements.length;
        var reloadElements = [];
        for (var i = 0; i < nbTargetElements; i++) {

            var element = (typeof targetElements[i] === 'string' ? document.getElementById(targetElements[i]) : targetElements[i]);

            if (element) {//MAYBE YOU ASK TO RELOAD AN ELEMENT BY ID BUT THIS ELEMENT IS NOT ALWAYS ON THE PAGE
                do {
                    if (element.id) {
                        var meta = window.babAddonWidgets.getMetadata(element.id);
                        if (meta.delayedAction) {
                            reloadElements.push(element);
                            break;
                        }
                    }
                    if (element.widgetParentNode) {
                        element = element.widgetParentNode;
                    } else {
                        element = element.parentNode;
                    }
                } while (element);
            }
        }
        return reloadElements;
    },


    /**
     * @param {HTMLElement[]}
     */
    reload: function(/* domNodes */) {

        var promises = [];
        var initList = [];

        var reloadElements = window.babAddonWidgets.getReloadElements(arguments);
        var nbReloadElements = reloadElements.length;

        for (var index = 0; index < nbReloadElements; index++) {
            var currentNode = reloadElements[index];
            var meta = window.babAddonWidgets.getMetadata(currentNode.id);
            var jqp = jQuery(currentNode).parent();
            var parent = jqp.get(0);
            if(parent) {
                jQuery(parent).addClass('widget-delayed-action-loading');
                jQuery(parent).find('input').attr("disabled", "disabled");
                jQuery(parent).find('input').addClass("widget-reload-disabled");

                var delayedUrl = meta.delayedAction;
                var form = jqp.closest('form');

                if (form.length > 0) {
                    var formSubmitUid = form.find('input[name=_formSubmitUid]');
                    delayedUrl += '&formSubmitUid='+formSubmitUid.val();
                }

                promises.push(
                    jQuery.ajax({
                        url: delayedUrl,
                        cache: false,
                        success: function(response, textStatus) {
                            jqp.removeClass('widget-delayed-action-loading');
                            jqp.find('input.widget-reload-disabled').removeProp("disabled");
                            jqp.find('input.widget-reload-disabled').removeClass("widget-reload-disabled");
                            jqp.removeProp('disabled');
                            jqp.css('visibility', 'hidden');
                            if (textStatus != 'notmodified') {
                                jqp.html(response);
                            }
                            var newmeta = window.babAddonWidgets.getMetadata(parent.firstChild.id);
                            newmeta.delayedAction = meta.delayedAction;
                            window.babAddonWidgets.addMetadata(parent.firstChild.id, newmeta);
                            jqp.css('visibility', 'visible');

                            window.bab.init(parent);
                            window.bab.init(jQuery('body > .ui-dialog').get(0));

                            initList.push(parent);
                        }
                    })
                );
            }
        }

        var when = jQuery.when.apply(jQuery, promises).then(function() {
            jQuery('.widget-form.widget-persistent').each(function() {
                widgets_initFormData(this);
            });
            
            // If an element was focused before the reload, we reset focus on it as well as the selected part.
            if (window.babAddonWidgets.focusedItemName && window.babAddonWidgets.focusedItemSelectionStart !== null) {
                lastFocus = document.getElementsByName(window.babAddonWidgets.focusedItemName)[0];
                if (lastFocus) {
                    lastFocus.focus();
                    lastFocus.value = window.babAddonWidgets.focusedItemValue;
                    lastFocus.setSelectionRange(
                		window.babAddonWidgets.focusedItemSelectionStart, 
                		window.babAddonWidgets.focusedItemSelectionEnd,
                		window.babAddonWidgets.focusedItemSelectionDirection
            		);
                }
            }
        });
    },
    
    
    /**
     * Get a reloadAction function from a reloadItem string
     * @param {string[]|HTMLElement[]} reloadItem
     * @return {Function}
     */
    getReloadFunction: function(reloadItem) {
    	var reloadElements = window.babAddonWidgets.getReloadElements(reloadItem);
        var nbReloadElements = reloadElements.length;
        for (var i = 0; i < nbReloadElements; i++) {
            var domNode = reloadElements[i];
            var parent = jQuery(domNode).parent().get(0);
            jQuery(parent).addClass('widget-loading-content');
        }
        
        return function() {
        	for (var i = 0; i < nbReloadElements; i++) {
                window.babAddonWidgets.reload(reloadElements[i]);
                var parent = jQuery(reloadElements[i]).parent().get(0);
                jQuery(parent).removeClass('widget-loading-content');
            }
        };
    },
    
    


    /**
     * called for actions on setAjaxAction
     * 
     * @param {string}							action
     * @param {string}							item
     * @param {string[]|HTMLElement[]|Function} reloadAction		Reload action or list of items to reload
     */
    ajax: function(action, item, reloadAction, data) {

        item = typeof item !== 'undefined' ? item : false;
        
        if (typeof reloadAction !== 'function') {
        	reloadAction = window.babAddonWidgets.getReloadFunction(reloadAction);
        }
        
        /**
         * Get submit button from item
    	 * @return {jQuery}
    	 */
    	function getSubmitButton() {
    		
    		if (!item) {
    			return jQuery();
    		}
    		
    		var jItem = jQuery(item);
    		
    		if (jItem.hasClass('widget-submitbutton')) {
    			return jItem;
    		}
    		
    		if (jItem.hasClass('widget-form')) {
    			return jItem.find('widget-submitbutton');
    		}
    		
    		return jQuery();
    	}
        

    	// If possible, lock the submit button, will be unlocked by endAjax()
        getSubmitButton().attr('disabled', 'disabled');
//        var focusedItem = document.activeElement;
//        window.babAddonWidgets.focusedItemName = document.activeElement.name ? document.activeElement.name : null;

        
        /**
         * Call reloadAction & Remove spinner & enable button
         * 
         */
        function endAjax(item)
        {
        	var jItem = jQuery(item);
        	
        	/**
        	 * Get form from item
        	 * @return {jQuery}
        	 */
        	function getForm() {
        		if (jItem.hasClass('widget-submitbutton')) {
        			if (jItem.attr('form')) {
        				containingForm = jQuery('#' + jItem.attr('form'));
        			}
        			return containingForm;
        		}
        		
        		if (jItem.hasClass('widget-form')) {
        			return jItem;
        		}
        		
        		return jQuery();
        	}

        	// Before reloading, if an element is focused we store its name as well as the selected part.
        	var focusedItem = document.activeElement;
        	window.babAddonWidgets.focusedItemName = ('name' in focusedItem) ? focusedItem.name : null;
        	window.babAddonWidgets.focusedItemValue = ('value' in focusedItem) ? focusedItem.value : null;
        	window.babAddonWidgets.focusedItemSelectionStart = ('selectionStart' in focusedItem) ? focusedItem.selectionStart : null;
        	window.babAddonWidgets.focusedItemSelectionEnd = ('selectionEnd' in focusedItem) ? focusedItem.selectionEnd : null;
        	window.babAddonWidgets.focusedItemSelectionDirection = ('selectionDirection' in focusedItem) ? focusedItem.selectionDirection : null;
        	
        	reloadAction();
            

            if (item) {
                if (jItem.hasClass('widget-no-close')) {
                    jItem.closest('.widget-dialog-container').removeClass('widget-delayed-action-loading');
                } else {                
	                var form = getForm();
	                if (form.attr('method') && form.attr('method').toLowerCase() == 'post') {
	                	// Close the loading dialog if form is in dialog
	                    var dialogContent = form.closest('.widget-dialog-container');
	                    dialogContent.removeClass('widget-delayed-action-loading');

						allDialog = dialogContent.find('.widget-close-alldialog');
						if (allDialog.length > 0) {
							jQuery('.widget-dialog-closebutton').each(function() {
								jQuery(this).click();
							});
						} else {
	                    	if (dialogContent.parent()) {
								dialogContent.parent().find('.widget-dialog-closebutton').click();
							}
						}
	                }
                }
            }
            
            // unlock submit button
            getSubmitButton().removeProp('disabled');
        };

        
        
        var xhr;
        
        /**
         * Open a new dialog with HTML content
         * @param {string} content
         */
        function openNewDialog(content) {
        	var jItem = jQuery(item);
            var itemMeta = window.babAddonWidgets.getMetadata(jItem.attr('id'));
            
            var modalMode = true;
            
            if(typeof itemMeta.dialogModalMode !== 'undefined' && itemMeta.dialogModalMode == false){
            	modalMode = itemMeta.dialogModalMode;
            }


			var dialogClass = '';
			if(typeof itemMeta.dialogClass !== 'undefined' && itemMeta.dialogClass == false){
            	dialogClass = itemMeta.dialogClass;
            }
	
			//var appendto = 'body';
			//if (jQuery('#dialog-container').length) {//USE TO APPLY DEFAULT STYLE
				//appendto = '#dialog-container';
			//}
			
	        //Fix for some char encoding;
	        var titleStr = '&nbsp;';
	        if (title = xhr.getResponseHeader('X-Ovi-PageTitle')) {
                titleStr = decodeURIComponent(title);
            } else if(title = xhr.getResponseHeader('X-Cto-PageTitle')) {
	        	titleStr = jQuery('<span></span>').html(xhr.getResponseHeader('X-Cto-PageTitle')).text();
	        }
            
            var itemId = jItem.attr('id')+'-dialog';
            var titleLayout = '<div id="'+itemId+'-title" class="widget-dialog-title"><span class="widget-dialog-title-text">'+titleStr+'</span><button type="button" data-a11y-dialog-hide aria-label="Close dialog" class="widget-dialog-closebutton" title="Close"><span class="ui-button-icon ui-icon ui-icon-closethick"></span></button></div>';
            
        	var dialog = jQuery('<div id="'+itemId+'" aria-labelledby="'+itemId+'-title" aria-hidden="true" class="'+dialogClass+' widget-dialog"><div class="widget-dialog-overlay"></div><div class="widget-dialog-container" role="document">'+titleLayout+'<div class="widget-dialog-content">'+content+'</div></div></div>');
        	
        	
			jQuery('body').append(dialog);
            dialog.find('.widget-form-buttons').appendTo(dialog);
            dialogA11y = new A11yDialog(dialog[0]);
            
        	dialogA11y.on('hide', function() {
	            endAjax(item);
	            document.documentElement.style.overflowY = '';
	            dialog.remove();
			});
			
        	dialogA11y.on('show', function() {
				document.documentElement.style.overflowY = 'hidden'
			});
            
            dialogA11y.show();
            window.bab.init(dialog);
            
            var parentSubmit = getSubmitButton();
            if(parentSubmit && parentSubmit.hasClass('widget-ajaxclose-dialog')) {
                var closestDialog = parentSubmit.closest('dialog');
                if (closestDialog.length > 0) {
                    closestDialog[0].hide();
                }
            }
            
            return dialog;
        }
        
        /**
         * Display a response by addMessage, by alert or display html in a dialog
         * if this function return false, the endAjax must be called directly 
         * 
         * 
         * @param {string} response	JSON or HTML
         * @return {boolean} 
         */
        function displayResponse(response) {
        	function addMessages(messages) {
        		messages.forEach(function (message) {
        			if (message.time == 'undefined') {
        				message.time = 0;
        			}
                    window.babAddonWidgets.addMessage(message.content, message.level, message.time);
                });
                return false;
        	}

        	function addItems(items) {
        		items.forEach(function (item) {
        			var localItem = item;
        			var target = '';
        			if (localItem.targetClass) {
        				target = '.'+localItem.targetClass;
        			} else {
        				target = '#'+localItem.targetId;
        			}
        			var elem = jQuery(localItem.item);
        			if (localItem.excludeId) {
        				elem = elem.not('#'+localItem.excludeId)
        			}
        			
        			var animate = true;
        			if (typeof localItem.animation != 'undefined' && localItem.animation == 0) {
						animate = false;
					}
        			var toggleShow = true;
        			if (elem.css('display') != 'none') {
						if (animate) {
        					elem.hide();
    					}
        			} else {
        				toggleShow = false;
        			}
        			
					elem.initItem = function() {
						setTimeout(//SOME WIDGET NEED TO be display before loading (ex: MAP)
							function() {
								parent = elem.parent();
								if(parent) {
									window.bab.init(parent);
								} else {
									window.bab.init(this);
								}
							},250
						)
					}
        			
        			
        			elem.displayItem = function() {
						if (animate) {
							elem.slideDown("400", function() {
	    						elem.initItem();
	    					});
						} else {
							elem.initItem();
						}
					}
        			
        			switch (localItem.position) {
						case 'top':
        					jQuery(target).prepend(elem);
        					elem.displayItem();
        					break;
        					
						case 'bottom':
        					jQuery(target).append(elem);
        					elem.displayItem();
        					break;
        					
						case 'before':
        					jQuery(target).before(elem);
        					elem.displayItem();
        					break;
        					
						case 'after':
        					jQuery(target).after(elem);
        					elem.displayItem();
        					break;
        					
        				case 'replace':
							if (animate) {
	        					jQuery(target).slideUp("400", function() {
		            				jQuery(target).html('');
		            				jQuery(target).show();
		            				jQuery(target).html(elem);
		            				if (toggleShow) {
			            				elem.displayItem();
		            				}
		        				});
	        				} else {
								jQuery(target).html('');
	            				jQuery(target).show();
	            				jQuery(target).html(elem);
	            				if (toggleShow) {
		            				elem.displayItem();
	            				}
							}
            				break;
        					
        				case 'target':
							if (animate) {
	        					jQuery(target).slideUp("400", function() {
	        						jQuery(target).replaceWith(elem);
		            				if (toggleShow) {
			            				elem.displayItem();
		            				}
		        				});
							} else {
								jQuery(target).replaceWith(elem);
	            				if (toggleShow) {
		            				elem.displayItem();
	            				}
							}
					}
                });
                return false;
        	}
        	
        	if (response.messages) {
            	return addMessages(response.messages);
            }
        	
        	try {
                var json = JSON.parse(response);
                
                if (json.message) {
                	alert(json.message);
                }
                if (json.reloadSelector) {
                    var elements = jQuery(json.reloadSelector).toArray();
                    for (var i = 0; i < elements.length; i++) {
                        window.babAddonWidgets.reload(elements[i]);
                    }
                }
                if (json.messages) {
                	addMessages(json.messages);
                }
                if (json.items) {
                	addItems(json.items);
                }
                if (json.url) {
                	window.location.replace(json.url);
                }
                
                if (json.messages || json.items || json.message || json.reloadSelector || json.url) {
                	return false;
                }
            } catch (e) {
            	//console.log('parseerror');
            }
            
            if (response.length === 0) {
            	return false;
            }

            // Response is not understandable json, display HTML
        	openNewDialog(response);
        	return true; // endAjax will be called on close dialog
        }
        
        
        /**
         * Capture the POST result of a login with HTML form 
         *
         */
		function captureLoginResult(dialog, next) {
			var f = dialog.find('.bab-login-form');
			f.submit(function(event) {
				var $inputs = dialog.find('.bab-login-form :input');
				var values = {};
			    $inputs.each(function() {
			        values[this.name] = jQuery(this).val();
			    });
			    
				jQuery.post({
					url: f.attr('action'),
					data: values, 
					error: function(jqXHR) {
						if (!displayResponse(jqXHR.responseText)) {
							endAjax(item);
						}
					},
					success: next
				});

				event.preventDefault();
			});
		}

        

        var ajaxOptions = {
            url: action,
            cache: false
        };

        if (typeof data != 'undefined') {
            ajaxOptions.method = 'POST';
            ajaxOptions.data = data;
        }
        
        xhr = jQuery.ajax(ajaxOptions)
        .done(function(response, textStatus, request) {
            if (!displayResponse(response)) {
                endAjax(item);
            }
        })
        .fail(function(jqXHR) {
            if (jqXHR.status == '401') {
                var loginDialog = openNewDialog(jqXHR.responseText);
                captureLoginResult(loginDialog, function() {
					loginDialog.find('.widget-dialog-closebutton').click();
                	xhr = jQuery.ajax(ajaxOptions);
                });
            } else {
            	if (!displayResponse(jqXHR.responseText)) {
            	    endAjax(item);
            	}
            }
        })
    },

    addMessage: function (message, level, timed = null) {
        var messageBox = jQuery('.bab-page-messagebox');
        
        if (timed === null) {
        	timed = 0;
        	var itemDefaultTime = jQuery('#widget-default-message-time');
        	if (itemDefaultTime && itemDefaultTime.attr('defaultTime')) {
        		timed = parseInt(itemDefaultTime.attr('defaultTime'));
        	}
        }
        
        if (messageBox.length < 1) {
            jQuery('body').prepend('<div class="bab-page-messagebox"></div>');
        }
        
        var button = jQuery('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
        var message = jQuery('<div class="alert alert-dismissible alert-' + level + '">' + message + '</div>');
        message.append(button);
        jQuery('.bab-page-messagebox').append(message);
        
        button.click(function() {
        	jQuery(this).parent().remove();
        	return false;
        });
        
        if (timed) {
        	setTimeout(function() {
        		message.fadeOut(800, function() {
        			button.click()
        		});
			}, timed);
        }
    },

    /**
     * @param	string	metaname
     * @param	arr		Object
     * @return Object
     */
    addMetadata: function(metaname, arr, id = null) {

        if (typeof window.babAddonWidgets.metadata == 'undefined') {
            window.babAddonWidgets.metadata = {};
        }
        
        if (id !== null) {
			jQuery(".widget-meta-"+id).remove();
		}

        window.babAddonWidgets.metadata[metaname] = arr;
    },


    /**
     * @param	string	metaname
     * @return Object
     */
    getMetadata: function(metaname) {

        if (typeof window.babAddonWidgets.metadata == 'undefined' || typeof window.babAddonWidgets.metadata[metaname] == 'undefined') {
            return {};
        }

        return window.babAddonWidgets.metadata[metaname];
    },




    /**
     * @param string	url		The url of the
     */
    _loadScript: function(url) {
        var scripts = document.scripts;
        var nbScripts = scripts.length;
        var i = 0;
        for (i = 0; i < nbScripts; i++) {
            if (url == scripts[i].getAttribute('src')) {
                return true;
            }
        }

        var script = document.createElement('script');
        script.setAttribute('type', 'text/javascript');
        script.setAttribute('src', url);
        script.setAttribute('defer', 'defer');
        document.body.appendChild(script);

//		console.debug("Append script (" + url + ")");

        script.onreadystatechange = function () {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                for (var i = 0; i < window.babAddonWidgets.neededJavascripts[url].length; i++) {
                    var node = document.getElementById(window.babAddonWidgets.neededJavascripts[url][i]);
                    if (node) {
                        window.bab.init(node.parentNode);
                    }
                }
            }
        };

        script.onload = function() {
            for (var i = 0; i < window.babAddonWidgets.neededJavascripts[url].length; i++) {
                if (window.babAddonWidgets.neededJavascripts[url][i]) {
                    var node = document.getElementById(window.babAddonWidgets.neededJavascripts[url][i]);
                    if (node) {
                        window.bab.init(node.parentNode);
                    }
                }
            }
        };

    },




    /**
     * Include a javascript file from the widget addon.
     * @param string	widget_id	The dom element id requiring the javascript for initialization.
     * @param string	url
     */
    needScript: function(widget_id, url, id = null) {
         //console.debug('babAddonWidgets.needScript(' + widget_id + ', ' + url + ')');

        if (typeof window.babAddonWidgets.neededJavascripts[url] == 'undefined') {
            window.babAddonWidgets.neededJavascripts[url] = [];
        }
        window.babAddonWidgets.neededJavascripts[url].push(widget_id);
        
        
        if (id !== null) {
			jQuery(".widget-needscript-"+id).remove();
		}

        // IE8 : document.readyState is "interactive" while the DOM is not fully loaded
        if ( document.readyState === "complete" || ( document.readyState !== "loading" && document.addEventListener )) {
            for (var url in window.babAddonWidgets.neededJavascripts) {
                window.babAddonWidgets._loadScript(url);
            }
        }

    },




    /**
     * Add a link to the specified css stylesheet file in the page head.
     * @param string	file
     */
    stylesheet: function(url) {
        if (!document.createElement || !document.getElementsByTagName) {
            alert('Your browser can\'t deal with the DOM standard. That means it\'s old.');
            return false;
        }

        var head = document.getElementsByTagName('head')[0];

        var links = head.getElementsByTagName('link');
        var nbLinks = links.length;
        var i = 0;
        for (i = 0; i < nbLinks; i++) {
            if (url == links[i].getAttribute('href')) {
                return true;
            }
        }

        var link = document.createElement('link');
        link.setAttribute('type', 'text/css');
        link.setAttribute('rel', 'stylesheet');
        link.setAttribute('href', url);

        head.appendChild(link);
        return true;
    }

};












document.onreadystatechange = function () {
    // IE8 : document.readyState is "interactive" while the DOM is not fully loaded
    if ( document.readyState === "complete" || ( document.readyState !== "loading" && document.addEventListener )) {
        for (var url in window.babAddonWidgets.neededJavascripts) {
            window.babAddonWidgets._loadScript(url);
        }
    }
};

