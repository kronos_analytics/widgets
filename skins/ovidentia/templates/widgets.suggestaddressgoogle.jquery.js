var widgets_suggestgoogleAutocomplete = {};

function widgets_suggestgoogleInit(domNode)
{
    if(!window.bab.suggestgoogleApi){
        window.bab.suggestgoogleApi = 'loading';

        widget = jQuery('.widget-suggestgoogle');
        var widget_meta = window.babAddonWidgets.getMetadata(widget.attr('id'));
        var apiKey_meta = widget_meta.babGoogleApiKey;
        var script = document.createElement("script");
        script.type = "text/javascript";
        document.getElementsByTagName("head")[0].appendChild(script);
        script.src = 'https://maps.googleapis.com/maps/api/js?key='+apiKey_meta+'&libraries=places&callback=widgets_suggestgoogleInitialize';
    }else{
        if(window.bab.suggestgoogleApi == 'loaded'){
            widgets_suggestgoogleInitialize();
        }
    }
}





/**
 * Register a form event to make sure the address has been selected from suggestions
 *
 * @param {jQuery} input
 */
function widgets_suggestaddressgoogleValidate(input, meta) {
	
	
    input.data( "initalValue", input.val());
    input.blur(function() {
    	// on unfocus, if the address input has been modified, check that it come from a suggestion
    	var blurredInput = jQuery(this);
    	setTimeout(function() {
    		// we wait for the suggest modification, if any
    		var lastSuggestUpdate = blurredInput.data("lastSuggestUpdate");

    		if (lastSuggestUpdate && (Date.now() - lastSuggestUpdate) < 500) {
    			// hidden fields where updated in the last 500ms
    			blurredInput.removeClass('widget-suggestaddressgoogle-notFromSuggest');
    			return;
    		}
    		if (blurredInput.val() === blurredInput.data("initalValue")) {
    			// content not modified
    			blurredInput.removeClass('widget-suggestaddressgoogle-notFromSuggest');
    			return;
    		}
    		
    		blurredInput.addClass('widget-suggestaddressgoogle-notFromSuggest');
    		
    	}, 500);
    	
    });
	

    if (form = input.closest('form')) {
        form.on('validate.widgets', function() {
            if (input.hasClass('widget-suggestaddressgoogle-notFromSuggest')) {
                input.focus();
                
                var message = meta.notFromSuggest;

                if (window.babAddonWidgets.validate || window.babAddonWidgets.validatemandatory) {
                	// this verification type is  always mandatory 
                    form.addClass('widget-invalid');
                    alert(message);
                    return false;
                }
            }

            return true;
        });
    }
}







function widgets_suggestgoogleInitialize()
{
	
    window.bab.suggestgoogleApi = 'loaded';
    window.bab.mapAi = 'loaded';
    jQuery('.widget-suggestgoogle').not('.widget-init-done').each(function () {
        jQuery(this).addClass('widget-init-done');
        var id = jQuery(this).attr('id');
        var widget_meta = window.babAddonWidgets.getMetadata(id);
        var suggestId = id+'_suggest';
        
        /** @type {!HTMLInputElement} */
        var suggestField = document.getElementById(suggestId);
        var jqSuggestField = jQuery(suggestField);

        var options = { types: ['geocode'] };
        if (widget_meta.restriction) {
            options.componentRestrictions = { country: widget_meta.restriction };
        }

        // Create the autocomplete object, restricting the search to geographical
        // location types.
        widgets_suggestgoogleAutocomplete[id] = new google.maps.places.Autocomplete(
            suggestField,
            options
        );
        
        
        widgets_suggestaddressgoogleValidate(jqSuggestField, widget_meta);
        


        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        widgets_suggestgoogleAutocomplete[id].addListener('place_changed', function() {
        	
            // Get the place details from the autocomplete object.
            var place = widgets_suggestgoogleAutocomplete[id].getPlace();
            var loctype = jQuery('#'+id+'_loctype');
            var street = jQuery('#'+id+'_street');
            var city = jQuery('#'+id+'_city');
            var postalCode = jQuery('#'+id+'_postalCode');
            var country = jQuery('#'+id+'_country');
            var latitude = jQuery('#'+id+'_latitude');
            var longitude = jQuery('#'+id+'_longitude');

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            var address = {};
            if (undefined !== place.address_components) {
	            for (var i = 0; i < place.address_components.length; i++) {
	                var addressType = place.address_components[i].types[0];
	                address[addressType] = place.address_components[i].long_name;
	            }
            }
            
            if (undefined !== place.types && place.types.length > 0) {
            	loctype.val(place.types[0]);
            } else {
            	loctype.val('');
            }

            //STREET
            if (typeof address.route !== 'undefined') {
                street.val(address.route);
            } else {
                street.val('');
            }
            
            if (typeof address.street_number !== 'undefined') {
                street.val(address.street_number + ' ' + street.val());
            }

            //CITY
            if (typeof address.locality !== 'undefined') {
                city.val(address.locality);
            } else {
                city.val('');
            }

            //POSTALCODE
            if (typeof address.postal_code !== 'undefined') {
                postalCode.val(address.postal_code);
            } else {
                postalCode.val('');
            }

            //COUNTRY
            if (typeof address.country !== 'undefined') {
                country.val(address.country);
            } else {
                country.val('');
            }

            //latitude
            if (place.geometry.location.lat()) {
                latitude.val(place.geometry.location.lat());
            } else {
                latitude.val('');
            }

            //latitude
            if (place.geometry.location.lng()) {
                longitude.val(place.geometry.location.lng());
            } else {
                longitude.val('');
            }
            
            jqSuggestField.data("lastSuggestUpdate", Date.now());
        });
    });

}


window.bab.addInitFunction(widgets_suggestgoogleInit);