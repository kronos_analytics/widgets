
function widget_topicPickerInit(domNode) {
    jQuery(domNode).find('.widget-topiccategorypicker').not('.widget-init-done').each(function() {

        jQuery(this).addClass('widget-init-done');

        var categoryPickerLineEdit = jQuery(this).children().get(0);
        if (!categoryPickerLineEdit) {
            return;
        }
        jQuery(this).append('<input type="hidden" name="' + categoryPickerLineEdit.name + '" value="' + categoryPickerLineEdit.value + '" />');

        jQuery(categoryPickerLineEdit).hide();

        var text = window.babAddonWidgets.getMetadata(categoryPickerLineEdit.id).categoryName;
        jQuery(this).append('<div>' + text + '</div>');
        categoryPickerLineEdit.name = '';


        jQuery(this).click(function() {
            var categoryPickerLineEdit = jQuery(jQuery(this).children().get(0));
            if (!categoryPickerLineEdit) {
                return;
            }
            var categoryPickerHidden = jQuery(jQuery(this).children().get(1));
            var categoryPickerLabel = jQuery(jQuery(this).children().get(2));
            bab_dialog.selectarticle(function(param) {
                categoryPickerLabel.text(param['content']);
                categoryPickerHidden.val(param['id']);
            }, 'show_categories&selectable_categories&memorize'
            );
        }
        );
    });

}


window.bab.addInitFunction(widget_topicPickerInit);
