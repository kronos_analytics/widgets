
function widget_formInit(domNode)
{
    window.onbeforeunload =	function(e) {
        var e = e || window.event;
        var ask = false;
        var message = '';

        jQuery('.widget-form').each(function (e) {
            if (this.checkUnsaved && this.isUnsaved) {
                var meta = window.babAddonWidgets.getMetadata(jQuery(this).attr('id'));
                message += meta.unsavedMessage + '\n' || '';
                ask = true;
            }
        });

        if (ask) {
            // For IE and Firefox
            if (e) {
                e.returnValue = message;
            }
            return message;
        }
    };

    

    jQuery(domNode).find('.widget-form').not('.widget-init-done').each(function() {
        var $form = jQuery(this);
        $form.addClass('widget-init-done');
        
        this.isUnsaved = false;
        if (window.babAddonWidgets.getMetadata($form.attr('id')).checkUnsaved) {
            this.checkUnsaved = true;
            // We store the original values of the form.
            this.orginalValues = $form.serializeArray();
            var form = this;

            $form.change(function () {
                var original = this.orginalValues;
                var current = $form.serializeArray();

                // We compare the original values and the current values of the form.
                var isSame = (current.length == original.length) && current.every(function(element, index) {
                    return (element.name === original[index].name) && (element.value === original[index].value);
                });

                form.isUnsaved = !isSame;
            });
        }
        

        if ($form.hasClass('widget-ajax')) {
            var meta = window.babAddonWidgets.getMetadata(jQuery(this).attr('id'));
            var eventName = meta.ajaxActionEvent;

            $form.bind(eventName, function(e) {
            	if (typeof CKEDITOR != 'undefined') {
            		for ( instance in CKEDITOR.instances ) {
            			CKEDITOR.instances[instance].updateElement();
            		}
            	}
                var method = jQuery(this).attr('method').toUpperCase();
                switch (method) {
                    case 'POST':
                        window.babAddonWidgets.ajax(meta.ajaxAction, this, meta.ajaxActionReload, $form.serializeArray());
                        break;
                    case 'GET':
                    default:
                        var ajaxActionElements = meta.ajaxAction.split('?');
                        window.babAddonWidgets.ajax(ajaxActionElements[0] + '?' + $form.serialize() + '&' + ajaxActionElements[1], this, meta.ajaxActionReload);
                        break;
                }

                //e.stopPropagation();
                if (eventName == 'click') {
                    return false;
                }
                return false;
            });
        }
        
        
        $form.find('.widget-autosubmitonchange').change(function () {
            jQuery(this).parents('form').submit();
        });
        
        // The submitbuttons may be out of the form but always have a "form" attribute.
        
        jQuery('.widget-submitbutton[form=' + $form.attr('id') +  ']').not('.widget-submitbutton-form-init-done').each(function() {
			
            jQuery(this).addClass('widget-submitbutton-form-init-done');

            if (jQuery(this).hasClass('widget-ajax')) {

                // The button submits the form through ajax.
            	
                jQuery(this).click(function(e) {
                	
                	if (typeof CKEDITOR != 'undefined') {
                		for ( instance in CKEDITOR.instances ) {
                			CKEDITOR.instances[instance].updateElement();
                		}
                	}
                	
                	var submitButton = this;
                  	containingForm = jQuery('#' + jQuery(submitButton).attr('form'));
                    containingForm.get(0).isUnsaved = false;

                    if (jQuery(this).hasClass('widget-submitbutton-mandatory')) {
                        window.babAddonWidgets.validate = true;
                        window.babAddonWidgets.validatemandatory = true;

                    } else if (jQuery(this).hasClass('widget-submitbutton-validate')) {
                        window.babAddonWidgets.validate = true;
                        window.babAddonWidgets.validatemandatory = false;
                    } else {
                        window.babAddonWidgets.validate = false;
                        window.babAddonWidgets.validatemandatory = false;
                    }
                    
                    var meta = window.babAddonWidgets.getMetadata(jQuery(submitButton).attr('id'));

                    var submitValidate = widgets_validateSubmitForm(containingForm);
                    if (!submitValidate) {
                        return false;
                    }
                	if (jQuery(submitButton).hasClass('widget-confirm')) {
                        try {
                            if (confirmationMessage = meta.confirmationMessage) {
                                var confirmed = window.confirm(confirmationMessage);
                                if (!confirmed) {
                                    e.stopPropagation();
                                    return false;
                                }
                            }
                            //Comment those lines, we want to continue execution...
                            //e.stopPropagation();
                            //return true;
                        } catch (ex) {
                            //return true;
                        }
                    }
                    jQuery(submitButton).closest('.widget-dialog').find('.widget-dialog-container').addClass('widget-delayed-action-loading');

                    var method = containingForm.attr('method').toUpperCase();
                    switch (method) {
                        case 'POST':
                            window.babAddonWidgets.ajax(meta.ajaxAction, this, meta.ajaxActionReload, containingForm.serializeArray());
                            break;
                        case 'GET':
                        default:
                            var ajaxActionElements = meta.ajaxAction.split('?');
                            window.babAddonWidgets.ajax(ajaxActionElements[0] + '?' + containingForm.serialize() + '&' + ajaxActionElements[1], this, meta.ajaxActionReload);
                            break;
                    }

                    //e.stopPropagation();
                    return false;
                });

            } else {

                jQuery(this).click(function(event) {
                    var form = jQuery(this).parents('form').get(0);
                    if (!form) {
						form = jQuery('#'+jQuery(this).attr('form')).get(0);
					}
                    form.isUnsaved = false;

                    if (jQuery(this).hasClass('widget-submitbutton-mandatory')) {
                        window.babAddonWidgets.validate = true;
                        window.babAddonWidgets.validatemandatory = true;

                    } else if (jQuery(this).hasClass('widget-submitbutton-validate')) {
                        window.babAddonWidgets.validate = true;
                        window.babAddonWidgets.validatemandatory = false;
                    } else {
                        window.babAddonWidgets.validate = false;
                        window.babAddonWidgets.validatemandatory = false;
                    }

                    // The button does not submit the form through ajax.
                    if (jQuery(this).hasClass('widget-popup')) {
                        var w = window.open('', 'popup', 'resizable,scrollbars,width=1024, height=700');
                        var form = jQuery(this).closest('form');
                        form.attr('target', 'popup');
                    }

                });

            }

        });
    })
    .submit(function(event) {
    	if (typeof CKEDITOR != 'undefined') {
    		for ( instance in CKEDITOR.instances ) {
    			CKEDITOR.instances[instance].updateElement();
    		}
    	}
        if (!window.babAddonWidgets.validate) {
            return validated;
        }
        var validated = widgets_validateSubmitForm(this);
        
        $form = jQuery(this);
        $form.removeClass('widget-invalid');
        $form.trigger('validate.widgets');

        validated = validated && !$form.hasClass('widget-invalid');

        if (validated && $form.attr('method') != 'get') {
            // For "POST" forms, disable the submit button once the form is submitted to avoid multiple submissions.
            $form.find('.widget-submitbutton').click(function(event) { return false; });
        }

        return validated;
    });






    jQuery(domNode).find('.widget-form.widget-persistent').not('.widget-persistent-init-done').each(function() {

        var $form = jQuery(this);
        $form.addClass('widget-persistent-init-done');

        if ($form.hasClass('widget-persistent-oninit')) {
            // force initialisation from local storage
            if (widgets_haveFormData(this)) {
                widgets_initFormData(this);
            } else {
                widgets_saveFormData(this);
            }
        } else {
            // save of initial form data to allow reload of form parts via ajax
            widgets_clearFormData(this);
            widgets_saveFormData(this);
        }

        $form.change(function () {
            if (!$form.hasClass('widget-persistent-inprogress')) {
                widgets_saveFormData(this);
            }
        });


        $form.submit(function () {
            widgets_clearFormData(this);
        });
    });

}


function widgets_validateSubmitForm(domnode)
{
    if (!window.babAddonWidgets.validate) {
        return true;
    }
    var canSubmit = true;
    var canSubmitId = [];
    var errorMessages = [];
    
    function addError(msg) {
    	if (-1 === errorMessages.indexOf(msg)) {
    		errorMessages.push(msg);
    	}
    }

    jQuery(domnode).find('.widget-rejected').removeClass('widget-rejected');
    
    jQuery(domnode).find('[type=number]').each(function() {
    	//@TODO STEP
        borderItem = jQuery(this);
    	if (borderItem.parents('.widget-discarded').length > 0) {
            return;
        }
    	var val = $(this).val();
    	if (val == undefined || !val.length) {
    		return;
    	}
    	if (isNaN(val)) {
            addError('Expecting a number.');
            borderItem.addClass('widget-rejected');
    		canSubmit = false;
    		return;
    	}
    	val = val*1;
        
    	var min = borderItem.attr("min");
    	if(min != undefined) {
    		min = min*1;
            if (val < min) {
            	addError('Expecting a bigger value.');
                borderItem.addClass('widget-rejected');
            	canSubmit = false;
        		return;
            }
    	}
    	var max = borderItem.attr("max");
    	if(max != undefined) {
    		max = max*1;
            if (val > max) {
            	addError('Expecting a lesser value.');
                borderItem.addClass('widget-rejected');
            	canSubmit = false;
        		return;
            }
    	}
    	
    });

    jQuery(domnode).find('.widget-filepicker.widget-filepicker-uploading').each(function() {
        jQuery(this).css('border', '1px solid red');
        if (window.babAddonWidgets.getMetadata(jQuery(this).attr('id')).waitUploadErrorMessage) {
            addError(window.babAddonWidgets.getMetadata(jQuery(this).attr('id')).waitUploadErrorMessage);
        }
        canSubmitId[jQuery(this).attr('id')] = true;
        canSubmit = false;
    });

    jQuery(domnode).find('.widget-input-mandatory')
        .not('.widget-discarded')
        .not('.widget-displaymode')
        .not('.widget-radioset')
        .not('.widget-userpicker')
        .not('.widget-multilanglineedit').each(function() {
        // Do not validate inputs inside a widget-discarded element.
        if (jQuery(this).parents('.widget-discarded').length > 0) {
            return;
        }

		var meta = window.babAddonWidgets.getMetadata(jQuery(this).attr('id'));

        val = jQuery(this).val() === '';
        borderItem = jQuery(this);
        if(jQuery(this).hasClass('widget-multiselect')) {
            val = jQuery(this).val() === null;
            borderItem = jQuery(this).parent().find('button');
        } else if (jQuery(this).hasClass('widget-checkbox')) {
            val = !jQuery(this).is(':checked');
        } else if (jQuery(this).hasClass('widget-filepicker')) {
            val = jQuery(this).find('.widget-filepicker-file').size() == 0;
        } else if (jQuery(this).hasClass('widget-hidden-suggest')) {
            borderItem = jQuery(window.babAddonWidgets.getMetadata(jQuery(this).attr('id')).parent_id);
        } else if (jQuery(this).hasClass('widget-simplehtmledit')) {
            borderItem = jQuery(this).parent().find('.wysiwyg');
        } else if (jQuery(this).hasClass('widget-radioset')) {
			//attachedRadios
			//jQuery(this)
			var meta = window.babAddonWidgets.getMetadata(jQuery(this).attr('id'));
			val = true;
			jQuery(meta.attachedRadios).each(function() {
				if (jQuery(this).is(":checked")) {
					val = false
					jQuery(this).removeClass('widget-radio-rejected');
				} else {
					jQuery(this).addClass('widget-radio-rejected');
				}
			});
			if (val) {
				jQuery(domnode).addClass('widget-form-rejected');
			} else {
				jQuery(domnode).removeClass('widget-form-rejected');
			}
	    }

        if (val && jQuery(this).attr('id')) {
            borderItem.addClass('widget-rejected');
            if (meta.mandatoryErrorMessage) {
                addError(meta.mandatoryErrorMessage);
            }
            canSubmitId[jQuery(this).attr('id')] = true;
            canSubmit = false;
        }
    });

    jQuery(domnode).find('.widget-timepicker').each(function() {
        // Do not validate inputs inside a widget-discarded element.
        if (jQuery(this).parents('.widget-discarded').length > 0) {
            return;
        }

        var time = jQuery(this).val();
        if (time === '') {
            return 0;
        }
        var values = time.split(':');
        if (values.length != 2) {
            jQuery(this).css('border', '1px solid red');
            addError("Format d'heure non valide.");
            canSubmitId[jQuery(this).attr('id')] = true;
            canSubmit = false;
        } else {
            var hours = parseInt(values[0]);
            var minutes = parseInt(values[1]);
            if (isNaN(hours) || isNaN(minutes) || hours < 0 || hours > 23 || minutes < 0 || minutes > 59 ) {
                jQuery(this).css('border', '1px solid red');
                addError("Format d'heure non valide.");
            } else {
                jQuery(this).css('border', '');
            }
        }
    });

    jQuery(domnode).find('.widget-input-ajax-validate').not('.widget-displaymode').not('.widget-radioset').filter(":visible").each(function() {
        if (jQuery(this).val() !== '' && jQuery(this).parents('.widget-discarded').length <= 0) {
            var validateInput = jQuery(this);
            var url = window.babAddonWidgets.getMetadata(validateInput.attr('id')).ajaxValidateAction;
            jQuery.ajax({
                url: url,
                dataType: 'html',
                data: { value: validateInput.val() },
                cache: false,
                async: false,
                success: function(response) {
                    if (response === '1') {
                        validateInput.css('border', '');
                        validateInput.addClass('widget-valid');
                        validateInput.removeClass('widget-invalid');
                    } else {
                        try {
                            response = jQuery.parseJSON(response);
                            if (response.status === 'OK') {
                                validateInput.css('border', '');
                                validateInput.attr('title', response.info);
                                validateInput.addClass('widget-valid');
                                validateInput.removeClass('widget-invalid');
                            }
                        } catch (e) {
                            canSubmit = false;
                            validateInput.css('border', '1px solid darkorange');
                            validateInput.addClass('widget-invalid');
                            validateInput.removeClass('widget-valid');
                            validateInput.attr('title', '');
                            addError(response);
                        }
                    }
                }
            });

        } else {
            if(typeof canSubmitId[jQuery(this).attr('id')] == "undefined"){
                jQuery(this).css('border', '');
            }
        }
    });

    var meta = window.babAddonWidgets.getMetadata(jQuery(domnode).attr('id'));
    if (meta.validateAction) {

        var ajaxOptions = {
            async: false,
            cache: false,
            method: 'GET',
            dataType: 'html',
            success: function(response) {
                jsonResponse = jQuery.parseJSON(response);
                for (var i = 0; i < jsonResponse.length; i++) {
                    addError(jsonResponse[i].message);
                    canSubmit = false;

                    if (jsonResponse[i].id) {
                        jQuery('#' + jsonResponse[i].id).addClass('widget-rejected');
                    }
                    else if (jsonResponse[i].name) {
                        jQuery('[name="' + jsonResponse[i].name + '"]').addClass('widget-rejected');
                    }
                }
            }
        };

        var ajaxActionElements = meta.validateAction.split('?');
        ajaxOptions.url = ajaxActionElements[0] + '?' + jQuery(domnode).serialize() + '&' + ajaxActionElements[1];

        jQuery.ajax(ajaxOptions);
    }

    if (errorMessages.length > 0) {
        window.babAddonWidgets.addMessage(errorMessages.join("\n"), 'danger');
    }


    return canSubmit;
}



/**
 *
 * @param form
 * @returns {Boolean}
 */
function widgets_haveFormData(form) {

    form = jQuery(form);
    var key;
    for (var i = 0; i < localStorage.length; i++) {
         key = localStorage.key(i);
         var pathStart = form.attr('id') + '/';
         if (key.substr(0, pathStart.length) == pathStart) {
             return true;
         }
    }

    return false;
}


/**
 * Fill form with saved data
 * @param form
 */
function widgets_initFormData(form) {
    jQuery(form).addClass('widget-persistent-inprogress');
    jQuery(form).find('input,textarea,select').not('input[type=hidden]').not('.widget-multiselect').not('.widget-checkbox-hidden').each(function() {
        var inputName = jQuery(this).attr('name');
        if (inputName !== undefined) {
            var key = form.id + '/' + inputName;
            var value = false;
            if (jQuery(form).hasClass('widget-local-storage')) {
                value = localStorage.getItem(key);
            } else if (jQuery(form).hasClass('widget-session-storage')) {
                value = sessionStorage.getItem(key);
                if (value == null) {//INIT session storage
                    value = jQuery(this).val();
                    if (jQuery(this).attr('type') == "checkbox" || jQuery(this).attr('type') == "radio") {
                        if (jQuery(this).is(':checked')) {
                            value = 1;
                        } else {
                            value = 0;
                        }
                    }
                    sessionStorage.setItem(key, value);
                }
            }
            //console.debug(inputName+ ' => '+value);
            if (value !== null) {
                if( jQuery(this).attr('type') == "checkbox" || jQuery(this).attr('type') == "radio") {
                    if (value && value != '0') {
                        jQuery(this).attr('checked', true);
                    } else {
                        jQuery(this).removeAttr('checked');
                    }
                } else {
                    jQuery(this).val(value);
                }
                jQuery(this).change();
            }
        }
    });
    jQuery(form).removeClass('widget-persistent-inprogress');
}


function widgets_saveFormData(form) {
    jQuery(form).find('input,textarea,select').not('input[type=hidden]').not('.widget-multiselect').not('.widget-checkbox-hidden').each(function() {
        var inputName = jQuery(this).attr('name');
        if (inputName !== undefined) {
            var key = form.id + '/' + inputName;
            var value = jQuery(this).val();
            if (jQuery(this).attr('type') == "checkbox" || jQuery(this).attr('type') == "radio") {
                if (jQuery(this).is(':checked')) {
                    value = 1;
                } else {
                    value = 0;
                }
            }
            if (jQuery(form).hasClass('widget-local-storage')) {
                localStorage.setItem(key, value);
            } else if (jQuery(form).hasClass('widget-session-storage')) {
                sessionStorage.setItem(key, value);
            }
        }
    });
}


function widgets_clearFormData(form) {
    var key;
    for (var i = 0; i < localStorage.length; ) {
        key = localStorage.key(i);
        var pathStart = form.id + '/';
        
        if (key.substr(0, pathStart.length) == pathStart) {
            if (jQuery(form).hasClass('widget-local-storage')) {
                localStorage.removeItem(key);
            } else if (jQuery(form).hasClass('widget-session-storage')) {
                sessionStorage.removeItem(key);
            }
        } else {
            i++;
        }
    }
}


window.bab.addInitFunction(widget_formInit);

