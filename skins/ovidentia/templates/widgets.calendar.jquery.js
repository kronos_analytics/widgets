
function widget_initCalendars(parentElement)
{


	parentElement.find('.widget-calendar-period')
	.each(function () {
		this.calendar =  jQuery(this).parents('.widget-calendar').get(0);
		this.container = jQuery(this).parents('.widget-calendar-periods-section').get(0);
		var calendarPeriod = jQuery(this).find('.widget-calendarperiod').get(0);
		this.eventId = window.babAddonWidgets.getMetadata(calendarPeriod.id).eventid;
	});


///*
	parentElement.find('.widget-calendar-period')
	.each(function () {
		var calendarPeriodContent = jQuery(this).find('.widget-calendar-period-content').get(0);
		if (jQuery(calendarPeriodContent).hasClass('display-only') || jQuery(calendarPeriodContent).hasClass('unmovable')) {
			return;
		}
		jQuery(this).resizable(
			{
				handles: 's',
				resize: function(event, ui) {
					var content = ui.helper.find('.widget-calendar-period-content');
					var maxHeight = jQuery(this.container).height() - ui.position.top;
					var height = Math.min(ui.size.height, maxHeight);
					var bottom = ui.position.top + height;
					
					bottom = (Math.floor(bottom / this.gridHeight)) * this.gridHeight;				
					height = (bottom - ui.position.top) - 2;
					content.height(height);
				},
				start: function(event, ui) {
					ui.helper.css('opacity', '0.5');
					var firstCell = jQuery(this.calendar).find('.widget-calendar-hour-row:first td:eq(1)');
					this.gridHeight = firstCell.outerHeight(); // Height of 1/2hour
					this.gridWidth = firstCell.outerWidth();
				},
				stop: function(event, ui) {
					var periodMovedUrl = window.babAddonWidgets.getMetadata(this.calendar.id).periodMoved;
					
					var content = ui.helper.find('.widget-calendar-period-content');
					var contentHeight = content.height() + 2;
					
					var offsetDuration = ((contentHeight - ui.originalSize.height) / this.gridHeight) *30;
	
					var calendarId = '#' + this.calendar.id;
					
					ui.helper.addClass('widget-updating');
	
					// Here we call server url for a resized period.
					jQuery.get(periodMovedUrl + '&event=' + this.eventId + '&offsetDuration=' + offsetDuration + '&ajax=1', function (data) {
						// Here we call server to refresh the calendar.
						jQuery.get(window.location.href + '&ajax=1', function (data) {
							jQuery(calendarId).replaceWith(data);
							widget_initCalendars(jQuery(calendarId));
//							widget_initContextMenus(jQuery(calendarId));
							window.bab.init(document.getElementById(calendarId));
							
						});
					});
				}
			}
		);
	}
	);

	parentElement.find('.widget-calendar-period')
	.each(function () {
		var calendarPeriodContent = jQuery(this).find('.widget-calendar-period-content').get(0);
		if (jQuery(calendarPeriodContent).hasClass('display-only') || jQuery(calendarPeriodContent).hasClass('unmovable')) {
			return;
		}
		jQuery(this).draggable(
			{
				helper: 'original',
				revert: false,
				scroll: true,
				opacity: 0.5,
				cursor: 'move',
				containment: '#calendar_periods_section',
				handle: '.widget-calendar-period-time',
				zIndex: 10000,
				start: function(event, ui) {
					
					this.nbDisplayedDays = jQuery(this.calendar).find('.widget-calendar-hour-row:first td').length - 1;
					
					var calendarPeriod = jQuery(this).find('.widget-calendarperiod').get(0);
					this.eventId = window.babAddonWidgets.getMetadata(calendarPeriod.id).eventid;
					this.originPosition = ui.helper.position();
					var firstCell = jQuery(this.calendar).find('.widget-calendar-hour-row:first td:eq(1)');
					
					this.gridHeight = firstCell.outerHeight(); // Height of 1/2hour
					this.gridWidth = firstCell.outerWidth();
					jQuery(this).draggable('option', 'grid', [this.gridWidth - 1, this.gridHeight]);
					if (this.nbDisplayedDays == 1) {
						// To avoid problems with the grid snapping when there is only one column, we force y axis on day view.
						jQuery(this).draggable('option', 'axis', 'y');
					}
				},
				stop: function(e, ui) {				
					var periodMovedUrl = window.babAddonWidgets.getMetadata(this.calendar.id).periodMoved;
					var offsetDays;
					if (this.nbDisplayedDays == 1) {
						// To avoid problems with the grid snapping when there is only one column, we force offsetDays to 0 on day view.
						offsetDays = 0;
					} else {
						offsetDays = Math.round((ui.position.left - this.originPosition.left) / this.gridWidth);
					}
					var offsetMinutes = ((ui.position.top - this.originPosition.top) / this.gridHeight) * 30;
					ui.helper.css('opacity', '0.5');
					
					var calendarId = '#' + this.calendar.id;
	
					ui.helper.addClass('widget-updating');
	
					// Here we call server url for a moved period.
					jQuery.get(periodMovedUrl + '&event=' + this.eventId + '&offsetDays=' + offsetDays + '&offsetMinutes=' + offsetMinutes + '&ajax=1', function (data) {
						// Here we call server to refresh the calendar.
						jQuery.get(window.location.href + '&ajax=1', function (data) {
							jQuery(calendarId).replaceWith(data);
							widget_initCalendars(jQuery(calendarId));
//							widget_initContextMenus(jQuery(calendarId));
							window.bab.init(document.getElementById(calendarId));
						});
					});
				}
			}
		);
	}
	);
//*/
	parentElement.find('.widget-calendar-period')
	.dblclick(function (event) {
		var periodDblClickUrl = window.babAddonWidgets.getMetadata(jQuery(this).find('.widget-calendarperiod').get(0).id).doubleClick;
		if (typeof periodDblClickUrl != 'undefined') {
			window.location.href = periodDblClickUrl;			
		}
		event.stopPropagation();
	}
	);


	parentElement.find('.widget-calendar-periods-section td')
	.dblclick(function () {
		var datetime = this.id.split('_');
		var calendar = this.parentNode.parentNode.parentNode;
		var doubleClickUrl = window.babAddonWidgets.getMetadata(calendar.id).doubleClick;
			
		window.location.href = doubleClickUrl + '&date=' + datetime[1] + '&time=' + datetime[2];			
	}
	);	
}




jQuery(document).ready(function() {
	widget_initCalendars(jQuery('body'));
});

	
