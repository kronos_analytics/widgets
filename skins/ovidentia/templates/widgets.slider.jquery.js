


function widget_sliderInit(domNode)
{
    // Prepare slider widgets
    jQuery(domNode).find(".widget-slider").not('.widget-init-done').each(function() {
    	var item = jQuery(this);
        var meta = window.babAddonWidgets.getMetadata(this.id);
        var input = item.find('input');

        
        if (typeof(meta.minValue) == 'undefined') {
        	meta.minValue = 1;
        }
        if (typeof(meta.maxValue) == 'undefined') {
        	meta.maxValue = 10;
        }
        if (typeof(meta.step) == 'undefined') {
        	meta.step = 1;
        }
        
        var value = input.val();
        if(!value || value <= meta.minValue) {
        	item.addClass('widget-slider-minvalue');
        } else {
        	if (value >= meta.maxValue) {
    			item.addClass('widget-slider-maxValue');
        	}
        }

        var sliderOptions = {
        	min: meta.minValue,
        	max: meta.maxValue,
        	step: meta.step,
        	value: input.val(),
        	change: function(event, ui) {
        		if (ui.value == meta.minValue) {
        			item.addClass('widget-slider-minvalue');
        		} else {
        			item.removeClass('widget-slider-minvalue');
        		}
        		if (ui.value == meta.maxValue) {
        			item.addClass('widget-slider-maxValue');
        		} else {
        			item.removeClass('widget-slider-maxValue');
        		}
        		input.val(ui.value);
        		input.change();
        	}
        };

        item
            .addClass('widget-init-done')
            .slider(sliderOptions);
        
        
        if (meta.showButton) {
//        	item.css({
//        		'width': '80%',
//	            'display': 'inline-block',
//	            'margin-left': '2%',
//	            'margin-right': '2%'
//        	})
        	
        	item.addClass('widget-slider-withbuttons');
        	        	
        	var lessButton = jQuery('<button class="widget-slider-button widget-slider-lessbutton">-</button>');
//        	lessButton.css('width', '8%');
        	item.before(lessButton);
        	lessButton.on('click', function() {
        		currentVal = item.slider('value');
        		if (meta.minValue < currentVal) {
        			var newValue = currentVal-meta.step;
        			if (newValue < meta.minValue) {
        				newValue = meta.minValue;
        			}
        			item.slider('value', newValue);
        		}
        		
        		return false;
        	});
        	        	
        	var moreButton = jQuery('<button class="widget-slider-button widget-slider-morebutton">+</button>');
//        	moreButton.css('width', '8%');
        	item.after(moreButton);
        	moreButton.on('click', function() {
        		currentVal = item.slider('value');
        		if (meta.maxValue > currentVal) {
        			var newValue = currentVal+meta.step;
        			if (newValue > meta.maxValue) {
        				newValue = meta.maxValue;
        			}
        			item.slider('value', newValue);
        		}
        		
        		return false;
        	});
        }
    });
}


jQuery(document).ready(function() {
    window.bab.initFunctions.push(widget_sliderInit);
    widget_sliderInit(document.getElementsByTagName("BODY")[0]);
});


