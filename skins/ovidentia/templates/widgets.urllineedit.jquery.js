



window.babAddonWidgets.urlLineEditEvent = function() {

    var enable = function(input) {
        input.addClass('widget-urllineedit-valid');
        input.removeClass('widget-urllineedit-invalid');
    };

    var disable = function(input) {
        input.removeClass('widget-urllineedit-valid');
        input.addClass('widget-urllineedit-invalid');
    };

    var input = jQuery(this);

    var value = input.val();

    // Empty url is ok.
    if ('' == value) {
        enable(input);
        return;
    }
    
    // Url containing spaces is not ok.
    if (value.match(/\s/)) {
        disable(input);
        return;
    }

    var meta = window.babAddonWidgets.getMetadata(input.attr('id'));
    
    if (meta.checkSchemes) {
        var arr = value.match(/(^[a-zA-Z]+):\/\//);

        if (arr == null) {
            disable(input);
            return;
        }
        var scheme = arr[1];

        var allowedSchemes = meta.allowedSchemes;

        disable(input);
        for (var i = 0; i < allowedSchemes.length; i++) {
            if (allowed[i] == scheme) {
                enable(input);
                return;
            }
        }
    }
    
    return;
};



function widget_urlLineEditInit(domNode) {

    jQuery(domNode).find('.widget-urllineedit').each(function() {
        var input = jQuery(this);

        if (input.data('widgetevent'))
        {
            return;
        }

        input.data('widgetevent', true);


        // lock form validation if not a valid url address

        input.blur(window.babAddonWidgets.urlLineEditEvent);
        input.keyup(window.babAddonWidgets.urlLineEditEvent);

        input.blur();


        // form event

        if (form = input.closest('form')) {
            form.on('validate.widgets', function() {
                var input = jQuery(this).find('.widget-urllineedit-invalid')[0];

                if (input) {
                    input.focus();
                    var message = window.babAddonWidgets.getMetadata(input.id).submitMessage;

                    if (window.babAddonWidgets.validatemandatory) {
                        form.addClass('widget-invalid');
                        alert(message);
                        return false;
                    }

                    if (window.babAddonWidgets.validate) {
                        var confirm = window.confirm(message);
                        if (!confirm) {
                            form.addClass('widget-invalid');
                        }
                        return confirm;
                    }
                }

                return true;
            });
        }

    });

}



window.bab.addInitFunction(widget_urlLineEditInit);

