
function widget_imageZoomerInit(domNode)
{
    jQuery(domNode).find(".widget-imagezoomer").not('.widget-init-done').each(function() {
    	var item = jQuery(this);
        var meta = window.babAddonWidgets.getMetadata(this.id);
    	

        if (typeof(meta.imageSet) == 'undefined') {
        	meta.imageSet = 'lightbox';
        }
        
		if (typeof jQuery('.widget-imagezoomer').lightbox == 'function') {
			item.lightbox();
		} else {
			item.attr('data-lightbox', meta.imageSet);			
		}
		
		var interval = setInterval(
			function() {
				if(lightbox) {
					lightbox.option({
						alwaysShowNavOnTouchDevices: true,
						albumLabel: meta.label,
						wrapAround: meta.wraparound
		  		    });
					
					clearInterval(interval);

				}
			}
		
		,100);
		
		item.addClass('widget-init-done');
    });
}

jQuery(document).ready(function() {
    window.bab.initFunctions.push(widget_imageZoomerInit);
    widget_imageZoomerInit(document.getElementsByTagName("BODY")[0]);
});
