



window.babAddonWidgets.InputPasswordValiateEvent = function()
{
    var layout = jQuery(this).parents('.widget-inputpassword');
    var parent = jQuery(this).parent();
    var meta = window.babAddonWidgets.getMetadata(layout.attr('id'));
    var rules = meta.rule;
    var val = jQuery(this).val();

    layout.find('.widget-inputpassword-rules').remove();
    parent.append('<div class="widget-inputpassword-rules"></div>');
    for (var element in rules) {
        var ruleDiv = jQuery('<div>'+rules[element].text+'</div>');
        jQuery(layout).find('.widget-inputpassword-rules').append(ruleDiv);
        if (val.match(rules[element].preg) === null) {
            ruleDiv.removeClass('widget-inputpassword-rule-right');
            ruleDiv.addClass('widget-inputpassword-rule-wrong');
        } else {
            ruleDiv.removeClass('widget-inputpassword-rule-wrong');
            ruleDiv.addClass('widget-inputpassword-rule-right');
        }
    }
    return true;
};


window.babAddonWidgets.InputPasswordConfirmEvent = function()
{
    var layout = jQuery(this).parents('.widget-inputpassword');
    var meta = window.babAddonWidgets.getMetadata(layout.attr('id'));
    var input1 = layout.find('.widget-inputpassword-1');
    var rules = meta.rule;
    var val = jQuery(this).val();

    jQuery(layout).find('.widget-inputpassword-confirm').remove();
    if (input1.val() != val) {
        jQuery(this).after('<span class="widget-inputpassword-confirm">'+meta.confirmMessage+'</span>');
        input1.removeClass('widget-inputpassword-confirm-right');
        input1.addClass('widget-inputpassword-confirm-wrong');
        jQuery(this).removeClass('widget-inputpassword-confirm-right');
        jQuery(this).addClass('widget-inputpassword-confirm-wrong');
    } else {
        input1.removeClass('widget-inputpassword-confirm-wrong');
        input1.addClass('widget-inputpassword-confirm-right');
        jQuery(this).removeClass('widget-inputpassword-confirm-wrong');
        jQuery(this).addClass('widget-inputpassword-confirm-right');
    }

    return true;
};



function widget_InputPasswordInit(domNode) {

    jQuery(domNode).find('.widget-inputpassword').not('.widget-inputpassword-initdone').each(function() {
        var layout = jQuery(this);
        var input1 = layout.find('.widget-inputpassword-1');
        var input2 = layout.find('.widget-inputpassword-2');
        layout.addClass('widget-inputpassword-initdone');
        var meta = window.babAddonWidgets.getMetadata(this.id);

        layout.data('widgetevent', true);

        input1.focus(window.babAddonWidgets.InputPasswordValiateEvent);
        input1.blur(window.babAddonWidgets.InputPasswordValiateEvent);
        input1.keyup(window.babAddonWidgets.InputPasswordValiateEvent);

        input2.focus(window.babAddonWidgets.InputPasswordConfirmEvent);
        input2.blur(window.babAddonWidgets.InputPasswordConfirmEvent);
        input2.keyup(window.babAddonWidgets.InputPasswordConfirmEvent);


        // form event

        if (form = layout.closest('form')) {
            if (form.data('widget-inputpassword-event')) {
                // if mutiple field of same type, prevent mutiple submit event
                return;
            }

            form.data('widget-inputpassword-event', true);

            form.on('validate.widgets', function() {
                var input = jQuery(this).find('.widget-inputpassword-rule-wrong')[0];

                if (input) {
                    input1.focus();
                    var message = meta.submitMessage;

                    form.addClass('widget-invalid');
                    alert(message);
                    return false;
                }

                var input = jQuery(this).find('.widget-inputpassword-confirm-wrong')[0];

                if (input) {
                    input2.focus();
                    var message = meta.confirmMessage;

                    form.addClass('widget-invalid');
                    alert(message);
                    return false;
                }

                return true;
            });
        }

    });

}



window.bab.addInitFunction(widget_InputPasswordInit);

