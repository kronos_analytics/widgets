var widget_reCAPTCHAonloadCallback = function() {
    window.widgetrecaptcha = 'loaded';

	jQuery('.widget-reCAPTCHA').not('.widget-init-done').each(function () {

    	var item = jQuery(this);
        var meta = window.babAddonWidgets.getMetadata(this.id);
        
        item.addClass('widget-init-done');
        
        if (meta.theme == 'undefined') {
        	meta.theme = 'light';
        }
        if (meta.size == 'undefined') {
        	meta.size = 'normal';
        }
        
        
		grecaptcha.render(
			this.id,
			{
		          'sitekey' : meta.babGoogleReCaptchaPublic,
		          'theme' : meta.theme,
		          'size' : meta.size
	        }
	    );
	});
}


function widget_reCAPTCHAloadScript(src){
    window.widgetrecaptcha = 'loading';
    var script = document.createElement("script");
    script.async = "async";
    script.defer = "defer";
    document.getElementsByTagName("head")[0].appendChild(script);
    script.src = src;
}


function widget_reCAPTCHAInit(domNode)
{
	if(!window.widgetrecaptcha){
        widget_reCAPTCHAloadScript('https://www.google.com/recaptcha/api.js?onload=widget_reCAPTCHAonloadCallback&render=explicit');
    }else{
        if(window.bab.mapAi == 'loaded'){
        	widget_reCAPTCHAonloadCallback();
        }
    }
}

window.bab.addInitFunction(widget_reCAPTCHAInit);