<?php

require_once dirname(__FILE__) . '/../mock/MockOvidentia.php';
require_once dirname(__FILE__) . '/containerWidgetTest.php';

class Widget_PeriodPickerTest extends Widget_ContainerWidgetTest
{
    protected $itemClass = 'Widget_PeriodPicker';
}
