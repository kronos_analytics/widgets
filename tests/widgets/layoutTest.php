<?php

require_once dirname(__FILE__) . '/../mock/MockOvidentia.php';
require_once dirname(__FILE__) . '/itemTest.php';

class Widget_LayoutTest extends Widget_ItemTest
{
    protected $itemClass = 'Widget_Layout';
    
    
    
    /**
     * Calling the addItem() method with a Widget_LineEdit on a layout
     * must increases the layout item count by 1.
     */
    public function testAddItemIncreasesItemCount()
    {
        $W = bab_Widgets();
    
        $layout = $this->construct();

        $items = $layout->getItems();
        $this->assertCount(0, $items);
        
        $lineEdit = $W->lineEdit();

        $layout->addItem($lineEdit);
        
        $items = $layout->getItems();
        
        $this->assertCount(1, $items);
    }
    
    
    /**
     * Calling the addItem(null) on a layout does not increase the layout item count.
     */
    public function testNullAddedToLayoutHasNoEffect()
    {
        $W = bab_Widgets();
        
        $layout = $this->construct();
        
        $layout->addItem($W->lineEdit());
        
        $this->assertCount(1, $layout->getItems());

        $layout->addItem(null);
        
        $this->assertCount(1, $layout->getItems());
    }
    
    
    
    /**
     * When an item is added to a layout, the item getParent() method
     * must return the same object as the layout getParent() method.
     */
    public function testItemAddedToLayoutHasLayoutParentAsParent()
    {
        // Creates a Mock_Widget_Item.
        $layout = $this->construct();
    
        $W = bab_Widgets();
    
        $lineEdit = $W->LineEdit();
    
        $layout->addItem($lineEdit);
        
        $layoutParent = $layout->getParent();
        $lineEditParent = $lineEdit->getParent();
    
        $this->assertSame(
            $lineEditParent,
            $layoutParent,
            'The parent of item added to layout is not the same as the layout parent (' . $this->itemClass . ')'
        );
    }
    
    
    
    /**
     * The array returned by a layout getClasses() method must contain
     * the string 'widget-layout'.
     */
    public function testLayoutGetClassesContainsWidgetLayoutClass()
    {
        $layout = $this->construct();
        
        $classes = $layout->getClasses();
        
        $this->assertContains('widget-layout', $classes);
    }
    
    
    /**
     * The array returned by getClasses() on a sortable layout
     * must contain the string 'widget-sortable'.
     */
    public function testSortableLayoutGetClassesContainsWidgetSortableClass()
    {
        $layout = $this->construct();
        $layout->sortable();
    
        $classes = $layout->getClasses();

        $this->assertContains('widget-sortable', $classes);
    }
    
    /**
     * The array returned by getClasses() on a sortable layout
     * must contain the string 'widget-sortable'.
     */
    public function testNotSortableLayoutGetClassesDoesNotContainsWidgetSortableClass()
    {
        $layout = $this->construct();
        $layout->sortable(false);
    
        $classes = $layout->getClasses();
    
        $this->assertNotContains('widget-sortable', $classes);
    }
}
