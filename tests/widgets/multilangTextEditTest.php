<?php

require_once dirname(__FILE__) . '/../mock/MockOvidentia.php';
require_once dirname(__FILE__) . '/multilangEditTest.php';

class Widget_MultilangTextEditTest extends Widget_MultilangEditTest
{
    protected $itemClass = 'Widget_MultilangTextEdit';
}
