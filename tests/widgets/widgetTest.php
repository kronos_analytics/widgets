<?php

require_once dirname(__FILE__) . '/../mock/MockOvidentia.php';
require_once dirname(__FILE__) . '/itemTest.php';

abstract class Widget_WidgetTest extends Widget_ItemTest
{
    protected $itemClass = 'Widget_Widget';
    
    
    public function testSetName()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();
        
        $item->setName('myName');
        
        $this->assertEquals(
            'myName',
            $item->getName()
        );
    }
    
    
    public function testSetMultipleNames()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();
    
        $item->setName(array('myContact', 'name'));
    
        $this->assertEquals(
            array('myContact', 'name'),
            $item->getName()
        );
    }
    




    /**
     * The html string returned by the display() method must contain the "widget-persistent" class.
     */
    public function testPersistentWidgetHasCorrespondingClassInDisplayedHtml()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();
    
        $item->setPersistent();
    
        $W = bab_Widgets();
    
        $canvas = $W->HtmlCanvas();
        $html = $item->display($canvas);
    
        /*
        $matcher = array(
            'attributes' => array(
                'class' => 'widget-persistent'
            )
        );
        $this->assertTag(
            $matcher,
            $html,
            'The html class "widget-persistent" was not present for a persistent widget ' . $this->itemClass
        );
        */
        
        $xpathQueryResult = $this->getXPathMatchClass($html, 'widget-persistent');
        $this->assertEquals( 1, $xpathQueryResult->length, 'The html class "widget-persistent" was not present for a persistent widget ' . $this->itemClass );
    }
}
