<?php

require_once dirname(__FILE__) . '/../mock/MockOvidentia.php';

abstract class Widget_ItemTest extends PHPUnit_Framework_TestCase
{
    protected $itemClass = 'Widget_Item';

    /**
     * @return Widget_Item
     */
    protected function construct()
    {
        $W = bab_Widgets();
        $W->includePhpClass($this->itemClass);
		$args = func_get_args();
		$item = $this->getMockForAbstractClass($this->itemClass, $args);
		return $item;
    }

    protected function setUp()
    {
        $W = bab_Widgets();
        $W->includePhpClass($this->itemClass);

        // Ensure that we start with something clean.

        $items = Widget_Item::getAllItems();
        foreach ($items as $item) {
            $item->destroy();
        }
    }



    /**
     * Match class in root node of html element
     */
    protected function getXPathMatchClass($html, $classname)
    {
        $xpath = $this->getXPath($html);
        $nodelist = $xpath->query('/html/body/*[contains(@class, \''.$classname.'\')]');

        if ($nodelist->length === 0) {
            //debug
            print $html;
        }

        return $nodelist;
    }

    /**
     * Recursive search of class
     */
    protected function getXPathSearchClass($html, $classname)
    {
        $xpath = $this->getXPath($html);

        $nodelist = $xpath->query('/html/body//*[contains(@class, \''.$classname.'\')]');

        if ($nodelist->length === 0) {
            //debug
            print $html;
        }

        return $nodelist;
    }

    /**
     * Match attribute in root node of html
     */
    protected function getXPathMatchAttribute($html, $name, $value)
    {
        $xpath = $this->getXPath($html);

        $nodelist = $xpath->query('/html/body/*[@'.$name.'=\''.$value.'\']');

        if ($nodelist->length === 0) {
            //debug
//            print $html;
        }

        return $nodelist;
    }

    /**
     * Recursive search of attribute
     */
    protected function getXPathSearchAttribute($html, $name, $value)
    {
        $xpath = $this->getXPath($html);

        $nodelist = $xpath->query('/html/body//*[@'.$name.'=\''.$value.'\']');

        if ($nodelist->length === 0) {
            //debug
            print $html;
        }

        return $nodelist;
    }


    protected function getXPath($html)
    {
        $document = new DOMDocument;
        $document->preserveWhiteSpace = false;
        $document->loadHTML($html);
        return new DOMXPath($document);
    }


    /**
     *
     */
    public function testNewItem()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();

        $this->assertInstanceOf($this->itemClass, $item);
    }



    /**
     * The widget id set using the setId() method must be stored
     * and retrieved using getId() method.
     */
    public function testSetId()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();

        $item->setId('my-unique-id');

        $this->assertEquals('my-unique-id', $item->getId());
    }


    /**
     * An item with a display() method must return a string
     * when an html canvas is passed as parameter.
     */
    public function testDisplayedHtml()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();

        $W = bab_Widgets();

        $canvas = $W->HtmlCanvas();
        $html = $item->display($canvas);

        $this->assertInternalType('string', $html);
    }


    /**
     * When metadata is added on a displayable item, the metadata key and value
     * must be present in the displayed output.
     */
    public function testMetadataIsPresentInDisplayedHtml()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();

        $metadataKey = 'myMetadataKey';
        $metadataValue = 'my metadata value';

        $item->setMetadata($metadataKey, $metadataValue);

        $W = bab_Widgets();

        $canvas = $W->HtmlCanvas();
        $html = $item->display($canvas);


        $this->assertContains(
            $metadataKey,
            $html,
            'The metadata key was no present in the displayed html for ' . $this->itemClass
        );
        $this->assertContains(
            $metadataValue,
            $html,
            'The metadata value was no present in the displayed html for ' . $this->itemClass
        );
    }



    /**
     * The string returned by the dump() method must contain the item id.
     */
    public function testDumpingItemContainsItemId()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();

        $dump = $item->dump();

        $this->assertContains(
            $item->getId(),
            $dump
        );
    }



    /**
     * The title set using setTitle() must be returned by getTitle().
     */
    public function testSetTitle()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();

        $title = 'The item title string';
        $item->setTitle($title);

        $this->assertEquals(
            $title,
            $item->getTitle()
        );
    }



    /**
     * An attribute added to an item using addAttribute() must be present
     * in the array returned by getAttributes().
     */
    public function testAddAttribute()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();

        $item->addAttribute('attributename', 'attributevalue');

        $attributes = $item->getAttributes();

        $this->assertArrayHasKey(
            'attributename',
            $attributes
        );

        $this->assertEquals(
            $attributes['attributename'],
            'attributevalue'
        );
    }


    /**
     * An attribute added to an item using addAttribute() must be present
     * in the array returned by getAttributes().
     */
    public function testRemoveAttribute()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();

        $item->addAttribute('attributename', 'attributevalue');

        $item->removeAttribute('attributename');

        $attributes = $item->getAttributes();

        $this->assertArrayNotHasKey(
            'attributename',
            $attributes
        );
    }


    /**
     * The html string returned by the display() method must contain have a
     * tag with the same id as the item id.
     */
    public function testIdIsPresentInDisplayedHtml()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();

        $W = bab_Widgets();

        $canvas = $W->HtmlCanvas();
        $html = $item->display($canvas);

        $this->assertInternalType('string', $item->getId());
        $this->assertNotEmpty($item->getId());

        $this->assertInternalType('string', $html);
        $this->assertNotEmpty($html, get_class($item).'::display() returned an empty string');
        /*
        $matcher = array('id' => $item->getId());

        $this->assertTag(
            $matcher,
            $html,
            'There were no tags with an id matching that of the item for ' . get_class($item)
        );
        */

        $xpathQueryResult = $this->getXPathMatchAttribute($html, 'id', $item->getId());
        $this->assertEquals(
            1,
            $xpathQueryResult->length,
            'There were no tags with an id matching that of the item for ' . get_class($item)
        );

    }


    /**
     * The html string returned by the display() method must contain the item attribute name and value.
     * @depends testAddAttribute
     */
    public function testAttributeIsPresentInDisplayedHtml()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();

        $item->addAttribute('attributename', 'attributevalue');

        $W = bab_Widgets();

        $canvas = $W->HtmlCanvas();
        $html = $item->display($canvas);

        $this->assertInternalType('string', $html);
        $this->assertNotEmpty($html, get_class($item).'::display() returned an empty string');
        /*
        $matcher = array('attributes' => array('attributename' => 'attributevalue'));
        $this->assertTag(
            $matcher,
            $html,
            'There were no tags with an attribute matching the name/value for ' . $this->itemClass
        );
        */
        $xpathQueryResult = $this->getXPathMatchAttribute($html, 'attributename', 'attributevalue');


        if (2 === $xpathQueryResult->length) {
//            var_dump($html);
        }

        $this->assertEquals(
            1,
            $xpathQueryResult->length,
            'There were no tags with an attribute matching the name/value for ' . $this->itemClass
        );



    }



    /**
     * The html string returned by the display() method must contain the item title.
     * @depends testSetTitle
     */
    public function testTitleIsPresentInDisplayedHtml()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();

        $title = 'The item title string';
        $item->setTitle($title);

        $W = bab_Widgets();

        $canvas = $W->HtmlCanvas();
        $html = $item->display($canvas);

        $this->assertInternalType('string', $html);
        $this->assertNotEmpty($html, get_class($item).'::display() returned an empty string');
        /*
        $matcher = array('attributes' => array('title' => $title));
        $this->assertTag(
            $matcher,
            $html,
            'There were no tags with a title attribute matching the item title for ' . $this->itemClass
        );
        */

        $xpathQueryResult = $this->getXPathMatchAttribute($html, 'title', $title);
        $this->assertEquals(
            1,
            $xpathQueryResult->length,
            'There were no tags with a title attribute matching the item title for ' . $this->itemClass
        );

    }
}
