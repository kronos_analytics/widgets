<?php

require_once dirname(__FILE__) . '/../mock/MockOvidentia.php';
require_once dirname(__FILE__) . '/layoutTest.php';

abstract class Widget_BoxLayoutTest extends Widget_LayoutTest
{
    protected $itemClass = 'Widget_BoxLayout';


    /**
     * When an item is added to a container, the item getParent()
     * must return the container.
     */
    public function testItemsAreInsertedAtTheSpecifiedPosition()
    {
        $W = bab_Widgets();

        $layout = $this->construct();

        $lineEdit1 = $W->LineEdit('lineEdit1');
        $lineEdit2 = $W->LineEdit('lineEdit2');
        $lineEdit3 = $W->LineEdit('lineEdit3');

        $layout->addItem($lineEdit1);
        $layout->addItem($lineEdit3);

        $layout->addItem($lineEdit2, 1);

        $items = $layout->getItems();

        $this->assertSame(
            array('lineEdit1', 'lineEdit2', 'lineEdit3'),
            array_keys($items),
            'The items are not in the right order (' . $this->itemClass . ')'
        );
    }
}
