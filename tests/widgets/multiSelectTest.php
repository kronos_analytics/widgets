<?php

require_once dirname(__FILE__) . '/../mock/MockOvidentia.php';
require_once dirname(__FILE__) . '/selectTest.php';

class Widget_MultiSelectTest extends Widget_SelectTest
{
    protected $itemClass = 'Widget_MultiSelect';
    
    
    protected function getHtmlName($inputName)
    {
        return parent::getHtmlName($inputName).'[]';
    }
}
