<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */

require_once $GLOBALS['babInstallPath'].'utilit/delegincl.php';






if (!class_exists('bab_AclGroups')) {

    /**
     * Groups displayed in an ACL form
     *
     */
    class bab_AclGroups
    {

        private $id_delegation = 0;


        private $delegation_acl_definition = null;

        /**
         *
         * @param int $id_delegation
         */
        public function __construct($id_delegation)
        {
            $this->id_delegation = $id_delegation;
        }


        /**
         * number of ancestors in ACL definition of delegation
         * @param int $id_parent
         * @return number
         */
        private function ancestorsInAcl($id_parent)
        {
            global $babDB;

            $ancestors = bab_Groups::getAncestors($id_parent);
            $ancestors = array_keys($ancestors);
            $ancestors[] = $id_parent;

            foreach ($ancestors as &$a) {
                $a += BAB_ACL_GROUP_TREE;
            }

            // check if one of the ancestors is in acl definition

            $req = "SELECT t.id
                FROM
                    ".BAB_DG_ACL_GROUPS_TBL." t
                WHERE
                    t.id_group IN(".$babDB->quote($ancestors).", ".$babDB->quote(BAB_ALLUSERS_GROUP).", ".$babDB->quote(BAB_REGISTERED_GROUP).")
                    AND t.id_object='".$babDB->db_escape_string($this->id_delegation)."'
            ";
            $res = $babDB->db_query($req);
            return $babDB->db_num_rows($res);
        }


        /**
         * number of groups in ACL definition of delegation for one group (the group or one of the childnodes)
         * @param int $id_group
         * @return int
         */
        private function groupsInAcl($id_group)
        {
            $group = bab_Groups::get($id_group);

            global $babDB;

            $req = "SELECT g.id
            FROM
                ".BAB_DG_ACL_GROUPS_TBL." t,
                ".BAB_GROUPS_TBL." g
            WHERE
                ((t.id_group <".BAB_ACL_GROUP_TREE." AND g.id=t.id_group) OR (t.id_group >".BAB_ACL_GROUP_TREE." AND (t.id_group - g.id) = ".BAB_ACL_GROUP_TREE."))
                AND g.lf >=".$babDB->quote($group['lf'])."
                AND g.lr <= ".$babDB->quote($group['lr'])."
                AND t.id_object='".$babDB->db_escape_string($this->id_delegation)."'
            ";
            $res = $babDB->db_query($req);
            return $babDB->db_num_rows($res);

        }


        /**
         * get default group list for one level of group
         * using the group API
         * @return array
         */
        private function getList($id_parent, Array $allowedChildren = null, Array $allowed = null)
        {
            if ($this->id_delegation && !isset($this->delegation_acl_definition)) {
                trigger_error('missing information');
            }


            $groups = bab_getGroups($id_parent, false);

            // get ancestors

            $ancestors = null;
            if ($this->id_delegation && true === $this->delegation_acl_definition) {
                $ancestors = $this->ancestorsInAcl($id_parent);
            }


            $list = array();
            foreach ($groups['id'] as $key => $id) {


                // if delegation, if ACL defined on delegation, if no childnode in ACL, if no "checked with childnodes" in ancestors : ignore

                if ($this->id_delegation && true === $this->delegation_acl_definition && 0 === $ancestors && 0 === $this->groupsInAcl($id)) {
                    continue;
                }


                $list[$id] = array(
                    'name'				=> $groups['name'][$key],
                    'position'			=> $groups['position'][$key],
                    'allowed'			=> (isset($allowed[$id]) || null === $allowed),					// group allowed to be checked on ACL form (right checkbox)
                    'allowedChildren'	=> (isset($allowedChildren[$id]) || null === $allowedChildren)	// group and children allowed to be checked on ACL form (left checkbox)
                );

            }

            return $list;
        }


        /**
         * get one group in a level of group
         * @param int $id_group
         * return array
         */
        private function getGroup($id_group, $allowedChildren = true, $allowed = true)
        {
            $group = bab_Groups::get($id_group);

            return array(
                $id_group => array(
                    'name'				=> $group['name'],
                    'position'			=> array('lf' => $group['lf'], 'lr' => $group['lr']),
                    'allowed'			=> $allowed,					// group allowed to be checked on ACL form (right checkbox)
                    'allowedChildren'	=> $allowedChildren				// group and children allowed to be checked on ACL form (left checkbox)
                )
            );
        }


        /**
         * Get allowed groups for ACL of a delegation for one level of groups
         * if a group template is set in table BAB_DG_ACL_GROUPS_TBL allow only the treeview defined in this template
         * otherwise, use the delegation group as root of allowed groups
         *
         * @param int 	$id_parent
         *
         * @return array
         */
        public function getLevel($id_parent)
        {
            $id_delegation = $this->id_delegation;

            if (0 == $id_delegation) {
                return $this->getList($id_parent);
            }

            global $babDB;

            // get LF and LR of parent group

            $parent = bab_Groups::get($id_parent);
            $allowed = array();
            $allowedChildren = array();


            $res = $babDB->db_query(
                "SELECT t.id_group, g.nb_groups FROM ".$babDB->backTick(BAB_DG_ACL_GROUPS_TBL)." t
                left join ".BAB_GROUPS_TBL." g on g.id=t.id_group
                WHERE t.id_object='".$babDB->db_escape_string($id_delegation)."'
                "
            );

            if (0 === $babDB->db_num_rows($res)) {
                // no ACL definition, get the delegation group as allowed root
                $this->delegation_acl_definition = false;

                list($delegation) = bab_getDelegationById($id_delegation);
                $delegation_group = bab_Groups::get($delegation['id_group']);


                // allow the predefined list if under delegation group

                if ($id_parent == $delegation['id_group'] || ($parent['lf'] > $delegation_group['lf'] && $parent['lr'] < $delegation_group['lr'])) {
                    return $this->getList($id_parent);
                }

                $allowed[$delegation['id_group']] = 1;
                $allowedChildren[$delegation['id_group']] = 1;

                if ($id_parent == BAB_REGISTERED_GROUP) {
                    // simplify the ACL treeview ...
                    return $this->getGroup($delegation['id_group']);
                }

            } else {

                $this->delegation_acl_definition = true;

                // There is an ACL definition on the delegation.
                while ($arr = $babDB->db_fetch_assoc($res)) {

                    if ($arr['id_group'] >= BAB_ACL_GROUP_TREE) {
                        $arr['id_group'] -= BAB_ACL_GROUP_TREE;

                        // If one of the left checked group is upper or equal to id_parent,
                        // return the predefined list, all fields allowed.

                        $checked_group = bab_Groups::get($arr['id_group']);

                        if ($id_parent == $arr['id_group'] || ($parent['lf'] > $checked_group['lf'] && $parent['lr'] < $checked_group['lr'])) {
                            return $this->getList($id_parent);
                        }

                        $allowed[$arr['id_group']] = 1;
                        $allowedChildren[$arr['id_group']] = 1;

                    } elseif ($arr['id_group'] == BAB_ALLUSERS_GROUP || $arr['id_group'] == BAB_REGISTERED_GROUP) {
                        return $this->getList($id_parent);
                    } else {
                        if ($arr['nb_groups'] !== null) {
                            // set of groups

                            $rs = $babDB->db_query("select id_group from ".BAB_GROUPS_SET_ASSOC_TBL." where id_set=".$babDB->quote($arr['id_group']));
                            while ($rr = $babDB->db_fetch_array($rs)) {
                                $allowed[$rr['id_group']] = 1;
                            }
                        } else {
                            $allowed[$arr['id_group']] = 1;
                        }
                    }
                }
            }

            return $this->getList($id_parent, $allowedChildren, $allowed);
        }
    }

}





/**
 *
 * @param int|string    $id_parent
 * @param bab_AclGroups $delegationGroups
 * @param int           $levels
 *
 * @return array
 */
function widget_getGroupLevel($id_parent, bab_AclGroups $delegationGroups, $levels)
{
    if (0 == $levels) {
        return array();
    }

    $id_delegation = (int) bab_rp('id_delegation');


    $sublevel = array();


    $json = array();


    if ('' === $id_parent) {
        $disabled = 'false';

        if ($id_delegation > 0) {
            global $babDB;

            //TODO move the query to the bab_AclGroups object

            $req = "SELECT t.id_group
                FROM
                    ".BAB_DG_ACL_GROUPS_TBL." t
                WHERE
                ((t.id_group <".BAB_ACL_GROUP_TREE." AND t.id_group = ".$babDB->quote(BAB_ALLUSERS_GROUP).") OR (t.id_group >".BAB_ACL_GROUP_TREE." AND (t.id_group - ".$babDB->quote(BAB_ALLUSERS_GROUP).") = ".BAB_ACL_GROUP_TREE."))
                AND t.id_object='".$babDB->db_escape_string($id_delegation)."'
            ";
            $res = $babDB->db_query($req);
            $found_in_acl = $babDB->db_num_rows($res);

            if (!$found_in_acl) {
                list($delegation) = bab_getDelegationById($id_delegation);

                if (BAB_ALLUSERS_GROUP !== (int) $delegation['id_group']) {
                    $disabled = 'true';
                }

            }
        }

        $json[] = "{ 'id' : '".BAB_ALLUSERS_GROUP."' ,'name' : '".bab_toHtml(bab_getGroupName(BAB_ALLUSERS_GROUP), BAB_HTML_JS)."', 'hasChildNodes' : true, 'setOfGroups' : false, 'groupTreeDisabled' : $disabled , 'groupDisabled' : $disabled }";
        $sublevel[] = 0;

    } else {

        $groups = $delegationGroups->getLevel($id_parent);

        foreach ($groups as $id => $group) {

            $hasChildNodes = 'false';
            $pos = $group['position'];
            if (1 !== ($pos['lr'] - $pos['lf'])) {
                $hasChildNodes = 'true';
            }

            $groupTreeDisabled = $group['allowedChildren'] ? 'false' : 'true';
            $groupDisabled = $group['allowed'] ? 'false' : 'true';

            $json[] = "{ 'id' : '$id' ,'name' : '".bab_toHtml($group['name'], BAB_HTML_JS)."', 'hasChildNodes' : $hasChildNodes, 'setOfGroups' : false, 'groupTreeDisabled' : $groupTreeDisabled , 'groupDisabled' : $groupDisabled }";
            $sublevel[] = $id;
        }

        // add group set if root

        if (0 === (int) $id_parent && 0 === $id_delegation) {
            $groups = bab_getGroups(null, false);

            if (isset($groups['id']) && is_array($groups['id'])) {
                foreach ($groups['id'] as $key => $id) {
                    $json[] = "{ 'id' : '$id' ,'name' : '".bab_toHtml($groups['name'][$key], BAB_HTML_JS)."', 'hasChildNodes' : false, 'setOfGroups' : true }";
                    $sublevel[] = $id;
                }
            }
        }
    }


    $return = array();
    $return[$id_parent] = '['.implode(',', $json).']';

    $levels--;

    if ($levels > 0) {
        foreach ($sublevel as $id) {
            if ($nextlevel = widget_getGroupLevel($id, $delegationGroups, $levels)) {
                $return = array_merge($return, $nextlevel);
            }
        }
    }

    return $return;
}



/**
 * Echo a json array of groups.
 *
 * @return void
 */
function widgets_getJsonGroups()
{
    if (!isset($_SESSION['Widget_Acl_uid'][bab_rp('uid')])) {
        die('access denied');
    }


    $id_parent = bab_rp('id_parent', null);

    if (null === $id_parent) {
        die('missing id_parent');
    } elseif ('' !== $id_parent && !is_numeric($id_parent)) {
        $id_parent = (int) $id_parent;
    }

    $id_delegation = (int) bab_rp('id_delegation');

    $levels = (int) bab_rp('levels');

    // get groups from API
    $delegationGroups = new bab_AclGroups($id_delegation);

    $arr = widget_getGroupLevel($id_parent, $delegationGroups, $levels);

    $main_json = array();
    foreach ($arr as $parent => $json) {
        if ('' === $parent) {
            $parent = 'null';
        }

        $main_json[] = "{ 'id_parent' : $parent, 'items' : $json }";
    }

    echo '['.implode(',', $main_json).']';
}




/**
 *
 */
function widgets_getJsonChecked()
{
    if (!isset($_SESSION['Widget_Acl_uid'][bab_rp('uid')])) {
        die('access denied');
    }

    $groups = bab_rp('groups', null);

    $arr = explode(',', $groups);
    $arr = array_unique($arr);
    $json = array();

    foreach ($arr as $g) {
        $id_group = rtrim($g, '+');

        if (empty($id_group)) {
            continue;
        }

        // for each checked node, get ancestors

        $ancestors = bab_Groups::getAncestors($id_group);
        unset($ancestors[BAB_ALLUSERS_GROUP]);

        $json[] = '['.implode(',', array_keys($ancestors)).']';
    }

    echo '['.implode(',', $json).']';
}





function widgets_getJsonGroupInfos()
{
    require_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
    require_once $GLOBALS['babInstallPath'].'utilit/groupsincl.php';
    $session = bab_getInstance('bab_Session');

    $groups = bab_rp('groups', null);

    if (null === $groups) {
        die('missing groups');
    }

    $json2 = array();
    foreach ($groups as $uid => $glist) {
        if (!isset($session->Widget_Acl_uid[$uid])) {
            die('access denied');
        }

        $arr = explode(',', $glist);
        $json = array();

        foreach ($arr as $g) {
            if ('' === $g) {
                continue;
            }

            if (mb_strlen($g) > 1 && '+' === mb_substr($g, -1)) {
                $id_group = (int) mb_substr($g, 0, -1);
                $withChildNodes = 'true';
            } else {
                $id_group = (int) $g;
                $withChildNodes = 'false';
            }

            try {
                if (bab_Groups::isGroupSet($id_group)) {
                    $setOfGroups = 'true';
                } else {
                    $setOfGroups = 'false';
                }
            } catch (Exception $e) {
                continue;
            }

            $json[] = "{ 'id' : '$id_group' ,'name' : '".bab_toHtml(bab_getGroupName($id_group, true), BAB_HTML_JS)."', 'withChildNodes' : $withChildNodes, 'setOfGroups' : $setOfGroups }";
        }

        $json2[] = " '".bab_toHtml($uid, BAB_HTML_JS)."' : [".implode(',', $json).']';
    }

    echo '{'.implode(',', $json2).'}';
}



/**
 *
 * @param int $id_group
 * @return string
 */
function widgets_getGroupLevel($id_group)
{

    $arr = bab_getGroups($id_group, false);

    if (empty($arr)) {
        return '';
    }
    $str = '<ul>';
    foreach ($arr['id'] as $pos => $id_childgroup) {
        $name = $arr['name'][$pos];
        $str .= sprintf('<li><a href="#" title="%s">%s</a>', $id_childgroup, bab_toHtml($name));
        $str .= widgets_getGroupLevel($id_childgroup);
        $str .= "</li>\n";
    }
    if ($id_group == 0) {
        $str .= '<li class="none"><a href="#" title="">'.widget_translate('None')."</a></li>\n";
    }
    $str .= '</ul>'."\n";

    return $str;
}



switch (bab_rp('idx'))
{
    case 'get':
        widgets_getJsonGroups();
        break;

    case 'checked':
        widgets_getJsonChecked();
        break;

    case 'infos':
        widgets_getJsonGroupInfos();
        break;

    case 'grouppicker':
        echo widgets_getGroupLevel(bab_gp('start', 0));
        break;
}

die;
