<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 * Echo a json array of generated thumbnail images.
 *
 * @return void
 */
function widgets_createThumbnails()
{
	
	$T = bab_functionality::get('Thumbnailer');
	/*@var $T Func_Thumbnailer */
	$ids = bab_rp('ids');

	if (empty($ids)) {
		return false;
	}
	
	include_once $GLOBALS['babInstallPath'].'utilit/session.class.php'; // since 7.8.0
	if (class_exists('bab_Session')) {

		$session = bab_getInstance('bab_Session');
		$widget_session = $session->addon_widgets;
		
	} else {
		
		$widget_session = $_SESSION['addon_widgets'];
	}

	$ids = explode(',', $ids);
	$json = array();

	foreach ($ids as $id) {

		if (!isset($widget_session['Widget_FileIcon'][$id])) {
			continue;
		}

		$infos = $widget_session['Widget_FileIcon'][$id];

		$T->setSourceFile($infos['path']);

		if (isset($infos['effects'])) {
    		$effects = $infos['effects'];
    		foreach ($effects as $method => $args) {
    			call_user_func_array(array($T, $method), $args);
    		}
		}

		$thumb = $T->getThumbnail($infos['width'], $infos['height']);

		if (null !== $thumb && false !== $thumb) {
			$json[] = "{ 'id' : '$id' ,'url' : '".bab_toHtml($thumb, BAB_HTML_JS)."' }";
		}
	}

	echo '['.implode(',', $json).']';
}



widgets_createThumbnails();
die;
