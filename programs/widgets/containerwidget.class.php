<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

//require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once dirname(__FILE__) . '/widget.class.php';




/**
 * A Widget_ContainerWidget is a widget that can be the parent of other items.
 * It is associated with a Widget_Layout to manage the way children items are
 * displayed.
 */
abstract class Widget_ContainerWidget extends Widget_Widget /* implements Widget_Displayable_Interface */
{
    /**
     * The layout associated to the container.
     *
     * @var Widget_Layout
     */
	private $layout = null;



	/**
	 * @param string $id			The item unique id.
	 * @param Widget_Layout $layout	The layout that will manage how widgets are displayed in this container.
	 * @return Widget_ContainerWidget
	 */
	public function __construct($id = null, $layout = null)
	{
		parent::__construct($id);
		if (isset($layout)) {
            $this->setLayout($layout);
		}
	}


	/**
	 * Sets the layout manager for this container to $layout.
	 *
	 * @param Widget_Layout|null $layout
	 * @return $this
	 */
	public function setLayout($layout)
	{
		$this->layout = $layout;
		if (null !== $layout) {
			$layout->setParent($this);
		}

		return $this;
	}

	/**
	 * Returns the layout manager for this container.
	 *
	 * @return Widget_Layout
	 */
	public function getLayout()
	{
		return $this->layout;
	}


	/**
	 * Adds the item the container's layout.
	 *
	 * @param Widget_Displayable_Interface	$item
	 * Depending on the associated layout, there may be additional parameters.
	 *
	 * @return $this
	 */
	public function addItem(Widget_Displayable_Interface $item = null)
	{
		assert('$this->layout instanceof Widget_Layout; /* There must be a layout associated to this container. */');
		// When we add an item in a container, we actually add the item to its associated layout.
		// As parameters depend on the layout class we call the method addItem with all the parameters.
		$args = func_get_args();
		call_user_func_array(array($this->layout, 'addItem'), $args);
		return $this;
	}

	/**
	 * Adds the item the container's layout.
	 *
	 * @param Widget_Item	$item
	 * Depending on the associated layout, there may be additional parameters.
	 *
	 * @since 1.0.88
	 * @return $this
	 *
	 * commented out because of some addItems methods declared in various addons inherited from Widget_Form
	 */
// 	public function addItems(Widget_Displayable_Interface $item = null)
// 	{
// 	    assert('$this->layout instanceof Widget_Layout; /* There must be a layout associated to this container. */');
// 	    // When we add an item in a container, we actually add the item to its associated layout.
// 	    // As parameters depend on the layout class we call the method addItem with all the parameters.
// 	    $args = func_get_args();
// 	    call_user_func_array(array($this->layout, 'addItems'), $args);
// 	    return $this;
// 	}



	/**
	 * For debugging purpose.
	 *
	 * @param string $prefix
	 * @return string
	 * @ignore
	 */
	public function dump($prefix = '')
	{
	    $dumpString = '<div>' . parent::dump() . '</div>';
	    $layout = $this->getLayout();
	    if (isset($layout)) {
	        $dumpString .= $layout->dump();
	    }
	    return $dumpString;
	}
}
