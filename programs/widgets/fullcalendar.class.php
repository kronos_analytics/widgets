<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
require_once dirname(__FILE__) . '/widget.class.php';
require_once dirname(__FILE__) . '/color.class.php';
require_once dirname(__FILE__) . '/label.class.php';
require_once dirname(__FILE__) . '/frame.class.php';
require_once dirname(__FILE__) . '/hboxlayout.class.php';
require_once dirname(__FILE__) . '/vboxlayout.class.php';




/**
 * Constructs a Widget_FullCalendar.
 *
 * @param string $id			The item unique id.
 * @return Widget_FullCalendar
 */
function Widget_FullCalendar($id = null)
{
    return new Widget_FullCalendar($id);
}



/**
 *
 */
class Widget_FullCalendar extends Widget_Widget implements Widget_Displayable_Interface
{
    const VIEW_MONTH = 'month';
    const VIEW_WEEK = 'agendaWeek';
    const VIEW_WORKWEEK = 'agendaWorkWeek';
    const VIEW_DAY = 'agendaDay';
    const VIEW_RESOURCE_MONTH = 'resourceMonth';
    const VIEW_RESOURCE_WEEK = 'resourceWeek';
    const VIEW_RESOURCE_WORKWEEK = 'resourceWorkWeek';
    const VIEW_RESOURCE_NEXTWEEKS = 'resourceNextWeeks';
    const VIEW_RESOURCE_DAY = 'resourceDay';

    const BUTTON_TODAY = 'today';
    const BUTTON_NEXT = 'next';
    const BUTTON_PREVIOUS = 'prev';

    const TITLE = 'title';


    /**
     * The day that should be displayed by the calendar.
     * @var BAB_DateTime
     */
    private $date;

    /**
     * The first day of the week (0-6) displayed in a week view.
     * @var int
     */
    private $firstDayOfWeek = 0;


    /**
     * Togle the weeknumber column in month view
     * @var boolean
     */
    private $showWeekNumber = false;


    /**
     * The time format.
     *
     * @see Widget_FullCalendar::setTimeFormat()
     * @var string
     */
    private $timeFormat = null;


    private $monthColumnFormat = null;


    private $gcalFeeds = array();


    /**
     * An optional associated date picker used for
     * navigation in the fullcalendar.
     * The datepicker widget or its id.
     *
     * @var Widget_DatePicker|string
     */
    private $associatedDatePicker = null;

    /**
     * @var string
     */
    private $view = self::VIEW_WEEK;

    /**
     * @var boolean
     */
    private $autoHeight = true;
    /**
     * @var array
     */
    private $resources = array();

    /**
     * @var Widget_Action
     */
    private $fetchPeriodsAction = null;

    /**
     * @var Widget_Action
     */
    private $onDrop = null;

    /**
     * @var Widget_Action
     */
    private $onDoubleClick = null;

    /**
     * @var Widget_Action
     */
    private $onPeriodDoubleClick = null;

    /**
     * @var Widget_Action
     */
    private $onPeriodMoved = null;

    /**
     * @var Widget_Action
     */
    private $onViewDisplayed = null;

    /**
     * @var Widget_Widget
     */
    private $titleWidget = null;

    /**
     * @var Widget_Widget
     */
    private $nextWidget = null;

    /**
     * @var Widget_Widget
     */
    private $previousWidget = null;

    /**
     * @var Widget_Widget
     */
    private $todayWidget = null;

    /**
     * @var Widget_Widget
     */
    private $monthViewWidget = null;

    /**
     * @var Widget_Widget
     */
    private $weekViewWidget = null;

    /**
     * @var Widget_Widget
     */
    private $dayViewWidget = null;

    /** @var BAB_DateTime $mindate  */
    private $mindate;

    /** @var BAB_DateTime $maxdate  */
    private $maxdate;

    /**
     * @param string $id
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
        $this->setTimeFormat('%R{-%R}');
        $this->setStartTime('00:00:00');
        $this->setEndTime('24:00:00');
        $this->setAutoHeight(true);
        $this->disableMouseover(false);
    }

    /**
     * Sets the type of view that should be displayed by the calendar.
     *
     * @param string $view		One of Widget_FullCalendar::VIEW_*
     * @return $this
     */
    public function setView($view)
    {
         $this->view = $view;
         $this->setMetadata('view', $view);
         return $this;
    }



    /**
     * Toggle the week number column in month view
     *
     * @param bool $show
     * @return $this
     */
    public function displayWeekNumber($show = true)
    {
        $this->setMetadata('displayWeekNumber', $show);
        return $this;
    }


    public function setMonthColumnFormat($format)
    {
        assert('is_string($format); /* The "format" parameter must be a string */');
        $this->monthColumnFormat = $format;
        $this->setMetadata('monthColumnFormat', $format);
        return $this;
    }



    /**
     * Returns the view that should be displayed by the calendar.
     *
     * @return string
     */
    public function getView()
    {
        if (!isset($this->view)) {
            $this->setView(self::VIEW_WEEK);
        }
        return $this->view;
    }

    /**
     * The cells are automatically resized or not to fit the page height
     *
     * @param boolean $autoHeight
     * @return $this
     */
    public function setAutoHeight($autoHeight)
    {
        $this->autoHeight = $autoHeight;
        $this->setMetadata('autoHeight', $autoHeight);
        return $this;
    }



    /**
     * Returns the value of the autoHeight parameter
     *
     * @return boolean
     */
    public function getAutoHeight()
    {
        if (!isset($this->autoHeight)) {
            $this->setAutoHeight(true);
        }
        return $this->autoHeight;
    }


    /**
     * Hide or show weekends.
     *
     * @param bool  $hidden
     * @return $this
     */
    public function hideWeekends($hidden = true)
    {
        $this->setMetadata('hideWeekends', $hidden);
        return $this;
    }



    /**
     * Sets the week days to hide in the calendar.
     *
     * @param array $hiddenDays     0 : Sunday, 1 : Monday... Eg: array(0, 6) for hiding weekends.
     * @return $this
     */
    public function setHiddenDays(array $hiddenDays)
    {
        $this->setMetadata('hiddenDays', $hiddenDays);
        return $this;
    }


    /**
     * Determines if timed events in agenda view should visually overlap.
     *
     * @param bool $overlap
     * @return $this
     */
    public function setSlotEventOverlap($overlap)
    {
        $this->setMetadata('slotEventOverlap', $overlap);
        return $this;
    }


    /**
     * Sets the slot duration in hh:mm::ss.
     *
     * Seconds are optional. The default is '00:30:00'.
     *
     * @param string $duration
     * @return $this
     */
    public function setSlotDuration($duration)
    {
        $this->setMetadata('slotDuration', $duration);
        return $this;
    }


    /**
     * Set the min time to display (on relevant display mod)
     *
     * @param string  $time with or without second
     * @return $this
     */
    public function setStartTime($time)
    {
        if (strlen($time) == 5) {
            $time.= ':00';
        }
        $this->setMetadata('minTime', $time);
        return $this;
    }


    /**
     * Set the max time to display (on relevant display mod)
     *
     * @param string  $time with or without second
     * @return $this
     */
    public function setEndTime($time)
    {
        if (strlen($time) == 5) {
            $time.= ':00';
        }
        $this->setMetadata('maxTime', $time);
        return $this;
    }


    /**
     * Sets the format of time.
     *
     * Only usage of the following is allowed:
     *  %H  Two digit representation of the hour in 24-hour format  00 through 23
     *  %I 	Two digit representation of the hour in 12-hour format 	01 through 12
     *  %l (lower-case 'L') 	Hour in 12-hour format, with a space preceeding single digits 	1 through 12
     *  %M 	Two digit representation of the minute 	00 through 59
     *  %p 	UPPER-CASE 'AM' or 'PM' based on the given time 	Example: AM for 00:31, PM for 22:23
     *  %P 	lower-case 'am' or 'pm' based on the given time 	Example: am for 00:31, pm for 22:23
     *  %r 	Same as "%I:%M:%S %p" 	Example: 09:34:17 PM for 21:34:17
     *  %R 	Same as "%H:%M" 	Example: 00:35 for 12:35 AM, 16:44 for 4:44 PM
     *  %S 	Two digit representation of the second 	00 through 59
     *  %T 	Same as "%H:%M:%S" 	Example: 21:34:17 for 09:34:17 PM
     *  %X 	Preferred time representation based on locale, without the date 	Example: 03:59:16 or 15:59:16
     *
     * @param string $format	The format string as described in the php strftime function.
     * @return $this
     */
    public function setTimeFormat($format)
    {
        assert('is_string($format); /* The "format" parameter must be a string */');
        $this->timeFormat = $format;
        $s = array('%H', '%I', '%l', '%M', '%p', '%P', '%r', '%R', '%S', '%T', '%X');
        $t = array('HH', 'hh', 'h', 'mm', 'TT', 'tt', 'hh:mm:ss TT', 'HH:mm', 'ss', 'HH:mm:ss', 'HH:mm:ss');
        $fcTimeFormat = str_replace($s, $t, $format);
        $this->setMetadata('timeFormat', $fcTimeFormat);
        return $this;
    }




    /**
     * Returns the format of the date.
     *
     * @return string
     */
    public function getTimeFormat()
    {
        return $this->timeFormat;
    }




    /**
     * Sets an associated datePicker.
     *
     * @param Widget_DatePicker|string $datePicker
     * @return $this
     */
    public function setAssociatedDatePicker($datePicker)
    {
        $this->associatedDatePicker = $datePicker;
        return $this;
    }




    /**
     * Sets the day that should be displayed by the calendar.
     *
     * @param BAB_DateTime $startDate
     * @return $this
     */
    public function setDate(BAB_DateTime $startDate)
    {
        $this->date = $startDate;
        $this->setMetadata('year', $startDate->getYear());
        $this->setMetadata('month', $startDate->getMonth());
        $this->setMetadata('date', $startDate->getDayOfMonth());
        return $this;
    }



    /**
     * Returns the day that should be displayed by the calendar.
     *
     * @return BAB_DateTime
     */
    public function getDate()
    {
        if (!isset($this->date)) {
            $this->setDate(BAB_DateTime::now());
        }
        return $this->date;
    }



    /**
     * Sets the first day of the week (0-6) displayed in a week view.
     *
     * @param int|null $firstDayOfWeek	a number in [0-6] or null to unset.
     * @return $this
     */
    public function setFirstDayOfWeek($firstDayOfWeek)
    {
        assert('is_null($firstDayOfWeek) || (is_numeric($firstDayOfWeek) && (int)$firstDayOfWeek >= 0 && (int)$firstDayOfWeek < 7); /* The "firstDayOfWeek" parameter must be a numeric value between 0 and 6 or null. */');
        $this->firstDayOfWeek = $firstDayOfWeek;
        $this->setMetadata('firstDayOfWeek', $firstDayOfWeek);
        return $this;
    }



    /**
     * Returns the first day of the week (0-6) displayed in the week view.
     *
     * @return int | null
     */
    public function getFirstDayOfWeek()
    {
        return $this->firstDayOfWeek;
    }



    /**
     * Defines the server-side action executed when fullcalendar fetchs events.
     *
     * The specified action should accept two timestamp parameters 'start' and 'end'
     * and return a json encoded array of events in the [start,end] range.
     *
     * @param Widget_Action $action
     *
     * @return $this
     */
    public function fetchPeriods(Widget_Action $action)
    {
        $this->fetchPeriodsAction = $action;
        $this->setMetadata('fetchPeriods', $action->url());
        return $this;
    }



    /**
     * Defines the server-side action executed when a dragable item is drop in the calendar.
     *
     * @param Widget_Action $action
     * @param bool          $ajax
     * @param bool          $remmoveAfterDrop
     * @param mixed         $reloadItem
     *
     * @return $this
     */
    public function setDrop(Widget_Action $action, $ajax = false, $remmoveAfterDrop = false, $reloadItem = null)
    {
        $this->onDrop = $action;
        $this->setMetadata('onDrop', $action->url());
        $this->setMetadata('onDropAjax', $ajax);
        $this->setMetadata('removeAfterDrop', $remmoveAfterDrop);


        if ('' !== $reloadItem && false !== $reloadItem) {
            if (!is_array($reloadItem)) {
                $reloadItem = array($reloadItem);
            }

            foreach ($reloadItem as $k => $v) {
                if ($v instanceof Widget_Item) {
                    $reloadItem[$k] = $v->getId();
                }
            }
            $this->setMetadata('onDropAjaxReload', $reloadItem);
        }
        return $this;
    }


    /**
     *
     * @param array $values
     * @param string $name
     * @param Widget_Layout $layout
     * @return Widget_VBoxLayout
     */
    public function getDropLayout(array $values, $name, Widget_Layout $layout = null)
    {
        /* @var $W Func_Widgets */
        $W = bab_Widgets();
        if ($layout === null) {
            $layout = $W->VboxLayout()->setVerticalSpacing(.5, 'em');
        }
        $layout->addClass('widget-fullcalenar-draglayout');

        foreach ($values as $id => $value) {
            if ($value instanceof Widget_Item) {
                $layout->addItem(
                    $W->VBoxItems(
                        $W->Hidden(null, $name, $id),
                        $value
                    )->addClass('widget-drop-item')
                );
            } else {
                $layout->addItem(
                    $W->VBoxItems(
                        $W->Hidden(null, $name, $id),
                        $W->Label($value)
                    )->addClass('widget-drop-item widget-labeled-drop-item')
                );
            }
        }

        return $layout;
    }



    /**
     * Defines the server-side action executed when a slot in the calendar is double-clicked.
     *
     * @param Widget_Action $action
     * @return $this
     */
    public function onDoubleClick(Widget_Action $action, $ajax = false, $reloadItem = null)
    {
        $this->onDoubleClick = $action;
        $this->setMetadata('doubleClick', $action->url());
        $this->setMetadata('doubleClickAjax', $ajax);
        if ('' !== $reloadItem && false !== $reloadItem) {
            if (!is_array($reloadItem)) {
                $reloadItem = array($reloadItem);
            }

            foreach ($reloadItem as $k => $v) {
                if ($v instanceof Widget_Item) {
                    $reloadItem[$k] = $v->getId();
                }
            }
            $this->setMetadata('doubleClickAjaxReload', $reloadItem);
        }
        return $this;
    }


    /**
     * Defines the server-side action executed when a period in the calendar is double-clicked.
     *
     * @param Widget_Action $action
     * @return $this
     */
    public function onPeriodDoubleClick(Widget_Action $action, $ajax = false, $reloadItem = null)
    {
        $this->onPeriodDoubleClick = $action;
        $this->setMetadata('periodDoubleClick', $action->url());
        $this->setMetadata('periodDoubleClickAjax', $ajax);
        if ('' !== $reloadItem && false !== $reloadItem) {
            if (!is_array($reloadItem)) {
                $reloadItem = array($reloadItem);
            }

            foreach ($reloadItem as $k => $v) {
                if ($v instanceof Widget_Item) {
                    $reloadItem[$k] = $v->getId();
                }
            }
            $this->setMetadata('periodDoubleClickAjaxReload', $reloadItem);
        }
        return $this;
    }


    /**
     * Defines the server-side action executed when a period is moved or
     * resized by the user.
     * The server side script can return a json encoded object:
     * - If the key 'error' is present the string it contains is displayed and
     *   the period is reverted to its original place.
     * - If the key 'warning' is present the string it contains is displayed
     *   and the period is moved anyway.
     *
     * The server-side action can receive the following parameters:
     *  - $event: the event identifier.
     *  - $offsetDays: the offset in days (can be negative).
     *  - $offsetMinutes: the additional offset in minutes (can be negative).
     *  - $offsetDuration: the change of duration in minutes (can be negative).
     *
     * @param Widget_Action $action
     * @return $this
     */
    public function onPeriodMoved(Widget_Action $action, $durationEditable = true)
    {
        $this->onPeriodMoved = $action;
        $this->setMetadata('periodMoved', $action->url());
        $this->setMetadata('durationEditable', $durationEditable);
        return $this;
    }


    /**
     * Defines the server-side action executed when the calendar view is displayed.
     * The action can take one parameter "date" that will contain the iso-formatted
     * date displayed by the calendar.
     *
     * @param Widget_Action $action
     * @return $this
     */
    public function onViewDisplayed(Widget_Action $action)
    {
        $this->onViewDisplayed = $action;
        $this->setMetadata('viewDisplayed', $action->url());
        return $this;
    }


    /**
     * Defines a widget or a list of widgets that will make the calendar display
     * the next period (day, week or month) when clicked.
     *
     * @param Widget_Widget $widget
     * @return $this
     */
    public function setTitleWidget($widget)
    {
        $this->titleWidget = $widget;
        $this->setMetadata('titleWidget', '#' . $widget->getId());
        return $this;
    }


    /**
     * Defines a widget or a list of widgets that will make the calendar display
     * the next period (day, week or month) when clicked.
     *
     * @param Widget_Widget $widget
     * @return $this
     */
    public function setNextButton($widget)
    {
        $this->nextWidget = $widget;
        $this->setMetadata('nextWidget', '#' . $widget->getId());
        return $this;
    }


    /**
     * Defines a widget or a list of widgets that will make the calendar display
     * the previous period (day, week or month) when clicked.
     *
     * @param Widget_Widget $widget
     * @return $this
     */
    public function setPreviousButton($widget)
    {
        $this->previousWidget = $widget;
        $this->setMetadata('previousWidget', '#' . $widget->getId());
        return $this;
    }


    /**
     * Defines a widget that will make the calendar move to
     * today when clicked.
     *
     * @param Widget_Widget $widget
     * @return $this
     */
    public function setTodayButton(Widget_Widget $widget)
    {
        $this->todayWidget = $widget;
        $this->setMetadata('todayWidget', '#' . $widget->getId());
        return $this;
    }

    /**
     * Defines a widget that will make the calendar display the
     * month view when clicked.
     *
     * @param Widget_Widget $widget
     * @return $this
     */
    public function setMonthViewButton(Widget_Widget $widget)
    {
        $this->monthViewWidget = $widget;
        $this->setMetadata('monthViewWidget', '#' . $widget->getId());
        return $this;
    }


    /**
     * Defines a widget that will make the calendar display the
     * week view when clicked.
     *
     * @param Widget_Widget $widget
     * @return $this
     */
    public function setWeekViewButton(Widget_Widget $widget)
    {
        $this->weekViewWidget = $widget;
        $this->setMetadata('weekViewWidget', '#' . $widget->getId());
        return $this;
    }

    /**
     * Defines a widget that will make the calendar display the
     * day view when clicked.
     *
     * @param Widget_Widget $widget
     * @return $this
     */
    public function setDayViewButton(Widget_Widget $widget)
    {
        $this->dayViewWidget = $widget;
        $this->setMetadata('dayViewWidget', '#' . $widget->getId());
        return $this;
    }



    /**
     * Sets the calendar header left content.
     * @param string $headerLeft
     *     A space or comma-separated combination of self::BUTTON_* or self::TITLE string.
     * @return $this
     */
    public function setHeaderLeft($headerLeft)
    {
        $this->setMetadata('headerLeft', $headerLeft);
        return $this;
    }



    /**
     * Sets the calendar header right content.
     * @param string $headerRight
     *     A space or comma-separated combination of self::BUTTON_* or self::TITLE string.
     * @return $this
     */
    public function setHeaderRight($headerRight)
    {
        $this->setMetadata('headerRight', $headerRight);
        return $this;
    }



    /**
     * Sets the calendar header center content.
     * @param string $headerCenter
     *     A space or comma-separated combination of self::BUTTON_* or self::TITLE string.
     * @return $this
     */
    public function setHeaderCenter($headerCenter)
    {
        $this->setMetadata('headerCenter', $headerCenter);
        return $this;
    }




    /**
     * Defines an google calendar url to be fetched.
     *
     * @param string $url
     * @return $this
     */
    public function addGcalFeed($url)
    {
        $this->gcalFeeds[] = $url;
        return $this;
    }


    /**
     * Specifies a list of resources to be displayed in a VIEW_RESOURCE_* view.
     *
     * @param array $resources
     *             array(
     *                 array(
     *                     'id' => 'resource1',
     *                     'name' => 'Some <strong>html</strong> string'
     *                 ),
     *                 array(
     *                     'id' => 'resource2',
     *                     'name' => 'Some other string'
     *                 ),
     *                 ...
     *             )
     * @return $this
     */
    public function setResources($resources)
    {
        $this->resources = $resources;
        return $this;
    }

    /**
     * ONLY WORK IN CERTAIN VIEW
     * Specify a date minimum which the date can't be before.
     * If $date is another datepicker the value of this widget is dynamically used.
     *
     * @param BAB_DateTime $date
     * @return $this
     */
    public function setMinDate(BAB_DateTime $date)
    {
        $this->mindate = $date;
        return $this;
    }

    /**
     * Return the minimum date "BAB_DateTime" object
     *
     * @return BAB_DateTime
     */
    public function getMinDate()
    {
        return $this->mindate;
    }


    /**
     * ONLY WORK IN CERTAIN VIEW
     * Specify a date maximum which the date can't be before
     * If $date is another datepicker the value of this widget is dynamically used.
     *
     * @param BAB_DateTime
     * @return $this
     */
    public function setMaxDate(BAB_DateTime $date)
    {
        $this->maxdate = $date;
        return $this;
    }

    /**
     * Return the maximum date "BAB_DateTime" object
     *
     * @return BAB_DateTime
     */
    public function getMaxDate()
    {
        return $this->maxdate;
    }



    /**
     * Sets the calendar number of weeks.
     * @param int $numberOfWeeks
     *     The number of weeks
     * @return $this
     */
    public function setNumberOfWeeks($numberOfWeeks)
    {
        $this->setMetadata('numberOfWeeks', $numberOfWeeks);
        return $this;
    }

    /**
     * Sets the calendar number of weeks displayed in a month view.
     * @param string $weekMode
     * 'fixed' (default) : The calendar will always be 6 weeks tall. The height will always be the same, as determined by height, contentHeight, or aspectRatio.
     * 'liquid' : The calendar will have either 4, 5, or 6 weeks, depending on the month. The height of the weeks will stretch to fill the available height, as determined by height, contentHeight, or aspectRatio.
     * 'variable' : The calendar will have either 4, 5, or 6 weeks, depending on the month. Each week will have the same constant height, meaning the calendar’s height will change month-to-month.
     * @return $this
     */
    public function setWeekMode($weekMode = 'fixed')
    {
        $this->setMetadata('weekMode', $weekMode);
        return $this;
    }


    /**
     * @see Widget_Widget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-fullcalendar';
        return $classes;
    }


    /**
     * Use to disable event mouseover tooltip display
     *
     * @param bool $disable
     * @return Widget_FullCalendar
     */
    public function disableMouseover($disable = true)
    {
        $this->setMetadata('disableMouseover', $disable);
        return $this;
    }



    public function setAfterAllRenderReload($reloadItem)
    {
        if (!is_array($reloadItem)) {
            $reloadItem = array($reloadItem);
        }

        foreach ($reloadItem as $k => $v) {
            if ($v instanceof Widget_Item) {
                $reloadItem[$k] = $v->getId();
            }
        }
        $this->setMetadata('ajaxAterAllRenderReload', $reloadItem);
    }


    /**
     * Initializes metadata.
     */
    protected function initMetadata()
    {
        if (isset($this->associatedDatePicker)) {
            if ($this->associatedDatePicker instanceof Widget_Item) {
                $this->setMetadata('associatedDatePicker', $this->associatedDatePicker->getId());
            } else {
                $this->setMetadata('associatedDatePicker', $this->associatedDatePicker);
            }
        }
        if (!empty($this->gcalFeeds)) {
            $this->setMetadata('gcalFeeds', $this->gcalFeeds);
        }
        if (!empty($this->resources)) {
            $this->setMetadata('resources', $this->resources);
        }

        if ($this->getMinDate() != null) {
            $this->setMetadata('mindate', $this->getMinDate()->getIsoDate());
        }

        if ($this->getMaxDate() != null) {
            $this->setMetadata('maxdate', $this->getMaxDate()->getIsoDate());
        }


        $dayNames = bab_DateStrings::getDays();
        $dayNamesShort = array();
        foreach ($dayNames as $dayName) {
            $dayNamesShort[] = mb_substr($dayName, 0 , 3);
        }
        if(!$this->getMetadata('dayNames')) {
            $this->setMetadata('dayNames', $dayNames);
        }
        if(!$this->getMetadata('dayNamesShort')) {
            $this->setMetadata('dayNamesShort', $dayNamesShort);
        }

        if(!$this->getMetadata('monthNames')) {
            $monthNames = bab_DateStrings::getMonths();
            $this->setMetadata('monthNames', $monthNames);
        }
        if(!$this->getMetadata('monthNamesShort')) {
            $monthNamesShort = bab_DateStrings::getShortMonths();
            $this->setMetadata('monthNamesShort', $monthNamesShort);
        }

        $this->setMetadata(
            'buttonText',
            array(
                'today' => widget_translate('Today'),
                'month' => widget_translate('Month'),
                'week' => widget_translate('Week'),
                'day' => widget_translate('Day'),
                'resourceWeek' => widget_translate('Week'),
                'resourceDay' => widget_translate('Day'),
                'resourceMonth' => widget_translate('Month')
            )
        );
    }


    /**
     * @see programs/widgets/Widget_Displayable_Interface#display($canvas)
     */
    public function display(Widget_Canvas $canvas)
    {
        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        $this->initMetadata();

        $addon = bab_getAddonInfosInstance('widgets');

        $calendarDiv = $canvas->div(
            $this->getId(),
            $this->getClasses(),
            array(),
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        ) . $canvas->metadata($this->getId(), $this->getMetadata())
        . $canvas->loadScript($this->getId(), $widgetsAddon->getTemplatePath() . 'widgets.fullcalendar.jquery.js' . '?' . $addon->getDbVersion())
        . $canvas->loadStyleSheet($widgetsAddon->getStylePath() . 'widgets.fullcalendar.css' . '?' . $addon->getDbVersion());

        return $calendarDiv;
    }
}
