<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

//require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once dirname(__FILE__) . '/containerwidget.class.php';



/**
 * Constructs a Widget_Tabs.
 *
 * @param string        $id         The item unique id.
 * @param Widget_Layout $layout     The layout that will manage how widgets are displayed in this container.
 * @return Widget_Tabs
 */
function Widget_Tabs($id = null, Widget_Layout $layout = null)
{
    return new Widget_Tabs($id, $layout);
}



/**
 * Widget_Tabs
 *
 */
class Widget_Tabs extends Widget_ContainerWidget implements Widget_Displayable_Interface
{

    protected $selectedItem = null;
    protected $displayAllTabs = true;


    /**
     * @param string        $id         The item unique id.
     * @param Widget_Layout $layout     The layout that will manage how widgets are displayed in this container.
     * @return Widget_Frame
     */
    public function __construct($id = null, Widget_Layout $layout = null)
    {
        if (null === $layout) {
            require_once FUNC_WIDGETS_PHP_PATH . 'vboxlayout.class.php';
            $layout = new Widget_VBoxLayout();
        }

        parent::__construct($id, $layout);
    }


    /**
     * Adds a tab to the tabs widget.
     *
     * @param string        $label
     * @param Widget_Item   $item
     * @param int           $position
     * @param string        $tabId      The tab unique id
     * @return Widget_Tabs
     */
    public function addTab($label, Widget_Displayable_Interface $item, $position = null, $tabId = null)
    {
        $W = new Func_Widgets;
        $tab = $W->Tab($tabId, $label);

        $this->addItem($tab, $position);
        $tab->addItem($item);

        return $this;
    }


    /**
     * Sets the currently selected tab widget.
     *
     * @param string|Widget_tab     $tab    The tab widget or its id.
     * @return Widget_Tabs
     */
    public function setSelectedTab($tab)
    {
        if ($tab instanceof Widget_Tab) {
            $this->selectedItem = $tab;
        } else {
            $this->selectedItem = Widget_Item::getById($tab);
        }

        return $this;
    }


    /**
     * Returns the currently selected tab widget.
     *
     * @return Widget_Tab|null
     */
    public function getSelectedTab()
    {
        return $this->selectedItem;
    }


    /**
     * Display all tabs or not
     * @param   boolean     $opt
     * @return  Widget_Tabs|boolean
     */
    public function displayAllTabs($opt = null)
    {
        if (null === $opt) {
            return $this->displayAllTabs;
        }

        if (false === $opt) {
            $this->addClass('widget-static-tabs');
        }

        $this->displayAllTabs = $opt;
        return $this;
    }




    /**
     * Adds a tab at the specified position.
     *
     * @param   Widget_Displayable_Interface    $item
     * @param   int                             $position
     */
    public function addItem(Widget_Displayable_Interface $item = null, $position = null)
    {
        return parent::addItem($item, $position);
    }


    /**
     * (non-PHPdoc)
     * @see Widget_Widget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-tabs';

        if ($this->displayAllTabs) {
            $classes[] = 'ui-tabs-nav';
        }
        $cookieName = 'widgets_Tabs_' . $this->getId();
        if ($this->isPersistent() && isset($_COOKIE[$cookieName])) {
            $items = $this->getLayout()->getItems();
            $i = 0;
            foreach ($items as $item) {
                if ($i == $_COOKIE[$cookieName]) {
                    $this->setSelectedTab($item);
                    break;
                }
                $i++;
            }
        }

        return $classes;
    }


    /**
     * (non-PHPdoc)
     * @see Widget_Displayable_Interface::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        $tabs = $canvas->tabs(
            $this->getId(),
            $this->getClasses(),
            $this->getLayout()->getItems(),
            $this->getCanvasOptions()
        )
        . $canvas->metadata($this->getId(), $this->getMetadata())
        . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.jquery.js');

        return $tabs;
    }
}
