<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';





/**
 * Constructs a Widget_MultiField.
 *
 * @param string		$id			The item unique id.
 * @return Widget_MultiField
 */
function Widget_MultiField($id = null)
{
	return new Widget_MultiField($id);
}



/**
 * A MultiField widget is a frame with vbox layout
 * 	<ul>
 *		<li>the widget must have a name</li>
 * 		<li>the <code>Widget_InputWidget</code> under this widget must have a numeric name</li>
		<li>the widget must contain one or more <code>Widget_InputWidget</code></li>
 * 	</ul>
 *
 * Others input widgets may be added on the client side if the canvas allow fields duplication with the same name pattern
 *
 */
class Widget_MultiField extends Widget_Frame implements Widget_Displayable_Interface {


	/**
	 * @param string $id			The item unique id.
	 */
	public function __construct($id = null)
	{
		require_once FUNC_WIDGETS_PHP_PATH . 'vboxlayout.class.php';
		$layout = new Widget_VBoxLayout();

		parent::__construct($id, $layout);

		$this->setMetadata('addlabel', widget_translate('More'));
		$this->setMetadata('removelabel', widget_translate('Less'));
	}


    /**
     * {@inheritDoc}
     * @see Widget_Frame::getClasses()
     */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-multifield';
		return $classes;
	}


	/**
	 * {@inheritDoc}
	 * @see Widget_Frame::display()
	 */
	public function display(Widget_Canvas $canvas)
	{
		$count = 0;
		$layout = $this->getLayout();
		$name = $this->getName();

		assert('$layout instanceOf Widget_VBoxLayout; /* Widget_MultiField must have Widget_VBoxLayout as layout. */');
		assert('!empty($name); /* Widget_MultiField must have a name. */');

		foreach ($layout->getItems() as $item) {
			if($item instanceOf Widget_InputWidget) {
			    $namePath = $item->getFullName();
			    $name = end($namePath);
				assert('is_numeric($name); /* The "name" ('.$name.') must be numeric because the Widget_InputWidget is child of Widget_MultiField. */');
				$count++;
			}
		}

		$widgetsAddon = bab_getAddonInfosInstance('widgets');

		return parent::display($canvas)
		  . $canvas->metadata($this->getId(), $this->getMetadata())
		  . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.multifield.jquery.js');
	}
}
