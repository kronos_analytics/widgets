<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';


require_once dirname(__FILE__) . '/boxlayout.class.php';


/**
 * Constructs a Widget_ListLayout.
 *
 * @param int		$nbColumns	The number of columns in the grid
 * @param string	$id			The item unique id.
 * @return Widget_ListLayout
 */
function Widget_ListLayout($id = null)
{
	return new Widget_ListLayout($id);
}


/**
 * The Widget_ListLayout class provides a way to arrange items from left-to-right and top-to-bottom.
 */
class Widget_ListLayout extends Widget_BoxLayout implements Widget_Displayable_Interface
{
    /**
     * (non-PHPdoc)
     * @see Widget_Layout::getClasses()
     */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-layout-list';
		return $classes;
	}


	/**
	 * (non-PHPdoc)
	 * @see Widget_Layout::display()
	 */
	public function display(Widget_Canvas $canvas)
	{
		$items = array();
		foreach ($this->getItems() as $item) {
			$items[] = $item;
		}
		
		return $canvas->ul(
			$this->getId(),
			$this->getClasses(),
			$items,
			$this->getCanvasOptions(),
		    $this->getTitle(),
			$this->getAttributes()
		)
		. $canvas->metadata($this->getId(), $this->getMetadata());
	}
}
