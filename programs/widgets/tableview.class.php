<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/containerwidget.class.php';
require_once dirname(__FILE__) . '/gridparent.class.php';



/**
 * Constructs a Widget_TableView.
 *
 * @param string $id The item unique id.
 *
 * @return Widget_TableView
 */
function Widget_TableView($columnClasses = array(), $id = null)
{
    return new Widget_TableView($columnClasses, $id);
}



/**
 * A Widget_TableView is a widget that displays a list of
 */
class Widget_TableView extends Widget_ContainerWidget implements Widget_Displayable_Interface
{
    /**
     * @var array
     */
    protected $columnClasses = array();

    /**
     * @var array
     */
    protected $_rowClasses = array();

    /**
     *
     * @var string
     */
    private $viewType;

    protected $options = array();

    const VIEW_ICONS = 'widget-view-as-icon';
    const VIEW_LIST = 'widget-view-as-list';
    const VIEW_DETAILS = 'widget-view-as-details';

    const VIEW_CARDS = 'widget-view-as-cards';


    const VIEW_CHART = 'widget-view-as-chart';

    const VIEW_CHART_PIE = 'widget-view-as-chart widget-chart-pie';
    const VIEW_CHART_DOUGHNUT = 'widget-view-as-chart widget-chart-doughnut';
    const VIEW_CHART_POLARAREA = 'widget-view-as-chart widget-chart-polararea';
    const VIEW_CHART_BAR = 'widget-view-as-chart widget-chart-bar';
    const VIEW_CHART_STACKEDBAR = 'widget-view-as-chart widget-chart-stackedbar';
    const VIEW_CHART_LINE = 'widget-view-as-chart widget-chart-line';
    const VIEW_CHART_RADAR = 'widget-view-as-chart widget-chart-radar';

    const CHART_DATA_SERIES_IN_ROWS = 'widget-chart-data-series-in-rows';
    const CHART_DATA_SERIES_IN_COLUMNS = 'widget-chart-data-series-in-columns';


    /**
     * @param array  $columnClasses     Optional columns classes
     * @param string $id                The item unique id.
     */
    public function __construct($columnClasses = array(), $id = null)
    {
        parent::__construct($id);
        $this->setColumnClasses($columnClasses);
        $layout = new Widget_GridParent();
        $layout->setTableView($this);
        $this->setLayout($layout);
    }




    /**
     * Creates a new section in the table view.
     *
     * @see Widget_GridParent::addSection()
     *
     * @param string $id        The unique identifier (for the layout) of the new section.
     * @param string $label     An optional text label.
     * @param string $className An optional class name.
     *
     * @return Widget_TableView
     */
    public function addSection($id = null, $label = null, $class = null, $colspan = null)
    {
        $this->getLayout()->addSection($id, $label, $class, $colspan);
        return $this;
    }

    /**
     * Sets the current section to which subsquent addItem call will add items.
     *
     * @see Widget_TableView::addSection()
     *
     * @param string $id The identifier of the section (previously added by Widget_TableView::addSection()).
     *
     * @return Widget_TableView
     */
    public function setCurrentSection($id)
    {
        $this->getLayout()->setCurrentSection($id);
        return $this;
    }

    /**
     * Sets how items are displayed in the list view.
     *
     * Possible values are:
     *  - Widget_TableView::VIEW_ICONS
     *  - Widget_TableView::VIEW_LIST
     *  - Widget_TableView::VIEW_DETAILS
     *  - Widget_TableView::VIEW_CHART
     *
     * @param string $viewType
     * @return Widget_TableView
     */
    public function setView($viewType)
    {
        $this->viewType = $viewType;
        return $this;
    }



    /**
     * Returns the type of list view (VIEW_ICONS, VIEW_LIST, VIEW_DETAILS).
     *
     * @see Widget_TableView::setView
     * @return string
     */
    public function getView()
    {
        return $this->viewType;
    }

    /**
     * Get cell content or null if empty
     * @return Widget_Item
     */
    public function getCell($row, $col)
    {
        $layout = $this->getLayout();
        return $layout->getCell($row, $col);
    }


    /**
     * Adds a class to a column.
     *
     * @param int       $columnId
     * @param string    $className
     * @return Widget_TableView
     */
    public function addColumnClass($columnId, $className)
    {
        $this->columnClasses[$columnId][$className] = $className;
        return $this;
    }


    /**
     * Removes the classes associated to a column.
     *
     * @param int       $columnId
     * @return Widget_TableView
     */
    public function removeColumnClasses($columnId)
    {
        unset($this->columnClasses[$columnId]);
        return $this;
    }

    /**
     * Sets the classes associated all columns.
     *
     * @param array       [ $columnId => [ 'class1', 'class2' ... ] ]
     * @return Widget_TableView
     */
    public function setColumnClasses($columnClasses)
    {
        $this->columnClasses = $columnClasses;
        return $this;
    }

    /**
     * Get column classes before drawing the cell
     * @param   int                                 $columnId
     * @param   Widget_Displayable_Interface        $cellContent
     * @return array
     */
    public function getColumnClasses($columnId, Widget_Displayable_Interface $cellContent = null)
    {
        $classes = array();

        if (isset($this->columnClasses[$columnId])) {
            $classes += $this->columnClasses[$columnId];
        }

        return $classes;
    }

    /**
     * Get row classes before drawing the row
     * @param int $rowId
     * @return Widget_TableView
     */
    public function getRowClasses($rowId)
    {
        static $oddRowArray = null;
        if ($oddRowArray === null) {
            $oddRowArray = [];
        }

        if (!isset($oddRowArray[$this->getId()])) {
            $oddRowArray[$this->getId()] = true;
        }

        $oddRowArray[$this->getId()] = !$oddRowArray[$this->getId()];
        $oddRow = $oddRowArray[$this->getId()];

        $classes = array('widget-table-row', $oddRow ? 'odd' : 'even');
        if (isset($this->_rowClasses[$rowId])) {
            $classes += $this->_rowClasses[$rowId];
        }

        return $classes;
    }


    /**
     * Add a class on a row
     * @param int       $rowId
     * @param string    $className
     * @return self
     */
    public function addRowClass($rowId, $className)
    {
        $this->_rowClasses[$rowId][$className] = $className;
        return $this;
    }


    /**
     * Add a style to the row when displayed as a chart.
     *
     * @param int       $rowId
     * @param string    $style
     * @return self
     */
    public function addRowChartStyle($rowId, $style)
    {
        return $this->addRowClass($rowId, 'widget-chart-' . $style);
    }

    /**
     * Sets the color of row when displayed as a chart.
     *
     * @param int       $rowId
     * @param string    $color      A Widget_Color::xxx (e.g. Widget_Color::PINK)
     * @return self
     */
    public function setRowChartColor($rowId, $color)
    {
        return $this->addRowClass($rowId, 'widget-chart-color-' . $color);
    }

    /**
     * Add a style to the column when displayed as a chart.
     *
     * @param int       $columnId
     * @param string    $style
     * @return self
     */
    public function addColumnChartStyle($columnId, $style)
    {
        return $this->addColumnClass($columnId, 'widget-chart-' . $style);
    }


    /**
     * Sets the color of column when displayed as a chart.
     *
     * @param int       $columnId
     * @param string    $color      A Widget_Color::xxx (e.g. Widget_Color::PINK)
     * @return self
     */
    public function setColumnChartColor($columnId, $color)
    {
        return $this->addColumnClass($columnId, 'widget-chart-color-' . $color);
    }

    /**
     * (non-PHPdoc)
     * @see Widget_Widget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-tableview';
//        if ($this->viewType == Widget_TableView::VIEW_CHART) {
            $classes[] = $this->viewType;
//       }
        return $classes;
    }



    /**
     * (non-PHPdoc)
     * @see Widget_Displayable_Interface::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        $layout = $this->getLayout();

        $this->setMetadata('options', $this->options);

        $display = $canvas->div(
            $this->getId(),
            $this->getClasses(),
            array($layout),
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        )
        . $canvas->metadata($this->getId(), $this->getMetadata())
        . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'ChartNew.js/ChartNew.js')
        . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.color.js')
        . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.tableview.jquery.js')
        . $canvas->loadAddonStyleSheet($widgetsAddon, 'widgets.tableview.css');

        return $display;
    }

    /**
     * @param array $options
     * @return self
     */
    public function setOptions($options = array())
    {
        $this->options = $options;
        return $this;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     *
     * @param string $optionKey
     * @param mixed $optionValue
     * @return self
     */
    public function setOption($optionKey, $optionValue)
    {
        $this->options[$optionKey] = $optionValue;
        return $this;
    }

    /**
     * @param string $optionKey
     * @return mixed
     */
    public function getOption($optionKey)
    {
        if (!isset($this->options[$optionKey])) {
            return null;
        }
        return $this->options[$optionKey];
    }

    /**
     * Fix the header of the tableView. When scrolling, the header will stay on top of the rows.
     *
     * @param boolean $isFixed      Toggle the isFixed status of the tableView header
     * @param string $fixedHeight   Height and unit of the tableView when the header is fixed. If auto, the tableView will have all its orignal height, but the header will always stay on top
     * @return Widget_TableView
     */
    public function setFixedHeader($isFixed = true, $fixedHeight = 'auto')
    {
        $this->setOption('fixedHeader', $isFixed);
        if($isFixed){
            $this->setOption('fixedHeaderTableHeight', $fixedHeight);
        }
        return $this;
    }
}

require_once dirname(__FILE__) . '/tablemodelview.class.php';
