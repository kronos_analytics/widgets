<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/containerwidget.class.php';



/**
 * Constructs a Widget_Section.
 *
 * @param Widget_Layout	$layout		The layout that will manage how widgets are displayed in this container.
 * @param string		$id			The item unique id.
 * @return Widget_Section
 */
function Widget_Section($headerText, Widget_Layout $layout = null, $id = null)
{
    return new Widget_Section($headerText, $layout, $id);
}



/**
 * A Widget_Section is similar to a Widget_Frame with a header.
 *
 */
class Widget_Section extends Widget_ContainerWidget implements Widget_Displayable_Interface
{

    /**
     * @var Widget_Layout
     */
    protected $header;

    /**
     * @var Widget_Layout
     */
    protected $headerTitleLayout;

    /**
     * @var Widget_Title
     */
    protected $headerTitle = null;

    /**
     * @var Widget_Title
     */
    protected $headerSubTitle = null;

    /**
     * @var Widget_Layout
     */
    protected $subLayout = null;

    /**
     * @var bool
     */
    protected $foldable = false;

    /**
     * @var bool
     */
    protected $folded = false;

    /**
     * @var int
     */
    protected $level;

    /**
     * @var Widget_Item
     */
    protected $contextMenu;


    /**
     * Creates a Widget_Section.
     *
     * @param string|Widget_Item       $headerText     The text displayed in the section header.
     * @param Widget_Item  $containerItem  The item that will be placed in the section.
     * @param int          $level          The header text will displayed as a Widget_Title of this level.
     * @param string       $id             The item unique id.
     */
    public function __construct($headerText = '', $containerItem = null, $level = 4, $id = null)
    {
        $W = bab_Widgets();

        if (!isset($containerItem)) {
            $this->subLayout = $W->VBoxLayout();
        } elseif (!$containerItem instanceof Widget_Layout) {
            $this->subLayout = $W->VBoxLayout();
            $this->subLayout->addItem($containerItem);
        } else {
            $this->subLayout = $containerItem;
        }
        $this->subLayout->setSizePolicy('widget-section-content');

        if ($headerText instanceof Widget_Item) {
            $this->setHeader($headerText);
        } else {
            $this->setHeaderText($headerText, $level);
            $this->headerTitle->addClass('handle');
        }

        $this->setSubHeaderText('', $level + 1);
        $this->headerSubTitle->addClass('handle');

        $this->headerTitleLayout = $W->VBoxItems();
        
        $this->headerTitleLayout->setSizePolicy(Widget_SizePolicy::MAXIMUM);

        $this->header = $W->HBoxItems(
            $W->Frame()->addClass('arrow handle'),
            $this->headerTitleLayout
        )
        ->setVerticalAlign('middle');


        $this->level = $level;
        parent::__construct($id);
        parent::setLayout($W->VBoxItems($this->header, $this->subLayout));
    }




    /**
     * display a colon after title text
     * @return Widget_Section
     */
    public function colon($colon = true)
    {
        $this->headerTitle->colon($colon);
        return $this;
    }



    /**
     * @param Widget_Displayable_Interface $item
     *
     * Depending on the associated layout, there may be additional parameters.
     *
     * @return Widget_Section
     */
    public function addItem(Widget_Displayable_Interface $item = null)
    {
        if (!isset($item)) {
            return $this;
        }

        // When we add an item in a container, we actually add the item to its associated layout.
        // As parameters depend on the layout class we call the method addItem with all the parameters.
        $args = func_get_args();
        call_user_func_array(array($this->subLayout, 'addItem'), $args);

        $args[0]->setParent($this);
        return $this;
    }



    /**
     * Sets the layout manager for this container to $layout.
     *
     * @param Widget_Layout|null $layout
     * @return Widget_Section
     */
    public function setSubLayout($layout)
    {
        $this->subLayout = $layout;

        if (null !== $layout) {
            $layout->setParent(parent::getLayout());
        }

        return $this;
    }


    public function setLayout($layout)
    {
        return $this->setSubLayout($layout);
    }


    /**
     * Gets the layout manager for this container to $layout.
     *
     * @return Widget_Layout
     */
    public function getSubLayout()
    {
        return $this->subLayout;
    }

    public function getLayout()
    {
        return $this->getSubLayout();
    }



    /**
     * Sets the text that will be used as header for the section
     *
     * @param string $headerText
     * @return Widget_Section
     */
    public function setHeaderText($headerText, $level = null)
    {
        if (!isset($this->headerTitle)) {
            $W = bab_Widgets();
            $this->headerTitle = $W->Title();
        }
        $this->headerTitle->setText($headerText);
        if (isset($level)) {
            $this->headerTitle->setLevel($level);
        }
        return $this;
    }



    /**
     * Define the header from a widget_item
     *
     * @param Widget_Item $itme
     * @return Widget_Section
     */
    public function setHeader($item)
    {
        $this->headerTitle = $item->addClass('handle');

        return $this;
    }



    /**
     * Sets the text that will be used as sub header for the section
     *
     * @param string $subHeaderText
     * @return Widget_Section
     */
    public function setSubHeaderText($subHeaderText, $level = null)
    {
        if (!isset($this->headerSubTitle)) {
            $W = bab_Widgets();
            $this->headerSubTitle = $W->Title();
        }
        $this->headerSubTitle->setText($subHeaderText);
        if (isset($level)) {
            $this->headerSubTitle->setLevel($level);
        }
        return $this;
    }



    /**
     * Sets/unset the foldable property (content visibility is toggled when clicking header).
     *
     * @return Widget_Section
     */
    public function setFoldable($foldable = true, $folded = null)
    {
        $this->foldable = $foldable;
        $this->folded = $folded;

        return $this;
    }




    public function addToolbar()
    {
        $W = bab_Widgets();
        $toolbar = $W->FlowLayout()
            ->addClass('widget-toolbar')
            ->addClass(Func_Icons::ICON_LEFT_16);
        $this->header->addItem($toolbar);

        return $toolbar;
    }



    /**
     * @return Widget_Menu
     */
    public function addPopupMenu()
    {
        $W = bab_Widgets();

        $menu = $W->Menu()
            ->setLayout($W->VBoxLayout())
            ->addClass(Func_Icons::ICON_LEFT_16);

        $this->header->addItem($menu);
        $menu->attachTo($this->headerTitle);

        return $menu;
    }



    /**
     * @return Widget_Item
     */
    public function addContextMenu($type = 'inline')
    {
        bab_functionality::includeOriginal('Icons');

        /* @var $W Func_Widgets */
        $W = bab_Widgets();

        if ($type === 'basic') {
            // no predefined icon size
            $this->contextMenu = $W->FlowLayout();
            $this->header->addItem($this->contextMenu);
            return $this->contextMenu;
        }


        if ($type === 'popup') {
            $this->contextMenu = $W->Menu()
                ->setLayout($W->FlowLayout())
                ->addClass(Func_Icons::ICON_LEFT_16);

            $this->header->addItem($this->contextMenu);
        } else {
            $this->contextMenu = $W->FlowLayout()
                //->addClass('widget-toolbar')
                ->addClass(Func_Icons::ICON_LEFT_16)
                ->addClass('widget-section-menu');
            $this->header->addItem($this->contextMenu);
        }

        return $this->contextMenu;
    }




    /**
     * @return array
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-section';
        if ($this->foldable) {
            $classes[] = 'widget-foldable';
        }
        $cookieName = 'widgets_Section_' . $this->getId();
        if (!isset($this->folded) && isset($_COOKIE[$cookieName])) {
            if ($_COOKIE[$cookieName] == 1) {
                $classes[] = 'widget-folded';
            }
        } elseif ($this->folded) {
            $classes[] = 'widget-folded';
        }
        $classes[] = 'widget-level-' . $this->level;

        return $classes;
    }



    /**
     * (non-PHPdoc)
     * @see Widget_Displayable_Interface::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        // add header (title or item)
        $this->headerTitleLayout->addItem($this->headerTitle);

        // add header subtitle if necessary
        $subtitle = $this->headerSubTitle->getText();
        if (!empty($subtitle)) {
            $this->headerTitleLayout->addItem($this->headerSubTitle);
        }

        if ($this->foldable) {
            $this->header->setSizePolicy('widget-section-header widget-foldable');
        } else {
            $this->header->setSizePolicy('widget-section-header');
        }
        $section = $canvas->div(
            $this->getId(),
            $this->getClasses(),
            array(parent::getLayout()),
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        );
        $section .= $canvas->metadata($this->getId(), $this->getMetadata());
        $section .= $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.section.jquery.js');
        return $section;
    }
}
