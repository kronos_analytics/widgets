<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

//require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once dirname(__FILE__) . '/widget.class.php';



/**
 * Constructs a Widget_Dockable.
 *
 * @param 	string			[$label]	The dock button label.
 * @param	Widget_Action	[$action]	The dock content url
 * @param 	string			[$id]		The item unique id.
 * @return Widget_Dockable
 */
function Widget_Dockable($label = null, Widget_Action $action = null, $id = null)
{
	return new Widget_Dockable($label, $action, $id);
}



/**
 * A Widget_Dockable.
 *
 */
class Widget_Dockable extends Widget_Widget implements Widget_Displayable_Interface
{
	/**
	 *
	 * @var string
	 */
	private $label = null;

	private $item = null;

	private $content = null;


	/**
	 * @param 	string			[$label]	The dock button label.
	 * @param	Widget_Action	[$action]	The dock content url
	 * @param 	string			[$id]		The item unique id.
	 */
	public function __construct($label = null, Widget_Action $action = null, $id = null)
	{
		parent::__construct($id);

		if (null === $label) {
			$this->setLabel('');
		} else {
			$this->setLabel($label);
		}

		if (null !== $action)
		{
			$this->setAction($action);
		}

		$this->setOpenFloat();
		$this->displayMainButton();
		$this->displayDockButton();
	}


	/**
	 * Sets the button label.
	 *
	 * @param string $text
	 * @return self
	 */
	public function setLabel($text)
	{
		$this->label = $text;
		return $this;
	}


	/**
	 * Sets the content Ajax Url.
	 *
	 * @param string $url
	 * @return self
	 */
	public function setUrl($url)
	{
		$this->setMetadata('contentUrl', $url);
		return $this;
	}


	/**
	 * Sets the content Ajax action
	 *
	 * @param Widget_Action $action
	 * @return self
	 */
	public function setAction(Widget_Action $action)
	{
		$this->setUrl($action->url());
		return $this;
	}


	/**
	 * Sets the content
	 *
	 * @return self
	 */
	public function setContent(Widget_Displayable_Interface $item)
	{
		$this->content = $item;
		return $this;
	}


	/**
	 * Set defaut open status as docked
	 * @return self
	 */
	public function setOpenDocked()
	{
		$this->setMetadata('openMode', 'docked');
	}

	/**
	 * Set defaut open status as float
	 * @return self
	 */
	public function setOpenFloat()
	{
		$this->setMetadata('openMode', 'float');
		return $this;
	}

	/**
	 * @param	bool	$status
	 * @return self
	 */
	public function displayMainButton($status = true)
	{
		$this->setMetadata('displayMainButton', $status);
		return $this;
	}


	/**
	 * Set the parameter to false to disable the dock/undock button
	 * @param	bool	$status
	 * @return self
	 */
	public function displayDockButton($status = true)
	{
		$this->setMetadata('displayDockButton', $status);
		return $this;
	}


	/**
	 * Set the parameter to true to enable the full size template
	 * @param	bool	$status
	 * @return self
	 */
	public function displayDockFullSize($status = false)
	{
		$this->setMetadata('displayDockFullSize', $status);
		return $this;
	}


	/**
	 * @return array
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-dockable';
		return $classes;
	}


	/**
	 * (non-PHPdoc)
	 * @see programs/widgets/Widget_Displayable_Interface#display($canvas)
	 */
	public function display(Widget_Canvas $canvas)
	{
		$displayableItems = array($this->label);
		if (isset($this->content)) {
			$displayableItems[] = $this->content->addClass('dock-content');
		}

		$widgetsAddon = bab_getAddonInfosInstance('widgets');

		return $canvas->div(
			$this->getId(),
			$this->getClasses(),
			$displayableItems,
			$this->getCanvasOptions(),
			$this->getTitle()
		)
		. $canvas->metadata($this->getId(), $this->getMetadata())
		. $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.dockable.jquery.js');
	}
}
