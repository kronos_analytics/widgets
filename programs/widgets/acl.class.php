<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/inputwidget.class.php';


/**
 * Constructs a Widget_Acl.
 *
 * @param string		$id			The item unique id.
 * @return Widget_LineEdit
 */
function Widget_Acl($id = null)
{
	return new Widget_Acl($id);
}



/**
 * A Widget_Acl is a widget that lets the user set access rights on a item.
 */
class Widget_Acl extends Widget_InputWidget implements Widget_Displayable_Interface
{

	private $id_delegation = null;


	/**
	 * @param string $id			The item unique id.
	 * @return Widget_LineEdit
	 */
	public function __construct($id = null)
	{
		parent::__construct($id);

		// this uid will be used to verify the origin of ajax request
		$_SESSION['Widget_Acl_uid'][$this->getId()] = $this->getId();
		$this->setMetaData('uid', $this->getId());
		$this->setMetaData('selfpage', bab_getSelf());
		$this->setMetaData('buttonText', widget_translate('Set groups'));
		$this->setMetaData('withChildNodes', widget_translate('And the group children'));
		$this->setMetaData('setOfGroups', widget_translate('Set of groups'));
		$this->setMetaData('deleteTitle', widget_translate('Remove group'));
		$this->setMetaData('deployButton', widget_translate('Deploy to checked boxes'));
		$this->setMetaData('setGroupsImage', bab_getAddonInfosInstance('widgets')->getStylePath().'images/22x22/add_group.png');

		$this->setMetaData('maxHeight', 150);
		$this->setMetaData('maxHeightUnit', 'px');
	}

	/**
	 * Disable a group by ID
	 * @param 	int 	$id_group
	 * @param	bool	$withChildNodes
	 */
	public function disableGroup($id_group, $withChildNodes = false)
	{
		$disabled = $this->getMetadata('disabledGroups');
		if (null === $disabled) {
			$disabled = array();
		}

		if ($withChildNodes) {
			$id_group = ((string) $id_group).'+';
		}

		$disabled[$id_group] = (string) $id_group;

		$this->setMetaData('disabledGroups', $disabled);

		return $this;
	}


	/**
	 *
	 * @param int $id_delegation
	 */
	public function setIdDelegation($id_delegation)
	{
		$this->id_delegation = $id_delegation;
	}


	/**
	 * Get delegation used by ACL form
	 * @return int
	 */
	public function getIdDelegation()
	{
		if (null !== $this->id_delegation) {
			return $this->id_delegation;
		}

		if (function_exists('bab_getCurrentAdmGroup')) {
			return bab_getCurrentAdmGroup();
		}

		$babBody = bab_getBody();
		return $babBody->currentAdmGroup;
	}


	/**
	 * Sets the button text label.
	 *
	 * @param string $label
	 * @return Widget_Acl
	 */
	public function setLabel($label)
	{
		$this->setMetaData('buttonText', $label);

		return $this;
	}


	/**
	 * Set the maximum height of group list
	 *
	 * @param	float 	$maxheight
	 * @param	string	$unit		em | px
	 *
	 * @return Widget_Acl
	 */
	public function setMaxHeight($maxheight, $unit = 'em')
	{
		$this->setMetaData('maxHeight', $maxheight);
		$this->setMetaData('maxHeightUnit', $unit);

		return $this;
	}



	/**
	 * (non-PHPdoc)
	 * @see programs/widgets/Widget_InputWidget::getClasses()
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-acl';
		return $classes;
	}


	public function getDisplayValueVBox(Widget_Canvas $canvas)
	{
		$arr = explode(',', $this->getValue());

		$r = array();

		foreach ($arr as $g) {
			if ('' === $g) {
				continue;
			}

			if (mb_strlen($g) > 1 && '+' === mb_substr($g, -1)) {
				$id_group = (int) mb_substr($g, 0, -1);

				try {
    				$r[] = $canvas->span(
    				    null,
    				    array(),
    				    array(bab_getGroupName($id_group) .' ' . widget_translate('and childnodes'))
    				);
				} catch (Exception $e) {
				    bab_debug($e);
				}
			} else {

				$id_group = (int) $g;
				try {
				    $r[] = $canvas->span(null, array(), array(bab_getGroupName($id_group)));
				} catch (Exception $e) {
				    bab_debug($e);
				}
			}
		}

		return $canvas->ul(null, array(), $r);
	}


	/**
	 * (non-PHPdoc)
	 * @see programs/widgets/Widget_Displayable_Interface::display()
	 */
	public function display(Widget_Canvas $canvas)
	{

		if ($this->isDisplayMode()) {

			$classes = $this->getClasses();
			$classes[] = 'widget-displaymode';

			$items = array();
			if ($this->getTitle()) {
			    $items[] = $canvas->span(null, array('widget-strong'), array($this->getTitle().' :'));
			}

			$items[] = $this->getDisplayValueVBox($canvas);

			return $canvas->vbox(null, $classes, $items);


		} else {


			$this->setMetadata('id_delegation', $this->getIdDelegation());

			$title = $this->getTitle();
			if (empty($title)) {
				$title = widget_translate('Access rights');
			}

			$widgetsAddon = bab_getAddonInfosInstance('widgets');

			return $canvas->lineInput(
				$this->getId(),
				$this->getClasses(),
				$this->getFullName(),
				$this->getValue(),
				5,
				null,
				$this->isDisabled(),
			    false,
			    false,
			    'text',
			    null,
			    $this->getCanvasOptions(),
				$title,
			    $this->getAttributes()
			)
			. $canvas->metadata($this->getId(), $this->getMetadata())
			. $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.acl.jquery.js')
			. $canvas->loadAddonStyleSheet($widgetsAddon, 'widgets.acl.css');
		}
	}
}
