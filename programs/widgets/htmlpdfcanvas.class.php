<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
include_once dirname(__FILE__).'/htmlcanvas.class.php';


/**
 * Constructs a Widget_HtmlPdfCanvas
 *
 * @return Widget_HtmlPdfCanvas
 */
function Widget_HtmlPdfCanvas()
{
    return new Widget_HtmlPdfCanvas;
}

/**
 * This Canvas will generate HTML strings without scripts.
 *
 */
class Widget_HtmlPdfCanvas extends Widget_HtmlCanvas
{

    /**
     * Display an image
     * @param string				$id				The unique id (for the page).
     * @param string[]				$classes		An array containing the classes as returned by Widget_Item::getClasses().
     * @param string				$text			The alternate text if the image cannot be displayed.
     * @param string				$url			The url of the image.
     * @param Widget_CanvasOptions	$options
     * @param string				$title			The optional tooltip
     * @param string[]              $attributes             An array of name => value attributes.
     * @return 	string
     */
    public function image($id, $classes, $text, $url, $options = null, $title = null, $attributes = null)
    {
        $style = self::getStyle($options);
        $html  = '<img '
            . self::htmlId($id)
            . self::htmlClasses($classes)
            . self::htmlTitle($title)
            . self::htmlAttributes($attributes)
            . $style
            . ' src="'.bab_toHtml(realpath('.') . '/' . urldecode($url)).'"'
            . ' alt="'.bab_toHtml($text).'" />';


        return $html;
    }

    /**
     * output url for link in the canvas
     * If this is a relative url, add the domain
     * @param string $url
     * @return string
     */
    protected function outputUrl($url)
    {
        if (0 !== mb_strpos($url, 'http')) {
            return bab_getBabUrl().$url;
        }

        return url;
    }



    /**
     * Create metadata container
     *
     * @param	string	$name
     * @param	array	$metadata
     * @return string
     */
    public function metadata($name, $metadata = null)
    {
        return '';
    }


    /**
     * Load script
     * @param	string	$widget_id
     * @param	string	$filename
     * @return string
     */
    public function loadScript($widget_id, $filename) {

        return '';
    }


    /**
     * Load CSS style sheet
     * @param	string	$filename
     * @return string
     */
    public function loadStyleSheet($filename) {

        return '';
    }
}