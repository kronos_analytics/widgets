<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';



/**
 * Constructs a Widget_Color.
 * @return Widget_Color
 */
function Widget_Color()
{
	return new Widget_Color();
}


/**
 * The Widget_Color class can be used to manipulate colors.
 */
class Widget_Color
{
    const RED = 'red';
    const PINK = 'pink';
    const PURPLE = 'purple';
    const DEEPPURPLE = 'deeppurple';
    const INDIGO = 'indigo';
    const BLUE = 'blue';
    const LIGHTBLUE = 'lightblue';
    const CYAN = 'cyan';
    const TEAL = 'teal';
    const GREEN = 'green';
    const LIGHTGREEN = 'lightgreen';
    const LIME = 'lime';
    const YELLOW = 'yellow';
    const AMBER = 'amber';
    const ORANGE = 'orange';
    const DARKORANGE = 'darkorange';
    const BROWN = 'brown';
    const GREY = 'grey';
    const BLUEGREY = 'bluegrey';


	private $color 			= null;
	private $color_method 	= 'RVB';

    private static function hue($m1, $m2, $h)
    {
        if ($h < 0) {
            $h = $h + 1;
        }
        if ($h > 1) {
            $h = $h - 1;
        }
        if ($h * 6 < 1) {
            return $m1 + ($m2 - $m1) * $h * 6;
        }
        if ($h * 2 < 1) {
            return $m2;
        }
        if ($h * 3 < 2) {
            return $m1 + ($m2 - $m1) * (2 / 3 - $h) * 6;
        }
        return $m1;
    }


	/**
	 * Converts an array of HSL components into an array of RGB components.
     *
	 * @param float[]		$hsl	3 floats beetween 0.0 and 1.0
	 *
	 * @return float[]				3 floats beetween 0.0 and 1.0
	 */
	private static function hsl2rgb($hsl)
	{
		$hue = $hsl[0];
		$saturation = $hsl[1];
		$lightness = $hsl[2];

		if ($saturation == 0.0) {
			$red = $green = $blue = $lightness;
		} else {
			if ($lightness <= 0.5) {
				$m2 = $lightness * ($saturation + 1);
			} else {
				$m2 = $lightness + $saturation -($lightness * $saturation);
			}
			$m1 = $lightness * 2 - $m2;
			$red = self::hue($m1, $m2, ($hue + 1 / 3));
			$green = self::hue($m1, $m2, $hue);
			$blue = self::hue($m1, $m2, ($hue - 1 / 3));
		}
		return array($red, $green, $blue);
	}


	/**
	 * Converts an array of RGB components into an array of HSL components.
	 *
	 * @param float[]   $rgb 	RGB colors set	3 floats beetween 0.0 and 1.0
	 * @return float[] 			HSL set			3 floats beetween 0.0 and 1.0
	 */
	private static function rgb2hsl($rgb)
	{
		$clrR = $rgb[0];
		$clrG = $rgb[1];
		$clrB = $rgb[2];

		$clrMin = min($clrR, $clrG, $clrB);
		$clrMax = max($clrR, $clrG, $clrB);
		$deltaMax = $clrMax - $clrMin;

		$lightness = ($clrMax + $clrMin) / 2;

		if (0 == $deltaMax) {
			$hue = 0;
			$saturation = 0;
		} else {
			if (0.5 > $lightness) {
				$saturation = $deltaMax / ($clrMax + $clrMin);
			} else {
				$saturation = $deltaMax / (2 - $clrMax - $clrMin);
			}
			$deltaR = ((($clrMax - $clrR) / 6) + ($deltaMax / 2)) / $deltaMax;
			$deltaG = ((($clrMax - $clrG) / 6) + ($deltaMax / 2)) / $deltaMax;
			$deltaB = ((($clrMax - $clrB) / 6) + ($deltaMax / 2)) / $deltaMax;
			if ($clrR == $clrMax) {
				$hue = $deltaB - $deltaG;
			} elseif ($clrG == $clrMax) {
				$hue = (1 / 3) + $deltaR - $deltaB;
			} elseif ($clrB == $clrMax) {
				$hue = (2 / 3) + $deltaG - $deltaR;
			}
			if (0 > $hue) {
			    $hue += 1;
			}
			if (1 < $hue) {
			    $hue -= 1;
			}
		}
		return array($hue, $saturation, $lightness);
	}



	/**
	 * Converts an array of YUV components into an array of RGB components.
	 *
	 * @param array $yuv 		YUV colors set
	 * @return array 			RGB set
	 */
	private static function yuv2rgb($yuv)
	{
		$rgb = array(
            $yuv[0] + 1.13983 * $yuv[2],
            $yuv[0] - 0.39465 * $yuv[1] - 0.58060 * $yuv[2],
            $yuv[0] + 2.03211 * $yuv[1]
		);
		foreach ($rgb as &$component) {
			if ($component < 0) {
				$component = 0;
			} elseif ($component > 1) {
				$component = 1;
			}
		}

		return $rgb;
	}

	/**
	 * Converts an array of RGB components into an array of YUV components.
	 *
	 * @param array $rgb 		RGB colors set
	 * @return array 			YUV set
	 */
	private static function rgb2yuv($rgb)
	{
	    $yuv = array(
            0.299 * $rgb[0] + 0.587 * $rgb[1] + 0.114 * $rgb[2],
            -0.14713 * $rgb[0] - 0.28886 * $rgb[1] + 0.436 * $rgb[2],
            0.615 * $rgb[0] - 0.51498 * $rgb[1] - 0.10001 * $rgb[2],
        );
	    if ($yuv[0] < 0) {
            $yuv[0] = 0;
	    } elseif ($yuv[0] > 1) {
            $yuv[0] = 1;
	    }

	    return $yuv;
	}

	/**
	 * Set color components in RGB colorspace.
	 *
	 * @param float    $red	number beetween 0 and 1
	 * @param float    $green	number beetween 0 and 1
	 * @param float    $blue	number beetween 0 and 1
	 */
	public function setRGB($red, $green, $blue)
	{
		$this->color_method = 'RGB';
		$this->color = array($red, $green, $blue);
		return $this;
	}

	/**
	 * Set color components in HSL colorspace.
	 *
	 * @param float    $hue	number beetween 0 and 1
	 * @param float    $saturation	number beetween 0 and 1
	 * @param float    $lightness	number beetween 0 and 1
	 *
	 * @return Widget_Color
	 */
	public function setHSL($hue, $saturation, $lightness)
	{
		$this->color_method = 'HSL';
		$this->color = array($hue, $saturation, $lightness);
		return $this;
	}

	/**
	 * Set color components in YUV colorspace.
	 *
	 * @param	int		$y	number beetween 0 and 1
	 * @param	int		$u	number beetween 0 and 1
	 * @param	int		$v	number beetween 0 and 1
	 */
	public function setYUV($y, $u, $v)
	{
		$this->color_method = 'YUV';
		$this->color = array($y, $u, $v);
		return $this;
	}

	/**
	 * Returns the components of the color in RGB colorspace.
	 *
	 * @return array
	 */
	public function getRGB()
	{
		switch ($this->color_method) {
			case 'HSL':
				return self::hsl2rgb($this->color);
			case 'YUV':
				return self::yuv2rgb($this->color);
			case 'RGB':
				return $this->color;
		}

		return null;
	}

	/**
	 * Returns the components of the color in HSL colorspace.
	 *
	 * @return array
	 */
	public function getHSL()
	{
		switch ($this->color_method) {
			case 'HSL':
				return $this->color;
			case 'YUV':
				return self::rgb2hsl(self::yuv2rgb($this->color));
			case 'RGB':
				return self::rgb2hsl($this->color);
		}

		return null;
	}

	/**
	 * Returns the components of the color in YUV colorspace.
	 *
	 * @return array
	 */
	public function getYUV()
	{
		switch ($this->color_method) {
			case 'HSL':
				return self::rgb2yuv(self::hsl2rgb($this->color));
		    case 'YUV':
				return $this->color;
			case 'RGB':
				return self::rgb2yuv($this->color);
		}

		return null;
	}


	/**
	 * Set the color value in RGB colorspace by specifing the hexadecimal value (without '#' prefix).
	 *
	 * @param	string		$hexa
	 */
	public function setHexa($hexa)
	{
		$decimal = hexdec($hexa);
		$red = ($decimal & 0xFF0000) >> 16;
		$green = ($decimal & 0xFF00) >> 8;
		$blue = $decimal & 0xFF;
		return $this->setRGB($red / 255.0, $green / 255.0, $blue / 255.0);
	}


	/**
	 * Returns the RGB hexadecimal code of the color without the '#' prefix.
	 *
	 * @return string
	 */
	public function getHexa()
	{
		$rgb = $this->getRGB();

		if (null === $rgb) {
			return null;
		}

		return sprintf('%02X%02X%02X', round(255 * $rgb[0]), round(255 * $rgb[1]), round(255 * $rgb[2]));
	}


	/**
	 * Create a color from a string
	 * set HSL color with hue from string
	 * @param string   $str
	 * @param float    $saturation	number beetween 0 and 1
	 * @param float    $lightness	number beetween 0 and 1
	 *
	 * @return Widget_Color
	 */
	public function setHueFromString($str, $saturation, $lightness)
	{
		$hue = hexdec(hash('crc32b', $str)) / 4294967295.0;
		$this->setHSL($hue, $saturation, $lightness);
		return $this;
	}
}
