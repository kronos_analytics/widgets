<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Flree Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';






/**
 * Constructs a Widget_Action.
 *
 * @return Widget_Action
 */
function Widget_Action()
{
	return new Widget_Action();
}





/**
 * A Widget_Action
 *
 */
class Widget_Action
{
	/**
	 * @var string[] A flat array containing name => value pairs where name can be of the form "param1[param2][param3]".
	 */
	private $parameters = array();

	/**
	 * @var string A corresponding icon name.
	 */
	private $icon = '';

	/**
	 * @var string A short description.
	 */
	private $title = '';

	private $isAjax = false;

	/**
	 * @var boolean Defines if the action is a 'save' method.
	 */
	private $requireSaveMethod = false;

	/**
	 * @var boolean Defines if the action is a 'delete' method.
	 */
	private $requireDeleteMethod = false;


	static private $controllerParamName = 'tg';
	static private $methodParamName = 'idx';

    public function __construct()
    {
        //$this->backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
    }

	/**
	 * Recreates the action from the specified url.
	 *
	 * @return Widget_Action
	 */
	public static function fromUrl($url)
	{
		$splitUrl = parse_url($url);
		$action = new Widget_Action();

		if (isset($splitUrl['query'])) {
		    $parameters = array();
			parse_str($splitUrl['query'], $parameters);
			$action->setParameters($parameters);
		}

		return $action;
	}


	/**
	 * Recreates the action from the http request.
	 * if parameters are set the request will be filtered by the parameters names
	 *
	 * @return Widget_Action
	 */
	public static function fromRequest()
	{
		$arr = func_get_args();
		if (!empty($arr)) {

			$parameters = array();

			foreach ($arr as $param) {
				$parameters[$param] = bab_rp($param);
			}

		} else {
			$parameters = $_GET + $_POST;
		}

		$action = new Widget_Action();
		$action->setParameters($parameters);

		return $action;
	}


    /**
     * Set controller method.
     *
     * @param string $controller
     * @param string $method
     * @return $this
     */
    public function setMethod($controller, $method, $parameters)
    {
        $this->setParameters($parameters);
        $this->setParameter(self::$controllerParamName, $controller);
        $this->setParameter(self::$methodParamName, $method);

        return $this;
    }



	/**
	 * Returns the full name corresponding to the action method.
	 *
	 * @return array
	 */
	public function getFullName()
	{
		return array(self::$methodParamName, $this->getMethod());
	}


	/**
	 * Returns the controller name corresponding to the action.
	 *
	 * @return string
	 */
	public function getController()
	{
		return $this->getParameter(self::$controllerParamName);
	}

	/**
	 * Returns the method name corresponding to the action.
	 *
	 * @return string
	 */
	public function getMethod()
	{
		if (!isset($this->parameters[self::$methodParamName])) {
			return null;
		}

		$method = $this->getParameter(self::$methodParamName);
		if (is_array($method)) {
		    $keys = array_keys($method);
			$method = array_pop($keys);
		}

		return $method;
	}

	/**
	 * Returns the parameters of the action.
	 *
	 * @return array
	 */
	public function getParameters()
	{
		return $this->parameters;
	}


	/**
	 * Returns the value of the specified parameter.
	 *
	 * @param string $name		The name of the parameter.
	 * @return mixed
	 */
	public function getParameter($name)
	{
		return $this->parameters[$name];
	}


	/**
	 * Checks the existence of the parameter.
	 *
	 * @param string $name
	 * @return bool
	 */
	public function parameterExists($name)
	{
		return array_key_exists($name, $this->parameters);
	}

	/**
	 * Sets a parameter of the action.
	 *
	 * @param string $name
	 * @param string $value
	 * @return $this
	 */
	public function setParameter($name, $value)
	{
		$this->parameters[$name] = $value;
		return $this;
	}


	/**
	 * Sets parameters of the action.
	 *
	 * @param array $values		An associative, possibly nested, array of (name => value) pairs.
	 * @return $this
	 */
	public function setParameters($values)
	{
		$this->parameters = $values;
		return $this;
	}


    /**
     * Returns the urlencoded string corresponding to the value.
     *
     * @param string|int|float|bool $v
     * @return string
     */
    protected function urlencode($v)
    {
        if (!is_string($v) && !is_numeric($v) && !is_bool($v)) {
            trigger_error('Url parameter is not a string, numeric or boolean in '.$this->getMethod().' : '.print_r($v, true));
            if (isset($this->backtrace)) {
                bab_debug($this->backtrace);
            }
        }

        return urlencode($v);
    }


	/**
	 * Returns a non-encoded url corresponding to the action.
	 *
	 * @return string		The non-encoded url string correspondig to the action.
	 */
	public function url()
	{
		$url = '';

		foreach ($this->parameters as $name => $value) {

			if (!is_null($value)) {

				if ('' === $url) {
					$url .= bab_getSelf().'?';
				} else {
					$url .= '&';
				}

				if (is_array($value)) {
					if ($name === self::$methodParamName) {
					    $keys = array_keys($value);
						$value = array_pop($keys);
						$url .= $this->urlencode($name) . '=' . $this->urlencode($value);
					} else {
						$url .= $this->urlAsArray($name, $value);
					}
				} else {

					$url .= $this->urlencode($name) . '=' . $this->urlencode($value);
				}
			}
		}

		if ('' === $url) {
			// homepage url
			$url = '?';
		}

		return $url;
	}

	/**
	 * @param	string	$name
	 * @param	array	$arr
	 * @return string			URL
	 */
	private function urlAsArray($name, $arr)
	{
		$params = array();

		foreach ($arr as $key => $value) {

			if (!is_null($value)) {
				if (is_array($value)) {
					$params[] = $this->urlAsArray($name.'['.$key.']', $value);
				} else {
					$params[] = $this->urlencode($name.'['.$key.']').'='.$this->urlencode($value);
				}
			}
		}

		return implode('&', $params);
	}



	/**
	 * Sets the action corresponding icon.
	 *
	 * @param string $icon
	 * @return $this
	 */
	public function setIcon($icon)
	{
		$this->icon = $icon;
		return $this;
	}


	/**
	 * Returns the action corresponding icon.
	 *
	 * @return string
	 */
	public function getIcon()
	{
		return $this->icon;
	}


	/**
	 * Sets the action's title.
	 *
	 * The title is a short descriptive text.
	 *
	 * @since 1.0.8
	 *
	 * @param string $title
	 * @return $this
	 */
	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	/**
	 * Returns the action's title.
	 *
	 * The title is a short descriptive text.
	 *
	 * @since 1.0.8
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}


	/**
	 * Sets whether the action is ajax compliant.
	 *
	 * @param boolean $isAjax
	 * @return $this
	 */
	public function setAjax($isAjax = true)
	{
		$this->isAjax = $isAjax;
		return $this;
	}


	/**
	 * Returns whether the action is ajax compliant.
	 *
	 * @return boolean
	 */
	public function isAjax()
	{
		return $this->isAjax;
	}


    /**
     * Sets whether the action is a 'save' method, i.e. it will modify data.
     *
     * @since 1.0.113
     * @param boolean $requireSaveMethod
     * @return self
     */
    public function setRequireSaveMethod($requireSaveMethod)
    {
        $this->requireSaveMethod = $requireSaveMethod;
        return $this;
    }


    /**
     * Returns whether the action is a 'save' method, i.e. it will modify data.
     *
     * @since 1.0.113
     * @return boolean
     */
    public function isSaveMethodRequired()
    {
        return $this->requireSaveMethod;
    }


    /**
     * Sets whether the action is a 'delete' method, i.e. it will modify data.
     *
     * @since 1.0.113
     * @param boolean $requireDeleteMethod
     * @return self
     */
    public function setRequireDeleteMethod($requireDeleteMethod)
    {
        $this->requireDeleteMethod = $requireDeleteMethod;
        return $this;
    }


    /**
     * Returns whether the action is a 'delete' method, i.e. it will modify data.
     *
     * @since 1.0.113
     * @return boolean
     */
    public function isDeleteMethodRequired()
    {
        return $this->requireDeleteMethod;
    }


    /**
     * Do a header location to this action and exit program.
     */
    public function location()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
        $url = new bab_url($this->url());
        $url->location();
    }
}
