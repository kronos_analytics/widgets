<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__).'/radioset.class.php';



/**
 * Constructs a Widget_RadioMenu.
 *
 * @param string		$id			The item unique id.
 * @return Widget_RadioSet
 */
function Widget_RadioMenu($id = null)
{
    return new Widget_RadioMenu($id);
}


/**
 * A Widget_RadioMenu is a widget that let the user chose an item in a list
 *
 *
 */
class Widget_RadioMenu extends Widget_RadioSet implements Widget_Displayable_Interface
{
    private $_options 			= array();

    private $_visibleOptions	= array();

    private $_dynamicWidth		= false;

    /**
     * @param string $id			The item unique id.
     * @return Widget_RadioSet
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
    }



    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-radio-menu';
        return $classes;
    }

    /**
     * Set with of the generated menu button (pixels)
     * @param	int	$width
     * @return Widget_RadioMenu
     */
    public function width($width)
    {
        $this->setMetadata('width', $width);
        return $this;
    }

    /**
     * Set with of the generated select drop-down  (pixels)
     * @param	int	$width
     * @return Widget_RadioMenu
     */
    public function menuWidth($width)
    {
        $this->setMetadata('menuWidth', $width);
        return $this;
    }


    /**
     * Add a widget option for menu
     * @param mixed 						$value
     * @param Widget_Displayable_Interface 	$item					Item displayed in list
     * @param Widget_Displayable_Interface	$selectedVisibleItem	Item visible when selected (usefull only with javascript/jQuery script)
     * @return Widget_RadioSet
     */
    public function addOption($value, Widget_Displayable_Interface $item, Widget_Displayable_Interface $selectedVisibleItem = null) {

        $item->addClass('widget-radio-menu-item-selectable');

        $this->_options[$value] = $item;


        if (isset($selectedVisibleItem)) {

            $this->_visibleOptions[$value] = $selectedVisibleItem;

        }

        return $this;
    }

    public function setDynamicWidth($dynamic = true){
        $this->_dynamicWidth = $dynamic;
        return $this;
    }

    /**
     * Remove option by value
     * @param mixed $value
     * @return Widget_RadioMenu
     */
    public function removeOption($value)
    {
        unset($this->_options[$value]);
        return $this;
    }


    public function display(Widget_Canvas $canvas)
    {
        if ($this->isDisplayMode()) {

            $classes = $this->getClasses();
            $classes[] = 'widget-displaymode';


            $opt = $this->getOptions();
            if (isset($this->_options[$this->getValue()])) {

                $item = $this->_options[$this->getValue()];
                return $item->display($canvas);

            } else {

                return '';
            }

        } else {

            $display = array();

            $i = 0;
            foreach($this->_options as $value => $item) {

                $checked = ((string) $value) === ((string) $this->getValue());
                $input = $canvas->radioInput($this->getId().$i, array(), $this->getFullName(), $value, $checked, $this->isDisabled(), array());
                $display[] = $canvas->flow(null, array('widget-radio-menu-item'), array($input, $item));
                $i++;
            }

            $meta = array();
            foreach($this->_visibleOptions as $value => $item) {
                $meta[$value] = $item->display($canvas);
            }
            $this->setMetadata('visible', $meta);
            $this->setMetadata('dynamicWidth', $this->_dynamicWidth);

            $widgetsAddon = bab_getAddonInfosInstance('widgets');

            return $canvas->vbox($this->getId(), $this->getClasses(), $display, null, null, array("style"=>"display: none;"))
                . $canvas->metadata($this->getId(), $this->getMetadata())
                . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.radiomenu.jquery.js')
                . $canvas->loadAddonStyleSheet($widgetsAddon, 'widgets.radiomenu.css');

        }
    }

}