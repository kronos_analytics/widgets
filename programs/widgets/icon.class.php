<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/widget.class.php';



/**
 * Constructs a Widget_Icon.
 *
 * @param string		$text		The label text.
 * @param string		$image		The image source (url).
 * @param string		$id			The item unique id.
 * @return Widget_Icon
 */
function Widget_Icon($text = '', $image = '', $id = null)
{
	return new Widget_Icon($text, $image, $id);
}


/**
 * An icon is an image with an associated text label.
 *
 * The image can be a stock image or a real url to an image.
 */
class Widget_Icon extends Widget_Widget implements Widget_Displayable_Interface
{
	private	$text;
	private	$stockImage;
	private $imageUrl;
	private $width;


	/**
	 * @param string	$text			The label text.
 	 * @param string	$stockImage		The image name.
	 * @param string 	$id				The item unique id.
	 * @return Widget_Icon
	 */
	public function __construct($text = null, $stockImage = null, $id = null)
	{
		parent::__construct($id);
		$this->setText($text);
		$this->setStockImage($stockImage);
		$this->setImageUrl(null);
		$this->setWidth(null);
	}


	/**
	 * Sets the icon's text label.
	 *
	 * @param string	$labelText
	 * @return Widget_Icon
	 */
	public function setText($text)
	{
		$this->text = $text;
		return $this;
	}


	/**
	 * Returns the icon's text label.
	 *
	 * @return string
	 */
	public function getText()
	{
		return $this->text;
	}



	/**
	 * Sets the icon's stock image.
	 *
	 * @param string	$stockImage
	 *
	 * @see Widget_Stock
	 * @return Widget_Icon
	 */
	public function setStockImage($stockImage)
	{
		$this->stockImage = $stockImage;
		return $this;
	}


	/**
	 * Sets the icon's image url.
	 *
	 * @param string	$imageUrl
	 * @return Widget_Icon
	 */
	public function setImageUrl($imageUrl)
	{
		$this->imageUrl = $imageUrl;
		return $this;
	}


	/**
	 * Sets the icon's width.
	 *
	 * @param string	$width
	 * @return Widget_Icon
	 */
	public function setWidth($width)
	{
		$this->width = $width;
		return $this;
	}


	/**
	 * Returns the icon's width.
	 *
	 * @return string
	 */
	public function getWidth()
	{
		return $this->width;
	}


	/**
	 * (non-PHPdoc)
	 * @see programs/widgets/Widget_Item#getClasses()
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();

		// generic classname for Icons fonctionality
		$classes[] = 'widget-icon';
		$classes[] = 'icon';
		if (isset($this->stockImage)) {
			$classes[] = /*'widget-' .*/ $this->stockImage;
		}
		return $classes;
	}



	/**
	 * (non-PHPdoc)
	 * @see programs/widgets/Widget_Displayable_Interface#display()
	 */
	public function display(Widget_Canvas $canvas)
	{
		$options = $this->getCanvasOptions();
		if (!isset($options)) {
			$options = Widget_Canvas::Options();
		}
		$containerOptions = Widget_Canvas::Options();


		if (isset($this->imageUrl)) {
			$options->backgroundImage($this->imageUrl);
		}
		if (!is_null($this->getWidth())) {
			$options->width($this->getWidth());
		}
		 /*else {
			// try to estimate icon width if we got the url
			if (isset($this->imageUrl)) {
				list($width) = @getimagesize($this->imageUrl);
				$containerOptions->horizontalSpacing($width);
			}
		}*/


		$iconContent = array(
						  	$canvas->span('',
						  		array('widget-icon-image-container', 'icon-image-container'),
								array(),
								$containerOptions
							),
						  );

		$text = $this->getText();
		if (is_string($text)) {
			$iconTexts = explode("\n", $text);
			$iconText = array_shift($iconTexts);
			$iconContent[] = $canvas->span('', array('widget-icon-label', 'icon-label'), array($canvas->text($iconText)));
			foreach ($iconTexts as $subText) {
				$iconContent[] = $canvas->span('', array('widget-icon-sub-label', 'icon-sub-label'), array($canvas->text($subText)));
			}
		} else {
			$iconContent[] = $canvas->span('', array('widget-icon-label', 'icon-label'), array($text));
		}


		$icon =  $canvas->span(
			$this->getId(),
			$this->getClasses(),
			$iconContent,
			$options,
			$this->getTitle(),
		    $this->getAttributes()
		);
		$icon .= $canvas->metadata($this->getId(), $this->getMetadata());

		return $icon;
	}


}

