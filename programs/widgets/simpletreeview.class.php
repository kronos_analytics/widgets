<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/widget.class.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/tree.php';


/**
 * Constructs a Widget_SimpleTreeView.
 *
 * @param string        $id         The item unique id.
 * @return Widget_SimpleTreeView
 */
function Widget_SimpleTreeView($id = null)
{
    return new Widget_SimpleTreeView($id);
}



/**
 * Widget_SimpleTreeView wrapper to bab_TreeView
 *
 * @method 	 appendElement(bab_TreeViewElement $element, string $parentId) Appends $element as the last child of the element with the id $parentId.
 */
class Widget_SimpleTreeView extends Widget_Widget
{
    /**
     * @var bab_TreeView
     */
    protected $embeddedItem = null;


    /**
     * @param string $id            The item unique id.
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
        $this->constructEmbeddedItem();

        $this->setClasses('bab-orgchart-entitysearch bab-hide-non-matching-items bab-expand-matching-items-sub-tree');

        // default : do not save one node in cookie, use ->setPersistent(true) to activate, this must be used with a fixed ID for the widget
        $this->setPersistent(false);
    }

    /**
     *
     */
    protected function constructEmbeddedItem()
    {
        $this->embeddedItem = new bab_TreeView($this->getEmbeddedItemId());

        $this->embeddedItem->_templateFile = 'addons/widgets/simpletreeview.html';
        $this->embeddedItem->_templateSection = 'simple_treeview';
        $this->embeddedItem->_templateCss = 'treeview_css';
        $this->embeddedItem->_templateScripts = 'treeview_scripts';
    }


    /**
     * @return string
     */
    protected function getEmbeddedItemId()
    {
        return $this->getId() . '_tree';
    }



    /**
     * @see Widget_Item::setId()
     */
    public function setId($id)
    {
        parent::setId($id);
        if (isset($this->embeddedItem)) {
            // Use reflections to allow access to protected properties.
            $treeviewReflection = new ReflectionObject($this->embeddedItem);
            $_id = $treeviewReflection->getProperty('_id');
            $t_treeViewId = $treeviewReflection->getProperty('t_treeViewId');
            if (method_exists($_id, 'setAccessible')) {
                $_id->setAccessible(true);
                $_id->setValue($this->embeddedItem, $this->getEmbeddedItemId());
            }
            if (method_exists($t_treeViewId, 'setAccessible')) {
                $t_treeViewId->setAccessible(true);
                $t_treeViewId->setValue($this->embeddedItem, $this->getEmbeddedItemId());
            }
        }
        return $this;
    }



    public function __call($name, $arguments)
    {
        $return = call_user_func_array(array($this->embeddedItem, $name), $arguments);
        if ($return === $this->embeddedItem) {
            return $this;
        }
        return $return;
    }


    /**
     * @param string $title         The title (label) of the node.
     * @param string $id            A unique element id in the treeview default is Root.
     * @return bab_SimpleTreeViewElement
     */
    public function createRootNode($title, $id = 'Root')
    {
        $root = $this->createElement($id, 'directory', $title, '', '');
        $root->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/category.png');
        $this->embeddedItem->appendElement($root, NULL);

        return $root;
    }

    /**
     * Shows toolbar with expand / collapse and a search field will be added a the top of the treeview
     * @return self
     */
    public function showToolbar()
    {
        $this->embeddedItem->addAttributes(bab_TreeView::SHOW_TOOLBAR);
        return $this;
    }


    /**
     * Hides toolbar with expand / collapse and a search field will be added a the top of the treeview
     * @return self
     */
    public function hideToolbar()
    {
        $this->embeddedItem->removeAttributes(bab_TreeView::SHOW_TOOLBAR);
        return $this;
    }


    /**
     *
     * @param string $id
     * @return self
     */
    public function highlightElement($id)
    {
        $this->embeddedItem->highlightElement($id);
        return $this;
    }


    /**
     * @param string $id            A unique element id in the treeview.
     * @param string $type          Will be used as a css class to style the element.
     * @param string $title         The title (label) of the node.
     * @param string $description   An additional description that will appear as a tooltip.
     * @param string $link          A link when clicking the node title.
     *
     * @return Widget_SimpleTreeViewElement
     */
    public function createElement($id, $type = '', $title = '', $description = '', $link = '')
    {
        $element = new Widget_SimpleTreeViewElement($id, $type, $title, $description, $link);
        return $element;
    }


    /**
     * @return string
     */
    protected function getStyleSheet()
    {
        return $GLOBALS['babInstallPath'] . 'styles/tree.css';
    }

    /**
     * @see Widget_Widget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-treeview';
        return $classes;
    }


    /**
     * @see Widget_Item::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        // persistent status to template variable
        if ($this->isPersistent()) {
            $this->embeddedItem->addAttributes(bab_TreeView::MEMORIZE_OPEN_NODES);
        } else {
            $this->embeddedItem->removeAttributes(bab_TreeView::MEMORIZE_OPEN_NODES);
        }
        
        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        return $canvas->div(
            $this->getId(),
            $this->getClasses(),
            $this->embeddedItem->printTemplate()
        ) . $canvas->metadata($this->getId(), $this->getMetadata())
        . $canvas->loadScript($this->getId(), $widgetsAddon->getTemplatePath() . 'widgets.simpletreeview.js')
        . $canvas->loadStyleSheet($this->getStyleSheet());
    }


    /**
     * Adds the item to the treeview under the specified parent.
     *
     * @param Widget_Displayable_Interface $item
     * @param string                       $parentId
     *
     * @return self
     */
    public function addItem(Widget_Displayable_Interface $item = null, $parentId = null)
    {
        if (!isset($item)) {
            return $this;
        }
        $element = $this->createElement($item->getId());
        $element->setItem($item);
        $this->appendElement($element, $parentId);
        return $this;
    }
}


$GLOBALS['Widget_SimpleTreeViewElementCanvas'] = bab_Widgets()->HtmlCanvas();

class Widget_SimpleTreeViewElement extends bab_TreeViewElement
{

    /**
	 * @param string $id			A unique element id in the treeview.
	 * @param string $type			Will be used as a css class to style the element.
	 * @param string $title			The title (label) of the node.
	 * @param string $description	An additional description that will appear as a tooltip.
	 * @param string $link			A link when clicking the node title.
     */
    public function __construct($id, $type, $title, $description, $link)
    {
        parent::__construct($id, $type, $title, $description, $link);
    }

    /**
     * @param Widget_Displayable_Interface $item
     * @return self
     */

    public function setItem(Widget_Displayable_Interface $item)
    {
        global $Widget_SimpleTreeViewElementCanvas;

        $html = $item->display($Widget_SimpleTreeViewElementCanvas);

        $this->_title = $html;

        return $this;
    }


    public function addMember($memberName, $role = '', $url = '')
    {
        return;
    }
}
