<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__) . '/filepicker.class.php';


/**
 * Constructs a Widget_FilePicker.
 *
 * @param string		$id			The item unique id.
 * @return Widget_FilePicker
 */
function Widget_ImagePicker($id = null)
{
    return new Widget_ImagePicker($id);
}


/**
 * A Widget_ImagePicker is a widget that let the user upload images.
 * by default, upload are commited to a temporary folder
 *
 * value of this input widget is an array containing the list of files in the folder
 *
 */
class Widget_ImagePicker extends Widget_FilePicker implements Widget_Displayable_Interface
{

    /**
     * @param string $id
     */
    public function __construct($id = null)
    {
        parent::__construct($id);

        $this->thumbWidth 	= 128;
        $this->thumbHeight 	= 128;

        $this->setAcceptedMimeTypes(array('image/gif', 'image/jpeg', 'image/png'));
    }


    /**
     * Set preview image width and height in pixels.
     *
     * @param int $width
     * @param int $height
     * @return Widget_ImagePicker
     */
    public function setDimensions($width, $height)
    {
        $this->thumbWidth = $width;
        $this->thumbHeight = $height;

        return $this;
    }

    /**
     * Set default image for image picker, this image will be displayed only if no image loaded
     * @param 	bab_Path $path
     * @return Widget_ImagePicker
     */
    public function setDefaultImage(bab_Path $path)
    {
        if ($T = bab_functionality::get('Thumbnailer')) {
            $T->setSourceFile($path->toString());
            $this->defaultImage = $T->getThumbnail($this->thumbWidth, $this->thumbHeight);
        }

        return $this;
    }




    /**
     * Add a button next to image picker to suggest images with google search
     *
     * @link http://code.google.com/intl/fr/apis/ajaxsearch/signup.html
     *
     * @param 	string                                    $apikey		Google API key to use
     * @param 	(Widget_Displayable_Interface|string)[]   $searchCombo	Array of (Widget_Displayable_Interface | widget ID string | keyword string)
     * @param 	string 	                                  $size			Google search option			IMAGESIZE_SMALL | IMAGESIZE_MEDIUM | IMAGESIZE_LARGE | IMAGESIZE_EXTRA_LARGE
     * @param	string	                                  $type			Google search option			IMAGETYPE_FACES | IMAGETYPE_PHOTO | IMAGETYPE_CLIPART | IMAGETYPE_LINEART
     * @param	string	                                  $prefix       A string that will be prefixed to the search keywords (no space added).
     *
     * @return Widget_ImagePicker
     */
    public function addGoogleSearch($apikey, Array $searchCombo, $size = null, $type = null, $prefix = null)
    {
        $monitor = array();
        foreach ($searchCombo as $input) {
            if ($input instanceOf Widget_Displayable_Interface) {
                $input = $input->getId();
            }
            $monitor[] = $input;
        }

        $this->setMetadata('googleSearchApiKey', $apikey);	// http://www.google.com/jsapi?key=$apikey
        $this->setMetadata('googleSearchWidgets', $monitor);
        $this->setMetadata('googleSearchSize', $size);
        $this->setMetadata('googleSearchType', $type);
        $this->setMetadata('googleSearchPrefix', $prefix);
        $this->setMetadata('suggestTitle', widget_translate('Images found on Google'));
        $this->setMetadata('suggestMissingKeywordsError', widget_translate('Error : the search keywords are missing, fill the form first'));
        $this->setMetadata('suggestNoResultsError', widget_translate('Google found no result for the keyword(s)'));
        $this->setMetadata('suggestExternalLinkLabel', widget_translate('More results on Google Images...'));

        return $this;
    }



    public function setGravatarEmailField(Widget_EmailLineEdit $widget)
    {
        $this->setMetadata('gravatarEmailId', $widget->getId());
        return $this;
    }



    public function setAssociatedCroper(Widget_ImageCropper $widget)
    {
        $this->setMetadata('imageCropper', $widget->getId());
        return $this;
    }
}
