<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__) . '/suggestlineedit.class.php';






/**
 * Constructs a Widget_SuggestCountry.
 *
 * @param string		$id			The item unique id.
 * @return Widget_SuggestLineEdit
 */
function Widget_SuggestCountry($id = null)
{
	return new Widget_SuggestCountry($id);
}


/**
 * A Widget_SuggestCountry is a widget that let the user enter a country. 
 * Propose countries suggestions based on Geonames functionality
 * @link http://wiki.ovidentia.org/index.php/Base_de_donn%C3%A9s_GeoNames
 */
class Widget_SuggestCountry extends Widget_SuggestLineEdit implements Widget_Displayable_Interface 
{


	/**
	 * @param string $id			The item unique id.
	 * @return Widget_LineEdit
	 */
	public function __construct($id = null)
	{
		parent::__construct($id);

		if (false !== $keyword = $this->getSearchKeyword()) {

			if ($GeoNames = @bab_functionality::get('GeoNames')) {
				$countries = $GeoNames->searchCountryFromName($keyword);
				$i = 0;

				foreach($countries as $country) {
					$i++;
					if ($i > Widget_SuggestLineEdit::MAX) {
						break;
					}

					$this->addSuggestion($country->iso, $country->getName());
				}
			}

			$this->sendSuggestions();
		}
	}

	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-suggestcountry';
		return $classes;
	}


	public function display(Widget_Canvas $canvas) {
		return parent::display($canvas);
	}

}