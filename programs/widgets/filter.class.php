<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once FUNC_WIDGETS_PHP_PATH . 'widget.class.php';



/**
 * Constructs a Widget_Filter.
 *
 * @param string		$id			The item unique id.
 * @param Widget_Layout	$layout		The layout that will manage how widgets are displayed in this container.
 * @return Widget_Filter
 */
function Widget_Filter($id = null, Widget_Layout $layout = null)
{
	return new Widget_Filter($id, $layout);
}



class Widget_Filter extends Widget_ContainerWidget implements Widget_Displayable_Interface
{
    /**
     * @var Widget_Item $title
     */
    private	$title = null;

	/**
	 * @var Widget_Item $filter		The Widget_Item (usually a form) that will appear in the filter part.
	 */
	private	$filter;

	/**
	 * @var Widget_Item $filtered	The Widget_Item (for example a tableview) on which the filter should be applied.
	 */
	private	$filtered;

	/**
	 * @param string		$id			The item unique id.
	 * @param Widget_Layout	$layout		The layout that will manage how widgets are displayed in this container.
	 * @return Widget_Filter
	 */
	public function __construct($id = null, Widget_Layout $layout = null)
	{
		if (null === $layout) {
			require_once FUNC_WIDGETS_PHP_PATH . 'layout.class.php';
			$layout = new Widget_Layout();
		}

		parent::__construct($id, $layout);
	}


	/**
	 * (non-PHPdoc)
	 * @see programs/widgets/Widget_ContainerWidget#addItem($item)
	 */
	public function addItem(Widget_Displayable_Interface $item = null)
	{
		trigger_error('On the Filter widget, use methods setFilter($w) and setFiltered($w)');
	}




	/**
	 *
	 * @param Widget_Item $filter
	 * @return Widget_Filter
	 */
	public function setTitle(Widget_Item $title)
	{
	    $this->title = $title;
	    parent::addItem($title);
	    return $this;
	}


	/**
	 *
	 * @return Widget_Item
	 */
	public function getTitle()
	{
	    return $this->title;
	}


	/**
	 *
	 * @param Widget_Item $filter
	 * @return Widget_Filter
	 */
	public function setFilter(Widget_Item $filter)
	{
		$this->filter = $filter;
		parent::addItem($filter);
		return $this;
	}


	/**
	 *
	 * @return Widget_Item
	 */
	public function getFilter()
	{
		return $this->filter;
	}


	/**
	 *
	 * @param Widget_Item $filtered
	 * @return Widget_Filter
	 */
	public function setFiltered(Widget_Item $filtered)
	{
		$this->filtered = $filtered;
		parent::addItem($filtered);
		return $this;
	}


	/**
	 *
	 * @return Widget_Item
	 */
	public function getFiltered()
	{
		return $this->filtered;
	}


	/**
	 * (non-PHPdoc)
	 * @see programs/widgets/Widget_Item#getClasses()
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-filter';
		return $classes;
	}


	/**
	 * (non-PHPdoc)
	 * @see programs/widgets/Widget_Displayable_Interface#display($canvas)
	 */
	public function display(Widget_Canvas $canvas)
	{
	    $items = array(
	        $canvas->div('', array('filter'), array($this->getFilter())),
	        $canvas->div('', array('filtered'), array($this->getFiltered()))
	    );

	    $title = $this->getTitle();
	    if ($title) {
	        array_unshift($items, $canvas->div('', array('title'), array($title)));
	    }



		return $canvas->div(
			$this->getId(),
			$this->getClasses(),
		    $items,
		    null,
		    null,
		    $this->getAttributes()
		) . $canvas->metadata($this->getId(), $this->getMetadata());
	}
}
