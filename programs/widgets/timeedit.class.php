<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/lineedit.class.php';


/**
 * Constructs a Widget_DateEdit.
 *
 * @param string		$id			The item unique id.
 * @return Widget_TimeEdit
 */
function Widget_TimeEdit($id = null)
{
	return new Widget_TimeEdit($id);
}


/**
 * A Widget_TimeEdit is a widget that lets the user enter a time.
 */
class Widget_TimeEdit extends Widget_InputWidget implements Widget_Displayable_Interface
{
    
    protected $second = false;
    

    /**
     * (non-PHPdoc)
     * @see Widget_InputWidget::getClasses()
     */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-timeedit';
		return $classes;
	}
	
	
	public function showSecond($show = true)
	{
	    $this->second = $show;
	    
	    return $this;
	}
	
	
	
	

    /**
     * (non-PHPdoc)
     *
     * @see programs/widgets/Widget_Displayable_Interface::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $attributes = $this->getAttributes();
        if($this->second) {
            $value = substr($this->getValue(), 0, 8);
            $size = 8;
            $attributes['step'] = 1;
        } else {
            $value = substr($this->getValue(), 0, 5);
            $size = 5;
            $attributes['step'] = 60;
        }

        if ($this->isDisplayMode()) {

            $classes = $this->getClasses();
            $classes[] = 'widget-displaymode';

            return $canvas->richtext(
                $this->getId(),
                $classes,
                $this->getValue(),
                BAB_HTML_ALL ^ BAB_HTML_P,
                $this->getCanvasOptions()
            );
        } else {
            $widgetsAddon = bab_getAddonInfosInstance('widgets');

            return $canvas->lineInput(
                $this->getId(),
                $this->getClasses(),
                $this->getFullName(),
                $value,
                $size,
                $size,
                $this->isDisabled(),
                false,
                false,
                'time',
                $this->getPlaceHolder(),
                $this->getCanvasOptions(),
                $this->getTitle(),
                $attributes
            )
            . $canvas->metadata($this->getId(), $this->getMetadata())
            . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.timeedit.jquery.js');
        }
    }
}
