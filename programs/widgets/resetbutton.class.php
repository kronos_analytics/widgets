<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/widget.class.php';


/**
 * Constructs a Widget_ResetButton.
 *
 * @param string $id			The item unique id.
 * @return Widget_ResetButton
 */
function Widget_ResetButton($id = null)
{
	return new Widget_ResetButton($id);
}



/**
 * This widget is used to reset its parent form.
 */
class Widget_ResetButton extends Widget_Widget
{

	/**
	 * The button text label.
	 *
	 * @var string $_label
	 */
	private $_label = null;

	/**
	 * The button action.
	 *
	 * @var string $_action
	 */
	private $_action = null;
	
	
	/**
	 * The button title
	 *
	 */
	private $_title = null;
	 

	/**
	 * @var bool
	 */
	private $validate = false;
	
	/**
	 * @param string $id			The item unique id.
	 * @return Widget_ResetButton
	 */
	public function __construct($id = null)
	{
		parent::__construct($id);
	}

	/**
	 * Sets the button text label.
	 *
	 * @param string $label
	 * @return Widget_ResetButton
	 */
	public function setLabel($label)
	{
		$this->_label = $label;

		return $this;
	}


	/**
	 * Returns the button text label.
	 *
	 * @return string
	 */
	public function getLabel()
	{
		return $this->_label;
	}

	
	/**
	 * (non-PHPdoc)
	 * @see Widget_InputWidget::getClasses()
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-resetbutton';
		$classes[] = 'ui-default-state';
		return $classes;
	}

	
	/**
	 * (non-PHPdoc)
	 * @see Widget_Item::display()
	 */
	function display(Widget_Canvas $canvas)
	{
        if ($this->isDisplayMode()) {
		    $classes = $this->getClasses();
		    $classes[] = 'widget-displaymode';
			$ouput = $canvas->span(
    			$this->getId(),
    			$classes,
    			array(),
    			$this->getCanvasOptions(),
    			'',
    			$this->getAttributes()
    		) . $canvas->metadata($this->getId(), $this->getMetadata());
			return $ouput;
		}

		return $canvas->resetInput(
			$this->getId(),
			$this->getClasses(),
			$this->getLabel(),
			$this->isDisabled(),
			$this->getTitle(),
		    $this->getAttributes()
		) . $canvas->metadata($this->getId(), $this->getMetadata());
	}
}
