<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';


require_once dirname(__FILE__) . '/layout.class.php';



/**
 * The Widget_BoxLayout class provides a way to align widget in a row in a container.
 */
abstract class Widget_BoxLayout extends Widget_Layout
{
    /**
     *
     * @var Widget_Item[]
     */
	private $itemPositions;

	/**
	 * @param string $id
	 */
	public function __construct($id = null)
	{
		parent::__construct($id);
		
		$this->itemPositions = array();
	}


	/**
	 * Returns an array of the items managed by the layout.
	 *
	 * @return Widget_Item[] An array of the items managed by the layout.
	 */
	public function getItems()
	{
		return $this->itemPositions;
	}


	/**
	 * Adds $item to this layout. The position of the item can be specified.
	 * If the position is not specified (or null), the item is appended.
	 * If $position is negative, the item will be added at the n'th position from the end of the layout.
	 *
	 * @param Widget_Displayable_Interface	$item
	 * @param int		$position
	 * @return $this
	 */
	public function addItem(Widget_Displayable_Interface $item = null, $position = null)
	{
		assert('is_null($position) || is_int($position);');
		parent::addItem($item);
        if ($item === null) {
            return $this;
        }
		if (is_null($position)) {
			$this->itemPositions = Widget_arrayAppend(
			    $this->itemPositions,
			    array($item->getId() => $item)
			);
		} else {
			if ($position < 0) {
				$maxPos = count($this->itemPositions)-1;
				$position = $maxPos + $position;
			}
			$this->itemPositions = Widget_arrayInsert(
			    $this->itemPositions,
			    array($item->getId() => $item),
			    $position
			);
		}
		return $this;
	}


	/**
	 * Adds $item to this layout. The position of the item can be specified.
	 * If the position is not specified (or null), the item is appended.
	 * If $position is negative, the item will be added at the n'th position from the end of the layout.
	 *
	 * @param Widget_Displayable_Interface	$item
	 * @param int		$position
	 * @return $this
	 */
	public function removeItem(Widget_Displayable_Interface $item = null)
	{
		parent::removeItem($item);
        if ($item === null) {
            return $this;
        }
		unset($this->itemPositions[$item->getId()]);
		return $this;
	}
}
