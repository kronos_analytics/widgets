<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';


require_once dirname(__FILE__) . '/boxlayout.class.php';


/**
 * Constructs a Widget_FlexLayout.
 *
 * @param string	$id			The item unique id.
 * @param string    $wrap
 * @param string    $justifyContent
 * @param string    $alignItems
 * @param string    $direction
 *
 * @return Widget_FlexLayout
 */
function Widget_FlexLayout($id = null, $wrap = null, $justifyContent = 'between', $alignItems = 'center', $direction = 'row')
{
    return new Widget_FlexLayout($id, $wrap, $justifyContent, $alignItems, $direction);
}


/**
 * The Widget_FlowLayout class provides a way to arrange items from left-to-right and top-to-bottom.
 */
class Widget_FlexLayout extends Widget_BoxLayout implements Widget_Displayable_Interface
{

    const JUSTIFY_START = 'start';
    const JUSTIFY_END = 'end';
    const JUSTIFY_CENTER = 'center';
    const JUSTIFY_BETWEEN = 'between';
    const JUSTIFY_AROUND = 'around';
    const JUSTIFY_EVENLY = 'evenly';

    const ALIGN_CONTENT_NORMAL = 'normal';
    const ALIGN_CONTENT_START = 'start';
    const ALIGN_CONTENT_END = 'end';
    const ALIGN_CONTENT_CENTER = 'center';
    const ALIGN_CONTENT_BETWEEN = 'between';
    const ALIGN_CONTENT_AROUND = 'around';
    const ALIGN_CONTENT_EVENLY = 'evenly';
    const ALIGN_CONTENT_STRETCH = 'stretch';

    const ALIGN_ITEMS_START = 'start';
    const ALIGN_ITEMS_END = 'end';
    const ALIGN_ITEMS_CENTER = 'center';
    const ALIGN_ITEMS_STRETCH = 'stretch';
    const ALIGN_ITEMS_BASELINE = 'baseline';

    const DIRECTION_ROW = 'row';
    const DIRECTION_ROW_REVERSE = 'row-reverse';
    const DIRECTION_COLUMN = 'column';
    const DIRECTION_COLUMN_REVERSE = 'column-reverse';


    protected $containerOptions = null;

    protected $justify = 'between';
    protected $alignContent = 'normal';
    protected $align = 'center';
    protected $direction = 'row';
    protected $wrap = 'nowrap';

    protected $rowGap = false;
    protected $rowUnit = 'px';
    protected $colGap = false;
    protected $colUnit = 'px';

    public function __construct($id = null, $wrap = null, $justifyContent = 'between', $alignItems = 'center', $direction = 'row')
    {
        $layout = parent::__construct($id);

        $this->containerOptions = $this->options();

        $this->justifyContent($justifyContent);
        $this->alignContent('normal');
        $this->alignItems($alignItems);
        $this->direction($direction);
        $this->wrap($wrap);

        return $layout;
    }

    /**
     * Controls the space between items
     *
     * @param int|float             $colGap
     * @param string                $colUnit    'px' (default), 'pt', 'em' or '%'
     * @param boolean|int|float     $rowGap     if false col gap value and unit will be use as row gap and unit
     * @param string                $rowUnit    'px' (default), 'pt', 'em' or '%'
     */
    public function setGap($colGap, $colUnit = 'px', $rowGap = false, $rowUnit = 'px')
    {
        $this->setVerticalGap($colGap, $colUnit);
        if ($rowGap === false) {
            $this->setHorizontalGap($colGap, $colUnit);
        } else {
            $this->setHorizontalGap($rowGap, $rowUnit);
        }

        return $this;
    }

    /**
     * Controls the horizontal spacing between items
     *
     * @param int|float	        $gap
     * @param string            $unit      'px' (default), 'pt', 'em' or '%'
     */
    public function setHorizontalGap($gap, $unit = 'px')
    {
        assert('is_numeric($gap); /* The spacing parameter must be an integer */');
        $this->containerOptions->colGap($gap, $unit);

        return $this;
    }

    /**
     * Controls the vertical spacing between items
     *
     * @param int|float	        $gap
     * @param string            $unit      'px' (default), 'pt', 'em' or '%'
     */
    public function setVerticalGap($gap, $unit = 'px')
    {
        assert('is_numeric($gap); /* The spacing parameter must be an integer */');
        $this->containerOptions->rowGap($gap, $unit);

        return $this;
    }



    /**
     * Define justifty content mod, available values are:
     * start, end, center, between (default), around, evenly
     *
     * @param string $mode
     * @url https://css-tricks.com/snippets/css/a-guide-to-flexbox/#justify-content
     */
    public function justifyContent($mode)
    {
        switch ($mode) {
            case self::JUSTIFY_START:
            case self::JUSTIFY_END:
            case self::JUSTIFY_CENTER:
            case self::JUSTIFY_BETWEEN:
            case self::JUSTIFY_AROUND:
            case self::JUSTIFY_EVENLY:
                $this->justify = $mode;
                break;
            default:
                $this->justifyContent('between');
        }

        return $this;
    }



    /**
     * Define justifty content mod, available values are:
     * start, end, center, between (default), around, evenly
     *
     * @param string $mode
     * @url https://css-tricks.com/snippets/css/a-guide-to-flexbox/#align-content
     */
    public function alignContent($mode)
    {
        switch ($mode) {
            case self::ALIGN_CONTENT_NORMAL:
            case self::ALIGN_CONTENT_START:
            case self::ALIGN_CONTENT_END:
            case self::ALIGN_CONTENT_CENTER:
            case self::ALIGN_CONTENT_BETWEEN:
            case self::ALIGN_CONTENT_AROUND:
            case self::ALIGN_CONTENT_EVENLY:
            case self::ALIGN_CONTENT_STRETCH:
                $this->alignContent = $mode;
                break;
            default:
                $this->alignContent('normal');
        }

        return $this;
    }

    /**
     * Define align itemt mod, available values are:
     * start, end, center (default), stretch, baseline
     *
     * @param string $mode
     * @url https://css-tricks.com/snippets/css/a-guide-to-flexbox/#align-items
     */
    public function alignItems($mode)
    {
        switch ($mode) {
            case self::ALIGN_ITEMS_START:
            case self::ALIGN_ITEMS_END:
            case self::ALIGN_ITEMS_CENTER:
            case self::ALIGN_ITEMS_STRETCH:
            case self::ALIGN_ITEMS_BASELINE:
                $this->align = $mode;
                break;
            default:
                $this->alignItems('center');
        }

        return $this;
    }



    /**
     * Define the flex direction, available values are:
     * row (default), row-reverse, column, column-reverse
     *
     * @param string $mode
     * @url https://css-tricks.com/snippets/css/a-guide-to-flexbox/#flex-direction
     */
    public function direction($mode)
    {
        switch ($mode) {
            case self::DIRECTION_ROW:
            case self::DIRECTION_ROW_REVERSE:
            case self::DIRECTION_COLUMN:
            case self::DIRECTION_COLUMN_REVERSE:
                $this->direction = $mode;
                break;
            default:
                $this->direction('row');
        }

        return $this;
    }

    /**
     * Define the wrap parameter of the flex layout
     * To reset put reverse parama to any none boolean value
     *
     * @param boolean $reverse
     * @url https://css-tricks.com/snippets/css/a-guide-to-flexbox/#flex-wrap
     */
    public function wrap($reverse = false)
    {
        if ($reverse === true) {
            $this->wrap = 'wrap-reverse';
        } elseif ($reverse === false) {
            $this->wrap = 'wrap';
        } else {
            $this->wrap = 'nowrap';
        }

        return $this;
    }

	/**
	 * Adds $item to this layout. The position of the item can be specified.
	 * If the position is not specified (or null), the item is appended.
	 *
	 * @param Widget_Displayable_Interface $item
	 * @param int $position
	 * @return Widget_FlowLayout
	 */
	public function addItem(Widget_Displayable_Interface $item = null, $position = null)
	{
		parent::addItem($item, $position);
		return $this;
	}

	/**
	 * @return array
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-layout-flex';

		return $classes;
	}

	/**
	 * NOT PROTECTED BUT SHOULD ONLY ME CALL IN THE DISPLAY
	 *
	 * @return array
	 */
	public function getDisplayClasses()
	{
	    $classes = $this->getClasses();

	    $classes[] = 'widget-layout-flex-justify-'.$this->justify;
	    $classes[] = 'widget-layout-flex-align-content-'.$this->alignContent;
	    $classes[] = 'widget-layout-flex-align-'.$this->align;
	    $classes[] = 'widget-layout-flex-direction-'.$this->direction;
	    $classes[] = 'widget-layout-flex-'.$this->wrap;

	    return $classes;
	}

	/**
	 * @return Widget_CanvasOptions
	 */
	public function getContainerOptions()
	{
	    return $this->containerOptions;
	}


	/**
	 * @param Widget_Canvas $canvas
	 */
	public function display(Widget_Canvas $canvas)
	{
		$items = array();
		foreach ($this->getItems() as $item) {
			$items[] = $item;
		}

		$classes = $this->getDisplayClasses();

		$attributes = $this->getAttributes();

		return $canvas->flex(
            $this->getId(),
		    $classes,
		    $items,
		    $this->getContainerOptions(),
			$this->getCanvasOptions(),
			$this->getTitle(),
		    $attributes
		)
		. $canvas->metadata($this->getId(), $this->getMetadata());
	}
}
