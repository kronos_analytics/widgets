<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';


require_once dirname(__FILE__) . '/containerwidget.class.php';



/**
 * Constructs a Widget_BusinessApplicationSection.
 *
 *
 * @param string		$id			The item unique id.
 * @return Widget_BusinessApplicationSection
 */
function Widget_BusinessApplicationSection($id = null)
{
	return new Widget_BusinessApplicationSection($id);
}



/**
 * A Widget_BusinessApplicationSection is the most basic container.
 *
 */
class Widget_BusinessApplicationSection extends Widget_ContainerWidget implements Widget_Displayable_Interface
{

	private $title = false;

	/**
	 *
	 * @param string $id			The item unique id.
	 * @return Widget_BusinessApplicationSection
	 */
	public function __construct($id = null)
	{
		$W = bab_Widgets();
		parent::__construct($id, $W->VBoxLayout());
	}


	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-businessapplicationsection';
		return $classes;
	}


	public function setTitle($title) {
		$this->title = $title;
	}


	public function display(Widget_Canvas $canvas)
	{
		$divcontent = array();

		if (false !== $this->title) {
			$divcontent[] = $canvas->h('', array(),array($this->title),3);
		}


		$divcontent[] = $this->getLayout();


		return $canvas->div(
							$this->getId(),
							$this->getClasses(),
							$divcontent
						);
	}

}