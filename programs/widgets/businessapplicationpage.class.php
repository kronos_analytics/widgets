<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
bab_Widgets()->includePhpClass('Widget_Page');


/**
 *
 *
 * @return Widget_BusinessApplicationPage
 */
function Widget_BusinessApplicationPage($id = null, Widget_Layout $layout = null)
{
    return new Widget_BusinessApplicationPage($id, $layout);
}









class Widget_BusinessApplicationPage extends Widget_Page {

    /**
     * Functions menu
     */
    private $functions 	= array();

    /**
     * railway links
     */
    private $rail 		= array();

    /**
     * Menu items links
     * @deprecated	replaced by Widget_Tabs
     */
    private $menuitems 	= array();

    /**
     * Widgets to display in content
     */
    private $subwidgets = array();

    /**
     * template variable, set to true if menuitems is not empty
     */
    public	$items		= false;

    /**
     * Javascript string
     */
    private $js_strings	= array();

    /**
     * template variable
     * debug informations
     */
    public $bab_debug = '';


    /**
     * template variable, homepage link label
     */
    public $t_home = 'Home';

    /**
     * template variable, homepage link url
     */
    public $homeurl = '?';

    /**
     * template variable, back to portal link label
     */
    public $t_portal = 'Back to portal';

    /**
     * template variable, back to portal link url
     */
    public $portalurl = false;

    /**
     * template variable, disable header for iframe or popup page
     */
    public $display_header = true;


    /**
     *
     */
    private $css = null;


    public function __construct($id = null, Widget_Layout $layout = null)
    {
        parent::__construct($id, $layout);

        $addon = bab_getAddonInfosInstance('widgets');

        $this->prependJavascriptFile($GLOBALS['babInstallPath'].'scripts/ovidentia.js');
        $this->prependJavascriptFile($GLOBALS['babInstallPath'].'scripts/bab_dialog.js');

        $this->addStyleSheet($addon->getStylePath().'widgets.css');
        $this->addStyleSheet($addon->getStylePath().'businessapplicationpage.css');
    }


    /**
     * Join business application javascript environement based on jQuery
     * @return Widget_BusinessApplicationPage
     */
    public function addJsFunctionalities() {

        if ($jquery = bab_functionality::get('jquery')) {
            /*@var $jquery Func_Jquery */
            $this->prependJavascriptFile($jquery->getJavascriptUrl('jquery'));
            $this->prependJavascriptFile($jquery->getJavascriptUrl('jquery-ui'));
        }

        $addon = bab_getAddonInfosInstance('widgets');
        $this->prependJavascriptFile($addon->getTemplatePath().'businessapplicationpage.js');

        return $this;
    }




    /**
     * Set the root node link for railway
     * @param	string	$homeurl
     * @param	string	[$text]
     * @return Widget_BusinessApplicationPage
     */
    public function setHomeRailItem($url, $text = null) {
        if (null !== $text) {
            $this->t_home = bab_toHtml($text);
        }

        $this->homeurl = bab_toHtml($url);

        return $this;
    }

    /**
     * Set the right link, a go back to portal button
     * @param	string	$homeurl
     * @param	string	[$text]
     * @return Widget_BusinessApplicationPage
     */
    public function setPortalLink($url, $text = null) {
        if (null !== $text) {
            $this->t_portal = bab_toHtml($text);
        }

        $this->portalurl = bab_toHtml($url);

        return $this;
    }

    /**
    * Get html
    * @param string	$file template
    * @param string	[$template]
    */
    public function getHtml($file, $template = false) {
        $addon = bab_getAddonInfosInstance('widgets');
        return bab_printTemplate($this, $addon->getRelativePath().$file, $template);
    }



    /**
     * Add a link to the railway
     * @param string $var 			item name, css classname, unique on railway
     * @param string $text 		short label
     * @param string $url
     * @param string [$longtext]	long label
     * @return Widget_BusinessApplicationPage
     */
    public function addRailItem($var, $text, $url, $longtext = false) {
        $this->rail[$var] = array($text, $url, $longtext);

        return $this;
    }

    /**
     * Ajouter un onglet
     * @deprecated Replaced by Widget_Tabs
     * @see self::setCurrentItemMenu()
     * @param $var 		nom de l'onglet a utiliser aussi dans setCurrentItemMenu()
     * @param $text 	libelle de l'onglet, il sera passe dans la fonction de traduction
     * @param $url 		url de l'onglet, il sera encode en entities html
     * @return Widget_BusinessApplicationPage
     */
    public function addItemMenu($var, $text, $url) {

        $this->menuitems[$var] = array(
            'text' => $text,
            'url' => $url,
            'selected' => false
        );

        $this->items = true;

        return $this;
    }

    /**
     * Define selected menu item
     * @deprecated Replaced by Widget_Tabs
     * @see self::addItemMenu()
     * @param $var menu item name
     * @return Widget_BusinessApplicationPage
     */
    public function setCurrentItemMenu($var) {

        if (!isset($this->menuitems[$var])) {
            throw new Exception(sprintf('The current selected menu item "%s" does not exists', $var) );
        }

        $this->menuitems[$var]['selected'] = true;
        return $this;
    }



    /**
     * Add a javascript string
     * @param	int|string		$key
     * @param	string			$str
     * @return Widget_BusinessApplicationPage
     */
    public function addJsString($key, $str) {
        $this->js_strings[$key] = $str;
        return $this;
    }


    /**
     * Template method
     */
    final public function getnextstyle() {

        if (null === $this->css) {
            $this->css = $this->getStyleSheets();
        }


        return (list(,$this->stylesheet) = each($this->css));
    }

    /**
     * Template method
     */
    final public function getnextscript() {
        return (list(,$this->script) = each($this->scripts));
    }

    /**
     * Template method
     */
    final public function getnextstring() {
        static $i = 1;
        if (list($this->key, $value) = each($this->js_strings)) {

            if (!is_int($this->key)) {
                $this->key = bab_toHtml("'".$this->key."'");
            }

            $this->value = bab_toHtml($value, BAB_HTML_JS);
            $this->comma = $i == count($this->js_strings) ? "\n" : ",\n";
            $i++;
            return true;
        }
        return false;
    }


    /**
     * Template method
     */
    final public function getnextrail() {
        if ($arr = each($this->rail)) {

            $this->text = bab_toHtml($arr[1][0]);
            $this->url = bab_toHtml($arr[1][1]);
            $this->longtext = bab_toHtml($arr[1][2]);
            $this->htmlid = bab_toHtml($arr[0]);

            return true;
        }
        return false;
    }

    /**
     * Create a widget section with title
     * @param	string	[$title]
     * @param	string	[$classname]
     * @return	Widget_Frame
     */
    public function createSection($title = '', $classname = false) {

        $W = bab_Widgets();

        $section = $W->create('Widget_BusinessApplicationSection')->addClass('bloc');
        $this->addItem($section);

        if (!empty($classname)) {
            $section->addClass($classname);
        }

        if (!empty($title)) {
            $section->setTitle($title);
        }

        return $section;
    }



    /**
     * Create a widget bloc with title
     * @param	string	[$title]
     * @param	string	[$classname]
     * @return	Widget_Frame
     */
    public function createFrame($title = '', $classname = false) {

        $W = bab_Widgets();



        // workaround for menu items
        if ($this->menuitems) {

            $tabs = $W->Tabs()
            ->displayAllTabs(false)
            ;

            $this->addItem($tabs);

            foreach($this->menuitems as $uid => $arr) {

                if (!isset($arr['url'])) {
                    print_r($this->menuitems);
                    die();
                }


                $query = parse_url($arr['url']);
                if (isset($query['query'])) {
                    parse_str($query['query'], $output);
                } else {
                    $output = array();
                }

                $tabs->addItem(
                    $tab = $W->Tab('tab_'.$uid, $arr['text'])
                        ->setAction($W->Action()->setParameters($output))
                        ->setTitle(false)
                );

                if ($arr['selected']) {
                    $tabs->setSelectedTab($tab);
                    $div = $tab->addClass('bloc');
                }
            }

            if (!isset($div)) {
                trigger_error('Methods "addItemMenu" and "setCurrentItemMenu" must be used before the first usage of "createFrame" method');
            }

            $this->menuitems = array();

        }


        if (!isset($div)) {

            $div = $W->create('Widget_Frame', null, $W->VBoxLayout())->addClass('bloc');
            $this->addItem($div);
        }







        if (false !== $classname) {
            $div->addClass($classname);
        }

        if (!empty($title)) {
            $div->addItem($W->create('Widget_Title', $title, 3));
        }


        return $div;
    }

    /**
     * Create a list of links
     *
     * Format for links :
     * array( 0 => array(
     * 		'url' 		=> '',
     * 		'title' 	=> '',
     * 		'classname 	=> ''	))
     *
     * @return	Widget_Frame
     */
    public function html_links($links) {

        $W = bab_Widgets();
        $frame = $W->create('Widget_Frame')->addClass('html_links');


        foreach($links as $arr) {

            $link = $W->create('Widget_Link', $arr['title'], $arr['url'])
                ->addClass('ui-default-state')
                ->setItem($W->Label($arr['title']));

            if (isset($arr['classname'])) {
                $link->addClass($arr['classname']);
            }

            $frame->addItem($link);
        }

        return $frame;
    }



    /**
     * Create a table for vertical visualisation of a list of label, value
     * may be static
     *
     * Format for list :
     * array( 0 => array(
     * 		'title' => '',
     * 		'value' => '',
     * 		'classname' => ''))
     *
     * classname is optional
     *
     * @param	array 	$list
     * @param	string	[$caption]
     * @return 	string	html
     */
    public function html_trList($list, $caption = '') {

        if (empty($list)) {
            return '';
        }

        $W = bab_Widgets();
        $layout = $W->create('Widget_PairVBoxLayout');

        if (!empty($caption)) {
            $layout->setTitle($caption);
        }


        $frame = $W->create('Widget_Frame', null, $layout);

        foreach($list as $arr) {

            $row = $W->create('Widget_Pair')
                ->setFirst($W->create('Widget_Label', $arr['title']))
                ->setSecond($W->create('Widget_Label', $arr['value']));

            if (!empty($arr['classname'])) {
                $row->addClass($arr['classname']);
            }

            $frame->addItem($row);
        }

        return $frame;
    }







    public function display(Widget_Canvas $canvas)
    {
        $time_start = microtime(true);

        $this->_body_classes = implode(' ',$this->getClasses());

        $this->content = parent::display($canvas);

        $this->bab_debug = bab_getDebug();

        if (isset($_COOKIE['bab_debug'])) {
            $time_end = microtime(true);
            $this->debug_time = $time_end - $time_start;
        }

        return $canvas->BusinessApplicationPage($this);
    }


    /**
     * Display with html canvas
     */
    public function displayHtml() {
        $W = bab_Widgets();
        $this->pageEcho($W->HtmlCanvas());
    }





    /**
     * display error message and exit
     * @param	string	$msgerror
     */
    public function kill($msgerror) {
        $this->addError($msgerror);
        $this->displayHtml();
    }

}
