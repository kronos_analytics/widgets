<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__).'/inputwidget.class.php';



/**
 * Constructs a Widget_RadioSet.
 *
 * @param string		$id			The item unique id.
 * @return Widget_RadioSet
 */
function Widget_RadioSet($id = null)
{
    return new Widget_RadioSet($id);
}


/**
 * A Widget_RadioSet is a widget that let the user chose an item in a list
 * the widget can be used by adding a RadioSet in page and call the setOption method
 * or displaying the Widget_Radio individually and call the setRadioSet method
 *
 * @see Widget_Radio
 */
class Widget_RadioSet extends Widget_InputWidget implements Widget_Displayable_Interface
{
    private $_options 				= array();
    private $horizontalview 		= false;
    private $horizontalviewforced	= false;
    private $horizontalviewspace 	= 10;
    private $horizontalviewunit 	= 'px';

    private $FlexLayout = false;

    private $attachedRadios = array();

    /**
     * name path to use in radio widget associated to this radioSet
     * @var array
     */
    private $radioNamePath		= null;


    /**
     * @param string $id			The item unique id.
     * @return Widget_RadioSet
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
    }



    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-radioset';
        return $classes;
    }

    /**
     * Set options
     * @param	array 	$option
     * @return Widget_RadioSet
     */
    public function setOptions($options) {

        $this->_options = $options;
        return $this;
    }

    /**
     *
     * @param string $value
     * @param string $label
     * @return Widget_RadioSet
     */
    public function addOption($value, $label) {
        $this->_options[$value] = $label;
        return $this;
    }


    /**
     * Get options without optgroups
     * @return 	array
     */
    public function getOptions() {
        return $this->_options;
    }


    /**
     * Set name path to use in radio widgets associated to this radioSet
     * @param array $path
     * @return Widget_RadioSet
     */
    public function setRadioNamePath(Array $path)
    {
        $this->radioNamePath = $path;
        return $this;
    }


    /**
     * (non-PHPdoc)
     * @see Widget_InputWidget::getFullName()
     */
    public function getFullName()
    {
        if (null !== $this->radioNamePath) {
            return $this->radioNamePath;
        }

        return parent::getFullName();
    }



    /**
     * specify if the radio buttons are positioned vertically
     * @param	bool	$val
     * @return  Widget_RadioSet
     */
    public function setHorizontalView($val = true, $forced = false, $hspace = 10, $unit = 'px') {
        $this->horizontalview = $val;
        $this->horizontalviewforced = $forced;
        $this->horizontalviewspace = $hspace;
        $this->horizontalviewunit = $unit;
        return $this;
    }



    /**
     * specify if the radio buttons are positioned with a flex container
     *
     * @param boolean $enable
     * @param string $wrap
     * @param string $justifyContent
     * @param string $alignItems
     * @param string $direction
     *
     * @return Widget_RadioSet
     */
    public function setFlexView($enable = true, $wrap = false, $justifyContent = 'between', $alignItems = 'center', $direction = 'row') {
        $W = bab_Widgets();

        if ($enable) {
            $this->FlexLayout = $W->FlexLayout(null, $wrap, $justifyContent, $alignItems, $direction);
        } else {
            $this->FlexLayout = false;
        }

        return $this;
    }



    /**
     *
     * @param widget_Radio $radio
     * @return Widget_RadioSet
     */
    public function attachRadio(widget_Radio $radio)
    {
        $this->attachedRadios[] = '#' . $radio->getId();
        return $this;
    }


    /**
     * (non-PHPdoc)
     * @see Widget_Displayable_Interface::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        if ($this->isDisplayMode()) {

            $classes = $this->getClasses();
            $classes[] = 'widget-displaymode';

            $value = '';
            $opt = $this->getOptions();
            if (isset($opt[$this->getValue()])) {
                $value = $opt[$this->getValue()];
            }

            return $canvas->richtext(
                    $this->getId(),
                    $classes,
                    $value,
                    BAB_HTML_ALL ^ BAB_HTML_P,
                    $this->getCanvasOptions()
            );


        } else {

            if (!empty($this->attachedRadios)) {
                $this->setMetadata('attachedRadios', implode(',', $this->attachedRadios));
            }

            $display = array();

            $i = 0;
            foreach($this->getOptions() as $value => $text) {

                $checked = ((string) $value) === ((string) $this->getValue());

                $input = $canvas->radioInput($this->getId().$i, array(), $this->getFullName(), $value, $checked, $this->isDisabled(), array());
                $label = $canvas->label(null, array(), $text, $this->getId().$i);

                $i++;

                $display[] = $input.''.$label;
            }

            if ($this->horizontalview && $this->horizontalviewforced) {
                $html = $canvas->hbox($this->getId(), $this->getClasses(), $display, Widget_Item::Options()->horizontalSpacing($this->horizontalviewspace, $this->horizontalviewunit));
            } elseif ($this->horizontalview && !$this->horizontalviewforced) {
                $html = $canvas->hbox($this->getId(), $this->getClasses(), $display, Widget_Item::Options()->horizontalSpacing($this->horizontalviewspace, $this->horizontalviewunit));
            } elseif ($this->FlexLayout) {//NEW FLEX LAYOUT
                $classes = $this->FlexLayout->getDisplayClasses();
                $RSClass = $this->getClasses();
                foreach($RSClass as $class) {
                    $classes[] = $class;
                }

                $html = $canvas->flex(
                    $this->getId(),
                    $classes,
                    $display
                );
            } else {
                $html = $canvas->vbox($this->getId(), $this->getClasses(), $display);
            }

            return $html.$canvas->metadata($this->getId(), $this->getMetadata());
        }
    }

}