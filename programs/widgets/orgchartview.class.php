<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/widget.class.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/tree.php';
include_once $GLOBALS['babInstallPath'] . 'utilit/orgchart.php';
require_once dirname(__FILE__) . '/simpletreeview.class.php';


/**
 * Constructs a Widget_OrgchartView.
 *
 * @param Widget_Layout $layout     The layout that will manage how widgets are displayed in this container.
 * @param string        $id         The item unique id.
 * @return Widget_OrgchartView
 */
function Widget_OrgchartView($id = null, Widget_Layout $layout = null)
{
    return new Widget_OrgchartView($id, $layout);
}



/**
 * Widget_OrgchartView wrapper to bab_OrgChart
 */
class Widget_OrgchartView extends Widget_SimpleTreeView
{
    private $realId = '';


    /**
     * @var widget_Action
     */
    private $saveDefaultStateAction = null;

    /**
     * @var widget_Action
     */
    private $saveStateAction = null;

    /**
     * @param string $id            The item unique id.
     * @return Widget_OrgchartView
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
        $this->constructEmbeddedItem();

        $path = 'orgchart/' . $this->getRealId() . '/';

        $controller = bab_CoreController();

        $saveDefaultStateAction = $controller->Configuration()->setGlobalValues($path);
        $this->setSaveDefaultStateAction($saveDefaultStateAction);

        $restoreDefaultStateAction = $controller->Configuration()->delete();
        $this->setRestoreDefaultStateAction($restoreDefaultStateAction);

        $saveStateAction = $controller->Configuration()->setValues($path);
        $this->setSaveStateAction($saveStateAction);

        $configuration = new bab_Configuration();

        $openNodes = $configuration->get($path . 'openNodes');
        $openMembers = $configuration->get($path . 'openMembers');
        $zoomFactor = $configuration->get($path . 'zoomFactor');
        $verticalThreshold = $configuration->get($path . 'verticalThreshold');
        $relativeThreshold = $configuration->get($path . 'relativeThreshold');

        $this->setOpenNodes(explode(',', $openNodes));
        $this->setOpenMembers(explode(',', $openMembers));
        $this->setZoomFactor($zoomFactor);
        $this->setVerticalThreshold($verticalThreshold);
        $this->setRelativeThreshold($relativeThreshold);
    }


    public function setId($id)
    {
        $this->realId = $id;
        parent::setId($id . '_container');
    }

    public function getCookieId()
    {
        return $this->getEmbeddedItemId();
    }

    /**
     * Returns the item id.
     *
     * @return string
     */
    public function getRealId()
    {
        return $this->realId;
    }

    /**
     * @return string
     */
    protected function getEmbeddedItemId()
    {
        return $this->getRealId();
    }

    /**
     *
     */
    protected function constructEmbeddedItem()
    {
        $this->embeddedItem = new bab_OrgChart($this->getEmbeddedItemId());
        $this->embeddedItem->_templateFile = 'addons/widgets/simpletreeview.html';
    }



    public function setAdminMode($admin = true)
    {
        $this->embeddedItem->_adminMode = $this->embeddedItem->t_adminMode = $admin;
        $this->setMetadata('adminMode', $admin);
    }


    /**
     * @param string $id            A unique element id in the orgchartview.
     * @param string $type          Will be used as a css class to style the element.
     * @param string $title         The title (label) of the node.
     * @param string $description   An additional description that will appear as a tooltip.
     * @param string $link          A link when clicking the node title.
     *
     * @return Widget_OrgchartViewElement
     */
    public function createElement($id, $type = '', $title = '', $description = '', $link = '')
    {
        $element = new Widget_OrgchartViewElement($id, $type, $title, $description, $link);
        return $element;
    }

    /**
     * @return string
     */
    protected function getStyleSheet()
    {
        return bab_getAddonInfosInstance('widgets')->getStylePath().'widgets.orgchart.css';
    }


    /**
     * (non-PHPdoc)
     * @see Widget_SimpleTreeView::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-orgchartview';
        return $classes;
    }


    /**
     * Defines initially open nodes.
     *
     * @param array $nodeIds
     * @return self
     */
    public function setOpenNodes(array $nodeIds = null)
    {
        if (isset($nodeIds)) {
            $this->embeddedItem->setOpenNodes($nodeIds);
        }
        $this->setMetadata('openNodes', $nodeIds);
        return $this;
    }



    /**
     * Defines initially open members nodes.
     *
     * @param array $nodeIds
     * @return self
     */
    public function setOpenMembers(array $nodeIds = null)
    {
        if (isset($nodeIds)) {
            $this->embeddedItem->setOpenMembers($nodeIds);
        }
        $this->setMetadata('openMembers', $nodeIds);
        return $this;
    }


    /**
     * Sets the initial zoom factor.
     *
     * @param float $zoomFactor
     * @return self
     */
    public function setZoomFactor($zoomFactor = 1.0)
    {
        $this->embeddedItem->setZoomFactor($zoomFactor);
        $this->setMetadata('zoomFactor', $zoomFactor);
        return $this;
    }


    /**
     * Sets the depth level from which the org chart branches are displayed vertically.
     *
     * @param int $threshold
     * @return self
     */
    public function setVerticalThreshold($threshold)
    {
        $this->embeddedItem->setVerticalThreshold($threshold);
        $this->setMetadata('verticalThreshold', $threshold);
        return $this;
    }


    /**
     * Gets the depth level from which the org chart branches are displayed vertically.
     *
     * @return int|null
     */
    public function getVerticalThreshold()
    {
        return $this->getMetadata('verticalThreshold');
    }


    /**
     * Sets if the depth level is relative.
     *
     * @param bool $relative
     * @return self
     */
    public function setRelativeThreshold($relative)
    {
        $this->setMetadata('relativeThreshold', $relative);
        return $this;
    }


    /**
     * Gets if the vertical threshold depth level is relative to the starting level.
     *
     * @return bool
     */
    public function hasRelativeThreshold()
    {
        return (bool)$this->getMetadata('relativeThreshold');
    }


    /**
     *
     * @param widget_Action $action
     * @return self
     */
    public function setSaveDefaultStateAction(widget_Action $action)
    {
        $this->saveDefaultStateAction = $action;
        $this->setMetadata('saveDefaultStateAction', $action->url());
        return $this;
    }


    /**
     *
     * @return widget_Action
     */
    public function getSaveDefaultStateAction()
    {
        return $this->saveDefaultStateAction;
    }


    /**
     *
     * @param widget_Action $action
     * @return Widget_OrgchartView
     */
    public function setRestoreDefaultStateAction(widget_Action $action)
    {
        $this->setMetadata('restoreDefaultStateAction', $action->url());
        return $this;
    }


    /**
     *
     * @param widget_Action $action
     * @return Widget_OrgchartView
     */
    public function setSaveStateAction(widget_Action $action)
    {
        $this->saveStateAction = $action;
        $this->setMetadata('saveStateAction', $action->url());
        return $this;
    }


    /**
     *
     * @return widget_Action
     */
    public function getSaveStateAction()
    {
        return $this->saveStateAction;
    }
}


$GLOBALS['Widget_OrgchartViewElementCanvas'] = bab_Widgets()->HtmlCanvas();

class Widget_OrgchartViewElement extends bab_OrgChartElement
{


    /**
     *
     * @param Widget_Displayable_Interface $item
     * @return Widget_OrgchartViewElement
     */
    public function setItem(Widget_Displayable_Interface $item)
    {
        global $Widget_OrgchartViewElementCanvas;

        $html = $item->display($Widget_OrgchartViewElementCanvas);

        $this->_icon = '_item_';
        $this->_title = $html;

        return $this;
    }
}
