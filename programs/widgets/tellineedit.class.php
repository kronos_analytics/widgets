<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/lineedit.class.php';


/**
 * Constructs a Widget_TelLineEdit.
 *
 * @param string        $id         The item unique id.
 * @return Widget_TelLineEdit
 */
function Widget_TelLineEdit($id = null)
{
    return new Widget_TelLineEdit($id);
}


/**
 * A Widget_TelLineEdit is a widget that lets the user enter a telephone number.
 */
class Widget_TelLineEdit extends Widget_LineEdit implements Widget_Displayable_Interface
{
    const BUSINESS      = 'bus';
    const HOME          = 'hom';
    const MOBILE        = 'mob';
    const FAX           = 'fax';


    /**
     * @var string
     */
    private $type;



    /**
     * @param string $id            The item unique id.
     * @return Widget_LineEdit
     */
    public function __construct($id = null)
    {
        parent::__construct($id);

        $this->setSubmitMessage(widget_translate('The telephone number is not valid.'));
        $this->setTelType(self::BUSINESS);
    }


    /**
     * Sets the telephone number type.
     * Can be one from the predefined constants:
     *  Widget_TelLineEdit::BUSINESS
     *  Widget_TelLineEdit::HOME
     *  Widget_TelLineEdit::MOBILE
     *  Widget_TelLineEdit::FAX
     *
     * @param   string  $type
     */
    public function setTelType($type)
    {
        $this->type = $type;
        return $this;
    }


    /**
     * Message displayed on form submit if there is a tel line edit widget with an invalid tel number
     * @return Widget_TelLineEdit
     */
    public function setSubmitMessage($str)
    {
        $this->setMetadata('submitMessage', $str);
        return $this;
    }



    /**
     * (non-PHPdoc)
     * @see Widget_LineEdit::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-tellineedit';

        if (isset($this->type)) {
            $classes[] = 'widget-tellineedit-'.$this->type;
        }

        return $classes;
    }


    /**
     * (non-PHPdoc)
     * @see Widget_LineEdit::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        if (!$this->isMandatory()) {
            $message = $this->getMetadata('submitMessage');
            $message .= " \n".widget_translate('Do you want to submit the form anyway?');
            $this->setSubmitMessage($message);
        }

        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        return parent::display($canvas)
            . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.tellineedit.jquery.js');
    }
}
