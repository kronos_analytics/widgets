<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

//require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once dirname(__FILE__) . '/containerwidget.class.php';



/**
 * Constructs a Widget_Tab.
 *
 * @param string        $id         The item unique id.
 * @param string        $label
 * @param Widget_Layout $layout     The layout that will manage how widgets are displayed in this container.
 * @return Widget_Tab
 */
function Widget_Tab($id = null, $label = null, Widget_Layout $layout = null)
{
    return new Widget_Tab($id, $layout);
}



/**
 * Widget_Tab
 *
 */
class Widget_Tab extends Widget_ContainerWidget implements Widget_Displayable_Interface
{
    /**
     * @var string
     */
    private $label = null;

    /**
     * @var string
     */
    private $_title = null;

    /**
     * @var Widget_Action
     */
    private $action = null;

    /**
     * @var Widget_CanvasOptions
     */
    private $tabPanelCanvasOptions = null;


    /**
     * used to generate unique ids.
     * @var int
     */
    private static $idCounter = 1;



    /**
     * @param string        $id         The item unique id.
     * @param string        $label
     * @param Widget_Layout $layout     The layout that will manage how widgets are displayed in this container.
     * @return Widget_Frame
     */
    public function __construct($id = null, $label = null, Widget_Displayable_Interface $layout = null)
    {
        if (null === $layout) {
            require_once FUNC_WIDGETS_PHP_PATH . 'vboxlayout.class.php';
            $layout = new Widget_VBoxLayout();
        }

        parent::__construct($id, $layout);

        $this->setLabel($label);
    }



    /**
     * Generates and return a unique id for the current page.
     * use a counter only affected by other tabs
     */
    protected function createId()
    {
        return strtolower(get_class($this)) . self::$idCounter++;
    }


    /**
     * Add an action to execute when clicked
     * @param Widget_Action $action
     * @return Widget_Tab
     */
    public function setAction(Widget_Action $action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return null|Widget_Action
     */
    public function getAction()
    {
        return $this->action;
    }


    /**
     * Set tab label
     * @param   string  $str
     * @return Widget_Tab
     */
    public function setLabel($str)
    {
        $this->label = $str;
        return $this;
    }

    /**
     * get tab label
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }


    /**
     * Set tab title, replace label for title in frame if defined
     * @param   string  $str
     * @return Widget_Tab
     */
    public function setTitle($str)
    {
        $this->_title = $str;
        return $this;
    }

    /**
     * disable title in tab frame
     * @return Widget_Tab
     */
    public function noTitle()
    {
        $this->_title = false;
        return $this;
    }

    /**
     * get tab title
     * @return string
     */
    public function getTitle()
    {
        return null !== $this->_title ? $this->_title : $this->label;
    }


    /**
     * (non-PHPdoc)
     * @see Widget_Widget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-tab';
        $classes[] = 'ui-tabs-nav-item';


        if ($this === $this->getParent()->getSelectedTab()) {
            $classes[] = 'ui-tabs-selected';
            $classes[] = 'widget-tab-selected';
        }

        return $classes;
    }


    /**
     * get classes for tab panel
     */
    public function getTabPanelClasses()
    {

        $classes = parent::getClasses();
        $classes[] = 'ui-tabs-panel';


        return $classes;
    }


    /**
     * Set tab panel canvas options
     * @param   Widget_CanvasOptions    $options
     * @return  Widget_Tab
     */
    public function setTabPanelCanvasOptions(Widget_CanvasOptions $options)
    {
        $this->tabPanelCanvasOptions = $options;
        return $this;
    }



    /**
     * (non-PHPdoc)
     * @see Widget_Displayable_Interface::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $items = array();

        if (false === $this->getParent()->displayAllTabs() && $this !== $this->getParent()->getSelectedTab()) {
            return '';
        }

        $items[] = $canvas->anchor($this->getId());

        $title = $this->getTitle();
        if ($title instanceof Widget_Item) {
            $items[] = $title->display($canvas);
        } elseif ($title) {
            $items[] = $canvas->h('', array('widget-tab-panel-header'), $this->getTitle(), 4);
        }

        $items[] = $this->getLayout();


        $tab = $canvas->div(
            $this->getId(),
            $this->getTabPanelClasses(),
            $items,
            $this->tabPanelCanvasOptions
        )
        . $canvas->metadata($this->getId(), $this->getMetadata());
        return $tab;
    }
}
