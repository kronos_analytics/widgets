<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';



/**
 * Canvas options
 *
 * Object linked to an item to describe style attributes of an item
 */
class Widget_CanvasOptions
{
	private $textColor = null;
	private $backgroundColor = null;
	private $backgroundImage = null;

	private $left = null;
	private $top = null;
	private $width = null;
	private $height = null;
	private $hspacing = null;
	private $vspacing = null;
	private $minWidth = null;
	private $minHeight = null;

	private $leftUnit = 'px';
	private $topUnit = 'px';
	private $widthUnit = 'px';
	private $heightUnit = 'px';
	private $hspacingUnit = 'px';
	private $vspacingUnit = 'px';
	private $minWidthUnit = 'px';
	private $minHeightUnit = 'px';


	private $valign = null;
	private $halign = null;

	private $rowGap = null;
	private $rowGapUnit = 'px';
	private $colGap = null;
	private $colGapUnit = 'px';

	private $rowTemplate = null;
	private $colTemplate = null;
	private $areaTemplate = null;

	private $manual = null;



//	public function __get($name)
//	{
//		if (method_exists($this, $name)) {
//			return $this->$name();
//		}
//		throw new UnexpectedValueException();
//	}
//
//
//	public function __set($name, $value)
//	{
//		if (method_exists($this, $name)) {
//			return $this->$name($value);
//		}
//		throw new UnexpectedValueException();
//	}


	/**
	 * Sets or returns the left coordinate of the item.
	 * If the unit is not specified, the previously defined left unit is
	 * used, or 'px' if the left unit was never defined.
	 *
	 * @param float $left
	 * @param string $unit		'px', 'pt', 'em' or '%'.
	 * @return $this|float
	 */
	public function left($left = null, $unit = null)
	{
		if ($left === null) {
			return $this->left;
		}
		$this->left = $left;
		if ($unit !== null) {
			$this->leftUnit($unit);
		}
		return $this;
	}


	/**
	 * Sets or returns the left coordinate unit of the item.
	 *
	 * @param string $unit		'px', 'pt', 'em' or '%'.
	 * @return $this|string
	 */
	public function leftUnit($unit = null)
	{
		if ($unit === null) {
			return $this->leftUnit;
		}
		$this->leftUnit = $unit;
		return $this;
	}

	/**
	 * Sets or returns the top coordinate of the item.
	 * If the unit is not specified, the previously defined top unit is
	 * used, or 'px' if the top unit was never defined.
	 *
	 * @param float $top
	 * @param string $unit		'px', 'pt', 'em' or '%'.
	 * @return $this|float
	 */
	public function top($top = null, $unit = null)
	{
		if ($top === null) {
			return $this->top;
		}
		if ($unit !== null) {
			$this->topUnit($unit);
		}
		$this->top = $top;
		return $this;
	}


	/**
	 * Sets or returns the top coordinate unit of the item.
	 *
	 * @param string $unit		'px', 'pt', 'em' or '%'.
	 * @return $this|string
	 */
	public function topUnit($unit = null)
	{
		if ($unit === null) {
			return $this->topUnit;
		}
		$this->topUnit = $unit;
		return $this;
	}


	/**
	 * Sets or returns the width of the item.
	 * If the unit is not specified, the previously defined width unit is
	 * used, or 'px' if the width unit was never defined.
	 *
	 * @param float $width
	 * @param string $unit		'px', 'pt', 'em' or '%'.
	 * @return $this|float
	 */
	public function width($width = null, $unit = null)
	{
		if (!isset($width)) {
			return $this->width;
		}
		$this->width = $width;
		if (isset($unit)) {
			$this->widthUnit = $unit;
		}
		return $this;
	}

	/**
	 * Sets or returns the minimum width of the item.
	 * If the unit is not specified, the previously defined width unit is
	 * used, or 'px' if the width unit was never defined.
	 *
	 * @param float $width
	 * @param string $unit		'px', 'pt', 'em' or '%'.
	 * @return $this|float
	 */
	public function minWidth($width = null, $unit = null)
	{
		if (!isset($width)) {
			return $this->minWidth;
		}

		$this->minWidth = $width;
		if (isset($unit)) {
			$this->minWidthUnit = $unit;
		}
		return $this;
	}


	/**
	 * Sets or returns the minimum height of the item.
	 * If the unit is not specified, the previously defined height unit is
	 * used, or 'px' if the height unit was never defined.
	 *
	 * @param float $height
	 * @param string $unit		'px', 'pt', 'em' or '%'.
	 * @return $this|float
	 */
	public function minHeight($height = null, $unit = null)
	{
		if (!isset($height)) {
			return $this->minHeight;
		}

		$this->minHeight = $height;
		if (isset($unit)) {
			$this->minHeightUnit = $unit;
		}
		return $this;
	}


	/**
	 * Sets or returns the width unit of the item.
	 *
	 * @param string $unit		'px', 'pt', 'em' or '%'.
	 * @return $this|string
	 */
	public function widthUnit($unit = null)
	{
		if ($unit === null) {
			return $this->widthUnit;
		}
		$this->widthUnit = $unit;
		return $this;
	}


	/**
	 * Sets or returns the minimum width unit of the item.
	 *
	 * @param string $unit		'px', 'pt', 'em' or '%'.
	 * @return $this|string
	 */
	public function minWidthUnit($unit = null)
	{
		if ($unit === null) {
			return $this->minWidthUnit;
		}
		$this->minWidthUnit = $unit;
		return $this;
	}


	/**
	 * Sets or returns the minimum height unit of the item.
	 *
	 * @param string $unit		'px', 'pt', 'em' or '%'.
	 * @return $this|string
	 */
	public function minHeightUnit($unit = null)
	{
		if ($unit === null) {
			return $this->minHeightUnit;
		}
		$this->minHeightUnit = $unit;
		return $this;
	}


	/**
	 * Sets or returns the height of the item.
	 * If the unit is not specified, the previously defined height unit is
	 * used, or 'px' if the height unit was never defined.
	 *
	 * @param float $height
	 * @param string $unit		'px', 'pt', 'em' or '%'.
	 * @return $this|float
	 */
	public function height($height = null, $unit = null)
	{
		if ($height === null) {
			return $this->height;
		}
		$this->height = $height;
		if ($unit !== null) {
			$this->heightUnit($unit);
		}
		return $this;
	}


	/**
	 * Sets or returns the height unit of the item.
	 *
	 * @param string $unit		'px', 'pt', 'em' or '%'.
	 * @return $this|string
	 */
	public function heightUnit($unit = null)
	{
		if ($unit === null) {
			return $this->heightUnit;
		}
		$this->heightUnit = $unit;
		return $this;
	}


	/**
	 * Sets or returns the horizontal spacing of the item.
	 * If the unit is not specified, the previously defined horizontal spacing unit is
	 * used, or 'px' if the horizontal spacing unit was never defined.
	 *
	 * @param float $spacing
	 * @param string $unit		'px', 'pt', 'em' or '%'.
	 * @return $this|float
	 */
	public function horizontalSpacing($spacing = null, $unit = null)
	{
		if (null === $spacing) {
			return $this->hspacing;
		}
		$this->hspacing = $spacing;
		if ($unit !== null) {
			$this->horizontalSpacingUnit($unit);
		}
		return $this;
	}


	/**
	 * Sets or returns the horizontal spacing unit of the item.
	 *
	 * @param string $unit		'px', 'pt', 'em' or '%'.
	 * @return $this|string
	 */
	public function horizontalSpacingUnit($unit = null)
	{
		if ($unit === null) {
			return $this->hspacingUnit;
		}
		$this->hspacingUnit = $unit;
		return $this;
	}


	/**
	 * Sets or returns the vertical spacing of the item.
	 * If the unit is not specified, the previously defined vertical spacing unit is
	 * used, or 'px' if the vertical spacing unit was never defined.
	 *
	 * @param float $spacing
	 * @param string $unit		'px', 'pt', 'em' or '%'.
	 * @return $this|float
	 */
	public function verticalSpacing($spacing = null, $unit = null)
	{
		if (null === $spacing) {
			return $this->vspacing;
		}
		$this->vspacing = $spacing;
		if ($unit !== null) {
			$this->verticalSpacingUnit($unit);
		}
		return $this;
	}


	/**
	 * Sets or returns the vertical spacing unit of the item.
	 *
	 * @param string $unit		'px', 'pt', 'em' or '%'.
	 * @return $this|string
	 */
	public function verticalSpacingUnit($unit = null)
	{
		if ($unit === null) {
			return $this->vspacingUnit;
		}
		$this->vspacingUnit = $unit;
		return $this;
	}


	/**
	 * Sets or returns background color of the item.
	 *
	 * @param string $backgroundColor		A valid css color (eg. #FFFFFF).
	 * @return $this|string
	 */
	public function backgroundColor($backgroundColor = null)
	{
		if ($backgroundColor === null) {
			return $this->backgroundColor;
		}
		$this->backgroundColor = $backgroundColor;
		return $this;
	}


	/**
	 * Sets or returns background image of the item.
	 *
	 * @param string $backgroundImage		An url to an image.
	 * @return $this|string
	 */
	public function backgroundImage($backgroundImage = null)
	{
		if ($backgroundImage === null) {
			return $this->backgroundImage;
		}
		$this->backgroundImage = $backgroundImage;
		return $this;
	}


	/**
	 * Sets or returns text color of the item.
	 *
	 * @param string $textColor		A valid css color (eg. #000000).
	 * @return $this|string
	 */
	public function textColor($textColor = null)
	{
		if ($textColor === null) {
			return $this->textColor;
		}
		$this->textColor = $textColor;
		return $this;
	}


	/**
	 * Sets or returns vertical align of the item.
	 *
	 * @param string $align		A valid css vertical alignment.
	 * @return $this|string
	 */
	public function verticalAlign($align = null)
	{
		if (null === $align) {
			return $this->valign;
		}
		$this->valign = $align;
		return $this;
	}


	/**
	 * Sets or returns horizontal align of the item.
	 *
	 * @param string $align		A valid css horizontal alignment.
	 * @return $this|string
	 */
	public function horizontalAlign($align = null)
	{
		if (null === $align) {
			return $this->halign;
		}
		$this->halign = $align;
		return $this;
	}


	/**
	 * Sets or returns the vertical gap of the items.
	 * If the unit is not specified, the previously defined vertical spacing unit is
	 * used, or 'px' if the vertical spacing unit was never defined.
	 *
	 * @param float $spacing
	 * @param string $unit		'px', 'pt', 'em' or '%'.
	 * @return $this|float
	 */
	public function rowGap($rowGap = null, $unit = null)
	{
	    if (null === $rowGap) {
	        return $this->rowGap;
	    }
	    $this->rowGap = $rowGap;
	    if ($unit !== null) {
	        $this->rowGapUnit($unit);
	    }
	    return $this;
	}


	/**
	 * Sets or returns the vertical spacing unit of the item.
	 *
	 * @param string $unit		'px', 'pt', 'em' or '%'.
	 * @return $this|string
	 */
	public function rowGapUnit($unit = null)
	{
	    if ($unit === null) {
	        return $this->rowGapUnit;
	    }
	    $this->rowGapUnit = $unit;
	    return $this;
	}


	/**
	 * Sets or returns the vertical gap of the items.
	 * If the unit is not specified, the previously defined vertical spacing unit is
	 * used, or 'px' if the vertical spacing unit was never defined.
	 *
	 * @param float $spacing
	 * @param string $unit		'px', 'pt', 'em' or '%'.
	 * @return $this|float
	 */
	public function colGap($colGap = null, $unit = null)
	{
	    if (null === $colGap) {
	        return $this->colGap;
	    }
	    $this->colGap = $colGap;
	    if ($unit !== null) {
	        $this->colGapUnit($unit);
	    }
	    return $this;
	}


	/**
	 * Sets or returns the vertical spacing unit of the item.
	 *
	 * @param string $unit		'px', 'pt', 'em' or '%'.
	 * @return $this|string
	 */
	public function colGapUnit($unit = null)
	{
	    if ($unit === null) {
	        return $this->colGapUnit;
	    }
	    $this->colGapUnit = $unit;
	    return $this;
	}

	/**
	 * define the area template of a grid
	 *
	 * @param array $css
	 * @return string|Widget_CanvasOptions
	 */
	public function areaTemplate(array $css = null)
	{
	    if ($css === null) {
	        return $this->areaTemplate;
	    }
	    $this->areaTemplate = $css;
	    return $this;
	}



	/**
	 * define the row template of a grid
	 *
	 * @param array $css
	 * @return string|Widget_CanvasOptions
	 */
	public function rowTemplate(string $css = null)
	{
	    if ($css === null) {
	        return $this->rowTemplate;
	    }
	    $this->rowTemplate = $css;
	    return $this;
	}



	/**
	 * define the col template of a grid
	 *
	 * @param array $css
	 * @return string|Widget_CanvasOptions
	 */
	public function colTemplate(string $css = null)
	{
	    if ($css === null) {
	        return $this->colTemplate;
	    }
	    $this->colTemplate = $css;
	    return $this;
	}

	/**
	 * Use to cumulate options value to define CSS with addAttribute style
	 *
	 * @param string $value
	 * @return string|Widget_CanvasOptions
	 */
	public function manual(string $value = null)
	{
	    if ($value === null) {
	        return $this->manual;
	    }
	    $this->manual = $value;
	    return $this;
	}
}
