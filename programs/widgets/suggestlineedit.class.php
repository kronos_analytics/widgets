<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/urlincl.php';
require_once dirname(__FILE__) . '/lineedit.class.php';
require_once dirname(__FILE__) . '/suggestedit.class.php';

/**
 * Constructs a Widget_SuggestLineEdit.
 *
 * @param string		$id			The item unique id.
 * @return Widget_SuggestLineEdit
 */
function Widget_SuggestLineEdit($id = null)
{
	return new Widget_SuggestLineEdit($id);
}


/**
 * A Widget_SuggestLineEdit is a widget that let the user enter a single line of text and proposes suggestions.
 */
class Widget_SuggestLineEdit extends Widget_SuggestEdit implements Widget_Displayable_Interface, Widget_LineEdit_Interface
{
    /**
     * @var int
     */
	private $size;

    /**
     * @var int
     */
	private $maxSize;

    /**
     * @var bool
     */
	private $readOnly = false;


	/**
	 * @param string $id			The item unique id.
	 */
	public function __construct($id = null)
	{
		parent::__construct($id);
		$this->size = null;
		$this->maxSize = null;
	}


	/**
	 * Sets the visible size of the line edit .
	 *
	 * @param int $size
	 * @return self
	 */
	public function setSize($size)
	{
		assert('is_int($size); /* The "size" parameter must be an integer */');
		$this->size = $size;
		return $this;
	}


	/**
	 * Returns the visible size (in characters) of the line edit.
	 *
	 * @return int
	 */
	public function getSize()
	{
		return $this->size;
	}


	/**
	 * Sets the readonly parameter.
	 *
	 * @param bool $value
	 * @return self
	 */
	public function setReadOnly($value)
	{
		assert('is_bool($value); /* The "value" parameter must be a boolean */');
		$this->readOnly = $value;
		return $this;
	}


	/**
	 * Returns the value of the readonly parameter.
	 *
	 * @return bool
	 */
	public function isReadOnly()
	{
		return $this->readOnly;
	}

	/**
	 * @deprecated
	 * @see Widget_SuggestLineEdit::isReadOnly()
	 * @return bool
	 */
	public function getReadOnly()
	{
	    return $this->readOnly;
	}


	/**
	 * Sets the maximum input size (in characters) of the line edit.
	 *
	 * @param int $maxSize
	 * @return self
	 */
	public function setMaxSize($maxSize)
	{
		assert('is_int($maxSize);  /* The "maxSize" parameter must be an integer */');
		$this->maxSize = $maxSize;
		return $this;
	}

	/**
	 * Returns the maximum input size (in characters) of the line edit.
	 *
	 * @return int
	 */
	public function getMaxSize()
	{
		return $this->maxSize;
	}




	/**
	 * (non-PHPdoc)
	 * @see programs/widgets/Widget_LineEdit#getClasses()
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-suggestlineedit';
		$classes[] = 'widget-lineedit';
		return $classes;
	}


	/**
	 * (non-PHPdoc)
	 * @see programs/widgets/Widget_LineEdit#display($canvas)
	 */
	public function display(Widget_Canvas $canvas)
	{
		$output = parent::display($canvas);

		$maxSize = $this->getMaxSize();
		if (isset($maxSize)) {
			$value = substr($this->getValue(), 0, $maxSize);
		} else {
			$value = $this->getValue();
		}


		if ($this->isDisplayMode()) {

			$classes = $this->getClasses();
			$classes[] = 'widget-displaymode';

			if ($this->getSize()) {
				$options = $this->getCanvasOptions();
				if (is_null($options)) {
					$options = $this->Options();
				}
				$options->minWidth(1.4 * $this->getSize(), 'ex');
				$this->setCanvasOptions($options);
			}

			$output .= $canvas->richtext(
				$this->getId(),
				$classes,
				$this->getValue(),
				BAB_HTML_ALL ^ BAB_HTML_P,
				$this->getCanvasOptions()
			);

		} else {

			$output .= $canvas->lineInput(
				$this->getId(),
				$this->getClasses(),
				$this->getFullName(),
				$value,
				$this->getSize(),
				$this->getMaxSize(),
				$this->isDisabled(),
				$this->isReadOnly(),
			    false,
			    'text',
			    $this->getPlaceHolder(),
			    $this->getCanvasOptions(),
			    $this->getTitle(),
			    $this->getAttributes()
			)
			. $canvas->metadata($this->getId(), $this->getMetadata());

		}

		return $output;
	}
}
