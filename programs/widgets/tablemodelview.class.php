<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/tableview.class.php';
require_once dirname(__FILE__) . '/submitbutton.class.php';


/**
 * @param string | ORM_Field $field
 * @param string $description
 *
 * @return widget_TableModelViewColumn
 */
function widget_TableModelViewColumn($field, $description = null)
{
    if (null === $field) {
        return null;
    }

    return new widget_TableModelViewColumn($field, $description);
}



/**
 * A class used to define the content and the properties of a
 * TableModelView column.
 */
class widget_TableModelViewColumn
{
    private $field;
    private $fieldPath;
    private $description;

    private $visible = true;
    private $sortable = true;
    private $summable = false;
    private $inList = true;
    private $exportable = true;
    private $searchable = true;
    private $selectable = true;
    private $selectableName = null;

    private $classes = array();


    /**
     *  The field should not appear in the search panel
     */
    const SEARCHABLE_NO = 0;
    /**
     *  The field should appear in the defalt part of the search panel
     */
    const SEARCHABLE_DEFAULT = 1;
    /**
     *  The field should appear in the main part of the search panel
     */
    const SEARCHABLE_MAIN = 2;

    /**
     * @param ORM_Field|string  $field
     * @param string|null       $description    If null and $field is an ORM_Field, the field description is used.
     */
    public function __construct($field, $description = null)
    {
        if ($field instanceof ORM_Field) {
            $this->field = $field;
            $this->fieldPath = $field->getPath();
            if ($field instanceof ORM_NumericField && !($field instanceof ORM_UserField)) {
                $this->addClass('widget-align-right');
            }
            if (!isset($description)) {
                $description = $field->getDescription();
            }
        } else {
            $this->field = null;
            $this->fieldPath = $field;
        }

        $this->description = $description;
    }


    /**
     * @return ORM_Field
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @return string
     */
    public function getFieldPath()
    {
        return $this->fieldPath;
    }


    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Sets the column visibility.
     * If $visible is false, the column will be hidden, but if
     * column selection is allowed, the user will be able to make
     * it visible.
     *
     * @param bool $visible
     * @return self
     */
    public function setVisible($visible = true)
    {
        $this->visible = $visible;
        return $this;
    }


    /**
     * Returns the column visibility.
     *
     * @return bool
     */
    public function isVisible()
    {
        return $this->visible;
    }


    /**
     * Set a field searchable with the default filterPanel
     * a field is searchable by default
     *
     * @see widget_TableModelView::filterPanel()
     *
     * @param bool|int $searchable  The self::SEARCHABLE_xxx values are available since 1.0.112
     * @return self
     */
    public function setSearchable($searchable = self::SEARCHABLE_DEFAULT)
    {
        if (is_bool($searchable)) {
            $searchable = ($searchable ? self::SEARCHABLE_DEFAULT : self::SEARCHABLE_NO);
        }
        $this->searchable = $searchable;
        return $this;
    }


    /**
     * @since 1.0.112
     * @return int      self::SEARCHABLE_xxx constant
     */
    public function getSearchable()
    {
        return $this->searchable;
    }

    /**
     * Test if a field should be searchable
     *
     * @see widget_TableModelView::filterPanel()
     *
     * @return bool
     */
    public function isSearchable()
    {
        return ($this->searchable != self::SEARCHABLE_NO);
    }


    /**
     * Test if a field should appear in the main search panel
     *
     * @since 1.0.112
     * @see widget_TableModelView::filterPanel()
     *
     * @return bool
     */
    public function isMainSearch()
    {
        return ($this->searchable === self::SEARCHABLE_MAIN);
    }


    /**
     * @param bool $sortable
     * @return self
     */
    public function setSortable($sortable = true)
    {
        $this->sortable = $sortable;
        return $this;
    }


    /**
     * @return bool
     */
    public function isSortable()
    {
        return $this->sortable;
    }




    /**
     * @param bool $summable
     * @return self
     */
    public function setSummable($summable = true)
    {
        $this->summable = $summable;
        return $this;
    }


    /**
     * @return bool
     */
    public function isSummable()
    {
        return $this->summable;
    }



    /**
     * Enable or disable column visibility in HTML list
     * @param bool $inList
     * @return self
     */
    public function setInList($inList = true)
    {
        $this->inList = $inList;
        return $this;
    }

    /**
     * @return bool
     */
    public function isInList()
    {
        return $this->inList;
    }


    /**
     * @param bool $exportable
     * @return self
     */
    public function setExportable($exportable = true)
    {
        $this->exportable = $exportable;
        return $this;
    }


    /**
     * @return bool
     */
    public function isExportable()
    {
        return $this->exportable;
    }



    /**
     * @param bool   $selectable
     * @param string $selectableName
     * @return self
     */
    public function setSelectable($selectable = true, $selectableName = null)
    {
        $this->selectable = $selectable;
        $this->selectableName = $selectableName;
        return $this;
    }



    /**
     * @return string
     */
    public function getSelectableName()
    {
        if (isset($this->selectableName)) {
            return $this->selectableName;
        }
        return $this->getDescription();
    }


    /**
     * @return bool
     */
    public function isSelectable()
    {
        return $this->selectable;
    }


    /**
     * Add the specified class names to the column.
     *
     * @param string $className,... One or more class names.
     * @return self
     */
    public function addClass($className /*,... */)
    {
        $args = func_get_args();
        $numArgs = func_num_args();
        for ($i = 0; $i < $numArgs; $i++) {
            $this->classes[] = $args[$i];
        }
        return $this;
    }




    /**
     * Returns an array of css classes associated to the column.
     * @return string[]
     */
    public function getClasses()
    {
        return $this->classes;
    }
}




class widget_TableModelView extends Widget_TableView
{
    /**
     * @var ORM_Iterator    The data source.
     */
    protected $iterator = null;

    /**
     * @var int             The current page.
     */
    private $currentPage = null;

    /**
     * @var int             The maximum number of rows displayed on the page.
     */
    private $pageLength = null;

    /**
     *
     * @var int             The maximum number of rows displayed
     */
    private $limit = null;

    /**
     *
     * @var string
     */
    private $anchorname;

    /**
     *
     * @var bool
     */
    private $doGrouping = false;

    /**
     *
     * @var bool
     */
    protected $allowColumnSelection = null;

    /**
     *
     * @var bool
     */
    protected $displayNumberOfRows = false;

    /**
     *
     * @var bool
     */
    protected $displayTotalRow = false;

    /**
     *
     * @var bool
     */
    protected $displaySubTotalRow = false;

    /**
     *
     * @var bool
     */
    protected $multiSelect = false;

    /**
     * @var Widget_PageSelector
     */
    private $pageSelector;


    /**
     * @var bool
     */
    private $configurationStorageInSession = false;

    /**
     * @var widget_TableModelViewColumn[]   Information about columns
     */
    protected $columns = array();

    /**
     * @var widget_TableModelViewColumn[]   Information about visible columns
     */
    protected $visibleColumns = array();

    /**
     * @var widget_TableModelViewColumn[]   Information about columns by position
     */
    protected $columnsIndex = array();

    protected $colCount = null;

//    protected $visibleColumns = array();
    public $columnsDescriptions = array();

    public $sortBaseUrl = null;
    public $sortParameterName = null;

    public $sortAjaxAction = null;

    public $pageAjaxAction = null;

    public $pageLengthAjaxAction = null;

    protected $sortAscending = null;
    protected $sortField = null;

    protected $sortRecordField = null;

    /**
     * @var ORM_RecordSet       The record set associated to the tablemodelview.
     */
    private $recordSet = null;


    /**
     * The default sort field and direction path (fieldname:{up|down})
     * @var string
     */
    protected $defaultSortField = null;


    /**
     * The counter is used to generate unique ids.
     * @var int $counter
     */
    private static $counter = 1;

    /**
     * Filter form submit button
     */
    protected $submit = null;


    /**
     * @var ORM_Record
     */
    protected $previousRecord = null;


    protected $toggleCheckAllBox = null;

    /**
     *
     * @var bool
     */
    protected $disableResponsive = false;


    /**
     * @param string $id      The item unique id.
     */
    public function __construct($id = null)
    {
        parent::__construct(null, $id);
    }


    /**
     * Generates and return a unique id for the current page.
     * use a counter only affected by other widget of same type
     */
    protected function createId()
    {
        return strtolower(get_class($this)) . self::$counter++;
    }


    /**
     * Defines if the user will be allowed to select visible columns.
     *
     * @param bool $allow
     * @return self
     */
    public function allowColumnSelection($allow = true)
    {
        $this->allowColumnSelection = $allow;
        return $this;
    }

    /**
     * @return bool
     */
    public function isColumnSelectionAllowed()
    {
        if (null === $this->allowColumnSelection) {
            // autodetect column selection status (default)
            foreach ($this->columns as $column) {
                /*@var $column widget_TableModelViewColumn */
                if (! $column->isVisible()) {
                    $this->allowColumnSelection = true;
                    return true;
                }
            }

            $this->allowColumnSelection = false;
            return false;
        }


        return $this->allowColumnSelection;
    }

    public function disableResponsive($disable = true)
    {
        $this->disableResponsive = $disable;
        return $this;
    }

    public function getDisableResponsive()
    {
        return $this->disableResponsive;
    }

    /**
     * @param   bool    $status
     * @return self
     */
    public function displayNumberOfRows($status = true)
    {
        $this->displayNumberOfRows = $status;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNumberOfRowsDisplayed()
    {
        return $this->displayNumberOfRows;
    }



    /**
     * @param   bool    $status
     * @return self
     */
    public function displayTotalRow($status = true)
    {
        $this->displayTotalRow = $status;
        return $this;
    }

    /**
     * @return bool
     */
    public function isTotalRowDisplayed()
    {
        return $this->displayTotalRow;
    }

    /**
     * @param   bool    $status
     * @return self
     */
    public function displaySubTotalRow($status = true)
    {
        $this->displaySubTotalRow = $status;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSubTotalRowDisplayed()
    {
        return $this->displaySubTotalRow;
    }



    /**
     * Sets the data source.
     *
     * @param ORM_Iterator $iterator
     * @return self
     */
    public function setDataSource(ORM_Iterator $iterator)
    {
        $this->iterator = $iterator;
        return $this;
    }


    /**
     * Get data source
     * @return ORM_Iterator
     */
    public function getDataSource()
    {
        return $this->iterator;
    }


    /**
     * @return string
     */
    private function getCsvHeader($separator)
    {
        if (!isset($this->iterator) || empty($this->columns)) {
            return '';
        }

        $set = $this->iterator->getSet();

        $this->initColumns($set);


        $line = array();
        foreach ($this->columns as $columnPath => $column) {
            /* @var $column widget_TableModelViewColumn */
            if (!$column->isExportable()) {
                continue;
            }
            if (!isset($this->columnsDescriptions[$columnPath])) {
                $this->columnsDescriptions[$columnPath] = (string) $column->getDescription();
            }
            $text = $this->columnsDescriptions[$columnPath];
            $line[] = '"' . str_replace(array('"', "\r\n"), array('""',"\n"), $text) . '"';
        }

        return implode($separator, $line) . "\r\n";
    }


    /**
     * @param ORM_Record $record
     * @param int $row
     * @param string $separator
     *
     * @return string
     */
    private function getCsvRow(ORM_Record $record, &$row, $separator)
    {
        if ($record = $this->initRow($record, $row)) {
            $row++;
        }
        $line = array();
        foreach ($this->columns as $fieldPath => $column) {
            /* @var $column widget_TableModelViewColumn */
            if (!$column->isExportable()) {
                continue;
            }
            $text = $this->computeCellTextContent($record, $fieldPath);
            $line[] = '"' . str_replace(array('"', "\r\n"), array('""',"\n"), $text) . '"';
        }

        return implode($separator, $line) . "\r\n";
    }


    /**
     * @param string $separator
     *
     * @return string
     */
    public function exportCsv($separator = ',')
    {
        $set = $this->iterator->getSet();

        $this->initColumns($set);
        $csv = $this->getCsvHeader($separator);

        $row = 0;
        $this->iterator->seek(0);
        while ($this->iterator->valid()) {
            $record = $this->iterator->current();
            $csv .= $this->getCsvRow($record, $row, $separator);
            $this->iterator->next();
        }

        return $csv;
    }





    /**
     * @param string|null   $filename
     * @param string        $extension
     *
     * @return string
     */
    private function computeFilename($filename, $extension)
    {
        if (!isset($filename)) {
            $filename = 'export';
            $name = $this->getName();
            if (!empty($name)) {
                $filename .= '-' . $name;
            }

            $filename .= $extension;
        }
        return $filename;
    }


    /**
     *
     * @param string $filename
     */
    public function downloadExcel($filename = null)
    {
        if (!isset($this->iterator) || empty($this->columns)) {
            die();
        }
        /* @var $ExcelExport Func_ExcelExport */
        $ExcelExport = bab_functionality::get('ExcelExport');

        if (!isset($ExcelExport)) {
            $message = widget_translate('The ExcelExport functionality must be available to export as an Excel spreadsheet.');
            throw new Exception($message);
        }

        $filename = $this->computeFilename($filename, '.xls');

        $ExcelExport->setDownloadFilename($filename);

        $workbook = $ExcelExport->getWorkbook('0.9.2');

        bab_setTimeLimit(3600);


        $worksheet = $workbook->addWorksheet('Page 1');

        $set = $this->iterator->getSet();

        $this->initColumns($set);


        $col = 0;
        $row = 0;
        foreach ($this->columns as $columnPath => $column) {
            /* @var $column widget_TableModelViewColumn */
            if (!$column->isExportable()) {
                continue;
            }
            if (!isset($this->columnsDescriptions[$columnPath])) {
                $this->columnsDescriptions[$columnPath] = (string) $column->getDescription();
            }
            $text = $this->columnsDescriptions[$columnPath];
            $worksheet->setColumn($col, $col + 1, 15);
            $worksheet->write($row, $col, bab_convertStringFromDatabase($text, 'CP1252'));
            $col++;
        }


        $this->iterator->seek(0);
        while ($this->iterator->valid()) {
            $record = $this->iterator->current();

            if ($record = $this->initRow($record, $row)) {
                $row++;
            }
            $col = 0;
            foreach ($this->columns as $fieldPath => $column) {
                /* @var $column widget_TableModelViewColumn */
                if (!$column->isExportable()) {
                    continue;
                }
                $text = $this->computeCellTextContent($record, $fieldPath);
                $worksheet->write($row, $col, bab_convertStringFromDatabase($text, 'CP1252'));
                $col++;
            }

            $this->iterator->next();
        }

        $workbook->close();
        die;
    }



    /**
     *
     * @param string $filename
     */
    public function downloadXlsx($filename = null)
    {
        if (!isset($this->iterator) || empty($this->columns)) {
            die();
        }
        /* @var $ExcelxExport Func_ExcelxExport */
        $ExcelxExport = bab_functionality::get('ExcelxExport');

        if (!$ExcelxExport) {
            $message = widget_translate('The ExcelxExport functionality must be available to export as an Excel spreadsheet.');
            throw new Exception($message);
        }

        $filename = $this->computeFilename($filename, '.xlsx');

        $set = $this->iterator->getSet();
        $this->initColumns($set);

        $writer = $ExcelxExport->getXlsxWriter();

        bab_setTimeLimit(3600);

        $sheetName = 'Page 1';

        $col = 0;
        $row = 0;

        $columnTypes = array();
        $columnHeaders = array();

        foreach ($this->columns as $columnPath => $column) {
            /* @var $column widget_TableModelViewColumn */
            if (!$column->isExportable()) {
                continue;
            }
            if (!isset($this->columnsDescriptions[$columnPath])) {
                $this->columnsDescriptions[$columnPath] = (string) $column->getDescription();
            }

            $field = $column->getField();
            if ($field instanceof ORM_DateField) {
                $type = 'DD/MM/YYYY';
            } elseif ($field instanceof ORM_DatetimeField) {
                $type = 'DD/MM/YYYY HH:mm';
            } elseif ($field  instanceof ORM_NumericField) {
                $type = '#,##0';
            } else {
                $type = 'string';
            }

            $text = $this->columnsDescriptions[$columnPath];
            $columnHeaderText = bab_convertStringFromDatabase(str_replace(chr(11), '', $text), bab_charset::UTF_8);

            $columnHeaders[$columnHeaderText] = $type;
            $columnTypes[$columnPath] = $type;

            $col++;
        }

        $writer->writeSheetHeader($sheetName, $columnHeaders);

        //$writer->writeSheetRow($sheetName, $row);


        $this->iterator->seek(0);
        while ($this->iterator->valid()) {
            $record = $this->iterator->current();

            if ($record = $this->initRow($record, $row)) {
                $row++;
            }
            $col = 0;

            $rowData = array();

            foreach ($this->columns as $columnPath => $column) {
                /* @var $column widget_TableModelViewColumn */
                if (!$column->isExportable()) {
                    continue;
                }

                switch ($columnTypes[$columnPath]) {
                    case 'string':
                        $text = $this->computeCellTextContent($record, $columnPath);
                        break;

                    case 'DD/MM/YYYY':
                        $text = self::getRecordFieldRawValue($record, $columnPath);
                        if ($text === '0000-00-00') {
                            $text = '';
                        }
                        break;

                    case 'DD/MM/YYYY HH:mm':
                        $text = self::getRecordFieldRawValue($record, $columnPath);
                        if ($text === '0000-00-00 00:00:00') {
                            $text = '';
                        }
                        break;

                    default:
                        $text = self::getRecordFieldRawValue($record, $columnPath);
                        break;
                }

                $rowData[] = bab_convertStringFromDatabase(str_replace(chr(11), '', $text), bab_charset::UTF_8);
                $col++;
            }

            $writer->writeSheetRow($sheetName, $rowData);
            $this->iterator->next();
        }

        header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        $writer->writeToStdOut();
        die;
    }




    /**
     * @param string    $filename
     * @param string    $separator
     * @param bool      $inline
     * @param string    $charset        Charset of output file
     *
     */
    public function downloadCsv($filename, $separator = ',', $inline = false, $charset = 'ISO-8859-15')
    {
        if (!isset($this->iterator) || empty($this->columns)) {
            throw new ErrorException('Failed to create the CSV file');
        }

        $filename = $this->computeFilename($filename, '.csv');

        //DOWNLOAD
        bab_setTimeLimit(3600);

        if (mb_strtolower(bab_browserAgent()) == 'msie') {
            header('Cache-Control: public');
        }

        if ($inline) {
            header('Content-Disposition: inline; filename="' . $filename . '"' . "\n");
        } else {
            header('Content-Disposition: attachment; filename="' . $filename . '"' . "\n");
        }

        $mime = 'text/csv';
        header('Content-Type: '.$mime."\n");
        header('Content-transfert-encoding: binary'."\n");


        if ('UTF-8 BOM' === $charset) {
            $charset = 'UTF-8';
            echo "\xEF\xBB\xBF";
        }

        $set = $this->iterator->getSet();

        $this->initColumns($set);
        echo bab_convertStringFromDatabase($this->getCsvHeader($separator), $charset);

        $row = 0;
        $this->iterator->seek(0);
        while ($this->iterator->valid()) {
            $record = $this->iterator->current();
            echo bab_convertStringFromDatabase($this->getCsvRow($record, $row, $separator), $charset);
            $this->iterator->next();
        }

        die(); // end of file

    }



    /**
     * Returns a simple printable html version of the modeltableview.
     *
     * @since 1.0.17
     *
     * @param $all      True to print all columns / False to print only currently visible columns.
     * @return string
     */
    public function exportPrintableHtml($all = true)
    {
        if (!isset($this->iterator) || empty($this->columns)) {
            return '';
        }

        $set = $this->iterator->getSet();

        $this->initColumns($set);

        $html = '<table class="widget-printable-table">';


        $line = array();
        foreach ($this->columns as $columnPath => $column) {
            $showColumn = $column->isExportable() && $column->isVisible() && $column->isInList();
            if (!$showColumn) {
                continue;
            }
            if (!isset($this->columnsDescriptions[$columnPath])) {
                $this->columnsDescriptions[$columnPath] = (string) $column;
            }
            $text = $this->columnsDescriptions[$columnPath];
            $line[] = bab_toHtml($text);
        }

        $html .= '<thead>';
        $html .= '<tr>';
        $html .= '<th>' . implode('</th><th>', $line) . '</th>';
        $html .= '</tr>';
        $html .= '</thead>';

        $row = 0;

        $html .= '<tbody>';
        $this->iterator->seek(0);
        while ($this->iterator->valid()) {
            $record = $this->iterator->current();
            $this->iterator->next();
            if ($record = $this->initRow($record, $row)) {
                $row++;
            }
            $line = array();
            foreach ($this->columns as $fieldPath => $column) {
                $showColumn = $column->isExportable() && $column->isVisible() && $column->isInList();
                if (!$showColumn) {
                    continue;
                }
                $text = $this->computeCellTextContent($record, $fieldPath);
                $line[] = bab_toHtml($text);
            }

            $html .= '<tr>';
            $html .= '<td>' . implode('</td><td>', $line) . '</td>';
            $html .= '</tr>';
        }

        $html .= '</tbody>';
        $html .= '</table>';



        return $html;
    }


    /**
     * Sets the maximum number of rows that shall be displayed at once.
     *
     * @param int $pageLength       or null to unset the limit.
     * @return self
     */
    public function setPageLength($pageLength = null)
    {
        $this->pageLength = $pageLength;
        return $this;
    }

    /**
     * Returns the maximum number of rows that shall be displayed at once.
     *
     * @return int      The maximum number of rows displayed or null if no limit.
     */
    public function getPageLength()
    {
        return $this->pageLength;
    }



    /**
     * Sets the maximum number of rows
     *
     * @param int $limit        or null to unset the limit.
     * @return self
     */
    public function setLimit($limit = null)
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * Returns the maximum number of rows
     *
     * @return int      The maximum number of rows
     */
    public function getLimit()
    {
        return $this->limit;
    }



    /**
     * Sets the currently displayed page. Displayed data depends on pageLength.
     *
     * @param integer $pageNumber       First page is 0.
     * @return self
     */
    public function setCurrentPage($pageNumber)
    {
        $this->currentPage = $pageNumber;
        return $this;
    }


    /**
     * Returns the currently displayed page.
     *
     * @return int      The currently displayed page. First page is 0.
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }


    /**
     * Defines the data source field that will be used to perform sorting.
     *
     * @since 1.0.66
     *
     * @param string $fieldPathAndOrder
     * @return $this
     */
    public function setSortField($fieldPathAndOrder)
    {
        list($fieldPath, $order) = explode(':', $fieldPathAndOrder . ':');
        $this->sortAscending = ($order !== 'down');
        $this->sortField = $fieldPath;

        return $this;
    }


    /**
     * Returns the data source field that will be used to perform sorting.
     *
     * @since 1.0.66
     *
     * @return string | null
     */
    public function getSortField()
    {
        if (!isset($this->sortField)) {
            return null;
        }
        return $this->sortField . ':' . ($this->sortAscending ? 'up' : 'down');
    }



    /**
     * Defines if grouping should be performed.
     *
     * @param bool  $doGrouping
     * @return self
     */
    public function setGrouping($doGrouping)
    {
        $this->doGrouping = $doGrouping;
        return $this;
    }


    /**
     * Return the grouping status of this tablemodelview.
     *
     * @return bool
     */
    public function isGrouping()
    {
        return $this->doGrouping;
    }


    /**
     * @param array     $columns
     * @return self
     *
     * @deprecated by setAvailableColumns
     * @see widget_TableModelView::setAvailableColumns
     */
    public function setVisibleColumns(array $columns, $recordSet = null)
    {
        if (!isset($recordSet)) {
            $recordSet = $this->iterator->getSet();
        }
        foreach ($columns as $path => $columnLabel) {
            if ($field = self::getRecordSetField($recordSet, $path)) {
                $col = widget_TableModelViewColumn($field, $columnLabel);
            } else {
                $col = widget_TableModelViewColumn($path, $columnLabel);
            }
            if (isset($col)) {
                $this->addColumn($col);
            }
        }

        return $this;
    }


    /**
     *
     * @return widget_TableModelViewColumn[]
     */
    public function getVisibleColumns()
    {
        if (empty($this->columnsDescriptions)) {
            $this->initColumns($this->iterator->getSet());
        }
        return $this->columns;
    }






    /**
     * Sets the sort field path.
     *
     * @param string $defaultSortField  fieldpath:{up|down}
     *
     * @return self
     */
    public function setDefaultSortField($defaultSortField)
    {
        $this->defaultSortField = $defaultSortField;
        return $this;
    }


    /**
     * Returns the current sort field path.
     * If no default sort field has been set, sets the first column with a field associated as
     * the default sort field.
     *
     * @since 1.0.17
     *
     * @return string
     */
    public function getDefaultSortField()
    {
        if (!isset($this->defaultSortField)) {
            foreach ($this->columns as $path => $column) {
                if (null !== $column->getField()) {
                    $this->defaultSortField = $path;
                    break;
                }
            }
        }

        return $this->defaultSortField;
    }

    /**
     * Get column description by column position
     * @return string
     */
    public function getColumnDescription($colIndex)
    {
        if (!isset($this->columnsIndex[$colIndex])) {
            return null;
        }

        return $this->columnsIndex[$colIndex]->getDescription();
    }


    /**
     * Get column description by column position
     * @return string
     */
    public function getColumnClasses($colIndex, Widget_Displayable_Interface $cellContent = null)
    {
        if (!isset($this->columnsIndex[$colIndex])) {
            return null;
        }

        return $this->columnsIndex[$colIndex]->getClasses();
    }


    /**
     * Adds a column to the table model view.
     *
     * @param widget_TableModelViewColumn $column The column description.
     * @param int $position                       Unused for the time being.
     * @return self
     */
    public function addColumn(widget_TableModelViewColumn $column, $position = null)
    {
        $colPath = $column->getFieldPath();
        $this->columns[$colPath] = $column;

        $colIndex = $this->getColumnIndex($colPath);

        if (isset($colIndex)) {
            $this->columnsIndex[$colIndex] = $column;
        } else {
            $this->columnsIndex[] = $column;
        }

        $classes = $column->getClasses();
        if (!empty($classes)) {
            $className = implode(' ', $classes);
            $this->addColumnClass($colIndex, $className);
        }
        return $this;
    }


    /**
     * Adds columns to the table model view.
     *
     * @see widget_TableModelView::addColumn()
     *
     * @param widget_TableModelViewColumn   $args,...
     * @return self
     */
    public function addColumns()
    {
        $columns= func_get_args();
        foreach ($columns as $column) {
            if (!$column instanceof widget_TableModelViewColumn) {
                throw new InvalidArgumentException('Expected widget_TableModelViewColumn elements');
            }
            $this->addColumn($column);
        }

        return $this;
    }


    /**
     * Removes a column from the table model view.
     *
     * @since 1.0.90
     *
     * @param widget_TableModelViewColumn $column The column description.
     * @return self
     */
    public function removeColumn(widget_TableModelViewColumn $column)
    {
        $colPath = $column->getFieldPath();
        unset($this->columns[$colPath]);

        $colIndex = $this->getColumnIndex($colPath);
        $this->removeColumnClasses($colIndex);

        return $this;
    }


    /**
     * Removes all columns.
     *
     * @since 1.0.90
     *
     * @return self
     */
    public function removeColumns()
    {
        $this->columns = array();
        $this->setColumnClasses(array());
        return $this;
    }


    /**
     * Sets the list of available columns.
     *
     * @since 1.0.90
     *
     * @param widget_TableModelViewColumn[]     $columns
     * @return self
     */
    public function setColumns(array $columns)
    {
        $this->removeColumns();
        foreach ($columns as $column) {
            $this->addColumn($column);
        }

        return $this;
    }



    /**
     * Adds the columns to the list of available columns.
     *
     * @param widget_TableModelViewColumn[]     $columns
     * @return self
     */
    public function setAvailableColumns(array $columns)
    {
        foreach ($columns as $column) {
            $this->addColumn($column);
        }

        return $this;
    }




    /**
     * Get a generic filter panel
     * Use setPageLength to define the default number of items per page
     *
     *
     * @param   string  $name       optional filter form name
     * @param   array   $filter     optional filter values, if not set, the filter will be used from request and the name parameter
     *
     *
     * @return Widget_Filter
     */
    public function filterPanel($name = 'search', $filter = null)
    {
        require_once dirname(__FILE__).'/filter.class.php';

        $filterPanel = new Widget_Filter;

        if (isset($name)) {
            $filterPanel->setName($name);
        }

        $search = bab_rp($name);
        if (null === $filter && $search && isset($search['filter'])) {
            $filter = $search['filter'];
        }

        $pageLength = $this->getPageLength();
        if (null === $pageLength) {
            $pageLength = 15;
        }

        $currentPage = $this->getCurrentPage();
        if (null === $currentPage) {
            $currentPage = 0;
        }


        $this->setPageLength(isset($filter['pageSize']) ? $filter['pageSize'] : $pageLength);
        $this->setCurrentPage(isset($filter['pageNumber']) ? $filter['pageNumber'] : $currentPage);

        $this->sortParameterName = $name . '[filter][sort]';

        if (isset($filter['sort'])) {
            $this->setSortField($filter['sort']);
        } elseif (!isset($this->sortField)) {
            $this->setSortField($this->getDefaultSortField());
        }

        $form = $this->getFilterForm();

        if (isset($filter)) {
            $form->setValues($filter, array($name, 'filter'));
        }

        $filterPanel->setFilter($form);
        $filterPanel->setFiltered($this);

        return $filterPanel;
    }




    /**
     * @param ORM_RecordSet $set
     * @param string        $fieldPath
     *
     * @return ORM_Field        or null
     */
    protected static function getRecordSetField(ORM_RecordSet $set, $fieldPath)
    {
        $fieldPathElements = explode('/', $fieldPath);
        $field = $set;
        foreach ($fieldPathElements as $fieldName) {

            if (!($field instanceof ORM_RecordSet)) {
                throw new Exception('Wrong field path on tablemodelview '.get_class($set).'->'.$fieldPath);
            }

            if (!$field->fieldExist($fieldName)) {
                return null;
            }
            $field = $field->$fieldName;


        }
        return $field;
    }



    /**
     * @param ORM_Record    $record
     * @param string        $fieldPath
     *
     * @return mixed
     */
    protected static function getRecordFieldRawValue(ORM_Record $record, $fieldPath)
    {
        $fieldPathElements = explode('/', $fieldPath);
        $value = $record;
        $field = $record->getParentSet();
        foreach ($fieldPathElements as $fieldName) {
            if (!$field->fieldExist($fieldName)) {
                return null;
            }
            $field = $field->$fieldName;
            $value = $value->$fieldName;
        }

        if ($value instanceof ORM_Record) {
            $value = $value->id;
        }

        return $value;
    }




    /**
     * @param ORM_Record    $record
     * @param string        $fieldPath
     *
     * @return mixed
     */
    protected static function getRecordFieldValue(ORM_Record $record, $fieldPath)
    {
        $fieldPathElements = explode('/', $fieldPath);
        $value = $record;
        $field = $record->getParentSet();

        if (!($field instanceof ORM_RecordSet)) {
            throw new Exception('Failed to get parent set from '.get_class($record));
        }

        foreach ($fieldPathElements as $fieldName) {

            if (!($field instanceof ORM_RecordSet)) {
                throw new Exception('Failed to get set from path '.$fieldPath.' field:'.$field->getName());
            }

            if (!$field->fieldExist($fieldName)) {
                return null;
            }
            $field = $field->$fieldName;
            $value = $value->$fieldName;


        }

        /*@var $field ORM_Field */

        return $field->output($value);
    }




    /**
     * @param ORM_Record    $record
     * @param string        $fieldPath
     *
     * @return mixed
     */
    protected static function getRecordFieldWidget(ORM_Record $record, $fieldPath)
    {
        $fieldPathElements = explode('/', $fieldPath);
        $value = $record;
        foreach ($fieldPathElements as $fieldName) {
            $value = $value->$fieldName;
        }
        return $value;
    }




    /**
     * Creates the key corresponding to an ORM_Field
     *
     * @param ORM_Field $field
     * @return string
     */
    public static function getFieldPath(ORM_Field $field)
    {
        $fieldPath = $field->getName();
        for ($parentSet = $field->getParentSet(); $parentSet !== null; $parentSet = $parentSet->getParentSet()) {
            if ($parentSet->getName() !== '') {
                $fieldPath = $parentSet->getName() . '/' . $fieldPath;
            }
        }
        return $fieldPath;
    }




    /**
     * Set default columns from fields
     * All columns from SET except primary keys and foreign keys
     *
     * @param   ORM_RecordSet   $set
     */
    private function setDefaultColumns(ORM_RecordSet $set)
    {
        $setFields = $set->getFields();

        foreach ($setFields as $setField) {
            if ($setField instanceof ORM_FkField || $setField instanceof ORM_PkField) {
                continue;
            }

            if ($setField instanceof ORM_RecordSet) {
                $this->setDefaultColumns($setField);
            } else {
                $col = widget_TableModelViewColumn($setField, $setField->getDescription());
                if (isset($col)) {
                    $this->addColumn($col);
                }
            }
        }
    }


    protected function initTotal()
    {

    }



    /**
     * Sets the recordSet associated to the tablemodelview.
     *
     * @since 1.0.88
     * @param ORM_RecordSet $recordSet
     * @return self
     */
    public function setRecordSet(ORM_RecordSet $recordSet)
    {
        $this->recordSet = $recordSet;
        return $this;
    }


    /**
     * Returns the recordSet associated to the tablemodelview.
     *
     * @since 1.0.88
     * @return ORM_RecordSet
     */
    public function getRecordSet()
    {
        if (isset($this->recordSet)) {
            return $this->recordSet;
        }

        $field = $this->getDataSource()->getCriteria()->getField();

        if (!isset($field)) {
            return null;
        }
        $recordSet = $field->getParentSet();
        if (!isset($recordSet)) {
            return null;
        }
        $this->recordSet = $recordSet;

        return $recordSet;
    }


    /**
     * Add the default columns of a widget table modelview
     * inherithed classes should use this method to add the collumns with the addColumn method
     *
     * @see widget_TableModelView::addColumn
     *
     * @param   ORM_RecordSet   $set
     *
     * @return self
     */
    public function addDefaultColumns(ORM_RecordSet $set)
    {
        // default behaviour witch need to be overloaded : display all columns of set
        $this->setDefaultColumns($set);

        return $this;
    }


    /**
     * @param bool $multiSelect
     * @return widget_TableModelView
     */
    public function setMultiSelect($multiSelect = true)
    {
        $this->multiSelect = $multiSelect;
        return $this;
    }


    /**
     *
     * @return bool
     */
    public function isMultiSelect()
    {
        return $this->multiSelect;
    }



    /**
     * Return the ids of selected rows.
     * @return array|null
     */
    public function getSelectedRows()
    {
        $W = bab_Widgets();
        $inSession = $this->isConfigurationStorageInSession();
        //$inSession = true;
        $selectedRows = $W->getUserConfiguration($this->getId() . '/selectedRows', 'widgets', $inSession);
        return $selectedRows;
    }

    /**
     * @return self
     */
    public function clearSelectedRows()
    {
        return $this->setSelectedRows(null);
    }

    /**
     *
     * @param array|null $selectedRows
     * @return self
     */
    public function setSelectedRows($selectedRows)
    {
        $W = bab_Widgets();
        $inSession = $this->isConfigurationStorageInSession();
        //$inSession = true;
        $selectedRows = $W->setUserConfiguration($this->getId() . '/selectedRows', $selectedRows, 'widgets', $inSession);
        return $this;
    }



    /**
     * Column initialisation from recordSet
     * @param   ORM_RecordSet   $set
     * return void
     */
    protected function initColumns(ORM_RecordSet $recordSet)
    {
        $W = bab_Widgets();

        if (0 === count($this->columns)) {
            // Missing column initialisation, default mode
            $this->setDefaultColumns($recordSet);
            return;
        }

        $columns = null;
        if ($this->isColumnSelectionAllowed()) {
            // If column selection is allowed, we fetch the visibility state/order
            // of the columns in the user configuration.

            $inSession = $this->isConfigurationStorageInSession();
            $columns = $W->getConfiguration($this->getId() . '/columns', 'widgets', $inSession);
        }
        if (!isset($columns)) {
            $columns = array();
            foreach ($this->columns as $columnPath => $column) {
                $columns[$columnPath] = $column->isVisible();
            }
        }

        if ($this->isMultiSelect()) {
            $tableModelViewColumn = new widget_TableModelViewColumn('_select_');
            $tableModelViewColumn->addClass('widget-align-center');
            $this->columns['_select_'] = $tableModelViewColumn;
            $columns = array('_select_' => true) + $columns;
        }


        $orderedColumnsIndex = array();
        $orderedColumns = array();
        $visibleColumns = array();
        $this->columnsDescriptions = array();

        $i = 0;
        foreach ($columns as $columnPath => $visible) {
            if (!isset($this->columns[$columnPath])) {
                continue;
            }
            $column = $this->columns[$columnPath];
            $column->setVisible($visible);
            $orderedColumns[$columnPath] = $column;
            if (!$visible) {
                continue;
            }
            $visibleColumns[$columnPath] = $column;
            $orderedColumnsIndex[] = $column;
            /* @var $column widget_TableModelViewColumn */
            $i++;

            $this->columnsDescriptions[$columnPath] = $column->getDescription();
        }

        $this->columnsIndex = $orderedColumnsIndex;
        $this->columns = $orderedColumns;
        $this->visibleColumns = $visibleColumns;
    }



    /**
     * @return number
     */
    protected function countColumns()
    {
        if (null === $this->colCount) {
            $this->colCount = 0;
            foreach ($this->visibleColumns as $column) {

//                 if (!$column->isInList() || !$column->isVisible()) {
//                     continue;
//                 }

                $this->colCount++;
            }
        }


        return $this->colCount;
    }


    /**
     * @return self
     */
    protected function addHeaderSection()
    {
        $layout = $this->getLayout();

        if ($layout instanceof Widget_GridParent) {
            $layout->addHeaderSection('header', null, 'widget-table-header');
            return $this;
        }


        $layout->addSection('header', null, 'widget-table-header');
        return $this;
    }



    /**
     *
     */
    protected function initTotalRow(ORM_RecordSet $recordSet)
    {
        if (!$this->isTotalRowDisplayed()) {
            return;
        }
        $W = bab_Widgets();

        $this->addSection('total', null, 'widget-table-total widget-table-header');
        $this->setCurrentSection('total');

//         $recordSet = $this->getRecordSet();
//         if (!isset($recordSet)) {
//             return;
//         }
        $summedRecordSet = clone $recordSet;

        $showSums = false;
        foreach ($this->columns as $column) {
            $field = $column->getField();
            if ($column->isSummable()) {
                $summedRecordSet->addFields(
                    $field->sum()
                        ->setName($field->getName())
                );
                $showSums = true;
            }
        }

        if (!$showSums) {
            return;
        }


        $records = $summedRecordSet->select($this->getDataSource()->getCriteria());

        foreach ($records as $record) {
            break;
        }

        $row = 0;
        $col = 0;


        $this->addItem(
            $this->handleTotalDisplayLabel($this->getDataSource()->count(), 'TOTAL')
                ->addClass('widget-small'),
            $row,
            $col,
            1,
            count($this->visibleColumns)
        );

        $row++;

        foreach ($this->visibleColumns as $columnPath => $column) {

            if (!$column->isSummable()) {
                $this->addItem($W->Label(''), $row, $col);
                $col++;
                continue;
            }

            if ($this->handleCell($record, $columnPath, $row, $col)) {
                $col++;
            }
        }
    }


    protected function getSortRecordField(ORM_RecordSet $recordSet)
    {
        if (!isset($this->sortRecordField)) {
            list($sortFieldPath) = explode(':', $this->getSortField());
            $this->sortRecordField = $recordSet->getFieldByPath($sortFieldPath);
        }
        return $this->sortRecordField;
    }

    /**
     * @param ORM_RecordSet $recordSet
     * @param int $row
     */
    private function handleSubTotalRow(ORM_RecordSet $recordSet, $previousRecord, $row)
    {
        $W = bab_Widgets();

        $summedRecordSet = clone $recordSet;

        $showSums = false;
        foreach ($this->columns as $column) {
            $field = $column->getField();
            if ($column->isSummable()) {
                $summedRecordSet->addFields(
                    $field->sum()
                        ->setName($field->getName())
                );
                $showSums = true;
            }
        }

        if (!$showSums) {
            return $row;
        }


        list($sortFieldPath) = explode(':', $this->getSortField());
        $sortRecordField = $this->getSortRecordField($recordSet);
        $criteria = $this->getDataSource()->getCriteria();


        $previousValue = self::getRecordFieldRawValue($previousRecord, $sortFieldPath);

        if ($previousValue instanceof ORM_Record) {
            $criteria = $criteria->_AND_(
                $sortRecordField->id->is($previousValue->id)
            );
        } else {
            $criteria = $criteria->_AND_(
                $sortRecordField->is($previousValue)
            );
        }

        $records = $summedRecordSet->select($criteria);

        $count = $recordSet->select($criteria)->count();

        foreach ($records as $record) {
            break;
        }

        $col = 0;

//         $this->addItem(
//             $this->handleTotalDisplayLabel($count, sprintf(widget_translate("Subtotal for '%s'"), self::getRecordFieldValue($previousRecord, $sortFieldPath)))
//                 ->addClass('widget-small'),
//             $row,
//             $col,
//             1,
//             count($this->visibleColumns)
//         );
//         $this->addRowClass($row, 'widget-tableview-subtotal');
//         $row++;

        foreach ($this->visibleColumns as $columnPath => $column) {
            if ($sortFieldPath == $columnPath) {
                $this->addItem(
                    $W->Html(
                        sprintf(
                            widget_translate('<span class="widget-strong">%d</span>%s"%s"'),
                            $count,
                            bab_nbsp(),
                            self::getRecordFieldValue($previousRecord, $sortFieldPath)
                        )
                    )->addClass('widget-small'),
                    $row, $col
                );
                $col++;
                continue;
            }
            if (!$column->isSummable()) {
                $this->addItem($W->Label(''), $row, $col);
                $col++;
                continue;
            }

            if ($this->handleCell($record, $columnPath, $row, $col)) {
                $col++;
            }
        }
        $this->addRowClass($row, 'widget-tableview-subtotal');
        $row++;

        return $row;
    }



    /**
     * @param ORM_RecordSet $recordSet
     * @param int $row
     */
    private function handleTotalRow(ORM_RecordSet $recordSet, $row)
    {
        $W = bab_Widgets();

        $summedRecordSet = clone $recordSet;

        $showSums = false;
        foreach ($this->columns as $column) {
            $field = $column->getField();
            if ($column->isSummable()) {
                $summedRecordSet->addFields(
                    $field->sum()
                        ->setName($field->getName())
                );
                $showSums = true;
            }
        }

        if (!$showSums) {
            return $row;
        }


        $criteria = $this->getDataSource()->getCriteria();

        $records = $summedRecordSet->select($criteria);

        $count = $recordSet->select($criteria)->count();

        foreach ($records as $record) {
            break;
        }

        $col = 0;

//         $this->addItem(
//             $this->handleTotalDisplayLabel($count, 'TOTAL')
//                 ->addClass('widget-small'),
//             $row,
//             $col,
//             1,
//             count($this->visibleColumns)
//         );
//         $this->addRowClass($row, 'widget-tableview-total');
//         $row++;

        list($sortFieldPath) = explode(':', $this->getSortField());

        foreach ($this->visibleColumns as $columnPath => $column) {
            if ($sortFieldPath == $columnPath) {
                $this->addItem(
                    $W->Label(
                        sprintf(
                            widget_translate("TOTAL (%d)"),
                            $count
                        )
                    )->addClass('widget-small'),
                    $row, $col
                );
                $col++;
                continue;
            }

            if (!$column->isSummable()) {
                $this->addItem($W->Label(''), $row, $col);
                $col++;
                continue;
            }

            if ($this->handleCell($record, $columnPath, $row, $col)) {
                $col++;
            }
        }
        $this->addRowClass($row, 'widget-tableview-total');
        $row++;

        return $row;
    }


    /**
     * Called before data rows
     * @return void
     */
    protected function initHeaderRow(ORM_RecordSet $set)
    {
        require_once $GLOBALS['babInstallPath'] . 'utilit/urlincl.php';

        bab_functionality::includefile('Icons');

        $W = bab_Widgets();


        $this->addHeaderSection();
        $this->setCurrentSection('header');

        $row = 0;
        $col = 0;

        $this->toggleCheckAllBox = null;
        $this->displayedIds = array();

        foreach ($this->visibleColumns as $columnPath => $column) {

            if (!isset($this->columnsDescriptions[$columnPath])) {
                $this->columnsDescriptions[$columnPath] = $column->getDescription();
            }

            $columnLabel = $this->columnsDescriptions[$columnPath];

            if ($this->isMultiSelect() && $columnPath === '_select_') {
                $this->toggleCheckAllBox  = $W->CheckBox();
                $this->toggleCheckAllBox->setTitle(widget_translate('Select all on page'));
                $columnItem = $this->toggleCheckAllBox;
            } elseif (isset($this->sortParameterName) && self::getRecordSetField($set, $columnPath) && $column->isSortable()) {
                if (!isset($this->sortBaseUrl)) {
                    $this->sortBaseUrl = bab_url::request_gp();
                }

                if ($this->sortField === $columnPath && $this->sortAscending) {
                    $direction = ':down';
                } else {
                    $direction = ':up';
                }

                $url = bab_url::mod($this->sortBaseUrl, $this->sortParameterName, $columnPath . $direction);
                if ($anchor = $this->getAnchor()) {
                    $url .= '#' . urlencode($anchor);
                }

                $columnItem = $W->Link($columnLabel, $url);
                if ($this->sortAjaxAction) {
                    $columnItem->setAjaxAction(
                        $this->sortAjaxAction->setParameter('sort', $columnPath . $direction),
                        $this
                    );
                }
            } else {
                $columnItem = $W->Label($columnLabel);
            }

            $items = $W->Items()->setSizePolicy('widget-actions-target');
            if ($col === 0 && $this->isColumnSelectionAllowed()) {
                $columnSelectionMenu = $this->columnSelectionMenu($this->columns);
                $columnSelectionMenu->setSizePolicy('pull-right');
                $items->addItem($columnSelectionMenu);
            }

            $items->addItem($columnItem);

//             $addFilterAction = $W->Action();
//             $addFilterAction->setMethod(
//                 'addon/widgets/configurationstorage',
//                 'addFilter',
//                 array(
//                     'key' => $this->getId(),
//                     'name' => $columnPath,
//                     'value' => '',
//                     'insession' => $this->isConfigurationStorageInSession()
//                 )
//             );

//             $items->addItem(
//                 $W->Link(
//                     '',
//                     $addFilterAction
//                 )->addClass('widget-actions', 'icon', Func_Icons::ICON_LEFT_16, Func_Icons::ACTIONS_LIST_FILTER)
//                 ->setAjaxAction()
//             );

            $this->addItem(
                $items,
                0,
                $col++
            );
        }

//        $this->addColumnSelectionColumn($col - 1);
    }



    protected function addColumnSelectionColumn($col)
    {
        if ($this->isColumnSelectionAllowed()) {
            $columnSelectionMenu = $this->columnSelectionMenu($this->columns);
            $this->addItem($columnSelectionMenu, 0, $col);
            $this->addColumnClass($col, 'widget-column-minimal-width');
        }
    }




    public function getDisplaySubTotalRowCheckbox()
    {
        $W = bab_Widgets();
        $inSession = $this->isConfigurationStorageInSession();

        $displaySubTotalRowCheckBox = $W->CheckBox()
            ->addClass('form-control')
            ->setName('value')
            ->setCheckedValue('1')
            ->setUncheckedValue('0');

        $displaySubTotalRowCheckBox->setValue($this->isSubTotalRowDisplayed() ? '1' : '0');

        $action = $W->Action();
        $action->setMethod(
            'addon/widgets/configurationstorage',
            'set',
            array(
                'key' => $this->getId() . '/displaySubTotalRow',
                'value' => '0',
                'insession' => $inSession
            )
        );

        $displaySubTotalRowCheckBox->setAjaxAction($action, $this, 'change');

        return $displaySubTotalRowCheckBox;
    }


    public function getPageLengthSelect()
    {
        $W = bab_Widgets();
        $inSession = $this->isConfigurationStorageInSession();

        $pageLengthSelect = $W->Select()
            ->addClass('form-control')
            ->addAttribute('name', 'value')
            ->addOption('5', '5')
            ->addOption('10', '10')
            ->addOption('12', '12')
            ->addOption('15', '15')
            ->addOption('20', '20')
            ->addOption('30', '30')
            ->addOption('50', '50')
            ->addOption('100', '100')
            ->addOption('200', '200')
            ->addOption('300', '300')
            ->addOption('500', '500')
            ->addOption('1000', '1000');

        $pageLengthSelect->setValue($this->getPageLength());

        $action = $W->Action();
        $action->setMethod(
            'addon/widgets/configurationstorage',
            'set',
            array(
                'key' => $this->getId() . '/pageLength',
                'value' => null,
                'insession' => $inSession
            )
        );

        $pageLengthSelect->setAjaxAction($action, $this, 'change');

        return $pageLengthSelect;
    }


    /**
     * Returns a menu with the list of column names.
     * Clicking a column name in the menu toggles its visibility in the tablemodelview.
     *
     * @return Widget_Menu
     */
    protected function columnSelectionMenu($columns)
    {
        $W = bab_Widgets();

        $form = $W->Form(
            null,
            $W->VBoxItems()
                ->setVerticalSpacing(2, 'rem')
                ->addClass('widget-100pc')
        );
        $form->setReadOnly(false);

        $inSession = $this->isConfigurationStorageInSession();



        //         $linesBox->setVerticalSpacing(3, 'em');
        //         $linesBox->addClass('widget-100pc');
        //         $linesBox->setSizePolicy('widget-20em');
        //         $form->addItem($linesBox);

        $linesBox = $W->HBoxItems();
        $linesBox->setHorizontalSpacing(3, 'em');
        $linesBox->addClass('widget-100pc');
        //        $linesBox->setSizePolicy('widget-20em');
        $form->addItem($linesBox);



        $linesBox->addItem(
            $W->LabelledWidget(
                widget_translate('Number of lines per page'),
                $this->getPageLengthSelect()
            )
        );


        $linesBox->addItem(
            $W->LabelledWidget(
                widget_translate('Display subtotal rows'),
                $this->getDisplaySubTotalRowCheckbox()
            )
        );


        $resetColumnsAction = $W->Action();
        $resetColumnsAction->setMethod('addon/widgets/configurationstorage', 'delete', array('path' => $this->getId() . '/columns'));

        $linesBox->addItem(
            $W->Link(
                widget_translate('Reset to default columns'),
                $resetColumnsAction
            )->addClass('icon', 'widget-close-dialog', 'widget-actionbutton', 'columns-reset', Func_Icons::ACTIONS_VIEW_REFRESH)
            ->setAjaxAction($resetColumnsAction, $this->getId())
        );

        $saveColumnsAction = $W->Action();
        $saveColumnsAction->setMethod(
            'addon/widgets/configurationstorage',
            'table',
            array(
                'key' => $this->getId(),
                'insession' => $inSession
            )
        );

        $columnSelectionMenu = $W->Menu(null, $W->Items($form));

        $columnSelectionMenu->setTitle(widget_translate('Table configuration'));

        $columnSelectionMenu->addClass(Func_Icons::ICON_LEFT_16, 'widget-tableview-column-menu', 'widget-popup-dialog');

        $columnsBox = $W->VBoxItems();
        $columnsBox->addClass('widget-100pc');

        $form->addItem($columnsBox);

        $columnsBox->addItem(
            $W->LabelledWidget(
                widget_translate('Choose displayed columns'),
                $columnList = $W->VBoxItems()
                    ->setSizePolicy('widget-3columns')
                    ->addClass('widget-sortable')
            )
        );
        $col = 0;

        foreach ($columns as $path => $column) {

            if (!$column->isInList()) {
                continue;
            }
            if ($column->isSelectable() && $column->getDescription() != '') {
                $columnList->addItem(
                    $W->Items(
                        $W->CheckBox()
                            ->setName(array('columns', $path))
                            ->setValue($column->isVisible()),
                        $W->Label(
                            $column->getSelectableName()
                        )
                    )->setVerticalAlign('middle')
                    //->addClass('icon', 'handle', Func_Icons::ACTIONS_ZOOM_FIT_HEIGHT)
                    ->setSizePolicy('widget-list-element')
                );
            }
            $col++;
        }


        $form->addItem(
            $W->SubmitButton()
                ->setLabel(widget_translate('Save configuration'))
                ->setAjaxAction($saveColumnsAction, '')//$this->getId())
                ->addClass('widget-close-dialog')
                ->setSizePolicy('widget-form-buttons')
        );

        return $columnSelectionMenu;
    }



    /**
     * Called after data rows
     *
     * @param   int $row        Last row number +1
     */
    protected function initFooterRow($row)
    {
        $this->addSection('footer', null, 'widget-table-footer');
        $this->setCurrentSection('footer');


        if ($this->isMultiSelect() && isset($this->toggleCheckAllBox)) {

            $inSession = $this->isConfigurationStorageInSession();

            $W = bab_Widgets();
            $toogleAction = $W->Action();
            $toogleAction->setMethod(
                'addon/widgets/configurationstorage',
                'selectRow',
                array(
                    'key' => $this->getId(),
                    'id' => $this->displayedIds,
                    'insession' => $inSession
                )
            );
            $this->toggleCheckAllBox->setAjaxAction($toogleAction, '', 'change');
        }

        // may be used in inherited classes
    }




    /**
     *
     * @param ORM_Record    $record
     * @param string        $fieldPath
     * @return string       The text of the item that will be placed in the cell
     */
    protected function computeCellTextContent(ORM_Record $record, $fieldPath)
    {
        $cellTextContent = self::getRecordFieldValue($record, $fieldPath);
        return $cellTextContent;
    }



    /**
     *
     * @param ORM_Record    $record
     * @param string        $fieldPath
     * @return Widget_Item  The item that will be placed in the cell
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();

        if ($this->isMultiSelect() && $fieldPath === '_select_') {
            $selectedRows = $this->getSelectedRows();

            $inSession = $this->isConfigurationStorageInSession();

            $selectRowAction = $W->Action();
            $selectRowAction->setMethod(
                'addon/widgets/configurationstorage',
                'selectRow',
                array(
                    'key' => $this->getId(),
                    'insession' => $inSession
                )
            );
            $cellContent = $W->CheckBox();
            $cellContent->setname(array('id', $record->id));
            $cellContent->setValue(isset($selectedRows[$record->id]));
            $cellContent->setAjaxAction($selectRowAction, false);

            $this->displayedIds[$record->id] = 1;


        } else {

            $field = self::getRecordSetField($record->getParentSet(), $fieldPath);
            if (isset($field)) {
                $value = self::getRecordFieldRawValue($record, $fieldPath);
                $cellContent = $field->outputWidget($value);
            } else {
                $cellContent = $W->Html(bab_toHtml($this->computeCellTextContent($record, $fieldPath)));
            }
        }
        return $cellContent;
    }


    /**
     * Handle cell content, add the cell widget the the tableview
     *
     * @param   ORM_Record  $record         row record
     * @param   string      $fieldPath      collumn name as path is recordSet
     * @param   int         $row            row number in table
     * @param   int         $col            col number in table
     * @param   string      $name           if name is set, the cell widget will be added with a namedContainer
     *
     * @return  bool        True if a cell was added
     */
    protected function handleCell(ORM_Record $record, $fieldPath, $row, $col, $name = null, $rowSpan = null, $colSpan = null)
    {
        $cellContent = $this->computeCellContent($record, $fieldPath);

        if (isset($name)) {
            require_once dirname(__FILE__) . '/namedcontainer.class.php';
            $named = new Widget_NamedContainer($name);
            $named->addItem($cellContent);
            $this->addItem($named, $row, $col);
            return true;
        }

        $this->addItem($cellContent, $row, $col, $rowSpan, $colSpan);
        return true;
    }



    /**
     * Called just before initRow().
     * Should be used insert rows before the record row
     *
     * @param ORM_Record    $record
     * @param int           $row
     * @return int
     */
    protected function initRowNumber(ORM_Record $record, $row)
    {
        return $row;
    }


    /**
     * Called just before handleRow().
     * Should be used to perform initialization based on the ORM_Record.
     *
     * @param ORM_Record    $record
     * @param int           $row
     * @return ORM_Record
     */
    protected function initRow(ORM_Record $record, $row)
    {
        return $record;
    }


    /**
     * @param int           $row
     * @return int
     */
    protected function handleNullRow($row)
    {
        if ($this->isSubTotalRowDisplayed()) {
            if (isset($this->previousRecord)) {

                $recordSet = $this->previousRecord->getParentSet();
                list($sortFieldPath) = explode(':', $this->getSortField());

                $previousValue = self::getRecordFieldRawValue($this->previousRecord, $sortFieldPath);

                if ($previousValue !== null) {
                    $row = $this->handleSubTotalRow($recordSet, $this->previousRecord, $row);
                }
            }
        }

        return $row;
    }


    /**
     * @param ORM_Record    $record
     * @param int           $row
     * @return int|true     The new row number. If true is returned (for compatibility) the row will be incremented by 1.
     */
    protected function handleRow(ORM_Record $record, $row)
    {
        $col = 0;

        if ($this->isSubTotalRowDisplayed()) {
            if (isset($this->previousRecord)) {

                $recordSet = $this->previousRecord->getParentSet();
                list($sortFieldPath) = explode(':', $this->getSortField());

                $previousValue = self::getRecordFieldRawValue($this->previousRecord, $sortFieldPath);


                if (isset($record)) {
                    $currentValue = self::getRecordFieldRawValue($record, $sortFieldPath);
                } else {
                    $currentValue = null;
                }

                if (($currentValue instanceof ORM_Record && $previousValue->id != $currentValue->id) || ($previousValue != $currentValue)) {
                    $row = $this->handleSubTotalRow($recordSet, $this->previousRecord, $row);
                }
            }
        }

        if (!isset($record)) {
            return $row;
        }


        foreach ($this->visibleColumns as $fieldPath => $column) {
            $colSpan = null;
            if ($this->handleCell($record, $fieldPath, $row, $col, null, null, $colSpan)) {
                $col++;
            }
        }
        $row++;

        $this->previousRecord = $record;

        return $row;
    }


    /**
     * Get a form filter
     * @see Widget_Filter
     *
     * @param   string          $id
     * @param   Widget_Layout   $layout
     *
     * @return Widget_Form
     */
    public function getFilterForm($id = null, $layout = null)
    {
        require_once dirname(__FILE__).'/form.class.php';
        require_once dirname(__FILE__).'/flowlayout.class.php';
        require_once dirname(__FILE__).'/submitbutton.class.php';

        if (null === $layout) {
            $layout = new Widget_FlowLayout();
            $layout->setVerticalSpacing(1, 'em')->setHorizontalSpacing(1, 'em');
            $layout->setVerticalAlign('bottom');
        }


        $form = new Widget_Form($id);
        $form->setReadOnly(true);
        $form->setName('filter');
        $form->setLayout($layout);
        $form->colon();

        $this->handleFilterFields($form);
        if (! $this->submit) {
            $this->submit = new Widget_SubmitButton();
            $this->submit->setLabel(widget_translate('Filter'));
        }

        $form->addItem($this->submit);
        $form->setSelfPageHiddenFields();
        $form->setAnchor($this->getAnchor());

        return $form;
    }


    /**
     * Checks if filter values, sort field, current page and other configurations
     * should be stored in the user session (true) or in the user registry (false).
     *
     * @return bool
     */
    public function isConfigurationStorageInSession()
    {
        return $this->configurationStorageInSession;
    }



    /**
     * Defines if filter values, sort field, current page and other configurations
     * should be stored in the user session (true) or in the user registry (false).
     *
     * @param bool $inSession
     * @return self
     */
    public function setConfigurationStorageInSession($inSession)
    {
        $this->configurationStorageInSession = $inSession;
        return $this;
    }


    /**
     * Set filter form ajax action.
     *
     * @param Widget_Action             $action      A controller method accepting 3 optional parameters.
     * @param Widget_Item|string|array  $reloadItem  A Widget_Item, id of a Widget_Item, or array of Widget_Items
     * @param string                    $event       Not used for widget_TableModelView
     * @return self
     */
    public function setAjaxAction(Widget_Action $action = null, $reloadItem = null, $event = 'click')
    {
        if (! isset($action)) {
            $W = bab_Widgets();

            $inSession = $this->isConfigurationStorageInSession();

            $action = $W->Action();
            $action->setMethod(
                'addon/widgets/configurationstorage',
                'table',
                array(
                    'key' => $this->getId(),
                    'insession' => $inSession
                )
            );
            $this->setAjaxAction($action, $this);

            $pageLength = $W->getUserConfiguration($this->getId() . '/pageLength', 'widgets', $inSession);
            if ( isset($pageLength)) {
                $this->setPageLength($pageLength);
            }

            $currentPage = $W->getUserConfiguration($this->getId() . '/page', 'widgets', $inSession);
            if (! isset($currentPage)) {
                $currentPage = 0;
            }
            $this->setCurrentPage($currentPage);

            $sortField = $W->getUserConfiguration($this->getId() . '/sort', 'widgets', $inSession);
            if (! isset($sortField)) {
                $sortField = $this->getDefaultSortField();
            }
            $this->setSortField($sortField);

            $displaySubTotalRow = $W->getUserConfiguration($this->getId() . '/displaySubTotalRow', 'widgets', $inSession);
            if (isset($displaySubTotalRow)) {
                $this->displaySubTotalRow($displaySubTotalRow);
            }

            $displayTotalRow = $W->getUserConfiguration($this->getId() . '/displayTotalRow', 'widgets', $inSession);
            if (isset($displayTotalRow)) {
                $this->displayTotalRow($displayTotalRow);
            }

        }

        $this->sortAjaxAction = $action;
        $this->pageAjaxAction = $action;
        $this->pageLengthAjaxAction = $action;

        $this->sortParameterName = 'sort';

        if (! $this->submit) {
            $this->submit = new Widget_SubmitButton();
            $this->submit->setLabel(widget_translate('Filter'));
        }
        $this->submit->setAjaxAction($action, $reloadItem);

        return $this;
    }




    /**
     * Returns the filters associated to the tablemodelview as an indexed array.
     *
     * @return array
     */
    public function getFilterValues()
    {
        $inSession = $this->isConfigurationStorageInSession();
        $W = bab_Widgets();
        $filter = $W->getUserConfiguration($this->getId() . '/filter', 'widgets', $inSession);
        if (!isset($filter)) {
            $filter = array();
        }
        return $filter;
    }





    /**
     * Sets the filters associated to the tablemodelview as an indexed array.
     *
     * @param array $filterValues
     */
    public function setFilterValues($filterValues)
    {
        $inSession = $this->isConfigurationStorageInSession();
        $W = bab_Widgets();
        $W->setUserConfiguration($this->getId() . '/filter', $filterValues, 'widgets', $inSession);

        return $this;
    }



    public function resetColumns()
    {
        $inSession = $this->isConfigurationStorageInSession();
        $W = bab_Widgets();
        $W->deleteUserConfiguration($this->getId() . '/columns', 'widgets', $inSession);

        return $this;
    }


    /**
     * Add the filter fields to the filter form
     * @param Widget_Form $form
     *
     */
    protected function handleFilterFields(Widget_Item $form)
    {
        $fields = $this->getVisibleColumns();

        foreach ($fields as $fieldName => $column) {
            /* @var $column widget_TableModelViewColumn */
            $field = $column->getField();
            if (! $column->isSearchable()) {
                continue;
            }

            if (! ($field instanceof ORM_Field)) {
                $field = null;
            }

            $label = $this->handleFilterLabelWidget($fieldName, $field);
            $input = $this->handleFilterInputWidget($fieldName, $field);

            if (isset($input) && isset($label)) {

                $input->setName($fieldName);
                $label->setAssociatedWidget($input);

                $form->addItem($this->handleFilterLabel($label, $input));
            }
        }
    }

    /**
     * Handle label for input field
     *
     * @param string    $fieldName
     * @param ORM_Field $field
     *
     * @return Widget_Label
     */
    protected function handleFilterLabelWidget($fieldName, ORM_Field $field = null)
    {
        require_once dirname(__FILE__).'/label.class.php';
        if (isset($this->columnsDescriptions[$fieldName])) {
            $description = $this->columnsDescriptions[$fieldName];
        }
        if (empty($description) && isset($this->columns[$fieldName])) {
            $description = $this->columns[$fieldName]->getDescription();
        }
        if (empty($description) && null !== $field) {
            $description = $field->getDescription();
        }
        if (empty($description) && null !== $field) {
            $description = $field->getName();
        }

        if (empty($description)) {
            $description = $fieldName;
        }

        return new Widget_Label($description);
    }


    /**
     * Handle label and input widget merge in one item before adding to the filter form
     * default is a vertical box layout
     *
     * @param Widget_Label                  $label
     * @param Widget_Displayable_Interface  $input
     * @return Widget_Item
     */
    protected function handleFilterLabel(Widget_Label $label, Widget_Displayable_Interface $input)
    {
        require_once dirname(__FILE__).'/vboxlayout.class.php';

        $layout = new Widget_VBoxLayout();

        $layout->addItem($label)
        ->addItem($input);

        return $layout;
    }




    /**
     * Returns the criteria corresponding to the specified value
     * for the specified field.
     *
     * @param ORM_Field $field
     * @param mixed $value
     *
     * @return ORM_Criteria
     */
    protected function getFieldFilterCriteria(ORM_Field $field, $value)
    {
        $criteria = null;

        switch (true) {
            case $field instanceof ORM_DateTimeField:
                $criteria = $field->getParentSet()->all();
                if ('' !== $value['from']) {
                    $criteria = $criteria->_AND_($field->greaterThanOrEqual($field->input($value['from'])));
                }

                if ('' !== $value['to']) {
                    // add one day
                    require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
                    $to = BAB_DateTime::fromIsoDateTime($field->input($value['to']));
                    $to->add(1, BAB_DATETIME_DAY);

                    $criteria = $criteria->_AND_($field->lessThan($to->getIsoDateTime()));
                }
                break;

            case $field instanceof ORM_DateField:
                $criteria = $field->getParentSet()->all();
                if ('' !== $value['from']) {
                    $criteria = $criteria->_AND_($field->greaterThanOrEqual($field->input($value['from'])));
                }

                if ('' !== $value['to']) {
                    // add one day
                    require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
                    $to = BAB_DateTime::fromIsoDateTime($field->input($value['to']));
                    $to->add(1, BAB_DATETIME_DAY);

                    $criteria = $criteria->_AND_($field->lessThan($to->getIsoDate()));
                }
                break;

            case $field instanceof ORM_UserField:
                if ($value == 0) {
                    break;
                }
                //NO BREAK
            case $field instanceof ORM_RecordSet:
            case $field instanceof ORM_FkField:
            case $field instanceof ORM_PkField:
            case $field instanceof ORM_EnumField:
            case $field instanceof ORM_IntField:
            case $field instanceof ORM_BoolField:
                if (is_array($value)) {
                    $criteria = $field->in($value);
                } else {
                    $criteria = $field->is($field->input($value));
                }
                break;

            case $field instanceof ORM_SetField:
                if (is_array($value)) {
                    $c = array();
                    foreach ($value as $v) {
                        $c[] = $field->contains($v);
                    }
                    $criteria = $field->getParentSet()->any($c);
                } else {
                    $criteria = $field->contains($value);
                }
                break;

            default:
                $criteria = $field->matchAll($field->input($value));

        }
        return $criteria;
    }



    /**
     * Build a default criteria from the filter.
     * This method needs orm field objects defined on columns.
     *
     * @param array $filter
     *                 Filter received from request, if filter not set try to
     *                 get it from the search param (default name used in
     *                 filterPanel() method)
     *
     * @return ORM_Criteria
     */
    public function getFilterCriteria($filter = null)
    {
        $Orm = bab_functionality::get('LibOrm');
        /*@var $Orm Func_LibOrm */
        $Orm->init();

        $criteria = new ORM_TrueCriterion;
        if (null === $filter) {
            $search = bab_rp('search');

            if (!isset($search['filter'])) {
                return $criteria;
            }

            $filter = $search['filter'];
        }

        if (empty($this->columns)) {
            trigger_error('The getFilterCriteria method should be called after the the addColumn method');
        }

        foreach ($this->columns as $fieldName => $column) {
            /* @var $column widget_TableModelViewColumn */
            $field = $column->getField();
            if (!($field instanceof ORM_Field)) {
                continue;
            }

            if (!isset($filter[$fieldName])) {
                continue;
            }

            $value = $filter[$fieldName];

            if ('' === $value) {
                continue;
            }

            $crit = $this->getFieldFilterCriteria($field, $value);
            if (isset($crit)) {
                $criteria = $criteria->_AND_($crit);
            }
        }

        return $criteria;
    }



    /**
     * Handle filter field input widget
     * If the method returns null, no filter field is displayed for the field
     *
     * @param   string          $name       table field name
     * @param   ORM_Field       $field      ORM field
     *
     * @return Widget_InputWidget | null
     */
    protected function handleFilterInputWidget($name, ORM_Field $field = null)
    {
        if (null === $field) {
            return null;
        }

        $W = bab_Widgets();

        if ($field instanceof ORM_DateField || $field instanceof ORM_DateTimeField) {
            return $W->PeriodPicker();

        } elseif ($field instanceof ORM_TimeField) {
            return $W->TimePicker();

        } elseif ($field instanceof ORM_StringField) {
            return $W->LineEdit()->setSize(min(array(20, $field->getMaxLength())));

        } elseif ($field instanceof ORM_ConcatOperation || $field instanceof ORM_ExtractValueOperation) {
            return $W->LineEdit();

        } elseif ($field instanceof ORM_EnumField || $field instanceof ORM_SetField) {
            $widget = $W->Select();
            $widget->addOption('', '');
            foreach ($field->getValues() as $key => $text) {
                $widget->addOption($key, $text);
            }

            return $widget;

        } elseif ($field instanceof ORM_FkField) {
            $widget = $W->Select();
            $widget->addOption('', '');
            $set = $field->newSet();

            if ($set === null) {
                return null;
            }

            $values = $set->select();
            foreach ($values as $record) {
                $widget->addOption($record->id, (string)$record->name);
            }

            return $widget;

        } elseif ($field instanceof ORM_RecordSet) {
            $widget = $W->Select();
            $widget->addOption('', '');
            $values = $field->select();

            foreach ($values as $record) {
                /*@var $record ORM_Record */
                $widget->addOption($record->id, $record->getRecordTitle());
            }

            return $widget;

        } elseif ($field instanceof ORM_UserField) {
            return $W->UserPicker()->addClass('widget-13em');

        } elseif ($field instanceof ORM_IntField) {
            return $W->LineEdit()->setSize(9);

        } elseif ($field instanceof ORM_TextField) {
            return $W->LineEdit()->setSize(20);

        } elseif ($field instanceof ORM_BoolInterface) {
            return $W->Select()
                ->addOption('', '')
                ->addOption(0, $field->output(false))
                ->addOption(1, $field->output(true));
        }

        return null;
    }



    /**
     * Returns the column numeric index for the specified column path.
     *
     * @param string $colPath
     *
     * @return int | null
     */
    public function getColumnIndex($colPath)
    {
        $colIndex = 0;
        $paths = array_keys($this->visibleColumns);
        foreach ($paths as $path) {
            if ($colPath === $path) {
                return $colIndex;
            }
            $colIndex++;
        }

        return null;
    }


    public function addColumnClass($columnId, $className)
    {
        if (isset($this->columnsIndex[$columnId])) {
            $column = $this->columnsIndex[$columnId];
            $column->addClass($className);
        }
        return $this;
    }


    /**
     *
     * @param ORM_RecordSet $set
     * @param ORM_Iterator $iterator
     */
    protected function initSortField($set, $iterator)
    {
        if (!isset($this->sortField)) {
            return;
        }

        $colIndex = $this->getColumnIndex($this->sortField);


        if (isset($colIndex) && $sortField = self::getRecordSetField($set, $this->sortField)) {
            if ($this->sortAscending) {
                $this->addColumnClass($colIndex, 'widget-table-column-sorted-asc');
                $iterator->orderAsc($sortField);
            } else {
                $this->addColumnClass($colIndex, 'widget-table-column-sorted-desc');
                $iterator->orderDesc($sortField);
            }
        }
    }


    /**
     * Fills the table view with data from the data source.
     */
    protected function init()
    {
        $iterator = $this->iterator;

        if (!isset($iterator)) {
            return;
        }

        $set = $iterator->getSet();

        $this->initColumns($set);
        $this->initSortField($set, $iterator);
        $this->initHeaderRow($set);

//         $this->initTotalRow($set);

        $this->addSection('body', null, 'widget-table-body');
        $this->setCurrentSection('body');

        $nbRows = $iterator->count();

        if (isset($this->pageLength)) {
            if ($this->currentPage > floor($nbRows / $this->pageLength)) {
                $this->currentPage = 0;
            }
        }

        $startRow = isset($this->pageLength) ? $this->currentPage * $this->pageLength : 0;
        $iterator->seek($startRow);

        $row = 0;

        $currentGroupValue = null;

        while ($iterator->valid() && (!isset($this->pageLength) || $row < $this->pageLength)) {

            if (isset($this->limit) && $row >= $this->limit) {
                break;
            }

            $record = $iterator->current();
            if ($this->isGrouping() && isset($this->sortField)) {
                $newGroupValue = self::getRecordFieldValue($record, $this->sortField);
                if ($newGroupValue !== $currentGroupValue) {
                    $this->addSection($newGroupValue, $newGroupValue);
                    $this->setCurrentSection($newGroupValue);
                    $currentGroupValue = $newGroupValue;
                }
            }
            $iterator->next();

            $row = $this->initRowNumber($record, $row);
            $record = $this->initRow($record, $row);
            $newRow = $this->handleRow($record, $row);
            if ($newRow === true) {
                $row++;
            } else {
                $row = $newRow;
            }
        }

        if (!$iterator->valid()) {
            $row = $this->handleNullRow($row);
            $row = $this->handleTotalRow($set, $row);
        }
        $this->initFooterRow($row);
    }



    /**
     * Set an anchor for destination page
     * @param string $anchorname
     * @return self
     */
    public function setAnchor($anchorname)
    {
        $this->anchorname = $anchorname;
        return $this;
    }


    /**
     * Get the anchor name of destination page
     * @return string | null
     */
    public function getAnchor()
    {
        return $this->anchorname;
    }

    /**
     * Get display of total number of rows
     *
     *
     * @return Widget_Item | null
     */
    protected function handleTotalDisplay()
    {
        if (!$this->isNumberOfRowsDisplayed()) {
            return null;
        }

        require_once dirname(__FILE__).'/frame.class.php';


        $frame = new Widget_Frame;
        $frame->addClass('totaldisplay');

        $n = $this->iterator->count();

        if ($n < 2) {
            return null;
        }

        $frame->addItem($this->handleTotalDisplayLabel($n));

        return $frame;
    }


    /**
     * Get display of total number of rows
     * @return Widget_Label
     */
    protected function handleTotalDisplayLabel($number, $desc = '')
    {
        require_once dirname(__FILE__).'/label.class.php';

        return new Widget_Label($desc . ' [' . sprintf(widget_translate('%d'), $number) . ']');
    }


    /**
     * @return Widget_PageSelector
     */
    protected function getPageSelector()
    {
        if (!isset($this->pageSelector)) {

            require_once dirname(__FILE__).'/pageselector.class.php';

            $selector = new Widget_PageSelector();
            $selector->setPageLength($this->getPageLength());
            $selector->setIterator($this->iterator);
            $selector->setCurrentPage($this->getCurrentPage());
            if (isset($this->pageAjaxAction)) {
                $action = clone $this->pageAjaxAction;
                $action->setParameter('sort', null);
                $selector->setAjaxAction($action, $this);
            }
            if (isset($this->sortField)) {
                $selector->setSortField($this->sortField);
            }

            $namePath = $this->getNamepath();
            $namePath[] = 'filter';
            $namePath[] = 'pageNumber';

            $selector->setPageNamePath($namePath);
            $selector->setAnchor($this->getAnchor());

            $this->pageSelector = $selector;
        }

        return $this->pageSelector;
    }



    /**
     * Get multi-page selector widget
     *
     *
     * @return Widget_PageSelector | null
     */
    protected function handlePageSelector()
    {
        $selector = $this->getPageSelector();

        if ($selector->getNbPages() > 1) {
            return $selector;
        }

        return null;
    }



    /**
     * Returns an array of css classes associated to the column.
     * @return string[]
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-tablemodelview';

        if (!$this->getDisableResponsive()) {
            $classes[] = 'widget-tableview-responsive';
        }

        if ($this->isNumberOfRowsDisplayed()) {
            $classes[] = 'widget-table-total-display';
        }

        $selector = $this->getPageSelector();
        if ($selector->getNbPages() > 1) {
            $classes[] = 'widget-filter-bottom';
        }

        return $classes;
    }


    /**
     * @see programs/widgets/Widget_TableView#display($canvas)
     * @todo make a variable of ['filter']['pageNumber']
     */
    public function display(Widget_Canvas $canvas)
    {
        $this->init();

        $this->addClass('depends-' . $this->getId());

        $items = array();

        $total = $this->handleTotalDisplay();
        $selector = $this->handlePageSelector();


        if (null !== $total) {
            $items[] = $total->display($canvas);
        }

        $list = parent::getLayout();
        $items[] = $list;

        if (null !== $selector) {
            $items[] = $selector->display($canvas);
        }

        $widgetsAddon = bab_getAddonInfosInstance('widgets');
        $this->setMetadata('options', $this->options);

        return $canvas->vbox(
            $this->getId(),
            $this->getClasses(),
            $items,
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        )
        . $canvas->metadata($this->getId(), $this->getMetadata())
        . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'ChartNew.js/ChartNew.js')
        . $canvas->loadScript($this->getId(), $widgetsAddon->getTemplatePath() . 'widgets.tableview.jquery.js')
        . $canvas->loadAddonStyleSheet($widgetsAddon, 'widgets.tableview.css');
    }
}
