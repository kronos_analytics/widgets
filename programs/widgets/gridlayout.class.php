<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';


require_once dirname(__FILE__) . '/boxlayout.class.php';


/**
 * Constructs a Widget_GridLayout.
 *
 * @param string	$id
 * @param string    $wrap
 * @param string    $justifyItems
 * @param string    $alignItems
 * @param string    $justifyContent
 * @param string    $alignContent
 *
 * @return Widget_GridLayout
 */
function Widget_GridLayout($id = null, $wrap = null, $justifyItems = self::JUSTIFY_ITEMS_STRETCH, $alignItems = self::ALIGN_ITEMS_STRETCH, $justifyContent = self::JUSTIFY_CONTENT_START, $alignContent = self::ALIGN_CONTENT_START)
{
    return new Widget_GridLayout($id, $wrap, $justifyItems, $alignItems, $justifyContent, $alignContent);
}


/**
 * The Widget_FlowLayout class provides a way to arrange items from left-to-right and top-to-bottom.
 */
class Widget_GridLayout extends Widget_BoxLayout implements Widget_Displayable_Interface
{

    const JUSTIFY_CONTENT_START = 'start'; //aligns the grid to be flush with the start edge of the grid container (default)
    const JUSTIFY_CONTENT_END = 'end'; //aligns the grid to be flush with the end edge of the grid container
    const JUSTIFY_CONTENT_CENTER = 'center'; //aligns the grid in the center of the grid container
    const JUSTIFY_CONTENT_STRETCH = 'stretch'; //resizes the grid items to allow the grid to fill the full width of the grid container
    const JUSTIFY_CONTENT_AROUND = 'around'; //places an even amount of space between each grid item, with half-sized spaces on the far ends
    const JUSTIFY_CONTENT_BETWEEN = 'between'; //places an even amount of space between each grid item, with no space at the far ends
    const JUSTIFY_CONTENT_EVENLY = 'evenly'; //places an even amount of space between each grid item, including the far ends

    const ALIGN_CONTENT_START = 'start'; //aligns the grid to be flush with the start edge of the grid container (default)
    const ALIGN_CONTENT_END = 'end'; //aligns the grid to be flush with the end edge of the grid container
    const ALIGN_CONTENT_CENTER = 'center'; //aligns the grid in the center of the grid container
    const ALIGN_CONTENT_STRETCH = 'stretch'; //resizes the grid items to allow the grid to fill the full height of the grid container
    const ALIGN_CONTENT_AROUND = 'around'; //places an even amount of space between each grid item, with half-sized spaces on the far ends
    const ALIGN_CONTENT_BETWEEN = 'between'; //places an even amount of space between each grid item, with no space at the far ends
    const ALIGN_CONTENT_EVENLY = 'evenly'; //places an even amount of space between each grid item, including the far ends

    const JUSTIFY_ITEMS_STRETCH = 'stretch'; //fills the whole width of the cell (default)
    const JUSTIFY_ITEMS_START = 'start'; //aligns items to be flush with the start edge of their cell
    const JUSTIFY_ITEMS_END = 'end'; //aligns items to be flush with the end edge of their cell
    const JUSTIFY_ITEMS_CENTER = 'center'; //aligns items in the center of their cell

    const ALIGN_ITEMS_STRETCH = 'stretch'; //fills the whole height of the cell (default)
    const ALIGN_ITEMS_START = 'start'; //aligns items to be flush with the start edge of their cell
    const ALIGN_ITEMS_END = 'end'; //aligns items to be flush with the end edge of their cell
    const ALIGN_ITEMS_CENTER = 'center'; //aligns items in the center of their cell
    const ALIGN_ITEMS_BASELINE = 'baseline'; //align items along text baseline

    protected $containerOptions = null;

    protected $justifyContent = 'between';
    protected $alignContent = 'normal';

    protected $justifyItems = 'center';
    protected $alignItems = 'center';

    protected $colTemplate;
    protected $rowTemplate;
    protected $areaTemplate;

    protected $rowGap = false;
    protected $rowUnit = 'px';
    protected $colGap = false;
    protected $colUnit = 'px';

    public function __construct($id = null, $wrap = null, $justifyItems = self::JUSTIFY_ITEMS_STRETCH, $alignItems = self::ALIGN_ITEMS_STRETCH, $justifyContent = self::JUSTIFY_CONTENT_START, $alignContent = self::ALIGN_CONTENT_START)
    {
        $layout = parent::__construct($id);

        $this->containerOptions = $this->options();

        $this->justifyItems($justifyItems);
        $this->alignItems($alignItems);

        $this->justifyContent($justifyContent);
        $this->alignContent($alignContent);

        return $layout;
    }

    /**
     * Defines the columns of the grid with a space-separated list of values.
     * ex: 1fr 1fr              2 columns with same size
     * ex: 1fr 100px 2fr        3 columns with roughtly 33% 100px 66% where % is the remaining after 100px had been taken
     * ex: repeat(5, 1fr)       5 columns with same size
     *
     * special keywords:
     *      > (x)fr:            fractional units, a fraction of the remaining space.
     *      > min-content:      the minimum size of the content. Imagine a line of text like "The very long hotdog.", the min-content is likely the width of the world "The".
     *      > max-content:      the maximum size of the content. Imagine the sentence above, the max-content is the length of the whole sentence.
     *      > auto:             this keyword is a lot like fr units, except that they "lose" the fight in sizing against fr units when allocating the remaining space.
     *      > fit-content:      Use the space available, but never less than min-content and never more than max-content.
     *      > minmax(x, y):     The minmax() function does exactly what it seems like: it sets a minimum and maximum value for what the length is able to be.
     *      > min()
     *      > max()
     *      > auto-fill:        Use in repeat function, Fit as many possible columns as possible on a row, even if they are empty.)
     *      > auto-fit:         Use in repeat function, Fit whatever columns there are into the space. Prefer expanding columns to fill space rather than empty columns.
     *
     * @param string $css
     *
     * @url https://css-tricks.com/snippets/css/complete-guide-grid/#aa-grid-template-columnsgrid-template-rows
     *
     * @return Widget_GridLayout
     */
    public function setColumnTemplate(string $css)
    {
        $this->containerOptions->colTemplate($css);

        return $this;
    }

    /**
     * Defines the row of the grid with a space-separated list of values.
     * ex: 1fr 1fr              2 rows with same size
     * ex: 1fr 100px 2fr        3 rows with roughtly 33% 100px 66% where % is the remaining after 100px had been taken
     * ex: repeat(5, 1fr)       5 rows with same size
     *
     * special keywords:
     *      > (x)fr:            fractional units, a fraction of the remaining space.
     *      > min-content:      the minimum size of the content. Imagine a line of text like "The very long hotdog.", the min-content is likely the width of the world "The".
     *      > max-content:      the maximum size of the content. Imagine the sentence above, the max-content is the length of the whole sentence.
     *      > auto:             this keyword is a lot like fr units, except that they "lose" the fight in sizing against fr units when allocating the remaining space.
     *      > fit-content:      Use the space available, but never less than min-content and never more than max-content.
     *      > minmax(x, y):     The minmax() function does exactly what it seems like: it sets a minimum and maximum value for what the length is able to be.
     *      > min()
     *      > max()
     *      > auto-fill:        Use in repeat function, Fit as many possible columns as possible on a row, even if they are empty.)
     *      > auto-fit:         Use in repeat function, Fit whatever columns there are into the space. Prefer expanding columns to fill space rather than empty columns.
     *      > masonry:          https://css-tricks.com/snippets/css/complete-guide-grid/#aa-masonry
     *
     * @param string $css
     *
     * @url https://css-tricks.com/snippets/css/complete-guide-grid/#aa-grid-template-columnsgrid-template-rows
     *
     * @return Widget_GridLayout
     */
    public function setRowTemplate(string $css)
    {
        $this->containerOptions->rowTemplate($css);

        return $this;
    }

    /**
     * Defines a grid template by referencing the names of the grid areas which are specified with the grid-area property of children items.
     * Repeating the name of a grid area causes the content to span those cells.
     * A period signifies an empty cell.
     * The syntax itself provides a visualization of the structure of the grid.
     *
     * ex:
     * array('header header header header', 'main main . sidebar', 'footer footer footer footer');
     *
     *
     * @param string $css
     *
     * @url https://css-tricks.com/snippets/css/complete-guide-grid/#aa-grid-template-areas
     *
     * @return Widget_GridLayout
     */
    public function setAreaTemplate(array $css)
    {
        $this->containerOptions->areaTemplate($css);

        return $this;
    }

    /**
     * Controls the space between items
     *
     * @param int|float             $rowGap
     * @param string                $rowUnit    'px' (default), 'pt', 'em' or '%'
     * @param boolean|int|float     $colGap     if false col gap value and unit will be use as row gap and unit
     * @param string                $colUnit    'px' (default), 'pt', 'em' or '%'
     */
    public function setGap($rowGap, $rowUnit = 'px', $colGap = false, $colUnit = 'px')
    {
        assert('is_numeric($rowGap); /* The spacing parameter must be an integer */');

        $this->setVerticalGap($rowGap, $rowUnit);
        if ($colGap !== false) {
            $this->setHorizontalGap($colGap, $colUnit);
        } else {
            $this->setHorizontalGap($rowGap, $rowUnit);
        }

        return $this;
    }

    /**
     * Controls the horizontal spacing between items
     *
     * @param int|float	        $gap
     * @param string            $unit      'px' (default), 'pt', 'em' or '%'
     */
    public function setHorizontalGap($gap, $unit = 'px')
    {
        assert('is_numeric($gap); /* The spacing parameter must be an integer */');
        $this->containerOptions->colGap($gap, $unit);

        return $this;
    }

    /**
     * Controls the vertical spacing between items
     *
     * @param int|float	        $gap
     * @param string            $unit      'px' (default), 'pt', 'em' or '%'
     */
    public function setVerticalGap($gap, $unit = 'px')
    {
        assert('is_numeric($gap); /* The spacing parameter must be an integer */');
        $this->containerOptions->rowGap($gap, $unit);

        return $this;
    }



    /**
     * Define justifty items mod, available values are:
     * stretch (default), start, end, center
     *
     * @param string $mode
     * @url https://css-tricks.com/snippets/css/complete-guide-grid/#aa-justify-items
     */
    public function justifyItems($mode)
    {
        switch ($mode) {
            case self::JUSTIFY_ITEMS_STRETCH:
            case self::JUSTIFY_ITEMS_START:
            case self::JUSTIFY_ITEMS_END:
            case self::JUSTIFY_ITEMS_CENTER:
                $this->justifyItems = $mode;
                break;
            default:
                $this->justifyItems(self::JUSTIFY_ITEMS_STRETCH);
        }

        return $this;
    }


    /**
     * Define align itemt mod, available values are:
     * start, end, center (default), stretch, baseline
     *
     * @param string $mode
     * @url https://css-tricks.com/snippets/css/complete-guide-grid/#aa-align-items
     */
    public function alignItems($mode)
    {
        switch ($mode) {
            case self::ALIGN_ITEMS_STRETCH:
            case self::ALIGN_ITEMS_START:
            case self::ALIGN_ITEMS_END:
            case self::ALIGN_ITEMS_CENTER:
            case self::ALIGN_ITEMS_BASELINE:
                $this->alignItems = $mode;
                break;
            default:
                $this->alignItems(self::ALIGN_ITEMS_STRETCH);
        }

        return $this;
    }


    /**
     * Define justifty content mod, available values are:
     * start (default), end, center, stretch, around, between, evenly
     *
     * @param string $mode
     * @url https://css-tricks.com/snippets/css/complete-guide-grid/#aa-justify-content
     */
    public function justifyContent($mode)
    {
        switch ($mode) {
            case self::JUSTIFY_CONTENT_START:
            case self::JUSTIFY_CONTENT_END:
            case self::JUSTIFY_CONTENT_CENTER:
            case self::JUSTIFY_CONTENT_STRETCH:
            case self::JUSTIFY_CONTENT_AROUND:
            case self::JUSTIFY_CONTENT_BETWEEN:
            case self::JUSTIFY_CONTENT_EVENLY:
                $this->justifyContent = $mode;
                break;
            default:
                $this->justifyContent(self::JUSTIFY_CONTENT_START);
        }

        return $this;
    }



    /**
     * Define align content mod, available values are:
     * start (default), end, center, stretch, between, around, evenly
     *
     * @param string $mode
     * @url https://css-tricks.com/snippets/css/complete-guide-grid/#aa-align-content
     */
    public function alignContent($mode)
    {
        switch ($mode) {
            case self::ALIGN_CONTENT_START:
            case self::ALIGN_CONTENT_END:
            case self::ALIGN_CONTENT_CENTER:
            case self::ALIGN_CONTENT_STRETCH:
            case self::ALIGN_CONTENT_AROUND:
            case self::ALIGN_CONTENT_BETWEEN:
            case self::ALIGN_CONTENT_EVENLY:
                $this->alignContent = $mode;
                break;
            default:
                $this->alignContent(self::ALIGN_CONTENT_START);
        }

        return $this;
    }

	/**
	 * Adds $item to this layout. The position of the item can be specified.
	 * If the position is not specified (or null), the item is appended.
	 *
	 * @param Widget_Displayable_Interface $item
	 * @param int $position
	 * @return Widget_FlowLayout
	 */
	public function addItem(Widget_Displayable_Interface $item = null, $position = null)
	{
		parent::addItem($item, $position);
		return $this;
	}

	/**
	 * @return array
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-layout-grid';

		return $classes;
	}

	/**
	 * NOT PROTECTED BUT SHOULD ONLY ME CALL IN THE DISPLAY
	 *
	 * @return array
	 */
	public function getDisplayClasses()
	{
	    $classes = $this->getClasses();

	    $classes[] = 'widget-layout-grid-justify-items-'.$this->justifyItems;
	    $classes[] = 'widget-layout-grid-align-items-'.$this->alignItems;

	    $classes[] = 'widget-layout-grid-justify-content-'.$this->justifyContent;
	    $classes[] = 'widget-layout-grid-align-content-'.$this->alignContent;

	    return $classes;
	}


	public function setVerticalSpacing($spacing)
	{
	    trigger_error('SetVerticalSpacing method could not be use on grid item, you should use setVerticalGap instead.');
	    return false;
	}


	public function setHorizontalSpacing($spacing)
	{
	    trigger_error('setHorizontalSpacing method could not be use on grid item, you should use setHorizontalGap instead.');
	    return false;
	}

	/**
	 * @return Widget_CanvasOptions
	 */
	public function getContainerOptions()
	{
	    return $this->containerOptions;
	}

	/**
	 * @param Widget_Canvas $canvas
	 */
	public function display(Widget_Canvas $canvas)
	{
		$items = array();
		foreach ($this->getItems() as $item) {
			$items[] = $item->noParent();
		}

		$classes = $this->getDisplayClasses();

		$attributes = $this->getAttributes();


		return $canvas->grid(
            $this->getId(),
		    $classes,
			$items,
		    $this->getContainerOptions(),
			$this->getCanvasOptions(),
			$this->getTitle(),
		    $attributes
		)
		. $canvas->metadata($this->getId(), $this->getMetadata());
	}
}
