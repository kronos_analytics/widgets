<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
bab_Widgets()->includePhpClass('Widget_BabPage');

/**
 * Constructs a Widget_ContextPage.
 *
 * @param 	string			$id			The item unique id.
 * @param	Widget_Layout	$layout
 * @return Widget_ContextPage
 */
function Widget_ContextPage($id = null, Widget_Layout $layout = null)
{
	return new Widget_ContextPage($id, $layout);
}


/**
 * A Widget_ContextPage is a babPage with contextual zone on the right
 * and a toolbar on top
 */
class Widget_ContextPage extends Widget_BabPage
{
	/**
	 * @var Widget_Layout
	 */
	private $mainPanel = null;

	/**
	 * @var Widget_Frame
	 */
	private $context = null;


	private $toolbars;


	public function __construct($id = null, Widget_Layout $layout = null)
	{
		parent::__construct($id, $layout);

		/* @var $I Func_Icons */
		$I = bab_Functionality::get('Icons');
		if ($I) {
			$I->includeCss();
		}

		$this->addClass('context-page');
	}




	/**
	 * Sets the layout for the page main panel.
	 *
	 * @param Widget_Layout $layout
	 *
	 * @return Widget_ContextPage
	 */
	public function setMainPanelLayout(Widget_Layout $layout)
	{
		$this->mainPanel = $layout;
		return $this;
	}


	/**
	 * Add item to page
	 * @param	Widget_Item $item
	 * @return	Widget_ContextPage
	 */
	public function addItem(Widget_Displayable_Interface $item)
	{
		if (!isset($this->mainPanel)) {
			$W = bab_Widgets();
			$this->mainPanel = $W->VBoxLayout();
		}
		$this->mainPanel->addItem($item);
		$this->mainPanel->setParent($this);
		return $this;
	}


	/**
	 * Add the item to the context panel of the page
	 * @param	Widget_Item $item
	 * @param	string		$title		An optional title which, if specified, will create a section title and append the item below.
	 * @return	Widget_ContextPage
	 */
	public function addContextItem(Widget_Displayable_Interface $item, $title = null, $position = null)
	{
		$W = bab_Widgets();

		if (null === $this->context) {
			$this->context = $W->Frame(
				'widget_context_panel',
				$W->VBoxLayout()->setVerticalSpacing(2, 'em')
			);
		}
		if (isset($title)) {
			$this->context->addItem(
				$W->Section(
					$title,
					$item,
					4
				),
				$position
			);
		} else {
			$this->context->addItem($item, $position);
		}
		return $this;
	}


	/**
	 * Add the item to the context panel of the page
	 * @param	Widget_Displayable_Interface $item
	 * @return	Widget_ContextPage
	 */
	public function addToolbar(Widget_Displayable_Interface $item)
	{
		$W = bab_Widgets();

		if (null === $this->toolbars) {
			$this->toolbars = $W->Frame(
				'widget_toolbars_panel',
				$W->VBoxItems(
				)
			);
		}
		$this->toolbars->addItem($item);

		return $this;
	}


	/**
	 * @param Widget_Canvas $canvas
	 * @return string
	 */
	public function display(Widget_Canvas $canvas)
	{
		$W = bab_Widgets();

		$layout = $W->Frame('widget_main_panel', $this->mainPanel);



		if ($this->context) {

			$layout = $W->HBoxItems(
				$layout
					->setSizePolicy('widget-main-panel'),
				$this->context
					->setSizePolicy('widget-context-panel')
			);

			$this->addClass('widget-two-panel-page');
		}

		if ($this->toolbars) {
			$layout = $W->VBoxItems(
				$this->toolbars,
				$layout
					->setSizePolicy(Widget_SizePolicy::MAXIMUM)
			);
		}

		$this->setLayout($layout);

		return parent::display($canvas);
	}


}