<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/layout.class.php';


/**
 * Constructs a Widget_GridParent.
 *
 * @param int		$nbColumns	The number of columns in the grid
 * @param string	$id			The item unique id.
 * @return Widget_GridParent
 */
function Widget_GridParent($nbColumns = null, $id = null)
{
    return new Widget_GridParent($nbColumns, $id);
}


/**
 * The Widget_GridParent class provides a way to arrange widgets/items in a grid.
 *
 */
class Widget_GridParent extends Widget_Layout implements Widget_Displayable_Interface
{

    private $_nbColumns = -1;

    /**
     *
     * @var Widget_TableView
     */
    protected $_table = null;

    protected $_sectionLabels = array('' => null);
    protected $_sectionLabelColspan = array('' => null);
    protected $_sectionClasses = array('' => null);
    protected $_sectionOptions = array('' => null);

    protected $_headerSectionId;

    protected $_sections = array('' => array());

    /**
     * The current section id.
     * @var string
     */
    private $_currentSection;

    /**
     * @param int    $nbColumns Number of columns in the grid.
     * @param string $id        The item unique id.
     */
    public function __construct($nbColumns = null, $id = null)
    {
        parent::__construct($id);

        if (isset($nbColumns)) {
            $this->setNbColumns($nbColumns);
        }

        $this->_currentSection = '';
    }


    /**
     * @param Widget_TableView $table
     *
     * @return self
     */
    public function setTableView(Widget_TableView $table = null)
    {
        $this->_table = $table;
        return $this;
    }

    /**
     * Get cell content or null if empty
     * @return Widget_Item
     */
    public function getCell($row, $col)
    {
        if (isset($this->_sections[$this->_currentSection][$row][$col])) {
            return $this->_sections[$this->_currentSection][$row][$col];
        }

        return null;
    }


    /**
     * Sets number of columns in the grid.
     *
     * @return self
     */
    public function setNbColumns($nbColumns)
    {
        $this->_nbColumns = $nbColumns;
        return $this;
    }


    /**
     * Creates a new header section in the layout.
     *
     * @param string	$id				The unique identifier (for the layout) of the new section.
     * @param string	$label			An optional text label.
     * @param string	$className		An optional class name.
     * @param int       $colspan        colspan for label row
     *
     * @return self
     */
    public function addHeaderSection($id, $label = null, $className = null, $colspan = null)
    {
        $this->_headerSectionId = $id;

        $this->_sections[$id] = array();
        $this->_sectionOptions[$id] = array();
        $this->_sectionLabels[$id] = $label;
        $this->_sectionClasses[$id] = $className;
        $this->_sectionLabelColspan[$id] = $colspan;

        return $this;
    }


    /**
     * Creates a new section in the layout.
     *
     * @param string	$id				The unique identifier (for the layout) of the new section.
     * @param string	$label			An optional text label.
     * @param string	$className		An optional class name.
     * @param int       $colspan        colspan for label row
     *
     * @return self
     */
    public function addSection($id, $label = null, $className = null, $colspan = null)
    {
        $this->_sections[$id] = array();
        $this->_sectionOptions[$id] = array();

        $this->_sectionLabels[$id] = $label;
        $this->_sectionClasses[$id] = $className;
        $this->_sectionLabelColspan[$id] = $colspan;

        return $this;
    }


    /**
     * Sets the current section to which subsquent addItem call will add items.
     *
     * @param string	$id		The identifier of the section (previously added by @see addSection).
     * @return self
     */
    public function setCurrentSection($id)
    {
        $this->_currentSection = $id;
        return $this;
    }


    /**
     * @return string
     */
    public function getCurrentSection()
    {
        return $this->_currentSection;
    }




    /**
     * Adds $item to this layout.
     * The position in the grid, as well as the spanning in the grid can be specified.
     *
     * @param Widget_Item	$item
     * @param int		$row
     * @param int		$col
     * @param int		$rowSpan
     * @param int		$colSpan
     * @return self
     */
    public function addItem(Widget_Displayable_Interface $item = null, $row = -1, $col = -1, $rowSpan = -1, $colSpan = -1)
    {
//		assert('is_int($row); /* The "row" parameter must be an int. */');
//		assert('is_int($col); /* The "col" parameter must be an int. */');
//		assert('is_int($rowSpan); /* The "rowSpan" parameter must be an int. */');
//		assert('is_int($colSpan); /* The "colSpan" parameter must be an int. */');

        if (null !== $item)
        {
            parent::addItem($item);
        }

        $this->_sections[$this->_currentSection][$row][$col] = $item;

        if ($colSpan > -1) {
            $this->_sectionOptions[$this->_currentSection][$row][$col]['colspan'] = $colSpan;
        }
        if ($rowSpan > -1) {
            $this->_sectionOptions[$this->_currentSection][$row][$col]['rowspan'] = $rowSpan;
        }
        if ($col > $this->_nbColumns - 1) {
            $this->_nbColumns = $col + 1;
        }
        return $this;
    }


    /**
     * Adds $item to this layout.
     * The position in the grid, as well as the spanning in the grid can be specified.
     *
     * @param array			$item
     * @param int			$row
     * @param int			$col
     * @param int			$rowSpan
     * @param int			$colSpan
     * @return self
     */
    public function addRow($items, $row = -1)
    {
//		assert('is_array($items); /* The "items" parameter must be an array. */');
//		assert('is_int($row); /* The "row" parameter must be an int. */');
        $col = 0;
        foreach ($items as $item) {
            $this->addItem($item, $row, $col++);
        }

        return $this;
    }


    /**
     * @see programs/widgets/Widget_Layout#getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-layout-gridparent';
        return $classes;
    }

    /**
     * @param	int		$columnId
     * @return array
     */
    private function getColumnClasses($columnId, Widget_Displayable_Interface $cellContent = null)
    {
        if (!isset($this->_table)) {
            return array();
        }

        return $this->_table->getColumnClasses($columnId, $cellContent);
    }

    /**
     * Add a class on a row
     * @param int       $rowId
     * @param string    $className
     * @return self
     */
    public function addRowClass($rowId, $className)
    {
        if (!isset($this->_table)) {
            return $this;
        }

        $this->_table->addRowClass($rowId, $className);

        return $this;
    }

    /**
     * @param	int		$rowId
     * @return array
     */
    private function getRowClasses($rowId)
    {
        if (!isset($this->_table)) {
            return array();
        }

        return $this->_table->getRowClasses($rowId);
    }



    private function getRows(Widget_Canvas $canvas, $sectionId, Array $section, $table = null)
    {
        if (count($section) == 0) {
            return null;
        }

        $this->setCurrentSection($sectionId);

        $rows = array();
        if (isset($this->_sectionLabels[$sectionId])) {
            $cells = array();
            $cells[] = $canvas->gridRowCell(null, array($this->_sectionClasses[$sectionId]), array($this->_sectionLabels[$sectionId]), null, null, $this->_sectionLabelColspan[$sectionId]);
            $rows[] = $canvas->gridRow(null, array('widget-table-section-header'), $cells);
        }
        foreach ($section as $rowId => $itemRow) {
            $cells = array();
            foreach ($itemRow as $colId => $item) {

                $colSpan = isset($this->_sectionOptions[$sectionId][$rowId][$colId]['colspan'])
                ? $this->_sectionOptions[$sectionId][$rowId][$colId]['colspan']
                : null;
                $rowSpan = isset($this->_sectionOptions[$sectionId][$rowId][$colId]['rowspan'])
                ? $this->_sectionOptions[$sectionId][$rowId][$colId]['rowspan']
                : null;

                $colDesc = null;
                if ($table instanceof widget_TableModelView) {
                    $colDesc = $table->getColumnDescription($colId);
                }

                $cells[] = $canvas->gridRowCell(null, $this->getColumnClasses($colId, $item), array($item), null, $rowSpan, $colSpan, $colDesc);
            }
            $rows[] = $canvas->gridRow(null, $this->getRowClasses($rowId), $cells);
        }

        return $rows;
    }


    private function displayHeaderSection(Widget_Canvas $canvas, $sectionId, Array $section)
    {
        $rows = $this->getRows($canvas, $sectionId, $section);

        if (!isset($rows)) {
            return null;
        }

        return $canvas->gridHeaderSection('header_section' . $sectionId, array('widget-table-section', $this->_sectionClasses[$sectionId]), $rows);
    }


    private function displaySection(Widget_Canvas $canvas, $sectionId, Array $section)
    {
        $rows = $this->getRows($canvas, $sectionId, $section, $this->_table);

        if (!isset($rows)) {
            return null;
        }

        return $canvas->gridSection('header_section' . $sectionId, array('widget-table-section', $this->_sectionClasses[$sectionId]), $rows);
    }



    /**
     * @see programs/widgets/Widget_Layout#display($canvas)
     */
    public function display(Widget_Canvas $canvas)
    {

        require_once dirname(__FILE__) . '/label.class.php';

        $sections = array();


        if (isset($this->_headerSectionId)) {
            if ($headerCanvas = $this->displayHeaderSection($canvas, $this->_headerSectionId, $this->_sections[$this->_headerSectionId])) {
                $sections[] = $headerCanvas;
            }
        }

        foreach ($this->_sections as $sectionId => $section) {

            if ($this->_headerSectionId === $sectionId) {
                continue;
            }

            if ($sectionCanvas = $this->displaySection($canvas, $sectionId, $section)) {
                $sections[] = $sectionCanvas;
            }
        }

        return $canvas->gridparent(
            $this->getId(),
            $this->getClasses(),
            $sections,
            array(),
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        ). $canvas->metadata($this->getId(), $this->getMetadata());
    }
}
