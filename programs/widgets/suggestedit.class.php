<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/urlincl.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/json.php';
require_once dirname(__FILE__) . '/lineedit.class.php';




/**
 * Widget_SuggestEdit.
 */
class Widget_SuggestEdit extends Widget_InputWidget
{
    /**
     * Proposition of a limit to use in scripts for calls to addSuggestion
     * @see Widget_SuggestEdit::addSuggestion()
     * @var	int
     */
    const MAX = 100;

    /**
     * Name of the id field
     * @var string|array
     */
    protected $id_name = null;


    /**
     * Value of the id field
     * @var string
     */
    protected $id_value = '';


    /**
     *
     */
    protected $hiddenId = null;

    protected $suggestlist = array();

    /**
     * The suggestCounter is used to generate unique ids.
     * @var int $suggestCounter
     */
    private static $suggestCounter = 1;


    /**
     * @var Widget_Action
     */
    private $suggestAction = null;

    private $convertCase = -1;


    /**
     * @param string $id			The item unique id.
     */
    public function __construct($id = null)
    {
        parent::__construct($id);

        $W = bab_Widgets();

        $this->hiddenId = $W->Hidden();
        $this->setMetadata('suggesturl', $_GET + $_POST);
        $this->setMetadata('suggestparam', $this->getId().'_search');
        $this->setMetadata('keyup', array());
        $this->setMetadata('id_target', $this->getId().'_ID');
    }


//     /**
//      * Generates and return a unique id for the current page.
//      * use a counter only affected by other suggest box
//      */
//     protected function createId()
//     {
//         return strtolower(get_class($this)) . self::$suggestCounter++;
//     }


    /**
     * @param string    $message
     */
    public function setSelectMandatory($message = '')
    {
        $this->hiddenId->setMandatory(true, $message);

        return $this;
    }


    /**
     *
     * @param Widget_Action $action The widget_Action that should
     * 			                    return the json encoded result as
     *                              the sendSuggestions() method.
     *
     *  @return self
     */
    public function setSuggestAction(Widget_Action $action, $suggestParamName = null)
    {
        $this->suggestAction = $action;
        $this->setMetadata('suggesturl', $action->getParameters());
        if (isset($suggestParamName)) {
            $this->setMetadata('suggestparam', $suggestParamName);
        }

        return $this;
    }


    /**
     * @return Widget_Action|null
     */
    public function getSuggestAction()
    {
        return $this->suggestAction;
    }


    /**
     * Convert an array as query string
     *
     * @param	string	$name
     * @param	array	$arr
     *
     * @return	string
     */
    private static function urlAsArray($name, $arr)
    {
        $params = array();

        foreach ($arr as $key => $value) {

            if (!is_null($value)) {
                if (is_array($value)) {
                    $params[] = self::urlAsArray($name.'['.$key.']', $value);
                } else {
                    $params[] = urlencode($name.'['.$key.']').'='.urlencode($value);
                }
            }
        }

        return implode('&', $params);
    }





    /**
     * get search keyword
     * return a string search keyword if the widget try to get suggestions
     * or false if the widget does not search for suggestions
     *
     * @return false|string
     */
    public function getSearchKeyword()
    {
        $keyword = bab_rp($this->getMetadata('suggestparam'), false);
        if (false === $keyword) {
            return false;
        }

        $keyword = bab_getStringAccordingToDataBase($keyword, 'UTF-8');

        return $keyword;
    }



    /**
     * Adds a suggestion.
     * id, info and extra can be pushed in other fields on suggest selection,
     * this can be done using the coresponding metadata.
     * 	<ul>
     * 		<li>id_target</li>
     * 		<li>info_target</li>
     * 		<li>extra_target</li>
     *  </ul>
     * each metadata can contain a widget id
     *
     *
     * @param	string	$id			id of the suggestion string, not displayed, @see Widget_SuggestEdit::getSuggestId()
     * @param	string	$value		value is the main suggestion string, it will be set in input value on click
     * @param	string	$info		info is displayed under the value in smaller font
     * @param	string	$extra		extra is not used by default
     * @param	string	$cssclass
     *
     * @return 	self
     */
    public function addSuggestion($id, $value, $info = '', $extra = '', $cssclass = '')
    {
        $this->suggestlist[] = array(
            'id' 	=> $id,
            'value'	=> $this->transformText($value),
            'info'	=> $info,
            'extra'	=> $extra,
            'cssclass' => $cssclass
        );

        return $this;
    }


    /**
     * Send suggestions if necessary
     * @return 	self
     */
    public function sendSuggestions()
    {
        header('Content-Type: application/json');
        die(bab_convertStringFromDatabase(bab_json_encode($this->suggestlist), bab_charset::UTF_8));
    }


    /**
     * Defines the minimum number of chars to type before autocompletion is triggered.
     *
     * @param int $minChars
     * @return self
     */
    public function setMinChars($minChars)
    {
        $this->setMetadata('minChars', $minChars);
        return $this;
    }


    /**
     * Set a javascript function name to call when the value is modified in the input
     * this can be by user input or by selection of a suggestion
     *
     * @param 	string 	$js_function
     * @param	string	$domain
     *
     * @return self
     */
    public function onKeyUp($js_function, $domain = 'window.babAddonWidgets')
    {
        $arr = $this->getMetadata('keyup');
        $arr[] = $domain.'.'.$js_function;

        $this->setMetadata('keyup', $arr);
        return $this;
    }




    /**
     * Get request value from full name
     * @param Array $fullname
     * @return string | null
     */
    protected function getRequestValueFromName(Array $fullname)
    {
        $r = $_REQUEST;
        foreach ($fullname as $name) {
            if (isset($r[$name])) {
                $r = $r[$name];
            } else {
                return null;
            }
        }

        return $r;
    }




    /**
     * Set the name of ID hidden field
     * if the the id name is not set with this method, the id property of suggestion will not be accessible
     * if the id name is set a hidden field with this name will exists
     *
     * @param	string|array	$name
     * @return self
     */
    public function setIdName($name)
    {
        $this->id_name = $name;
        return $this;
    }



    /**
     * Set the value of ID hidden field
     *
     * @see Widget_SuggestEdit::setIdName()
     *
     * @param ORM_Record|int|string     $value
     * @return self
     */
    public function setIdValue($value)
    {
        if ($value instanceof ORM_Record) {
            $pkFieldName = $value->getParentSet()->getPrimaryKey();
            $value = $value->getValue($pkFieldName);
        }

        $this->id_value = $value;
        return $this;
    }


    /**
     * Sets the value.
     *
     * @param mixed $value
     * @return self
     */
    public function setValue($value)
    {
        parent::setValue($value);
        if (is_int($value) || $value instanceof ORM_Record) {
            $this->setIdValue($value);
        }
        return $this;
    }

    /**
     * Use multivalues mode in suggest field
     * @param string $separator
     * @return self
     */
    public function setMultiple($separator)
    {
        assert('is_string($separator); /* The "separator" parameter must be a string. */');
        assert('0 < mb_strlen($separator); /* The "separator" parameter must not be empty. */');

        $this->setMetadata('multiple_separator', $separator);
        return $this;
    }


    /**
     * Use 'trigger' mode in suggest field.
     *
     * @param string $start
     * @param string $end
     * @return self
     */
    public function setTriggers($start, $end)
    {
        $this->setMetadata('trigger_start', $start);
        $this->setMetadata('trigger_end', $end);
        return $this;
    }



    /**
     * Sets the value of the text transformation.
     * Currently only uppercase / lowercase transformation.
     *
     * @param int  $transform          One of MB_CASE_LOWER, MB_CASE_UPPER or MB_CASE_TITLE.
     * @return self
     */
    public function setConvertCase($transform)
    {
        $this->convertCase = $transform;
        return $this;
    }


    /**
     * Returns the value of the text transformation.
     *
     * @return int
     */
    public function getConvertCase()
    {
        return $this->convertCase;
    }



    /**
     * Returns the text $text transformed according to the convertCase selected.
     *
     * @param string $text
     * @return string
     */
    protected function transformText($text)
    {
        $convertCase = $this->getConvertCase();

        switch ($convertCase) {
            case MB_CASE_LOWER:
            case MB_CASE_UPPER:
            case MB_CASE_TITLE:
                return mb_convert_case($text, $convertCase, bab_charset::getIso());
        }

        return $text;
    }




    /**
     * (non-PHPdoc)
     * @see Widget_InputWidget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-suggestedit';
        return $classes;
    }




    /**
     * @param Widget_Canvas $canvas
     * @return string
     */
    public function display(Widget_Canvas $canvas)
    {
        $output = '';
        $name = $this->getName();

        if (null === $this->id_name) {

            if (is_array($name)) {
                $name = implode('_', $this->getName());
            }
            $this->id_name = $name . '_ID';
        }


        $nameCount = 1;
        if (is_array($name)) {
            $nameCount = count($name);
        }

        $idFullname = $this->getFullName();
        for($i=0; $i< $nameCount; $i++) {
            array_pop($idFullname);
        }


        if (is_array($this->id_name)) {
            $idFullname = array_merge($idFullname, $this->id_name);
        } else {
            array_push($idFullname, $this->id_name);
        }

        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        $this->setValue($this->getValue());

        $hiden = $this->hiddenId->addClass('widget-hidden-suggest')
            ->setMetadata('parent_id', $this->getId())
            ->setId($this->getId() . '_ID')
            ->setName($idFullname)
            ->setValue($this->id_value);

        if (!$this->isDisplayMode()) {
            $output .= $hiden->display($canvas);

            $output .= $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.suggestedit.jquery.js');
            $output .= $canvas->loadAddonStyleSheet($widgetsAddon, 'widgets.suggestedit.css');
        }

        return $output;
    }



    /**
     * Method call by the suggest.php url.
     */
    public function outputSuggestions()
    {
        $this->setConvertCase(bab_rp('convertCase', -1));
        $this->suggest();
    }
}
