<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__) . '/inputwidget.class.php';


/**
 * Constructs a Widget_DatePicker.
 *
 * @param string        $id         The item unique id.
 * @return Widget_UserPicker
 */
function Widget_UserPicker($id = null)
{
    return new Widget_UserPicker($id);
}


/**
 * A Widget_DatePicker is a widget that let the user enter a date.
 */
class Widget_UserPicker extends Widget_InputWidget implements Widget_Displayable_Interface
{

    /**
     * @param string $id            The item unique id.
     * @return Widget_UserPicker
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
    }




    public function getClasses()
    {
        $classes = parent::getClasses();
        if (!$this->isDisplayMode()) {
            $classes[] = 'widget-userpicker';
        }
        $classes[] = 'icon';
        $classes[] = 'apps-users';
        return $classes;
    }



    public function display(Widget_Canvas $canvas)
    {
        $value = $this->getValue();

        $info = bab_getUserInfos($value);


        if ($this->isDisplayMode()) {

            $classes = $this->getClasses();
            $classes[] = 'widget-displaymode';

            return $canvas->richtext(
                $this->getId(),
                $classes,
                $info['sn'] . ' ' . $info['givenname'],
                BAB_HTML_ALL ^ BAB_HTML_P,
                $this->getCanvasOptions()
            );

        }

        if($info) {
            $this->setMetadata('userName', $info['sn'] . ' ' . $info['givenname']);
        }
        $jquery = bab_jquery();

        $widgetsAddon = bab_getAddonInfosInstance('widgets');


        return $canvas->span(
            $this->getId(),
            $this->getClasses(),
            array(
                $canvas->lineInput(
                    $this->getId().'input',
                    array(),
                    $this->getFullName(),
                    $value,
                    10,
                    10,
                    false,
                    false,
                    false,
                    'text',
                    $this->getPlaceHolder(),
                    null,
                    null,
                    null
                )
            ),
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        )
        . $canvas->metadata($this->getId(), $this->getMetadata())
        . $canvas->loadStyleSheet($GLOBALS['babInstallPath'] . 'styles/' . $jquery->getStyleSheetUrl())
        . $canvas->loadScript($this->getId(), $GLOBALS['babInstallPath'] . 'scripts/bab_dialog.js')
        . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.userpicker.jquery.js');
    }
}
