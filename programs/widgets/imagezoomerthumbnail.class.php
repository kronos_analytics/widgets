<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/imagezoomer.class.php';
bab_functionality::includeFile('Thumbnailer');


/**
 * Constructs a Widget_ImageZoomerThumbnail.
 *
 * @param bab_Path		$imagePath	The image source (path).
 * @param string		$labelText	The label text.
 * @param string		$id			The item unique id.
 * @return Widget_ImageZoomerThumbnail
 */
function Widget_ImageZoomerThumbnail(bab_Path $imagePath, $labelText = '', $id = null)
{
    return new Widget_ImageZoomerThumbnail($imagePath, $labelText, $id);
}



class Widget_ImageZoomerThumbnail extends Widget_ImageZoomer implements Widget_Displayable_Interface
{

    /**
     *
     * @var bab_Path
     */
    private $path;

    private $label;
    
    private $wrapAround;

    private $thumb_width = 48;
    private $thumb_height = 48;

    private $big_width = 500;
    private $big_height = 500;

    private $resizemode = null;


    /**
     * Constructs a Widget_ImageZoomerThumbnail.
     *
     * @param bab_Path		$imagePath	The image source (path).
     * @param string		$labelText	The label text.
     * @param string		$id			The item unique id.
     *
     */
    public function __construct(bab_Path $imagePath = null, $labelText = '', $id = null)
    {
        parent::__construct('', '', $labelText);
     
        $this->path = $imagePath;
        $this->setLabel(widget_translate("Image %1/%2"));
        $this->wrapAround(false);
    }
    
    /**
    * Set label
    * add %1 for image number
    * add %2 for image count
    * 
    */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function wrapAround($wrap = true)
    {
        $this->wrapAround = $wrap;
    }
    

    public function setThumbnailSize($width, $height)
    {
        $this->thumb_width = $width;
        $this->thumb_height = $height;

        return $this;
    }



    public function setZoomSize($width, $height)
    {
        $this->big_width = $width;
        $this->big_height = $height;

        return $this;
    }



    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-imagezoomerthumbnail';
        return $classes;
    }


    /**
     * Set resize mode for the small image only
     * @param int $resizemode	Func_Thumbnailer::KEEP_ASPECT_RATIO | Func_Thumbnailer::CROP_CENTER
     */
    public function setResizeMode($resizemode)
    {
        $this->resizemode = $resizemode;
        return $this;
    }



    /**
     * Load thumbnail if exists or prepare needed metadata and session infos for javascript request
     * @return Widget_FileIcon
     */
    private function checkForThumbnail($id, $width, $height, $resizemode = null, $method = 'setBigImageUrl')
    {
        if ($T = @bab_functionality::get('Thumbnailer'))
        {
            /*@var $T Func_Thumbnailer */
            $T->setSourceFile($this->path);
            $T->setImageFormat('png');

            if (null !== $resizemode)
            {
                $T->setResizeMode($resizemode);
            }

            // if there is already a thumbnail, use it
            $url = $T->getDelayedThumbnail($width, $height);//instead of  $url = $T->getCreatedThumbnail($width, $height);
                        
            if (null !== $url && false !== $url)
            {
                $this->$method($url);
                

                return false;

            }



            $addon = bab_getAddonInfosInstance('widgets');

            $T->setSourceFile($addon->getImagesPath().'photo.png');
            $T->setImageFormat('png');

            $this->$method($T->getThumbnail($width, $height));


            // push in session the needed path

            $arr = array(
                    'path' => $this->path->toString(),
                    'width' => $width,
                    'height' => $height,
                    'effects' => array(
					    'setResizeMode' => array(Func_Thumbnailer::KEEP_ASPECT_RATIO),
					    'setImageFormat' => array('png')
					)
            );

            if (null !== $resizemode)
            {
                $arr['effects']['setResizeMode'] = array($this->resizemode);
            }

            $_SESSION['addon_widgets']['Widget_FileIcon'][$id] = $arr;


            return true;
        }

        return null;
    }


    /**
     * {@inheritDoc}
     * @see Widget_ImageZoomer::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $this->setMetadata('selfpage', bab_getSelf());

        if (true === $this->checkForThumbnail($this->getId().'_small', $this->thumb_width, $this->thumb_height, $this->resizemode, 'setImageUrl')) {
            $this->setMetadata('pending_thumbnail_small', 1);
        }

        if (true === $this->checkForThumbnail($this->getId(), $this->big_width, $this->big_height, null, 'setBigImageUrl')) {
            $this->setMetadata('pending_thumbnail', 1);
        }

        $this->setMetadata('label', $this->label);
        $this->setMetadata('wraparound', $this->wrapAround);
        $this->setMetadata('selfpage', bab_getSelf());
        $this->setMetadata('controller', 'addon=widgets.thumbnail');

        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        return parent::display($canvas)
            . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.fileicon-imagethumbnail.jquery.js');
    }
}
