<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__) . '/lineedit.class.php';


/**
 * Constructs a Widget_TimePicker.
 *
 * @param string        $id         The item unique id.
 * @return Widget_TimePicker
 */
function Widget_DurationPicker($id = null)
{
    return new Widget_DurationPicker($id);
}


/**
 * A Widget_TimePicker is a widget that let the user enter a time.
 */
class Widget_DurationPicker extends Widget_LineEdit
{
    protected $hourText = 'h';
    protected $minuteText = 'm';
    protected $secondText = 's';

    protected $secondsHidden = false;

    /**
     * @param string $id            The item unique id.
     */
    public function __construct($id = null)
    {
        parent::__construct($id);


    }

    public function hideSeconds(bool $hide = true)
    {
        $this->secondsHidden = $hide;
        return $this;
    }


    public function setHourText($text)
    {
        $this->hourText = $text;
        return $this;
    }
    public function setMinuteText($text)
    {
        $this->minuteText = $text;
        return $this;
    }
    public function setSecondText($text)
    {
        $this->secondText = $text;
        return $this;
    }


    /**
     * (non-PHPdoc)
     * @see Widget_SuggestLineEdit::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-durationpicker';
        return $classes;
    }


    public function display(Widget_Canvas $canvas)
    {
        $this->setMetadata('hourText', $this->hourText);
        $this->setMetadata('minuteText', $this->minuteText);
        $this->setMetadata('secondText', $this->secondText);
        $this->setMetadata('hideSeconds', $this->secondsHidden);

        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        return parent::display($canvas)
            . $canvas->loadScript($this->getId(), $widgetsAddon->getTemplatePath() . 'widgets.durationpicker.jquery.js')
            . $canvas->loadStyleSheet($widgetsAddon->getStylePath() . 'widgets.durationpicker.css');
    }
}
