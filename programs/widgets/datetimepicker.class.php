<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/datepicker.class.php';
require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';


/**
 * Constructs a Widget_DateTimePicker.
 *
 * @param string		$id			The item unique id.
 * @return Widget_DateTimePicker
 */
function Widget_DateTimePicker($id = null)
{
    return new Widget_DateTimePicker($id);
}


/**
 * A Widget_DateTimePicker is a widget that let the user enter a date and a time.
 */
class Widget_DateTimePicker extends Widget_DatePicker implements Widget_Displayable_Interface
{
    /**
     * @param string $id			The item unique id.
     */
    public function __construct($id = null)
    {
        parent::__construct($id);

        $this->setFormat('%d-%m-%Y %H:%M');
        $this->setSize(17);
        $this->setMaxSize(17);
    }



    /**
     * @return string ISO date or empty string
     */
    public function getValue()
    {

        $value = parent::getValue();
        if ('' === $value) {
            return '';
        }

        if (!preg_match('/\d{4}-\d\d-\d\d/', $value)) {
            if (false === $value = $this->getISODate($value)) {
                return '';
            }
        }

        return $value;
    }



    /**
     * @see Widget_DatePicker::getClasses()
     */
    public function getClasses()
    {
        $classes = Widget_InputWidget::getClasses();

        if (!$this->isDisplayMode()) {
            $classes[] = 'widget-datetimepicker';
        } else {
            $classes[] = 'widget-displaymode';
        }
        if ($this->isInline()) {
            $classes[] = 'inline';
        }
        if ($this->isMonthSelectable()) {
            $classes[] = 'widget-datetimepicker-changemonth';
        }
        if ($this->isYearSelectable()) {
            $classes[] = 'widget-datetimepicker-changeyear';
        }

        return $classes;
    }


    /**
     * get title of widget (tooltip)
     * @return string
     */
    public function getTitle()
    {
        $title = parent::getTitle();
        if (!isset($title)) {
            $dateFormat = str_replace(array('%d', '%m', '%Y'), array('jj', 'mm', 'aaaa'), $this->getFormat());
            $dateExample = strftime($this->getFormat(), bab_mktime('2005-12-25'));
            return sprintf(widget_translate("Date format: '%s'.  Eg. '%s'"), $dateFormat, $dateExample);
        }
        return $title;
    }


    /**
     * get ISO from a date typed in the format set in the setFormat method
     * @param	string	$date
     * @return 	string|false	ISO date or false if error
     */
    public function getISODate($date)
    {
        if (empty($date)) {
            return '0000-00-00 00:00:00';
        }

        /**
         * We use the ?P<name> (named subpattern) notation which is compatible with
         * pre-5.2.2 versions of PHP and seems to also work with more recent versions.
         * @see http://php.net/preg_match => "Using named subpattern"
         */
        $expreg = str_replace(
            array('%d', '%m', '%Y', '%H', '%M', '%S'),
            array(
                '(?P<day>[0-9]{1,2})',
                '(?P<month>[0-9]{1,2})',
                '(?P<year>[0-9]{4})',
                '(?P<hour>[0-9]{1,2})',
                '(?P<minute>[0-9]{1,2})',
                '(?P<second>[0-9]{1,2})'
            ),
            '/'.preg_quote($this->getFormat(), '/').'/'
        );

        $regs = array();
        if (preg_match($expreg, $date, $regs)) {
            return sprintf(
                '%04d-%02d-%02d %02d:%02d:%02d',
                $regs['year'],
                $regs['month'],
                $regs['day'],
                $regs['hour'],
                $regs['minute'],
                $regs['second']
            );
        }

        return false;
    }


    /**
     * @see Widget_DatePicker::getScripts()
     */
    protected function getScripts(Widget_Canvas $canvas)
    {
        $widgetsAddon = bab_getAddonInfosInstance('widgets');
        $jquery = bab_jquery();

        return $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.datetimepicker.jquery.js')
            . $canvas->loadScript($this->getId(), $jquery->getI18nUrl(widget_getLang()))
            . $canvas->loadAddonStyleSheet($widgetsAddon, 'widgets.datetimepicker.css');
    }
}
