<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

//require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once dirname(__FILE__) . '/containerwidget.class.php';



/**
 * Constructs a Widget_Frame.
 *
 * @param Widget_Layout	$layout		The layout that will manage how widgets are displayed in this container.
 * @param string		$id			The item unique id.
 * @return Widget_Frame
 */
function Widget_Frame($id = null, Widget_Layout $layout = null)
{
	return new Widget_Frame($id, $layout);
}



/**
 * A Widget_Frame is the most basic container.
 *
 */
class Widget_Frame extends Widget_ContainerWidget implements Widget_Displayable_Interface
{

	/**
	 * @param Widget_Layout $layout	The layout that will manage how widgets are displayed in this container.
	 * @param string $id			The item unique id.
	 * @return Widget_Frame
	 */
	public function __construct($id = null, Widget_Layout $layout = null)
	{
		if (null === $layout) {
			require_once FUNC_WIDGETS_PHP_PATH . 'layout.class.php';
			$layout = new Widget_Layout();
		}

		parent::__construct($id, $layout);
	}

	public function addItems($items)
	{
	    if (! is_array($items)) {
	        $items = func_get_args();
	    }

	    foreach ($items as $item) {
	        $this->addItem($item);
	    }
	    return $this;
	}


    /**
     * (non-PHPdoc)
     * @see Widget_Widget::getClasses()
     */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-frame';
		return $classes;
	}


	/**
	 * (non-PHPdoc)
	 * @see Widget_Displayable_Interface::display()
	 */
	public function display(Widget_Canvas $canvas)
	{
		$frame = $canvas->div(
			$this->getId(),
			$this->getClasses(),
			array($this->getLayout()),
			$this->getCanvasOptions(),
			$this->getTitle(),
			$this->getAttributes()
		);
		$frame .= $canvas->metadata($this->getId(), $this->getMetadata());

		return $frame;
	}
}
