<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

//require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once dirname(__FILE__) . '/multilangedit.class.php';
require_once dirname(__FILE__) . '/textedit.class.php';



/**
 * Constructs a Widget_MultilangTextEdit.
 *
 * @param string		$id			The item unique id.
 * @return Widget_MultilangTextEdit
 */
function Widget_MultilangTextEdit($id = null)
{
	return new Widget_MultilangTextEdit($id);
}


/**
 * A Widget_MultilangTextEdit is a widget that lets the user enter a single line of text in multiple languages.
 */
class Widget_MultilangTextEdit extends Widget_MultilangEdit implements Widget_Displayable_Interface
{




    /**
     * (non-PHPdoc)
     * @see Widget_LineEdit::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-multilanglineedit';

        return $classes;
    }



    /**
     *
     * @param string $lang
     * @return Widget_TextEdit
     */
    protected function langWidgetEdit($lang)
    {
        $W = bab_Widgets();
        $lineEdit = $W->TextEdit($this->getId() . '_' . $lang);
        $lineEdit->setName($lang);
        $lineEdit->addClass('widget-lang');
        $lineEdit->addClass('widget-lang-' . $lang);
        $lineEdit->addClass('widget-100pc');
        //$lineEdit->setSizePolicy('widget-100pc');
        return $lineEdit;
    }



}
