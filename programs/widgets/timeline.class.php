<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
require_once dirname(__FILE__) . '/widget.class.php';




/**
 * Constructs a Widget_Timeline.
 *
 * @param string $id            The item unique id.
 * @return Widget_Timeline
 */
function Widget_Timeline($id = null)
{
    return new Widget_Timeline($id);
}



/**
 *
 */
class Widget_Timeline extends Widget_Widget implements Widget_Displayable_Interface
{
    /**
     * @var bab_DateTime
     */
    private $start;

    /**
     * @var bab_DateTime
     */
    private $end;

    /**
     * @var bool
     */
    public $autoWidth = true;

    public $bands = array();


    /**
     * The timeline periods
     * @var array   of Widget_TimelinePeriod
     */
    private $_periods = array();

    /**
     * The highlighted period spans
     * @var array   of Widget_TimelineSpan
     */
    private $spanHighlights = array();

    /**
     * @param string $id            The item unique id.
     * @return Widget_Timeline
     */
    public function __construct($id = null)
    {
        parent::__construct($id);

    }




    /**
     * Returns the minimum date of the timeline.
     *
     * @see self::setStart()
     *
     * @return bab_DateTime
     */
    public function getStart()
    {
        return $this->start;
    }


    /**
     * Use for setting a minimum date.
     * The timeline will not be able to be moved to anytime before this date.
     *
     * @param bab_DateTime $startDate   The start date or null for no limit.
     * @return Widget_Timeline
     */
    public function setStart($startDate)
    {
        $this->start = $startDate;
        return $this;
    }


    /**
     * Returns the maximum date of the timeline.
     *
     * @see self::setEnd()
     *
     * @return bab_DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }


    /**
     * Use for setting a maximum date.
     * The Timeline will not be able to be moved to anytime after this date.
     *
     * @param bab_DateTime $endDate   The end date or null for no limit.
     * @return Widget_Timeline
     */
    public function setEnd($endDate)
    {
        $this->end = $endDate;
        return $this;
    }



    /**
     * @see Widget_Timeline::setAutoWidth()
     * @return bool
     */
    public function isAutoWidth()
    {
        return $this->autoWidth;
    }

    
    /**
     * Defines if the timeline should automatically grow itself, as needed when too many events for
     * the available width are painted on the visible part of the Timeline.
     *
     * @param bool $autoWidth
     * @return Widget_Timeline
     */
    public function setAutoWidth($autoWidth)
    {
        $this->autoWidth = $autoWidth;
        return $this;
    }


    /**
     * Adds a band to the timeline.
     *
     * @param Widget_TimelineBand $band
     * @return Widget_Timeline
     */
    public function addBand(Widget_TimelineBand $band)
    {
        $this->bands[] = $band;
        return $this;
    }



    /**
     * Add background spans with month labels.
     *
     * @param int $startYear
     * @param int $endYear
     *
     * @return Widget_Timeline
     */
    public function addMonthSpans($startYear, $endYear = null)
    {
        $color = '#89a';
        if (!isset($endYear)) {
            $endYear = $startYear;
        }
        for ($year = $startYear; $year <= $endYear; $year++) {
            for ($month = 1; $month <= 12; $month++) {
                $monthStart = BAB_DateTime::fromIsoDateTime($year . '-' . $month . '-01 00:00:00');
                $monthEnd = $monthStart->cloneDate();
                $monthEnd->add(2, BAB_DATETIME_HOUR);

                $span = $this->createSpan($monthStart, $monthEnd, $color);
                $span->setOpacity(30);
                $this->addSpan($span);
                
                if (isset($GLOBALS['babMonths'][$month])) {
                    $span->endLabel = $GLOBALS['babMonths'][$month];
                } else {
                    $span->endLabel = bab_DateStrings::getMonth($month);
                }

            }
        }

        return $this;
    }


    /**
     * Add background spans with years labels.
     *
     * @param int $startYear
     * @param int $endYear
     *
     * @return Widget_Timeline
     */
    public function addYearSpans($startYear, $endYear)
    {
        $color = '#89a';
        for ($year = $startYear; $year <= $endYear; $year++) {
            $yearStart = BAB_DateTime::fromIsoDateTime($year . '-01-01 00:00:00');
            $yearEnd = $yearStart->cloneDate();
            $yearEnd->add(1, BAB_DATETIME_DAY);

            $span = $this->createSpan($yearStart, $yearEnd, $color);
            $span->setOpacity(30);
            $this->addSpan($span);

            $span->endLabel = $year;
        }

        return $this;
    }


    /**
     * Adds a highlighted span to the timeline.
     *
     * @param Widget_TimelineSpan $span
     * @return Widget_Timeline
     */
    public function addSpan(Widget_TimelineSpan $span)
    {
        $this->spanHighlights[] = $span;
        return $this;
    }


    /**
     * Adds a period to the calendar.
     *
     * @param Widget_TimelineMilestone $period
     * @return Widget_Timeline
     */
    public function addPeriod(Widget_TimelineMilestone $period)
    {
        $this->_periods[] = $period;
        return $this;
    }



    /**
     * Creates a new time span.
     *
     * @param BAB_DateTime  $start      The start of the time span.
     * @param BAB_DateTime  $end        The end of the time span.
     * @param string        $color      A css formatted color string.
     * @param int           $opacity    A value between 0 (transparent) and 100 (opaque).
     * @return Widget_TimelineSpan
     */
    public function createSpan($start, $end, $color, $opacity = null)
    {
        $span = new Widget_TimelineSpan();
        $span->setStart($start);
        $span->setEnd($end);
        $span->setColor($color);
        if (isset($opacity)) {
            $span->setColor($opacity);
        }
        return $span;
    }


    /**
     * Creates a new milestone.
     *
     * @param BAB_DateTime $startDate
     * @return Widget_TimelineMilestone
     */
    public function createMilestone($startDate)
    {
        return new Widget_TimelineMilestone($this, $startDate);
    }


    /**
     * Creates a new period.
     *
     * @param BAB_DateTime $startDate
     * @param BAB_DateTime $endDate
     * @return Widget_TimelinePeriod
     */
    public function createPeriod($startDate, $endDate)
    {
        return new Widget_TimelinePeriod($this, $startDate, $endDate);
    }


    public function onDoubleClick(widget_Action $action)
    {
        $this->onDoubleClick = $action;
        $this->setMetadata('doubleClick', $action->url());
    }


    public function onPeriodMoved(widget_Action $action)
    {
        $this->onPeriodMoved = $action;
        $this->setMetadata('periodMoved', $action->url());
    }


    /**
     * (non-PHPdoc)
     * @see Widget_Widget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-timeline';
        return $classes;
    }



    /**
     * (non-PHPdoc)
     * @see Widget_Displayable_Interface::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $bandsMetadata = array();
        foreach ($this->bands as $band) {
            /* @var $band Widget_TimelineBand */
            $bandsMetadata[] = array(
                'width' => $band->getWidth(),
                'type' => $band->getType(),
                'intervalUnit' => $band->getIntervalUnit(),
                'intervalPixels' => $band->getIntervalPixels(),
                'classes' => implode(' ', $band->getClasses())
            );
        }

        foreach ($this->_periods as $period) {
            if ($period->description instanceOf Widget_Displayable_Interface) {
                $period->description = $period->description->display($canvas);
            }
        }

        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        $html = $canvas->div(
            $this->getId(),
            $this->getClasses(),
            array(),
            Widget_Item::Options()->height(400, 'px'),
            $this->getTitle(),
            $this->getAttributes()
        );

        $html .= $canvas->metadata(
            $this->getId(),
            array(
                'start' => $this->getStart(),
                'stop' => $this->getEnd(),
                'autoWidth' => $this->isAutoWidth(),
                'periods' => $this->_periods,
                'bands' => $bandsMetadata,
                'spanHighlights' => $this->spanHighlights
            ) + $this->getMetadata()
        );

        $body = bab_getBody();
        $body->addJavascriptFile($widgetsAddon->getTemplatePath().'timeline/timeline-init.js');
        $body->addJavascriptFile($widgetsAddon->getTemplatePath().'timeline/timeline_js/timeline-api.js');
        $body->addJavascriptFile($widgetsAddon->getTemplatePath().'widgets.timeline.jquery.js');

        return $html;
    }
}



class Widget_TimelineBand
{
    /**
     * Possible type values;
     */
    const TYPE_ORIGINAL = 'original';
    const TYPE_OVERVIEW = 'overview';
    const TYPE_DETAILED = 'detailed';

    /**
     * Possible intervalUnit values
     */
    const INTERVAL_MILLISECOND = 'millisecond';
    const INTERVAL_SECOND = 'second';
    const INTERVAL_MINUTE = 'minute';
    const INTERVAL_HOUR = 'hour';
    const INTERVAL_DAY = 'day';
    const INTERVAL_WEEK = 'week';
    const INTERVAL_MONTH = 'month';
    const INTERVAL_YEAR = 'year';
    const INTERVAL_DECADE = 'decade';
    const INTERVAL_CENTURY = 'century';
    const INTERVAL_MILLENIUM = 'millenium';

    private $width;

    private $intervalUnit;

    private $intervalPixels;

    private $type;
    
    private $classes;

    public function __construct($width = '100%', $intervalUnit = self::INTERVAL_DAY, $intervalPixels = 50, $type = self::TYPE_ORIGINAL)
    {
        $this->setWidth($width);
        $this->setIntervalUnit($intervalUnit);
        $this->setIntervalPixels($intervalPixels);
        $this->setType($type);
        
        $this->classes = array();
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }

    public function getIntervalUnit()
    {
        return $this->intervalUnit;
    }

    public function setIntervalUnit($intervalUnit)
    {
        $this->intervalUnit = $intervalUnit;
        return $this;
    }

    public function getIntervalPixels()
    {
        return $this->intervalPixels;
    }

    public function setIntervalPixels($intervalPixels)
    {
        $this->intervalPixels = $intervalPixels;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
    
    /**
     * Add the specified class names to the item.
     *
     * @param string|array $className,... One or more class names.
     * @return $this
     */
    public function addClass($className /*,... */)
    {
        $args = func_get_args();
        $numArgs = func_num_args();
        for ($i = 0; $i < $numArgs; $i++) {
            if (!is_array($args[$i])) {
                $args[$i] = array($args[$i]);
            }
            foreach ($args[$i] as $class) {
                $this->classes[] = $class;
            }
        }
        return $this;
    }
    
    public function getClasses()
    {
        return $this->classes;
    }
}



class Widget_TimelineMilestone
{
    private $calendar;

    public $start;

    public $color;
    public $textColor = '#000';
    public $iconColor;

    public $title;

    public $link;

    public $image;

    public $description;

    public $isDuration;

    public $classname = '';
    
    private $baseIconPath;

    private $onDoubleClick;



    /**
     * Creates a timeline milestone.
     * Should use Widget_Timeline::createMilestone().
     *
     * @param Widget_Timeline $timeline     The timeline to which the milestone is attached.
     * @param bab_DateTime $start           The date of the milestone.
     */
    public function __construct(Widget_Timeline $timeline, bab_DateTime $start)
    {
        $widgetAddonInfo = bab_getAddonInfosInstance('widgets');
        $this->baseIconPath = $GLOBALS['babUrl'] . $widgetAddonInfo->getTemplatePath() . 'timeline/timeline_js/images/';
        
        $this->calendar = $timeline;
        $this->setStart($start);
        $this->setColor(null);
    }

    /**
     * Sets the start date of the period.
     *
     * @param bab_DateTime $start
     * @return Widget_TimelinePeriod
     */
    public function setStart(bab_DateTime $start)
    {
        $this->start = $start->getIsoDateTime();
        return $this;
    }

    /**
     * Sets the title of the period displayed on timeline.
     *
     * @param string $title
     * @return Widget_TimelinePeriod
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }


    /**
     * Set bubble content.
     *
     * @param Widget_Displayable_Interface $item
     * @return Widget_TimelinePeriod
     */
    public function setBubbleContent(Widget_Displayable_Interface $item)
    {
        $this->description = $item;
        return $this;
    }



    /**
     * Sets the color of the period displayed on timeline
     *
     * @param string $color
     * @return Widget_TimelinePeriod
     */
    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }
    
    public function setIconColor($color)
    {
        $this->icon = $this->baseIconPath . 'transparent-10x10.png';
        $this->iconColor = $color;
        return $this;
    }

    /**
     * Returns the start date of the period.
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Returns the title of the period.
     */
    public function getTitle()
    {
        return $this->title;
    }


    public function onDoubleClick(widget_Action $action)
    {
        $this->onDoubleClick = $action;
    }


    /**
     * Returns the color of the period.
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }
}


class Widget_TimelinePeriod extends Widget_TimelineMilestone
{
    public $end;


    /**
     * Creates a timeline period.
     * Should use Widget_Timeline::createPeriod().
     *
     * @param Widget_Timeline $calendar
     * @param bab_DateTime $start
     * @param bab_DateTime $end
     */
    public function __construct(Widget_Timeline $calendar, bab_DateTime $start, bab_DateTime $end)
    {
        parent::__construct($calendar, $start);
        $this->setEnd($end);
    }


    /**
     * Sets the end date of the period.
     *
     * @param bab_DateTime $end
     * @return Widget_TimelinePeriod
     */
    public function setEnd(bab_DateTime $end)
    {
        $this->end = $end->getIsoDateTime();
        return $this;
    }

    /**
     * Returns the end date of the period.
     */
    public function getEnd()
    {
        return $this->end;
    }
}



/**
 * This class is used to define a time span.
 *
 * This span can be highlighted with a color
 * @author laucho
 *
 */
class Widget_TimelineSpan
{
    public $startDate;
    public $endDate;
    public $color;
    public $opacity;
    public $inFront = false;
    public $startLabel;
    public $endLabel;


    /**
     * Sets the span opacity.
     *
     * @param int           $opacity    A value between 0 (transparent) and 100 (opaque).
     *
     * @return Widget_TimelineSpan
     */
    public function setOpacity($opacity)
    {
        $this->opacity = $opacity;
        return $this;
    }

    /**
     * Returns the span opacity.
     *
     * @return string
     */
    public function getOpacity()
    {
        return $this->opacity;
    }


    /**
     * Sets the color of the span.
     *
     * @param string $color
     * @return Widget_TimelineSpan
     */
    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }

    /**
     * Returns the color of the period.
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }


    /**
     * Sets the start date of the period.
     *
     * @param bab_DateTime $start
     * @return Widget_TimelineSpan
     */
    public function setStart(bab_DateTime $start, $startLabel = null)
    {
        $this->startDate = $start->getIsoDateTime();
        // To be iso 8601 compatible, needed by simile timeline.
        $this->startDate = str_replace(' ', 'T', $this->startDate);
        if (isset($startLabel)) {
            $this->startLabel = $startLabel;
        }
        return $this;
    }

    /**
     * Returns the start date of the period.
     */
    public function getStart()
    {
        return $this->startDate;
    }


    /**
     * Sets the end date of the period.
     *
     * @param bab_DateTime $end
     * @return Widget_TimelineSpan
     */
    public function setEnd(bab_DateTime $end, $endLabel = null)
    {
        $this->endDate = $end->getIsoDateTime();
        // To be iso 8601 compatible, needed by simile timeline.
        $this->endDate = str_replace(' ', 'T', $this->endDate);
        if (isset($endLabel)) {
            $this->endLabel = $endLabel;
        }
        return $this;
    }

    /**
     * Returns the end date of the period.
     */
    public function getEnd()
    {
        return $this->endDate;
    }
}
