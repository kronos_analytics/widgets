<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

if (!defined('FUNC_WIDGETS_PHP_PATH')) {
    define('FUNC_WIDGETS_PHP_PATH', bab_getAddonInfosInstance('widgets')->getPhpPath().'widgets/');
}
if (!defined('FUNC_WIDGETS_JS_PATH')) {
    define('FUNC_WIDGETS_JS_PATH', bab_getAddonInfosInstance('widgets')->getTemplatePath());
}


/**
 * Provides a user interface widgets factory.
 */
class Func_Widgets extends bab_Functionality
{
    const CLASS_PREFIX = 'Widget_';
    const CLASS_PREFIX_LENGTH = 7;


    /**
     * @return string
     *
     */
    public function getDescription()
    {
        return 'Provides user interface widgets.';
    }

    /**
     * Register myself as a functionality.
     * @static
     */
    public static function register()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
        $functionalities = new bab_functionalities();
        $functionalities->registerClass(__CLASS__, __FILE__);
    }


    /**
     * Includes all necessary CSS files to the current page.
     *
     * @return bool		false in case of error
     */
    public function includeCss()
    {
        $babBody = bab_getBody();
        $addon = bab_getAddonInfosInstance('widgets');
        $babBody->addStyleSheet($addon->getStylePath().'widgets.css');
        return true;
    }



    /**
     * Includes the definition of the class $itemClass.
     *
     * @param string	$itemClass		The classname.
     */
    public function includePhpClass($itemClass)
    {
        $itemClassPhpFilename = strtolower(substr($itemClass, self::CLASS_PREFIX_LENGTH)) . '.class.php';
        include_once FUNC_WIDGETS_PHP_PATH . $itemClassPhpFilename;
    }



    /**
     * @return widget_Controller
     */
    public function Controller()
    {
        require_once dirname(__FILE__) . '/controller.class.php';
        return new widget_Controller();
    }


    /**
     * Creates and returns a new item of type $itemClass.
     *
     * @param string	$itemClass		The classname of the item to create.
     * @param mixed		...				Additional parameters depending on item class.
     * @return Widget_Item					The newly created item or null.
     */
    public function create($itemClass)
    {
        $this->includePhpClass($itemClass);
        if (!class_exists($itemClass)) {
            return null;
        }
        $args = func_get_args();
        array_shift($args);
        $widget = call_user_func_array($itemClass, $args);
        return $widget;
    }


    /**
     * Constructs a Widget_Color.
     *
     * @return Widget_Color
     */
    public function Color()
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'color.class.php';
        return new Widget_Color();
    }

    /**
     * Constructs a Widget_Layout.
     *
     * @param string $id	The item unique id.
     * @return Widget_Layout
     */
    public function Layout($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'layout.class.php';
        return new Widget_Layout($id);
    }




    /**
     * Constructs a Widget_ListLayout.
     *
     * @param string $id	The item unique id.
     * @return Widget_ListLayout
     */
    public function ListLayout($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'listlayout.class.php';
        return new Widget_ListLayout($id);
    }


    /**
     * Constructs a Widget_VBoxLayout.
     *
     * @param string $id	The item unique id.
     * @return Widget_VBoxLayout
     */
    public function VBoxLayout($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'vboxlayout.class.php';
        return new Widget_VBoxLayout($id);
    }


    /**
     * Constructs a Widget_HBoxLayout.
     *
     * @param string $id	The item unique id.
     * @return Widget_HBoxLayout
     */
    public function HBoxLayout($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'hboxlayout.class.php';
        return new Widget_HBoxLayout($id);
    }


    /**
     * Constructs a Widget_FlowLayout.
     *
     * @param string $id	The item unique id.
     * @return Widget_FlowLayout
     */
    public function FlowLayout($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'flowlayout.class.php';
        return new Widget_FlowLayout($id);
    }


    /**
     * Constructs a Widget_FlexLayout.
     *
     * @param string $id	The item unique id.
     * @return Widget_FlexLayout
     */
    public function FlexLayout($id = null, $wrap = null, $justifyContent = 'between', $alignItems = 'center', $direction = 'row')
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'flexlayout.class.php';
        return new Widget_FlexLayout($id, $wrap, $justifyContent, $alignItems, $direction);
    }


    /**
     * Constructs a Widget_GridLayout.
     *
     * @param string $id	The item unique id.
     * @return Widget_GridLayout
     */
    public function GridLayout($id = null, $wrap = null, $justifyItems = 'stretch', $alignItems = 'stretch', $justifyContent = 'start', $alignContent = 'start')
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'gridlayout.class.php';
        return new Widget_GridLayout($id, $wrap, $justifyItems, $alignItems, $justifyContent, $alignContent);
    }


    /**
     * Constructs a Widget_MulticolumnLayout.
     *
     * @param string $id	The item unique id.
     * @return Widget_MulticolumnLayout
     */
    public function MulticolumnLayout($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'multicolumnlayout.class.php';
        return new Widget_MulticolumnLayout($id);
    }


    /**
     * Constructs a Widget_GridParent.
     *
     * @param int		$nbColumns	The number of columns in the grid
     * @param string	$id			The item unique id.
     * @return Widget_GridParent
     */
    public function GridParent($nbColumns = -1, $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'gridparent.class.php';
        return new Widget_GridParent($nbColumns, $id);
    }




    /**
     * Constructs a Widget_Page.
     *
     * @param Widget_Layout	$layout		The layout that will manage how widgets are displayed in this container.
     * @param string		$id			The item unique id.
     * @return Widget_Page
     */
    public function Page($id = null, Widget_Layout $layout = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'page.class.php';
        return new Widget_Page($id, $layout);
    }


    /**
     * Constructs a Widget_BabPage.
     *
     * @param Widget_Layout	$layout		The layout that will manage how widgets are displayed in this container.
     * @param string		$id			The item unique id.
     * @return Widget_BabPage
     */
    public function BabPage($id = null, Widget_Layout $layout = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'babpage.class.php';
        return new Widget_BabPage($id, $layout);
    }


    /**
     * Constructs a Widget_ContextPage.
     * A Widget_ContextPage is a babPage with contextual zone on the right
     * and a toolbar on top
     *
     * @param Widget_Layout	$layout		The layout that will manage how widgets are displayed in this container.
     * @param string		$id			The item unique id.
     * @return Widget_BabPage
     */
    public function ContextPage($id = null, Widget_Layout $layout = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'contextpage.class.php';
        return new Widget_ContextPage($id, $layout);
    }


    /**
     * Constructs a Widget_Label.
     *
     * @param string	$text	The label text.
     * @param string	$id		The item unique id.
     * @return Widget_Label
     */
    public function Label($text = '', $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'label.class.php';
        return new Widget_Label($text, $id);
    }


    /**
     * Constructs an item with a label and a description for a form.
     *
     * @param string                       	$labelText
     * @param Widget_Displayable_Interface 	$item
     * @param string                       	$fieldName
     * @param string                       	$description
     * @param string						$suffix		 suffix for input field, example : unit
     *
     * @return Widget_LabelledWidget
     */
    public function LabelledWidget(
        $labelText,
        Widget_Displayable_Interface $item = null,
        $fieldName = null,
        $description = null,
        $suffix = null,
        $balloon = null
        ) {
            require_once FUNC_WIDGETS_PHP_PATH . 'labelledwidget.class.php';
            $labelledItem = new Widget_LabelledWidget($labelText, $item, null, $suffix, $balloon);

            if (isset($fieldName)) {
                $labelledItem->setName($fieldName);
            }

            if (isset($description)) {
                $labelledItem->setDescription($description);
            }

            return $labelledItem;
    }


    /**
     * Constructs an item with a label and a description for a form.
     *
     * @param string                       	$labelText1
     * @param string                       	$labelText2
     * @param array						 	$rule
     * @param Widget_Layout                 $layout
     * @param string						$id
     *
     * @return Widget_InputPassword
     */
    public function InputPassword($labelText1 = '', $labelText2 = '', $rule = array(), $layout = null, $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'inputpassword.class.php';
        $labelledItem = new Widget_InputPassword($labelText1, $labelText2, $rule, $layout, $id);

        return $labelledItem;
    }



    /**
     * Constructs a Widget_RichText.
     *
     * @param string	$text	The rich text.
     * @param string	$id		The item unique id.
     * @return Widget_RichText
     */
    public function RichText($text = '', $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'richtext.class.php';
        return new Widget_RichText($text, $id);
    }


    /**
     * Constructs a Widget_Html.
     *
     * @param string	$text	The html text.
     * @param string	$id		The item unique id.
     * @return Widget_Html
     */
    public function Html($text = '', $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'html.class.php';
        return new Widget_Html($text, $id);
    }


    /**
     * Constructs a Widget_SimpleHtmlEdit.
     *
     * @param string	$id		The item unique id.
     * @return Widget_SimpleHtmlEdit
     */
    public function SimpleHtmlEdit($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'simplehtmledit.class.php';
        return new Widget_SimpleHtmlEdit($id);
    }


    /**
     * Constructs a Widget_BabHtmlEdit.
     *
     * @param string	$id		The item unique id.
     * @return Widget_BabHtmlEdit
     */
    public function BabHtmlEdit($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'babhtmledit.class.php';
        return new Widget_BabHtmlEdit($id);
    }


    /**
     * Constructs a Widget_SimpleTreeView.
     *
     * @param string	$id		The item unique id.
     * @return Widget_SimpleTreeView
     */
    public function SimpleTreeView($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'simpletreeview.class.php';
        return new Widget_SimpleTreeView($id);
    }


    /**
     * Constructs a Widget_OrgchartTreeView.
     *
     * @param string	$id		The item unique id.
     * @return Widget_OrgchartView
     */
    public function OrgchartView($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'orgchartview.class.php';
        return new Widget_OrgchartView($id);
    }


    /**
     * Constructs a Widget_Link.
     *
     * @param string | Widget_Displayable_Interface     $item   The link text or widget item.
     * @param string | Widget_Action | bab_url          $url    The link url or a Widget_Action.
     * @param string                                    $id     The item unique id.
     * @return Widget_Link
     */
    public function Link($item = '', $url = '', $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'link.class.php';
        return new Widget_Link($item, $url, $id);
    }

    /**
     * Constructs a Widget_ActionButton.
     *
     * @param string | Widget_Displayable_Interface     $item   The link text or widget item.
     * @param string | Widget_Action | bab_url          $url    The link url or a Widget_Action.
     * @param string                                    $id     The item unique id.
     * @return Widget_Link
     */
    public function ActionButton($item = '', $url = '', $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'actionbutton.class.php';
        return new Widget_ActionButton($item, $url, $id);
    }

    /**
     * Constructs a Widget_Title.
     *
     * @param string | Widget_Displayable_Interface		$text	The title text.
     * @param int										$level	The title level [1-6].
     * @param string									$id		The item unique id.
     * @return Widget_Title
     */
    public function Title($text = '', $level = 2, $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'title.class.php';
        return new Widget_Title($text, $level, $id);
    }



    /**
     * Constructs a Widget_Hidden.
     *
     * @param string		$id			The item unique id.
     * @param string		$name		The item unique name.
     * @param string		$value		The item unique value.
     * @return Widget_Hidden
     */
    public function Hidden($id = null, $name = null, $value = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'hidden.class.php';
        return new Widget_Hidden($id, $name, $value);
    }


    /**
     * Constructs a Widget_LineEdit.
     *
     * @param string		$id			The item unique id.
     * @return Widget_LineEdit
     */
    public function LineEdit($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'lineedit.class.php';
        return new Widget_LineEdit($id);
    }


    /**
     * Constructs a Widget_Slider.
     *
     * @param string		$id			The item unique id.
     * @return Widget_LineEdit
     */
    public function Slider($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'slider.class.php';
        return new Widget_Slider($id);
    }


    /**
     * Constructs a Widget_DurationPicker.
     *
     * @param string		$id			The item unique id.
     * @return Widget_DurationPicker
     */
    public function DurationPicker($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'durationpicker.class.php';
        return new Widget_DurationPicker($id);
    }


    /**
     * Constructs a Widget_NumberEdit.
     *
     * @param string		$id			The item unique id.
     * @return Widget_NumberEdit
     */
    public function NumberEdit($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'numberedit.class.php';
        return new Widget_NumberEdit($id);
    }


    /**
     * Constructs a Widget_EmailLineEdit.
     *
     * @param string		$id			The item unique id.
     * @return Widget_EmailLineEdit
     */
    public function EmailLineEdit($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'emaillineedit.class.php';
        return new Widget_EmailLineEdit($id);
    }


    /**
     * Constructs a Widget_UrlLineEdit.
     *
     * @param string		$id			The item unique id.
     * @return Widget_UrlLineEdit
     */
    public function UrlLineEdit($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'urllineedit.class.php';
        return new Widget_UrlLineEdit($id);
    }


    /**
     * Constructs a Widget_TelLineEdit for telephon number
     *
     * @param string		$id			The item unique id.
     * @return Widget_TelLineEdit
     */
    public function TelLineEdit($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'tellineedit.class.php';
        return new Widget_TelLineEdit($id);
    }


    /**
     * Constructs a Widget_RegExpLineEdit for telephon number
     *
     * @param string		$id			The item unique id.
     * @return Widget_RegExpLineEdit
     */
    public function RegExpLineEdit($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'regexplineedit.class.php';
        return new Widget_RegExpLineEdit($id);
    }


    /**
     * Constructs a Widget_SearchLineEdit for telephon number
     *
     * @param string		$id			The item unique id.
     * @return Widget_SearchLineEdit
     */
    public function SearchLineEdit($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'searchlineedit.class.php';
        return new Widget_SearchLineEdit($id);
    }


    /**
     * Constructs a Widget_RatingEdit.
     *
     * @param string		$id			The item unique id.
     * @return Widget_RatingEdit
     */
    public function RatingEdit($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'ratingedit.class.php';
        return new Widget_RatingEdit($id);
    }


    /**
     * Constructs a Widget_DateEdit.
     *
     * @param string		$id			The item unique id.
     * @return Widget_DateEdit
     */
    public function DateEdit($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'dateedit.class.php';
        return new Widget_DateEdit($id);
    }


    /**
     * Constructs a Widget_TimeEdit.
     *
     * @param string		$id			The item unique id.
     * @return Widget_TimeEdit
     */
    public function TimeEdit($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'timeedit.class.php';
        return new Widget_TimeEdit($id);
    }


    /**
     * Constructs a Widget_MultilangLineEdit.
     *
     * @param string		$id			The item unique id.
     * @return Widget_MultilangLineEdit
     */
    public function MultilangLineEdit($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'multilanglineedit.class.php';
        return new Widget_MultilangLineEdit($id);
    }

    /**
     * Constructs a Widget_MultilangTextEdit.
     *
     * @param string		$id			The item unique id.
     * @return Widget_MultilangTextEdit
     */
    public function MultilangTextEdit($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'multilangtextedit.class.php';
        return new Widget_MultilangTextEdit($id);
    }


    /**
     * Constructs a Widget_SuggestLineEdit.
     *
     * @param string		$id			The item unique id.
     * @return Widget_SuggestLineEdit
     */
    public function SuggestLineEdit($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'suggestlineedit.class.php';
        return new Widget_SuggestLineEdit($id);
    }


    /**
     * Constructs a Widget_SuggestTextEdit.
     *
     * @param string		$id			The item unique id.
     * @return Widget_SuggestTextEdit
     */
    public function SuggestTextEdit($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'suggesttextedit.class.php';
        return new Widget_SuggestTextEdit($id);
    }


    /**
     * Constructs a Widget_SuggestCountry. with country suggestions
     *
     * @param string		$id			The item unique id.
     * @return Widget_SuggestCountry
     */
    public function SuggestCountry($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'suggestcountry.class.php';
        return new Widget_SuggestCountry($id);

    }

    /**
     * Constructs a Widget_SuggestPostalCode. with postal code suggestions
     *
     * @param string		$id			The item unique id.
     * @return Widget_SuggestPostalCode
     */
    public function SuggestPostalCode($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'suggestpostalcode.class.php';
        return new Widget_SuggestPostalCode($id);

    }

    /**
     * Constructs a Widget_SuggestPlaceName. with city/town/village suggestions
     *
     * @param string		$id			The item unique id.
     * @return Widget_SuggestPlaceName
     */
    public function SuggestPlaceName($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'suggestplacename.class.php';
        return new Widget_SuggestPlaceName($id);

    }


    /**
     * Constructs a Widget_SuggestAddressGoogle.
     *
     * @param string $id			The item unique id.
     * @param string $restriction	Filter by country. The country must be passed as as a two-character, ISO 3166-1 Alpha-2 compatible country code.
     * @return Widget_SuggestAddressGoogle
     */
    public function SuggestAddressGoogle($id = null, $restriction = 'fr')
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'suggestaddressgoogle.class.php';
        return new Widget_SuggestAddressGoogle($id, $restriction);
    }

    /**
     * Constructs a Widget_SuggestUser. with id/name suggestions
     *
     * @param string		$id			The item unique id.
     * @return Widget_SuggestUser
     */
    public function SuggestUser($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'suggestuser.class.php';
        return new Widget_SuggestUser($id);

    }

    /**
     * Constructs a Widget_SuggestDirectoryEntry. with id/name suggestions
     *
     * @param int       $directory  The directory id the entities come from.
     * @param string    $id         The item unique id.
     * @return Widget_SuggestDirectoryEntry
     */
    public function SuggestDirectoryEntry($directory = 1, $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'suggestdirectoryentry.class.php';
        return new Widget_SuggestDirectoryEntry($directory, $id);

    }

    /**
     * Constructs a Widget_SuggestGroup. with id/name suggestions
     *
     * @param string		$id			The item unique id.
     * @return Widget_SuggestGroup
     */
    public function SuggestGroup($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'suggestgroup.class.php';
        return new Widget_SuggestGroup($id);

    }


    /**
     * Constructs a Widget_SuggestSelect.
     * propose suggestion and confirm selection with a select box.
     * a click on a suggestion grab the id of the object
     * typed text of suggest line edit populate the select box to get the id on the next submit
     *
     * @param string		$id			The item unique id.
     * @return Widget_SuggestSelect
     */
    public function SuggestSelect($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'suggestselect.class.php';
        return new Widget_SuggestSelect($id);

    }


    /**
     * Contstruct a Widget_DrilldownMenu
     * to select a value into a tree
     *
     * @param string		$id			The item unique id.
     * @return Widget_DrilldownMenu
     */
    public function DrilldownMenu($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'drilldownmenu.class.php';
        return new Widget_DrilldownMenu($id);
    }



    /**
     * Constructs a Widget_TextEdit.
     *
     * @param string		$id			The item unique id.
     * @return Widget_TextEdit
     */
    public function TextEdit($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'textedit.class.php';
        return new Widget_TextEdit($id);
    }


    /**
     * Constructs a Widget_Uploader.
     *
     * @param string		$id			The item unique id.
     * @return Widget_Uploader
     */
    public function Uploader($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'uploader.class.php';
        return new Widget_Uploader($id);
    }


    /**
     * Constructs a Widget_ColorPicker.
     *
     * @param string		$id			The item unique id.
     * @return Widget_ColorPicker
     */
    public function ColorPicker($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'colorpicker.class.php';
        return new Widget_ColorPicker($id);
    }


    /**
     * Constructs a Widget_FilePicker.
     *
     * @param string		$id			The item unique id.
     * @return Widget_FilePicker
     */
    public function FilePicker($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'filepicker.class.php';
        return new Widget_FilePicker($id);
    }

    /**
     * Constructs a Widget_ImagePicker.
     *
     * @param string		$id			The item unique id.
     * @return Widget_ImagePicker
     */
    public function ImagePicker($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'imagepicker.class.php';
        return new Widget_ImagePicker($id);
    }

    /**
     * Constructs a Widget_ImageCropper.
     *
     * @param string		$id			The item unique id.
     * @return Widget_ImageCropper
     */
    public function ImageCropper($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'imagecropper.class.php';
        return new Widget_ImageCropper($id);
    }


    /**
     * Constructs a Widget_Button.
     *
     * @param string $id			The item unique id.
     * @return Widget_Button
     */
    public function Button($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'button.class.php';
        return new Widget_Button($id);
    }


    /**
     * Constructs a Widget_SubmitButton.
     *
     * @param string $id			The item unique id.
     * @return Widget_SubmitButton
     */
    public function SubmitButton($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'submitbutton.class.php';
        return new Widget_SubmitButton($id);
    }


    /**
     * Constructs a Widget_ResetButton.
     *
     * @param string $id			The item unique id.
     * @return Widget_ResetButton
     */
    public function ResetButton($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'resetbutton.class.php';
        return new Widget_ResetButton($id);
    }


    /**
     * Constructs a Widget_CheckBox.
     *
     * @param string $id			The item unique id.
     * @return Widget_CheckBox
     */
    public function CheckBox($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'checkbox.class.php';
        return new Widget_CheckBox($id);
    }



    /**
     * Constructs a Widget_CheckBoxAll
     * Check or uncheck a list of checkboxes
     *
     * @param string $id			The item unique id.
     * @return Widget_CheckBoxAll
     */
    public function CheckBoxAll($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'checkboxall.class.php';
        return new Widget_CheckBoxAll($id);
    }


    /**
     * Constructs a Widget_CheckBoxModelView.
     *
     * @param string $id			The item unique id.
     * @return Widget_CheckBoxModelView
     */
    public function CheckBoxModelView($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'checkboxmodelview.class.php';
        return new Widget_CheckBoxModelView($id);
    }


    /**
     * Constructs a Widget_TimePicker.
     *
     * @param string $id			The item unique id.
     * @return Widget_TimePicker
     */
    public function TimePicker($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'timepicker.class.php';
        return new Widget_TimePicker($id);
    }


    /**
     * Constructs a Widget_DatePicker.
     *
     * @param string $id			The item unique id.
     * @return Widget_DatePicker
     */
    public function DatePicker($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'datepicker.class.php';
        return new Widget_DatePicker($id);
    }


    /**
     * Constructs a Widget_DateTimePicker.
     *
     * @param string $id			The item unique id.
     * @return Widget_DateTimePicker
     */
    public function DateTimePicker($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'datetimepicker.class.php';
        return new Widget_DateTimePicker($id);
    }


    /**
     * Constructs a Widget_PeriodPicker.
     *
     * @param string $id			The item unique id.
     * @param bool	 $withTime		If the periode is selected with time.
     * @return Widget_PeriodPicker
     */
    public function PeriodPicker($id = null, $withTime = false)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'periodpicker.class.php';
        return new Widget_PeriodPicker($id, $withTime);
    }


    /**
     * Constructs a Widget_UserPicker.
     *
     * @param string $id			The item unique id.
     * @return Widget_UserPicker
     */
    public function UserPicker($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'userpicker.class.php';
        return new Widget_UserPicker($id);
    }


    /**
     * Constructs a Widget_FunctionPicker.
     *
     * @param string $id			The item unique id.
     * @return Widget_FunctionPicker
     */
    public function FunctionPicker($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'functionpicker.class.php';
        return new Widget_FunctionPicker($id);
    }


    /**
     * Constructs a Widget_GroupPicker.
     *
     * @param string $id			The item unique id.
     * @return Widget_GroupPicker
     */
    public function GroupPicker($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'grouppicker.class.php';
        return new Widget_GroupPicker($id);
    }


    /**
     * Constructs a Widget_BabFilePicker.
     *
     * @param string $id			The item unique id.
     * @return Widget_BabFilePicker
     */
    public function BabFilePicker($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'babfilepicker.class.php';
        return new Widget_BabFilePicker($id);
    }


    /**
     * Constructs a Widget_SitemapItemPicker.
     *
     * @param string $id			The item unique id.
     * @return Widget_SitemapItemPicker
     */
    public function SitemapItemPicker($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'sitemapitempicker.class.php';
        return new Widget_SitemapItemPicker($id);
    }



    /**
     * Constructs a Widget_TopicPicker.
     *
     * @param string $id			The item unique id.
     * @return Widget_TopicPicker
     */
    public function TopicPicker($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'topicpicker.class.php';
        return new Widget_TopicPicker($id);
    }



    /**
     * Constructs a Widget_CategoryPicker.
     *
     * @param string $id			The item unique id.
     * @return Widget_TopicPicker
     */
    public function CategoryPicker($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'categorypicker.class.php';
        return new Widget_CategoryPicker($id);
    }


    /**
     * Constructs a Widget_TopicCategoryPicker.
     *
     * @param string $id			The item unique id.
     * @return Widget_TopicCategoryPicker
     */
    public function TopicCategoryPicker($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'topiccategorypicker.class.php';
        return new Widget_TopicCategoryPicker($id);
    }


    /**
     * Constructs a Widget_ListView.
     *
     * @param string		$id			The item unique id.
     * @return Widget_ListView
     */
    public function ListView($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'listview.class.php';
        return new Widget_ListView($id);
    }


    /**
     * Constructs a Widget_TableView.
     *
     * @param string		$columnClasses	Column calss table.
     * @param string		$id			    The item unique id.
     * @return Widget_TableView
     */
    public function TableView($columnClasses = array(), $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'tableview.class.php';
        return new Widget_TableView($columnClasses, $id);
    }


    /**
     * Constructs a Widget_BabTableView.
     *
     * @param string		$id			The item unique id.
     * @return Widget_BabTableView
     */
    public function BabTableView($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'babtableview.class.php';
        return new Widget_BabTableView(null, $id);
    }


    /**
     * Constructs a Widget_BabTableView.
     *
     * @param string		$id			The item unique id.
     * @return Widget_BabTableModelView
     */
    public function BabTableModelView($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'babtablemodelview.class.php';
        return new Widget_BabTableModelView($id);
    }


    /**
     * Constructs a Widget_TableModelView.
     *
     * @param string		$id			The item unique id.
     * @return Widget_TableModelView
     */
    public function TableModelView($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'tablemodelview.class.php';
        return new Widget_TableModelView($id);
    }


    /**
     * Constructs a Widget_BabTableView.
     *
     * @param string		$id			The item unique id.
     * @return Widget_BabTableArrayView
     */
    public function BabTableArrayView($header = null, $content = null, $name = null, $columns = null, $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'babtablearrayview.class.php';
        return new Widget_BabTableArrayView($header, $content, $name, $columns, $id);
    }

    /**
     * Constructs a Widget_TableModelView.
     * @param array         $header
     * @param array         $content
     * @param string|array  $name
     * @param array         $columns    Optional columns classes
     * @param string        $id         The item unique id.
     * @return Widget_TableArrayView
     */
    public function TableArrayView($header = null, $content = null, $name = null, $columns = null, $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'tablearrayview.class.php';
        return new Widget_TableArrayView($header, $content, $name, $columns, $id);
    }


    /**
     * Constructs a Widget_PageSelector
     *
     * @param string		$id			The item unique id.
     * @return Widget_PageSelector
     */
    public function PageSelector($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'pageselector.class.php';
        return new Widget_PageSelector($id);
    }


    /**
     * Constructs a Widget_Icon.
     *
     * @param string		$text	The label text.
     * @param string		$image	The image name.
     * @param string		$id		The item unique id.
     * @return Widget_Icon
     */
    public function Icon($text = '', $image = '', $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'icon.class.php';
        return new Widget_Icon($text, $image, $id);
    }

    /**
     * Create a icon for a file
     * the file will have a default icon based on filetype and a thumbnail if possible
     * the thumbnail is loaded after page load
     *
     * @see Widget_Icon
     * @see Widget_ImageThumbnail
     *
     * @param 	string		$text		The label text.
     * @param	bab_Path 	$path		path to file
     * @param	string		$id			The item unique id.
     */
    public function FileIcon($text, bab_Path $path, $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'fileicon.class.php';
        return new Widget_FileIcon($text, $path, $id);
    }


    /**
     * Constructs a Widget_Image.
     *
     * @param string		$imageSrc	The image source (url).
     * @param string		$labelText	The 'alt' label text.
     * @param string		$id			The item unique id.
     *
     * @return Widget_Image
     */
    public function Image($imageSrc = '', $labelText = '', $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'image.class.php';
        return new Widget_Image($imageSrc, $labelText, $id);
    }


    /**
     * Constructs a Widget_Image of a country flag
     *
     * @param string		$isoCode	2 char
     * @param string		$id			The item unique id.
     *
     * @return Widget_Image | null
     */
    public function CountryFlag($isoCode, $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'image.class.php';
        $imageSrc = bab_getAddonInfosInstance('widgets')->getImagesPath().'16x11_country/'.strtolower($isoCode).'.gif';
        if (!file_exists($imageSrc)) {
            return null;
        }

        return new Widget_Image($imageSrc, '', $id);
    }


    /**
     * Create thumbnail for a file
     * the file will have a default image and a thumbnail if possible
     * the thumbnail is loaded after page load
     *
     * @see Widget_Image
     * @see Widget_FileIcon
     *
     * @param bab_Path  $imageSrc   The image source (url).
     * @param string    $labelText  The 'alt' label text.
     * @param string    $id         The item unique id.
     *
     * @return Widget_ImageThumbnail
     */
    public function ImageThumbnail(bab_Path $imageSrc, $labelText = '', $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'imagethumbnail.class.php';
        return new Widget_ImageThumbnail($imageSrc, $labelText, $id);
    }


    /**
     * Constructs a Widget_ImageZoomer.
     * display a small cliquable image, the big image is displayed in a overlay frame when the thumbnail is clicked
     *
     *
     * @param string		$smallImageSrc	The image source, small version (url).
     * @param string		$bigImageSrc	The image source, big version (url).
     * @param string		$labelText	The 'alt' label text.
     * @param string		$id			The item unique id.
     * @return Widget_ImageZoomer
     */
    public function ImageZoomer($smallImageSrc = '', $bigImageSrc = '', $labelText = '', $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'imagezoomer.class.php';
        return new Widget_ImageZoomer($smallImageSrc, $bigImageSrc, $labelText, $id);
    }

    /**
     * Constructs a Widget_ImageZoomerThumbnail.
     * display a small cliquable image, the big image is displayed in a overlay frame when the thumbnail is clicked
     *
     * @param bab_Path		$imagePath	The image source (path).
     * @param string		$labelText	The 'alt' label text.
     * @param string		$id			The item unique id.
     * @return Widget_ImageZoomerThumbnail
     */
    public function ImageZoomerThumbnail(bab_Path $imagePath, $labelText = '', $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'imagezoomerthumbnail.class.php';
        return new Widget_ImageZoomerThumbnail($imagePath, $labelText, $id);
    }


    /**
     * Constructs a Widget_Frame.
     *
     * @param string		$id			The item unique id.
     * @param Widget_Layout	$layout		The layout that will manage how widgets are displayed in this container.
     * @return Widget_Frame
     */
    public function Frame($id = null, Widget_Layout $layout = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'frame.class.php';
        return new Widget_Frame($id, $layout);
    }


    /**
     * Constructs a Widget_NamedContainer.
     * A Widget_NamedContainer is a container not displayed on page but with a name and optional metadata
     *
     * @param	string	$name		The item name
     * @param 	string	$id			The item unique id.
     * @return Widget_NamedContainer
     */
    public function NamedContainer($name = null, $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'namedcontainer.class.php';
        return new Widget_NamedContainer($name, $id);
    }


    /**
     * Constructs a Widget_Menu.
     *
     * @param Widget_Layout	$layout		The layout that will manage how widgets are displayed in this container.
     * @param string		$id			The item unique id.
     * @return Widget_Menu
     */
    public function Menu($id = null, Widget_Layout $layout = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'menu.class.php';
        return new Widget_Menu($id, $layout);
    }


    /**
     * Constructs a Widget_Map.
     *
     * @param string		$id			The item unique id.
     * @return Widget_Map
     */
    public function Map($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'map.class.php';
        return new Widget_Map($id);
    }


    /**
     * Constructs a Widget_Form.
     *
     * @param Widget_Layout	$layout		The layout that will manage how widgets are displayed in this container.
     * @param string		$id			The item unique id.
     * @return Widget_Form
     */
    public function Form($id = null, Widget_Layout $layout = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'form.class.php';
        return new Widget_Form($id, $layout);
    }


    /**
     * Constructs a Widget_Form with embeded fields to manage CSV importation.
     *
     * @param Widget_Layout	$layout		The layout that will manage how widgets are displayed in this container.
     * @param string		$id			The item unique id.
     * @return Widget_CsvImportForm
     */
    public function CsvImportForm($id = null, Widget_Layout $layout = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'csvimportform.class.php';
        return new Widget_CsvImportForm($id, $layout);
    }


    /**
     * Constructs a Widget_MultiField.
     *
     * @param string		$id			The item unique id.
     * @return Widget_MultiField
     */
    public function MultiField($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'multifield.class.php';
        return new Widget_MultiField($id);
    }


    /**
     * Constructs a Widget_Select.
     *
     * @param string		$id			The item unique id.
     * @return Widget_Select
     */
    public function Select($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'select.class.php';
        return new Widget_Select($id);
    }


    /**
     * Constructs a Widget_Multiselect.
     *
     * @param string		$id			The item unique id.
     * @return Widget_Multiselect
     */
    public function Multiselect($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'multiselect.class.php';
        return new Widget_Multiselect($id);
    }


    /**
     * Constructs a Widget_RadioSet.
     *
     * @param string		$id			The item unique id.
     * @return Widget_RadioSet
     */
    public function RadioSet($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'radioset.class.php';
        return new Widget_RadioSet($id);
    }


    /**
     * Constructs a Widget_RadioMenu.
     *
     * @param string		$id			The item unique id.
     * @return Widget_RadioMenu
     */
    public function RadioMenu($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'radiomenu.class.php';
        return new Widget_RadioMenu($id);
    }


    /**
     * Constructs a Widget_Radio.
     *
     * @param string		$id			The item unique id.
     * @return Widget_Radio
     */
    public function Radio($id = null, $radioSet = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'radio.class.php';
        return new Widget_Radio($id, $radioSet);
    }


    /**
     * Constructs a Widget_Tabs.
     *
     * @param string		$id			The item unique id.
     * @return Widget_Tabs
     */
    public function Tabs($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'tabs.class.php';
        return new Widget_Tabs($id);
    }


    /**
     * Constructs a Widget_Tab.
     *
     * @param 	string			$id			The item unique id.
     * @param	string			$label
     * @param	Widget_Layout	$layout
     * @return Widget_Tab
     */
    public function Tab($id = null, $label = null, $layout = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'tab.class.php';
        return new Widget_Tab($id, $label, $layout);
    }


    /**
     * Constructs a Widget_Accordions.
     *
     * @param string		$id			The item unique id.
     * @return Widget_Accordions
     */
    public function accordions($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'accordions.class.php';
        return new Widget_Accordions($id);
    }


    /**
     * Constructs a Widget_Calendar.
     *
     * @deprecated Use FullCalendar() instead.
     *
     * @param string		$id			The item unique id.
     * @return Widget_Calendar
     */
    public function Calendar($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'calendar.class.php';
        return new Widget_Calendar($id);
    }


    /**
     * Constructs a Widget_FullCalendar.
     *
     * @param string		$id			The item unique id.
     * @return Widget_FullCalendar
     */
    public function FullCalendar($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'fullcalendar.class.php';
        return new Widget_FullCalendar($id);
    }


    /**
     * Constructs a Widget_Timeline.
     *
     * @param string		$id			The item unique id.
     * @return Widget_Timeline
     */
    public function Timeline($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'timeline.class.php';
        return new Widget_Timeline($id);
    }


    /**
     * Constructs a Widget_Filter.
     *
     * @param string		$id			The item unique id.
     * @return Widget_Filter
     */
    public function Filter($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'filter.class.php';
        return new Widget_Filter($id);
    }


    /**
     * Constructs a Widget_HtmlCanvas.
     *
     * @return Widget_HtmlCanvas
     */
    public function HtmlCanvas()
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'htmlcanvas.class.php';
        return new Widget_HtmlCanvas;
    }


    /**
     * Constructs a Widget_HtmlEmailCanvas. html without scripts
     *
     * @return Widget_HtmlEmailCanvas
     */
    public function HtmlEmailCanvas()
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'htmlemailcanvas.class.php';
        return new Widget_HtmlEmailCanvas;
    }


    /**
     * Constructs a Widget_HtmlPdfCanvas. html fit for tcpdf
     *
     * @return Widget_HtmlPdfCanvas
     */
    public function HtmlPdfCanvas()
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'htmlpdfcanvas.class.php';
        return new Widget_HtmlPdfCanvas;
    }


    /**
     * Construct a Widget_Action
     *
     * @return Widget_Action
     */
    public function Action()
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'action.class.php';
        return new Widget_Action;
    }

    /**
     * Set items in parameters as a default Layout
     * @return Widget_Layout
     */
    public function Items()
    {
        $layout = $this->Layout();
        foreach (func_get_args() as $item) {
            if (is_null($item)) {
                continue;
            }
            $layout->addItem($item);
        }

        return $layout;
    }


    /**
     * Set items in parameters as a HBoxLayout
     * @return Widget_HBoxLayout
     */
    public function HBoxItems()
    {
        $layout = $this->HBoxLayout();
        foreach (func_get_args() as $item) {
            if (is_null($item)) {
                continue;
            }
            $layout->addItem($item);
        }

        return $layout;
    }


    /**
     * Set items in parameters as a VBoxLayout
     * @return Widget_VBoxLayout
     */
    public function VBoxItems()
    {
        $layout = $this->VBoxLayout();
        foreach (func_get_args() as $item) {
            if (is_null($item)) {
                continue;
            }
            $layout->addItem($item);
        }

        return $layout;
    }


    /**
     * Set items in parameters as a FlexLayout
     * @return Widget_FlexLayout
     */
    public function FlexItems()
    {
        $layout = $this->FlexLayout();
        foreach (func_get_args() as $item) {
            if (is_null($item)) {
                continue;
            }
            $layout->addItem($item);
        }

        return $layout;
    }


    /**
     * Set items in parameters as a GridLayout
     * @return Widget_GridLayout
     */
    public function GridItems()
    {
        $layout = $this->GridLayout();
        foreach (func_get_args() as $item) {
            if (is_null($item)) {
                continue;
            }
            $layout->addItem($item);
        }

        return $layout;
    }


    /**
     * Set items in parameters as a FlowLayout
     * @return Widget_FlowLayout
     */
    public function FlowItems()
    {
        $layout = $this->FlowLayout();
        foreach (func_get_args() as $item) {
            if (is_null($item)) {
                continue;
            }
            $layout->addItem($item);
        }

        return $layout;
    }


    /**
     * Set items in parameters as a ListLayout
     * @return Widget_ListLayout
     */
    public function ListItems()
    {
        $layout = $this->ListLayout();
        foreach (func_get_args() as $item) {
            if (is_null($item)) {
                continue;
            }
            $layout->addItem($item);
        }

        return $layout;
    }


    /**
     * Pair widget
     *
     * @param Widget_Displayable_Interface $first
     * @param Widget_Displayable_Interface $second
     * @return Widget_Pair
     */
    public function Pair(Widget_Displayable_Interface $first, Widget_Displayable_Interface $second, $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'pair.class.php';
        $pair = new Widget_Pair($id);
        $pair->setFirst($first);
        $pair->setSecond($second);
        return $pair;
    }


    /**
     * Gauge
     *
     * @param string $id
     * @return Widget_Gauge
     */
    public function Gauge($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'gauge.class.php';
        return new Widget_Gauge($id);
    }


    /**
     * Progress bar
     *
     * @param string $id
     * @return Widget_ProgressBar
     */
    public function ProgressBar($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'progressbar.class.php';
        return new Widget_ProgressBar($id);
    }


    /**
     * Section
     *
     * @param string									$headerText			The section header
     * @param Widget_Layout	| Widget_ContainerWidget	$containerItem		The section content layout
     * @param int										$level				Similar to Title level
     * @param string 									$id
     * @return Widget_Section
     */
    public function Section($headerText, $containerItem = null, $level = 3,  $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'section.class.php';
        return new Widget_Section($headerText, $containerItem, $level,$id);
    }


    /**
     * ACL input widget
     * @param string $id
     * @return Widget_Acl
     */
    public function Acl($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'acl.class.php';
        return new Widget_Acl($id);
    }


    /**
     * Dockable
     *
     * @param 	string			[$label]	The dock button label.
     * @param	Widget_Action	[$action]	The dock content url
     * @param 	string			[$id]		The item unique id.
     *
     * @return Widget_Dockable
     */
    public function Dockable($label = null, Widget_Action $action = null, $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'dockable.class.php';
        return new Widget_Dockable($label, $action, $id);
    }


    /**
     * Constructs a Widget_DelayedItem.
     *
     * @param Widget_Action $delayedAction  A widget action returning a Widget_DisplayableInterface
     * @param string        $id             The item unique id.
     * @return Widget_DelayedItem
     */
    public function DelayedItem(Widget_Action $delayedAction = null, $id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'delayeditem.class.php';
        return new Widget_DelayedItem($delayedAction, $id);
    }



    /**
     * Contruct a Widget_Balloon
     * @param string $id
     * @param Widget_Layout $layout
     * @return Widget_Balloon
     */
    public function Balloon($id = null, Widget_Layout $layout = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'balloon.class.php';
        return new Widget_Balloon($id, $layout);
    }




    public function SitemapMenu($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'sitemapmenu.class.php';
        return new Widget_SitemapMenu($id);
    }


    /**
     * ReCAPTCHA widget
     * @param string $id
     * @return Widget_reCAPTCHA
     */
    public function reCAPTCHA($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'recaptcha.class.php';
        return new Widget_reCAPTCHA($id);
    }

    /**
     * Check if posted captcha are correctly filed.
     * @return boolean
     */
    public function checkReCaptcha()
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'recaptcha.class.php';
        return Widget_reCAPTCHA::checkReCaptcha();
    }



    /**
     * Call before page creation
     * @return mixed
     */
    public static function onBeforePageCreated()
    {
        if (isset($_SESSION['addon_widgets'])) {

            $requested_uid = bab_rp('widget_filepicker_job_uid');
            // uid can be an array of job uid to do (submit of full form) or one uid (ajax)

            if (!is_array($requested_uid)) {
                $requested_uid = array($requested_uid);
            }

            foreach ($_SESSION['addon_widgets'] as $widget_class => $jobs) {

                foreach ($jobs as $uid => $job) {

                    if (!in_array($uid, $requested_uid)) {

                        //unset($_SESSION['addon_widgets'][$widget_class][$uid]);
                        continue;
                    }

                    if (isset($job['method'])) {
                        if (isset($job['params'])) {
                            $params = $job['params'];
                        } else {
                            $params = array();
                        }

                        bab_Widgets()->includePhpClass($widget_class);
                        call_user_func_array(array($widget_class, $job['method']), $params);
                    }
                }
            }
        }
    }


    /**
     * @param string|array $string
     * @return string|array
     */
    private function recursiveConvertToDatabaseEncoding($value)
    {
        if (!is_array($value)) {
            return bab_convertToDatabaseEncoding($value);
        }
        foreach ($value as $k => $v) {
            $value[$k] = self::recursiveConvertToDatabaseEncoding($v);
        }
        return $value;
    }



    /**
     * This method can be used to store default key/values.
     * They can be retrieved later using  Func_Widgets::getDefaultConfiguration().
     *
     * @see Func_Widgets::getDefaultConfiguration()
     *
     * @param string $key			A key in the form of a path (eg. 'tableview_a/visiblecolumns/name').
     * @param mixed  $value			The value to store.
     * @param string $addon			The addon name (base registry directory where the configuration is stored).
     */
    public function setDefaultConfiguration($key, $value, $addon = 'widgets')
    {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/' . $addon);
        $registry->changeDirectory('default');
        $keyPath = explode('/', $key);
        $key = array_pop($keyPath);
        $path = implode('/', $keyPath);
        $registry->changeDirectory($path);

        if (is_array($value)) {
            $value = self::recursiveConvertToDatabaseEncoding($value);
        }

        $registry->setKeyValue($key, $value);
    }




    /**
     * This method can be used to retrieve the default stored configuration key/values.
     *
     * @see Func_Widgets::setDefaultConfiguration()
     *
     * @param string $key   		A key in the form of a path (eg. 'tableview_a/visiblecolumns/name').
     * @param string $addon			The addon name (base registry directory where the configuration is stored).
     * @return mixed|null
     */
    public function getDefaultConfiguration($key, $addon = 'widgets')
    {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/' . $addon);
        $registry->changeDirectory('default');
        $keyPath = explode('/', $key);
        $key = array_pop($keyPath);
        $path = implode('/', $keyPath);
        $registry->changeDirectory($path);

        return $registry->getValue($key);
    }




    /**
     * Deletes all the default configuration key/values under the specified path.
     *
     * @param string $path   The path to delete (eg. 'tableview_a/visiblecolumns').
     * @param string $addon			The addon name (base registry directory where the configuration is stored).
     */
    public function deleteDefaultConfiguration($path, $addon = 'widgets')
    {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/' . $addon);
        $registry->changeDirectory('default');
        $keyPath = explode('/', $path);
        $key = array_pop($keyPath);
        $path = implode('/', $keyPath);
        $registry->changeDirectory($path);
        if ($registry->isDirectory($key)) {
            $registry->changeDirectory($key);
            return $registry->deleteDirectory();
        }
        return $registry->removeKey($key);
    }


    /**
     * This method can be used to store key/values for the currently
     * connected user. They can be retrieved later using  Func_Widgets::getUserConfiguration().
     *
     * @see Func_Widgets::getUserConfiguration()
     *
     * @param string $key			A key in the form of a path (eg. 'tableview_a/visiblecolumns/name').
     * @param mixed  $value			The value to store.
     * @param string $addon			The addon name (base registry directory where the configuration is stored).
     * @param bool   $sessionOnly	If true, the configuration is taken from the _SESSION array.
     */
    public function setUserConfiguration($key, $value, $addon = 'widgets', $sessionOnly = false)
    {
        if (!bab_isUserLogged() || $sessionOnly) {
            $_SESSION['/' . $addon . '/' . $key] = $value;
            return;
        }
        $userId = bab_getUserId();
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/' . $addon);
        $registry->changeDirectory('user');
        $registry->changeDirectory($userId);
        $keyPath = explode('/', $key);
        $key = array_pop($keyPath);
        $path = implode('/', $keyPath);
        $registry->changeDirectory($path);

        if (is_array($value)) {
            $value = self::recursiveConvertToDatabaseEncoding($value);
        }
        $registry->setKeyValue($key, $value);
    }




    /**
     * This method can be used to retrieve stored configuration key/values for the currently
     * connected user.
     *
     * @see Func_Widgets::setUserConfiguration()
     *
     * @param string $key   		A key in the form of a path (eg. 'tableview_a/visiblecolumns/name').
     * @param string $addon			The addon name (base registry directory where the configuration is stored).
     * @param bool	 $sessionOnly	If true, the configuration is taken from the _SESSION array.
     * @return mixed|null
     */
    public function getUserConfiguration($key, $addon = 'widgets', $sessionOnly = false)
    {
        if (!bab_isUserLogged() || $sessionOnly) {
            if (isset($_SESSION['/' . $addon . '/' . $key])) {
                return $_SESSION['/' . $addon . '/' . $key];
            }
            return null;
        }
        $userId = bab_getUserId();
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/' . $addon);
        $registry->changeDirectory('user');
        $registry->changeDirectory($userId);
        $keyPath = explode('/', $key);
        $key = array_pop($keyPath);
        $path = implode('/', $keyPath);
        $registry->changeDirectory($path);
        return $registry->getValue($key);
    }


    /**
     *
     * @param string $key
     * @param string $addon
     * @param string $sessionOnly
     * @return mixed|NULL
     */
    public function getConfiguration($key, $addon = 'widgets', $sessionOnly = false)
    {
        $userValue = $this->getUserConfiguration($key, $addon, $sessionOnly);
        if (!isset($userValue)) {
            $userValue = $this->getDefaultConfiguration($key, $addon);
        }
        return $userValue;
    }


    /**
     * Deletes all the user configuration key/values under the specified path.
     *
     * @param string $path   The path to delete (eg. 'tableview_a/visiblecolumns').
     * @param string $addon			The addon name (base registry directory where the configuration is stored).
     * @param bool	 $sessionOnly	If true, the configuration is taken from the _SESSION array.
     */
    public function deleteUserConfiguration($path, $addon = 'widgets', $sessionOnly = false)
    {
        if (!bab_isUserLogged() || $sessionOnly) {
            unset($_SESSION['/' . $addon . '/' . $path]);
            return true;
        }
        $userId = bab_getUserId();
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/' . $addon);
        $registry->changeDirectory('user');
        $registry->changeDirectory($userId);
        $keyPath = explode('/', $path);
        $key = array_pop($keyPath);
        $path = implode('/', $keyPath);
        $registry->changeDirectory($path);
        if ($registry->isDirectory($key)) {
            $registry->changeDirectory($key);
            return $registry->deleteDirectory();
        }
        return $registry->removeKey($key);
    }




    /**
     * Returns user agent information.
     *
     * @return uagent_info
     */
    public function getUserAgentInfo()
    {
        require_once dirname(__FILE__) . '/mdetect.php';

        $userAgentInfo = bab_getInstance('uagent_info');
        return $userAgentInfo;
    }

    /**
     * Constructs a Widget_InternationalTelLineEdit for phone number
     *
     * @param string		$id			The item unique id.
     * @return Widget_InternationalTelLineEdit
     */
    public function InternationalTelLineEdit($id = null)
    {
        require_once FUNC_WIDGETS_PHP_PATH . 'internationaltellineedit.class.php';
        return new Widget_InternationalTelLineEdit($id);
    }
}


abstract class Widget_UserConfiguration
{

    const TYPE_SESSION = 'session';
    const TYPE_REGISTRY = 'registry';

    private $addon = 'widgets';

    private $userId = null;


    /**
     * @param int|null   $userId
     */
    public function __construct($userId = null)
    {
        if (isset($userId)) {
            $this->userId = $userId;
        } else {
            $this->userId = bab_getUserId();
        }
    }

    /**
     * @param string $addon		The addon name.
     * @return Widget_UserConfiguration
     */
    public function setAddon($addon)
    {
        $this->addon = $addon;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddon()
    {
        return $this->addon;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }


    /**
     * Sets a key/value pair in the user's configuration.
     *
     * @param string $key
     * @param mixed $value
     */
    abstract public function set($key, $value);

    /**
     * Gets the value of a key in the user's configuration.
     *
     * @param string $key
     * @return mixed
     */
    abstract public function get($key);
}


class Widget_UserConfigurationSession extends Widget_UserConfiguration
{

    /**
     * (non-PHPdoc)
     * @see Widget_UserConfiguration::set()
     */
    public function set($key, $value)
    {
        $_SESSION['/' . $this->getAddon() . '/' . $key] = $value;
        return $this;
    }

    /**
     * (non-PHPdoc)
     * @see Widget_UserConfiguration::get()
     */
    public function get($key, $default = null)
    {
        if (isset($_SESSION['/' . $this->getAddon() . '/' . $key])) {
            return $_SESSION['/' . $this->getAddon() . '/' . $key];
        }
        return $default;
    }
}


class Widget_UserConfigurationRegistry extends Widget_UserConfiguration
{

    /**
     * Returns the registry instance in the correct directory.
     *
     * @param string $key
     * @return bab_Registry
     */
    private function getRegistryDirectory($key)
    {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/' . $this->getAddon() . '/user/' . $this->getUserId());
        $keyPath = explode('/', $key);
        array_pop($keyPath);
        $path = implode('/', $keyPath);
        $registry->changeDirectory($path);

        return $registry;
    }

    /**
     * (non-PHPdoc)
     * @see Widget_UserConfiguration::set()
     */
    public function set($key, $value)
    {
        $registry = $this->getRegistryDirectory($key);

        $registry->setKeyValue($key, $value);
    }

    /**
     * (non-PHPdoc)
     * @see Widget_UserConfiguration::set()
     */
    public function get($key, $default = null)
    {
        $registry = $this->getRegistryDirectory($key);

        $registry->getValue($key, $default);
    }
}


/**
 * Returns the specified string translated into the current user's language.
 *
 * @param string		$str
 * @return string
 */
function widget_translate($str)
{
    $translation = bab_translate($str, 'widgets', widget_getLang());

    return $translation;
}
/**
 * Returns the default CMS language or a forced one.
 *
 * @param string		$str
 * @return string
 */
function widget_getLang()
{
    if (!defined('FUNC_WIDGETS_LANG')) {
        $lang = bab_getLanguage();
    } else {
        $lang = FUNC_WIDGETS_LANG;
    }

    return $lang;
}



/**
 * Returns the specified text with an appended colon (:).
 *
 * The colon string is internationalized.
 * If a colon is already present in the original string it is returned
 * unmodified.
 *
 * @param string $text
 * @return string
 */
function widget_addColon($text)
{
    $colon = widget_translate(':');
    $colonLength = strlen($colon);
    $alreadyHasColon = ($colonLength <= strlen($text)) && !substr_compare($text, $colon, -strlen($colon));

    if ($alreadyHasColon) {
        return $text;
    }

    $colon = str_replace(' ', bab_nbsp(), $colon);

    return $text . $colon;
}
