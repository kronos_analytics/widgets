<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__).'/widgets.php';
require_once dirname(__FILE__).'/widgets/sitemapmenu.class.php';

// auto login if possible
require_once $GLOBALS['babInstallPath']."utilit/cookieident.php";

// hack for sitemap reconstruction
if (!isset($babLanguage)) {
    $babLanguage = widget_getLang();
}

if (!isset($babUrlScript)) {
	$babUrlScript = $babUrl . bab_getSelf();
}

Widget_SitemapMenu::ajax(bab_rp('nodeId'), bab_rp('depth'), bab_rp('maxdepth'));
