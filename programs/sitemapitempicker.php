<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/widgets.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/sitemap.php';


class Widget_SitemapItemPickerContent
{
	/**
	 * @var bab_siteMapOrphanRootNode
	 */
	protected $sitemap = null;

	/**
	 * @var string
	 */
	protected $sitemapName = null;

	/**
	 * @var bab_Node
	 */
	protected $home = null;



	/**
	 * Returns an html <li> string corresponding to a bab_Node.
	 *
	 * @param bab_Node $node
	 * @return string
	 */
	private function getHtml(bab_Node $node)
	{
		$html = '';

		$siteMapItem = $node->getData();
		/* @var $siteMapItem bab_siteMapItem */

		$html .= '<li>';
		$html .= '<a href="#" title="'.bab_toHtml($siteMapItem->id_function).'">'.bab_toHtml($siteMapItem->name).'</a>';

		if ($node->hasChildNodes()) {
			$html .= '<ul>'."\n";

			$node = $node->firstChild();
			do {
				$html .= $this->getHtml($node);
			} while ($node = $node->nextSibling());
			$html .= '</ul>'."\n";
		}

		$html .= '</li>'."\n";

		return $html;
	}



	/**
	 * Initlializes the Widget_SitemapItemPickerContent with a sitemap and a basenode.
	 *
	 * @param string $sitemap
	 * @param string $basenode
	 * @throws Exception
	 * @return boolean
	 */
	public function init($sitemap = null, $basenode = null)
	{
		if (isset($sitemap)) {
			$this->sitemap = bab_siteMap::getByUid($sitemap);
			$this->sitemapName = $sitemap;
		} else {
		    require_once $GLOBALS['babInstallPath'] . 'utilit/settings.class.php';
			$siteSettings = bab_Settings::get()->getSiteSettings();

			$this->sitemap = bab_siteMap::getByUid($siteSettings['sitemap']);
			$this->sitemapName = $siteSettings['sitemap'];
			if (!isset($this->sitemap)) {
				$this->sitemapName = 'core';
				$this->sitemap = bab_siteMap::get();
			}
		}

		if (!isset($this->sitemap)) {
			throw new Exception(sprintf(widget_translate('Sorry, there was a problem while trying to access sitemap \'%s\'.'), $this->sitemapName));
		}

		if (empty($basenode)) {
			$basenode = bab_siteMap::getVisibleRootNodeByUid($this->sitemapName);
		}

		if (isset($basenode) && (!empty($basenode))) {
			$this->home = $this->sitemap->getNodeById($basenode);
		} else {
			$dgNode = $this->sitemap->firstChild();
			if (!($dgNode instanceof bab_Node)) {
				throw new Exception(sprintf(widget_translate('Sorry, there was a problem while trying to access sitemap \'%s\'.'), $this->sitemapName));
			}
			$this->home = $dgNode->firstChild();
		}

		if (!($this->home instanceof bab_Node)) {
			throw new Exception(sprintf(widget_translate('Sorry, there was a problem while trying to access sitemap \'%s\'.'), $this->sitemapName));
		}

		return true;
	}





	/**
	 *
	 * @return string
	 */
	public function toString()
	{
		$node = $this->home->firstChild();
		$html = '';

		if ($node) {

			$html .= '<ul>'."\n";

			do {
				$html .= $this->getHtml($node);
			} while ($node = $node->nextSibling());
			$html .= '<li class="none"><a href="#" title="">'.widget_translate('None')."</a></li>\n";

			$html .= '</ul>'."\n";
		}
		return $html;
	}
}


$sitemap = bab_gp('sitemap', null);
$basenode = bab_gp('basenode', null);



try {
	$sitemapItemPickerContent = bab_getInstance('Widget_SitemapItemPickerContent');
	$sitemapItemPickerContent->init($sitemap, $basenode);
	echo $sitemapItemPickerContent->toString();
} catch (Exception $exception) {
	die('<i>' . $exception->getMessage() . '</i>');
}

exit;
